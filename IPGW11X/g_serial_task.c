//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	シリアルタスク
//=============================================================================
#include	"g_common.h"
#include	"uart.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
#define			SER_MAX_LEN		2048
#define			SER_MBX_MAX		100
#define			SER_LED_ONTIME	30
static			uint8_t			readData[SER_MAX_LEN];
static			uint32_t		readDataLen;
static			MSG485_FORMAT	writeData;
static			MSG_BSC_FORMAT	writeDataI;
static			uint32_t		writeDataLen;
MSG_BSC_FORMAT	usb_writeData;		//	## USB UPDATE ##
uint32_t		usb_writeDataLen;	//	## USB UPDATE ##
const uint8_t			*debugCommans[] = {
								"RDS",		//	Ｓｗｉｔｃｈ＆Ｓｌｏｔ　読み込み						Ｎｏ．０
								"RUC1",		//	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み					Ｎｏ．１
								"RUC2",		//	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み					Ｎｏ．２
								"RDEB",		//	ＤＥＢＵＧ　読み込み									Ｎｏ．３
								"RRM1",		//	ＲＸ　ＭＯＮＩＴＯＲ　１　読み込み						Ｎｏ．４
								"RRM2",		//	ＲＸ　ＭＯＮＩＴＯＲ　２　読み込み						Ｎｏ．５
								"RRM3",		//	ＲＸ　ＭＯＮＩＴＯＲ　３　読み込み						Ｎｏ．６
								"RTXC",		//	ＴＸ　ＣＯＮＴＲＯＬ　読み込み							Ｎｏ．７
								"RVSM",		//	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ　読み込み		Ｎｏ．８
								"RHA1",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み		Ｎｏ．９
								"RHA2",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み		Ｎｏ．１０
								"RHA3",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み		Ｎｏ．１１
								"RHA4",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み		Ｎｏ．１２
								"RHA5",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み		Ｎｏ．１３
								"RHA6",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み		Ｎｏ．１４
								"RHA7",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み		Ｎｏ．１５
								"RHA8",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み		Ｎｏ．１６
								"RHA9",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み		Ｎｏ．１７
								"RHAA",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み	Ｎｏ．１８
								"RHD1",		//	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　１　読み込み		Ｎｏ．１９
								"RTXF",		//	ＴＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み		Ｎｏ．２０
								"RRXF",		//	ＲＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み		Ｎｏ．２１
								"RDC1",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　１　読み込み				Ｎｏ．２２
								"RDC2",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　２　読み込み				Ｎｏ．２３
								"RDC3",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　３　読み込み				Ｎｏ．２４
								"RDC4",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　４　読み込み				Ｎｏ．２５
								"RDC5",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　５　読み込み				Ｎｏ．２６
								"RDC6",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　６　読み込み				Ｎｏ．２７
								"RDC7",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　７　読み込み				Ｎｏ．２８
								"RDC8",		//	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　８　読み込み				Ｎｏ．２９
								"RHAB",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み	Ｎｏ．３０
								"RHAC",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み	Ｎｏ．３１
								"RHAD",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み	Ｎｏ．３２
								"RHD2",		//	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　２　読み込み		Ｎｏ．３３
								"RHD3",		//	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　３　読み込み		Ｎｏ．３４
								"RHD4",		//	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　４　読み込み		Ｎｏ．３５
								"RHD5",		//	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　５　読み込み		Ｎｏ．３６
								"RL3S",		//	Ｌ３ＳＷＩＴＣＨ１〜１４読み込み						Ｎｏ．３７
								"RBTS",		//	ＢＯＡＲＤ　ＴＥＭＰ　ＳＷＩＴＣＨ読み込み				Ｎｏ．３８
								"RIEE",		//	ＩＥＥＥ１５８８１〜１９読み込み						Ｎｏ．３９
								"RIFR",		//	ＩＮＦＯＲ　ＲＥＧ										Ｎｏ．４０
								"rSTA",		//	ＳＴＡＴＵＳ＆ＴＥＭＰ　読み込み						Ｎｏ．４１	ここからＣＳＺ３
                                "rRAV",     //  ＡＤＣ　ＶＯＬＴＡＧＥ　ＶＡＬ　読み込み                Ｎｏ．４２
                                "rRAS",     //  ＡＤＣ　ＶＯＬＴＡＧＥ　ＳＥＴ　読み込み                Ｎｏ．４３
                                "rSRS",		//	ＳＲＧ　ＳＥＴ　読み込み								Ｎｏ．４４
								"rSHP",		//	ＳＤＩ　ＨＰＨＡＳＥ　読み込み							Ｎｏ．４５
								"rSVP",		//	ＳＤＩ　ＶＰＨＡＳＥ　読み込み							Ｎｏ．４６
								"rSVF",		//	ＳＤＩ　ＶＦＭＴ　読み込み								Ｎｏ．４７
								"rSST",		//	ＳＤＩ　ＳＴＡＴＵＳ　読み込み							Ｎｏ．４８
								"rTES",		//	ＴＥＳＴ　読み込み										Ｎｏ．４９
								"rXPO",		//	ＸＰ　ＯＵＴ　読み込み									Ｎｏ．５０
								"wXPO",		//	ＸＰ　ＯＵＴ　書き込み									Ｎｏ．５１
								"rPST",		//	ＰＳＥＴ　読み込み										Ｎｏ．５２
								"RVM2",		//	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ２　読み込み	Ｎｏ．５３
								"    ",		//	リザーブ												Ｎｏ．５４
								"    ",		//	リザーブ												Ｎｏ．５５
								"    ",		//	リザーブ												Ｎｏ．５６
								"MONI",		//	ＭＯＮＩ												Ｎｏ．５７　ＭＯＮＩ
								"rALS",		//	アラームステータス										Ｎｏ．５８	アラームステータス
								"rSTM",		//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ						Ｎｏ．５９　ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ
								"rPRT",		//	ＧＰＩＯ読み込み										Ｎｏ．６０　ＧＰＩＯ
								"rCPA",		//	ＣＰＵ　アドレス読み込み								Ｎｏ．６１	ここからコンフィグ
								"wCPA",		//	ＣＰＵ　アドレス書き込み								Ｎｏ．６２
								"rIPL",		//	リスト読み込み											Ｎｏ．６３
								"wIPL",		//	リスト書き込み											Ｎｏ．６４
								"rKIK",		//	機器情報読み込み										Ｎｏ．６５
								"rDEB",		//	デバッグ情報読込										Ｎｏ．６６
								"rADR",		//	アドレス指定読み込み									Ｎｏ．６７
								"rMST",		//	アドレス指定書き込み									Ｎｏ．６８
								"rUDP",		//	テストコマンド送信										Ｎｏ．６９
								"rP0B",		//	ポートリセット											Ｎｏ．７０
								"rETH",		//	ＥＴＨ読み込み											Ｎｏ．７１
								"rP0B",		//	ポートリセット											Ｎｏ．７２
                                "OPT1",		//	光モジュール　DATA FIELDS - ADDRESS A0H　読み込み		Ｎｏ．７３
                                "OPT2",		//	光モジュール　DATA FIELDS - ADDRESS A2H　読み込み		Ｎｏ．７４
                                "rDCN",     //  ＤＯＷＮ　ＣＯＮＶＥＲＴＯＲ  読み込み                  Ｎｏ．７５
                                "rGM1",     //  ＧＡＭＭＡ＿１  読み込み                                Ｎｏ．７６
                                "rGM2",     //  ＧＡＭＭＡ＿２  読み込み                                Ｎｏ．７７
                                "rFS1",		//	ＦＳ１　読み込み		                                Ｎｏ．７８
                                "rFS2",		//	ＦＳ２　読み込み		                                Ｎｏ．７９
                                "rPLD",		//	ＰＡＹＬＯＡＤ  読み込み		                        Ｎｏ．８０
								"GALL",		//	全ＰＫＧデータ読み込み									Ｎｏ．８１
								"SALM",		//	疑似TRAP発生書き込み									Ｎｏ．８２
								"GALM",		//	疑似TRAP発生読み込み									Ｎｏ．８３
								"WRT1",		//	ＵＤＰ設定依頼											Ｎｏ．８４
								"    "		//	ＥＮＤ
							};

// ボードステート（パンダの互換性）
// １以上で意味を持たす
// １以上：HEAD DEL CONTROL 5 拡張対応
// ２以上：HEAD DEL CONTROL 9 拡張対応
static	uint32_t		fvaBoardState = 2;

//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	uint32_t	systemControl(MSG_BSC_FORMAT *mf);
static	uint32_t	mtxControl(MSG_BSC_FORMAT *mf);
static	void		moniMoniMoni(void);
static	union
		{
			uint8_t		cc[256];
			uint16_t	ss[128];
			uint32_t	ii[64];
		}	uu;


extern	void	g_fpgaRestart(void);

//=============================================================================
//==	シリアルタスク関数
//=============================================================================
void	g_serial_task(void)
{
	LOG_THREAD();

	uint32_t	i,ii,iu,is,brev;
	uint32_t	idlingTime	=	0;
	uint32_t	ledOnTime	=	0;
	uint8_t		rbuf;
	uint16_t	index;
	uint32_t	debugCommandKind	=	0;
	MSG_BSC_FORMAT	*mfi;
	uint8_t			csm;
	MSG485_FORMAT	*mf;
	readDataLen		=	0;
	//-------------------------------------------------------------------------
	//--	ＵＡＲＴ初期化
	//-------------------------------------------------------------------------
	uart_init(UART_USB_CH);
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	受信データ有り
		//---------------------------------------------------------------------
		if (uart_read(UART_USB_CH,&rbuf) == 1)
		{
			//-----------------------------------------------------------------
			//--	通信ＬＥＤ　ＯＮ
			//-----------------------------------------------------------------
			ledOnTime	=	SER_LED_ONTIME;
//			g_led_control(LED_NO_USB_SER,1);
			//-----------------------------------------------------------------
			//--	受信長をチェックしてバッファへ
			//-----------------------------------------------------------------
			if (readDataLen >= SER_MAX_LEN)
			{
				readDataLen	=	0;
			}
			readData[readDataLen]	=	rbuf;
			readDataLen++;
			//-----------------------------------------------------------------
			//--	受信データ解析
			//-----------------------------------------------------------------
			if (readDataLen >= (uint32_t)sizeof(MSG485_HEADER) && readData[0] == COM_CODE_DUM)
			{
				mf	=	(MSG485_FORMAT *)&readData[0];
				//-------------------------------------------------------------
				//--	受信完結？
				//-------------------------------------------------------------
				if (readDataLen ==
						((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)mf->head.length +
						(uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
				{
					//---------------------------------------------------------
					//--	電文解析
					//---------------------------------------------------------
					writeDataLen	=	0;
					//---------------------------------------------------------
					//--	送信元がＩＣＦ？
					//---------------------------------------------------------
					if (mf->head.srcID != ID485_ICF)
					{
						readDataLen	=	0;
						continue;
					}
					//---------------------------------------------------------
					//--	ＳＴＸ、ＥＴＸ、チェックサムチェック
					//---------------------------------------------------------
					csm	=	g_calcCS(readData,1,(readDataLen - sizeof(MSG485_FOOTER) - 1));
					if (mf->head.startOfText != COM_CODE_STX ||
						readData[readDataLen - 1] != COM_CODE_ETX ||
						csm != readData[readDataLen - sizeof(MSG485_FOOTER)])
					{
						readDataLen	=	0;
						continue;
					}
					//---------------------------------------------------------
					//--	設定電文
					//---------------------------------------------------------
					if (mf->dataCommon.itemCode == ITEM_CODE_SETTING)
					{
						writeDataLen	=	g_rs485Setting(readDataLen,mf,&writeData);
					}
					//---------------------------------------------------------
					//--	応答送信
					//---------------------------------------------------------
					if (writeDataLen != 0)
					{
						//-----------------------------------------------------
						//--	送信データ整形
						//-----------------------------------------------------
						g_rs485ResponseMakeCommon(1,writeDataLen,mf,&writeData);
						//-----------------------------------------------------
						//--	送信処理
						//-----------------------------------------------------
						for ( i = 0 ; i < writeDataLen ; i++ )
						{
							uart_write(UART_USB_CH,*((uint8_t *)&writeData + i));
						}
						//-----------------------------------------------------
						//--	受信バッファリセット
						//-----------------------------------------------------
						readDataLen	=	0;
						//-----------------------------------------------------
						//--	ＩＰＧ　リセット
						//-----------------------------------------------------
						if (mf->dataCommon.itemCode == ITEM_CODE_SETTING &&
							mf->dataCommon.controlCode == CONT_CODE_IPG_RESET)
						{
							tslp_tsk(10);
							g_reset();
						}
					}
				}
			}
			//-----------------------------------------------------------------
			//--	ＵＳＢアップデート	## USB UPDATE ##
			//-----------------------------------------------------------------
			if (readDataLen > ((uint32_t)sizeof(MSG_BSC_HEADER) + (uint32_t)sizeof(MSG_BSC_DATA_HEADER) + 2))
			{
				if (readData[0] == '@' && readData[1] == '@')
				{
					mfi	=	(MSG_BSC_FORMAT *)&readData[2];
					if (mfi->head.len == readDataLen)
					{
						usb_writeDataLen	=	g_updateControlUsb(mfi);
                        usb_writeData.dataHead.controlCode	=	mfi->dataHead.controlCode | 0x80;
						//-----------------------------------------------------
						//--	送信処理
						//-----------------------------------------------------
						for ( i = 0 ; i < 2 ; i++ )
						{
							uart_write(UART_USB_CH,'@');
						}
						for ( i = 0 ; i < usb_writeDataLen ; i++ )
						{
							uart_write(UART_USB_CH,*((uint8_t *)&usb_writeData + i));
						}
						//-----------------------------------------------------
						//--	受信バッファリセット
						//-----------------------------------------------------
						readDataLen	=	0;
					}
				}
			}
			//-----------------------------------------------------------------
			//--	受信データ解析（インバンド用コマンド）
			//-----------------------------------------------------------------
			if (readDataLen > (uint32_t)sizeof(MSG_BSC_HEADER))
			{
				mfi	=	(MSG_BSC_FORMAT *)&readData[0];
				writeDataLen	=	0;
				//-------------------------------------------------------------
				//--	システム制御電文
				//-------------------------------------------------------------
				if (mfi->dataHead.itemCode == ITEM_CODE_SYSTEM_CONTROL)
				{
					writeDataLen	=	systemControl(mfi);
				}
				//-------------------------------------------------------------
				//--	ＭＴＸ制御電文
				//-------------------------------------------------------------
				else if (mfi->dataHead.itemCode == ITEM_CODE_MTX_CONTROL)
				{
					writeDataLen	=	mtxControl(mfi);
				}
				//-------------------------------------------------------------
				//--	応答送信
				//-------------------------------------------------------------
				if (writeDataLen != 0)
				{
					//---------------------------------------------------------
					//--	応答ヘッダー作成
					//---------------------------------------------------------
					writeDataI.head.len				=	writeDataLen - sizeof(MSG_BSC_HEADER) - 2;
					writeDataI.head.id				=	0x00;
					writeDataI.head.srcCodeNo		=	myCode[0];
					writeDataI.head.dstCodeNo		=	mfi->head.srcCodeNo;
					writeDataI.head.total			=	0x01;
					writeDataI.head.no				=	0x01;
					writeDataI.head.sequenceNo		=	mfi->head.sequenceNo;
					writeDataI.head.result			=	0x00;
					writeDataI.head.AUX[0]			=	0x00;
					writeDataI.head.AUX[1]			=	0x00;
					writeDataI.head.AUX[2]			=	0x00;
					writeDataI.head.AUX[3]			=	0x00;
					writeDataI.dataHead.itemCode	=	mfi->dataHead.itemCode;
					if (mfi->dataHead.itemCode == ITEM_CODE_MTX_CONTROL &&
						mfi->dataHead.controlCode == CONT_CODE_MTX_CHG_REQ_IPRX)
							writeDataI.dataHead.controlCode		=	CONT_CODE_MTX_CHG_NOTI;
					else if (mfi->dataHead.itemCode == ITEM_CODE_MTX_CONTROL &&
						mfi->dataHead.controlCode == CONT_CODE_VRT_CHG_REQ_IPTX)
							writeDataI.dataHead.controlCode		=	CONT_CODE_VRT_CHG_NOTI;
					else	writeDataI.dataHead.controlCode		=	mfi->dataHead.controlCode | 0x80;
					//---------------------------------------------------------
					//--	送信処理
					//---------------------------------------------------------
					for ( i = 0 ; i < writeDataLen ; i++ )
					{
						uart_write(UART_USB_CH,*((uint8_t *)&writeDataI + i));
					}
					//---------------------------------------------------------
					//--	受信バッファリセット
					//---------------------------------------------------------
					readDataLen	=	0;
				}
			}
			//-----------------------------------------------------------------
			//--	デバッグコマンド対応
			//-----------------------------------------------------------------
			//-----------------------------------------------------------------
			//--	ディップスイッチ　読み込み
			//-----------------------------------------------------------------
			if (readDataLen == 3 && !memcmp(readData,debugCommans[0],3))
			{
				uart_write(UART_USB_CH,commonData.dipSw4);
				uart_write(UART_USB_CH,commonData.bdIndex);
				uart_write(UART_USB_CH,commonData.slotNo);
				uart_write(UART_USB_CH,commonData.RID);
				uart_write(UART_USB_CH,commonData.RA_OUT);
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	ＸＩＬＩＮＸレジスタ読み込み
			//--	ＣＳＺ３レジスタ読み込み
			//--	コンフィグレーション
			//-----------------------------------------------------------------
			else if (readDataLen == 4)
			{
				debugCommandKind	=	0;
				for ( i = 1 ; i < MAX_DEB_COMMAND ; i++ )
				{
					if (!memcmp(readData,debugCommans[i],4))
					{
						debugCommandKind	=	i;
						break;
					}
				}
				//-------------------------------------------------------------
				//--	ＣＯＭポート検索対応
				//-------------------------------------------------------------
				if (debugCommandKind == 0 && !memcmp(readData,"rFVA",4))
				{
					memcpy(uu.cc,"wFVA",4);
					for ( i = 0 ; i < 4 ; i++ )
					{
						uart_write(UART_USB_CH,uu.cc[i]);
					}
					//---------------------------------------------------------
					//--	受信バッファリセット	## USB UPDATE ## (BUG)
					//---------------------------------------------------------
					readDataLen	=	0;
				}
				//-------------------------------------------------------------
				//--	ボードステート検索対応
				//-------------------------------------------------------------
				if (debugCommandKind == 0 && !memcmp(readData,"sFVA",4))
				{
					memcpy(uu.cc,(char*)&fvaBoardState,4);
					for ( i = 0 ; i < 4 ; i++ )
					{
						uart_write(UART_USB_CH,uu.cc[i]);
					}
					readDataLen	=	0;
				}
				//-------------------------------------------------------------
				//--	パラメタ有コマンドはここではスルー
				//-------------------------------------------------------------
				if (debugCommandKind == 51 || debugCommandKind == 57 ||
					debugCommandKind == 62 || debugCommandKind == 63 ||
					debugCommandKind == 64 || debugCommandKind == 67 ||
					debugCommandKind == 68 || debugCommandKind == 69 ||
					debugCommandKind == 70 || debugCommandKind == 73 ||
					debugCommandKind == 74)
				{
					debugCommandKind	=	0;
				}
				//-------------------------------------------------------------
				//--	デバッグコマンド受信
				//-------------------------------------------------------------
				if (debugCommandKind != 0)
				{
					//---------------------------------------------------------
					//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み
					//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み
					//--	ＤＥＢＵＧ　読み込み
					//--	ＲＸ　ＭＯＮＩＴＯＲ　１　読み込み
					//--	ＲＸ　ＭＯＮＩＴＯＲ　２　読み込み
					//--	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ　読み込み
					//--	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ２　読み込み
					//---------------------------------------------------------
					if (debugCommandKind == 1 || debugCommandKind == 2 ||
						debugCommandKind == 3 || debugCommandKind == 4 ||
						debugCommandKind == 5 || debugCommandKind == 8 ||
						debugCommandKind == 53)
					{
						for ( i = 0 ; i < 4 ; i++ )
						{
							switch (debugCommandKind)
							{
							case	1:	uu.ii[i]	=	xiGetUnitControl1(i);			break;
							case	2:	uu.ii[i]	=	xiGetUnitControl2(i);			break;
							case	3:	uu.ii[i]	=	xiGetDebug(i);					break;
							case	4:	uu.ii[i]	=	xiGetRxMonitor1(i);				break;
							case	5:	uu.ii[i]	=	xiGetRxMonitor2(i);				break;
							case	8:	uu.ii[i]	=	xiGetVideoStreamMonitor(i);		break;
							case	53:	uu.ii[i]	=	xiGetVideoStreamMonitor2(i);	break;
							default:													break;
							}
						}
						for ( i = 0 ; i < 16 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＲＸ　ＭＯＮＩＴＯＲ　３　読み込み
					//--	ＴＸ　ＣＯＮＴＲＯＬ　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み
					//--	ＴＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み
					//--	ＲＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　２　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　３　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　４　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 6 || debugCommandKind == 7 ||
							debugCommandKind == 9  || debugCommandKind == 20 ||
							debugCommandKind == 21 || debugCommandKind == 30 ||
							debugCommandKind == 31 || debugCommandKind == 32 ||
							debugCommandKind == 33 || debugCommandKind == 34 ||
							debugCommandKind == 35)
					{
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								switch (debugCommandKind)
								{
								case	6:	uu.ii[iu * 4 + ii]	=	xiGetRxMonitor3(iu,ii);			break;
								case	7:	uu.ii[iu * 4 + ii]	=	xiGetTxControl(iu,ii);			break;
								case	9:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl1(iu,ii);	break;
								case	20:	uu.ii[iu * 4 + ii]	=	xiGetTxFecCalculatorol(iu,ii);	break;
								case	21:	uu.ii[iu * 4 + ii]	=	xiGetRxFecCalculatorol(iu,ii);	break;
								case	30:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl11(iu,ii);	break;
								case	31:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl12(iu,ii);	break;
								case	32:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl13(iu,ii);	break;
								case	33:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl2(iu,ii);	break;
								case	34:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl3(iu,ii);	break;
								case	35:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl4(iu,ii);	break;
								default:															break;
								}
							}
						}
						for ( i = 0 ; i < 32 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　５　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　６　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　７　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　８　読み込み
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　９　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 36)
					{
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl5(iu,ii);
							}
						}
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								uu.ii[8 + iu * 4 + ii]	=	xiGetHeaderDelControl6(iu,ii);
							}
						}
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								uu.ii[16 + iu * 4 + ii]	=	xiGetHeaderDelControl7(iu,ii);
							}
						}
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								uu.ii[24 + iu * 4 + ii]	=	xiGetHeaderDelControl8(iu,ii);
							}
						}
						for ( iu = 0 ; iu < 2 ; iu++ )
						{
							for ( ii = 0 ; ii < 4 ; ii++ )
							{
								uu.ii[32 + iu * 4 + ii]	=	xiGetHeaderDelControl9(iu,ii);
							}
						}
						for ( i = 0 ; i < 160 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み
					//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　４　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　５　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　６　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 10 || debugCommandKind == 11 ||
							debugCommandKind == 12 || debugCommandKind == 13 ||
							debugCommandKind == 14 || debugCommandKind == 15 ||
							debugCommandKind == 16 || debugCommandKind == 17 ||
							debugCommandKind == 18 || debugCommandKind == 25 ||
							debugCommandKind == 26 || debugCommandKind == 27)
					{
						for ( i = 0 ; i < 2 ; i++ )
						{
							for ( iu = 0 ; iu < 2 ; iu++ )
							{
								for ( is = 0 ; is < 2 ; is++ )
								{
									for ( ii = 0 ; ii < 4 ; ii++ )
									{
										switch (debugCommandKind)
										{
										case	10:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl2(i,is,iu,ii);		break;
										case	11:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl3(i,is,iu,ii);		break;
										case	12:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl4(i,is,iu,ii);		break;
										case	13:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl5(i,is,iu,ii);		break;
										case	14:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl6(i,is,iu,ii);		break;
										case	15:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl7(i,is,iu,ii);		break;
										case	16:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl8(i,is,iu,ii);		break;
										case	17:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl9(i,is,iu,ii);		break;
										case	18:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl10(i,is,iu,ii);	break;
										case	25:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl4(i,is,iu,ii);			break;
										case	26:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl5(i,is,iu,ii);			break;
										case	27:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl6(i,is,iu,ii);			break;
										default:																						break;
										}
									}
								}
							}
						}
						for ( i = 0 ; i < 128 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　１　読み込み
					//--	ＩＮＦＯＲ　ＲＥＧ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 19 || debugCommandKind == 40)
					{
						for ( i = 0 ; i < 2 ; i++ )
						{
							for ( iu = 0 ; iu < 2 ; iu++ )
							{
								for ( ii = 0 ; ii < 4 ; ii++ )
								{
									switch (debugCommandKind)
									{
									case	19:	uu.ii[i * 8 + iu * 4 + ii]	=	xiGetHeaderDelControl1(i,iu,ii);		break;
									case	40:	uu.ii[i * 8 + iu * 4 + ii]	=	xiGetInfoReg((i * 8 + iu * 4 + ii));	break;
									default:																			break;
									}
								}
							}
						}
						for ( i = 0 ; i < 64 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　１　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　２　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　３　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 22 || debugCommandKind == 23 ||
							debugCommandKind == 24)
					{
						for ( i = 0 ; i < 2 ; i++ )
						{
							for ( iu = 0 ; iu < 2 ; iu++ )
							{
								switch (debugCommandKind)
								{
								case	22:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl1(i,iu);		break;
								case	23:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl2(i,iu);		break;
								case	24:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl3(i,iu);		break;
								default:															break;
								}
							}
						}
						for ( i = 0 ; i < 16 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　７　読み込み
					//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　８　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 28 || debugCommandKind == 29)
					{
						for ( i = 0 ; i < 2 ; i++ )
						{
							for ( iu = 0 ; iu < 2 ; iu++ )
							{
								for ( ii = 0 ; ii < 8 ; ii++ )
								{
									switch (debugCommandKind)
									{
									case	28:	uu.ii[i * 16 + iu * 8 + ii]	=	xiGetDemuxControl7(i,iu,ii);		break;
									case	29:	uu.ii[i * 16 + iu * 8 + ii]	=	xiGetDemuxControl8(i,iu,ii);		break;
									default:																		break;
									}
								}
							}
						}
						for ( i = 0 ; i < 128 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	Ｌ３ＳＷＩＴＣＨ　１〜１４　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 37)
					{
						uu.ii[0]	=	xiGetL3SwitchControl1();
						uu.ii[1]	=	xiGetL3SwitchControl2();
						uu.ii[2]	=	xiGetL3SwitchControl3(0);
						uu.ii[3]	=	xiGetL3SwitchControl3(1);
						uu.ii[4]	=	xiGetL3SwitchControl4(0);
						uu.ii[5]	=	xiGetL3SwitchControl4(1);
						uu.ii[6]	=	xiGetL3SwitchControl5(0);
						uu.ii[7]	=	xiGetL3SwitchControl5(1);
						uu.ii[8]	=	xiGetL3SwitchControl6(0);
						uu.ii[9]	=	xiGetL3SwitchControl6(1);
						uu.ii[10]	=	xiGetL3SwitchControl7(0);
						uu.ii[11]	=	xiGetL3SwitchControl7(1);
						uu.ii[12]	=	xiGetL3SwitchControl8(0);
						uu.ii[13]	=	xiGetL3SwitchControl8(1);
						uu.ii[14]	=	xiGetL3SwitchControl9(0);
						uu.ii[15]	=	xiGetL3SwitchControl9(1);
						uu.ii[16]	=	xiGetL3SwitchControl10(0);
						uu.ii[17]	=	xiGetL3SwitchControl10(1);
						uu.ii[18]	=	xiGetL3SwitchControl11(0);
						uu.ii[19]	=	xiGetL3SwitchControl11(1);
						uu.ii[20]	=	xiGetL3SwitchControl12();
						uu.ii[21]	=	xiGetL3SwitchControl13(0);
						uu.ii[22]	=	xiGetL3SwitchControl13(1);
						uu.ii[23]	=	xiGetL3SwitchControl14();
						for ( i = 0 ; i < 96 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＢＯＡＲＤ　ＴＥＭＰ　ＳＷＩＴＣＨ読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 38)
					{
						uu.ii[0]	=	xiGetBoardRevision();
						uu.ii[1]	=	xiGetTemperature();
						uu.ii[2]	=	xiGetDipSw();
						for ( i = 0 ; i < 12 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＩＥＥＥ１５８８　１〜１９　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 39)
					{
						for ( i = 0 ; i < 19 ; i++ )
						{
							for ( ii = 0 ; ii < 2 ; ii++ )
							{
								uu.ii[i * 2 + ii]	=	xiGetRegister((0x18001800 + i * 4 + ii * 0x8000));
							}
						}
						for ( i = 0 ; i < 152 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＳＴＡＴＵＳ　＆　ＴＥＭＰ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 41)
					{
						uu.ss[0]	=	csz3GetStatus();
						uu.ss[1]	=	csz3GetTemp();
						uu.ss[1]	=	((uu.ss[1] & 0xff00) >> 8);//	MSB 8ビットのみ
						uu.ss[2]	=	csz3GetDipSw7();
						uu.ss[2]	=	(uu.ss[2] & 0x00ff);
						for ( i = 0 ; i < 6 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＡＤＣ　ＶＯＬＴＡＧＥ　ＶＡＬ　読み込み
					//--	ＡＤＣ　ＶＯＬＴＡＧＥ　ＳＥＴ　読み込み
					//--	ＳＲＧ　ＳＥＴ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 42 ||
							debugCommandKind == 43 || debugCommandKind == 44)
					{
						switch (debugCommandKind)
						{
						case	42:	uu.ss[0]	=	csz3GetADCVoltageVal();	break;
						case	43:	uu.ss[0]	=	csz3GetADCVoltageSet();	break;
						case	44:	uu.ss[0]	=	csz3GetSrgSet();		break;
						default:											break;
						}
						for ( i = 0 ; i < 2 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＳＤＩ　ＨＰＨＡＳＥ　読み込み
					//--	ＳＤＩ　ＶＰＨＡＳＥ　読み込み
					//--	ＳＤＩ　ＶＦＭＴ　読み込み
					//--	ＳＤＩ　ＳＴＡＴＵＳ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 45 || debugCommandKind == 46 ||
							debugCommandKind == 47 || debugCommandKind == 48)
					{
						for ( i = 0 ; i < 4 ; i++ )
						{
							switch (debugCommandKind)
							{
							case	45:	uu.ss[i]	=	csz3GetSdiHPhase(i);	break;
							case	46:	uu.ss[i]	=	csz3GetSdiVPhase(i);	break;
							case	47:	uu.ss[i]	=	csz3GetSdiVFmt(i);		break;
							case	48:	uu.ss[i]	=	csz3GetSdiStatus(i);	break;
							default:											break;
							}
						}
						for ( i = 0 ; i < 8 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＴＥＳＴ　＆　ＴＸ＿ＭＯＤＥ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 49)
					{
						uu.ii[0]	=	csz3GetTest();
						for ( i = 0 ; i < 4 ; i++ )
						{
							uu.cc[4 + i]	=	(csz3GetSdiTxmode(i) & 0xff);
						}
						for ( i = 0 ; i < 8 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＳＤＩ　ＰＳＥＴ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 52)
					{
						for ( i = 0 ; i < 4 ; i++ )
						{
							uu.cc[i]	=	csz3GetSdiXpPsel(i);
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＸＰ　ＯＵＴ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 50)
					{
						for ( i = 0 ; i < 16 ; i++ )
						{
							uart_write(UART_USB_CH,csz3GetXpOut(i));
						}
					}
					//---------------------------------------------------------
					//--	アラームステータス　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 58)
					{
						g_alarmStatus2unitInf(uu.cc,0);
						for ( i = 0 ; i < 20 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ　読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 59)
					{
						if (snmpTrapMoniWptr == snmpTrapMoniRptr)
						{
							memset(uu.cc,0,128);
						}
						else
						{
							memcpy(uu.cc,snmpTrapMoniBuf[snmpTrapMoniRptr],128);
							snmpTrapMoniRptr++;
							if (snmpTrapMoniRptr >= SNMP_TRAP_MONI_MAX)	snmpTrapMoniRptr	=	0;
						}
						for ( i = 0 ; i < 128 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＧＰＩＯ読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 60)
					{

						uu.cc[0]	=	GPIO_READ(RIN_GPIO, PIN0B, 0xFF);
						uu.cc[1]	=	GPIO_READ(RIN_GPIO, PIN1B, 0xFF);
						uu.cc[2]	=	GPIO_READ(RIN_GPIO, PIN2B, 0xFF);
						uu.cc[3]	=	GPIO_READ(RIN_GPIO, PIN3B, 0xFF);
						uu.cc[4]	=	GPIO_READ(RIN_GPIO, PIN4B, 0xFF);
						uu.cc[5]	=	GPIO_READ(RIN_GPIO, PIN5B, 0xFF);
						uu.cc[6]	=	GPIO_READ(RIN_GPIO, PIN6B, 0xFF);
						uu.cc[7]	=	GPIO_READ(RIN_GPIO, PIN7B, 0xFF);
						uu.cc[8]	=	GPIO_READ(RIN_RTPORT, RPIN0B, 0xFF);
						uu.cc[9]	=	GPIO_READ(RIN_RTPORT, RPIN1B, 0xFF);
						uu.cc[10]	=	GPIO_READ(RIN_RTPORT, RPIN2B, 0xFF);
						uu.cc[11]	=	GPIO_READ(RIN_RTPORT, RPIN3B, 0xFF);
						for ( i = 0 ; i < 12 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＣＰＵ　アドレス読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 61)
					{
						memcpy(&uu.cc[0],(uint8_t *)gNET_DEV[0].cfg.eth.mac,6);
						memcpy(&uu.cc[6],(uint8_t *)&gNET_ADR[0].ipaddr,4);
						memcpy(&uu.cc[10],(uint8_t *)&gNET_ADR[0].mask,4);
						memcpy(&uu.cc[14],(uint8_t *)&gNET_ADR[0].gateway,4);
						memcpy(&uu.cc[18],(uint8_t *)gNET_DEV[1].cfg.eth.mac,6);
						memcpy(&uu.cc[24],(uint8_t *)&gNET_ADR[1].ipaddr,4);
						memcpy(&uu.cc[28],(uint8_t *)&gNET_ADR[1].mask,4);
						memcpy(&uu.cc[32],(uint8_t *)&gNET_ADR[1].gateway,4);
						brev	=	xiGetBoardRevision();
						memcpy(&uu.cc[36],(uint8_t *)&brev,4);
						for ( i = 0 ; i < 40 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	機器情報読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 65)
					{
						g_getFPGAVersion();
						memcpy(uu.cc,(uint8_t *)&modelInf[0],sizeof(MSG_BSC_MCH_DATA));
						for ( i = 0 ; i < 72 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	デバッグ情報読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 66)
					{
						memcpy(uu.cc,(uint8_t *)&debugData,sizeof(DEBUG_INF_STR));
						for ( i = 0 ; i < 128 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					//---------------------------------------------------------
					//--	ＧＰＩＯ読み込み
					//---------------------------------------------------------
					else if (debugCommandKind == 71)
					{
						/** @note ETHの状態は別で確認 */
						uu.ii[0]	=	0;
						uu.ii[1]	=	0;
						for ( i = 0 ; i < 8 ; i++ )
						{
							uart_write(UART_USB_CH,uu.cc[i]);
						}
					}
					readDataLen	=	0;

				}
			}
			//---------------------------------------------------------
			//--	ＭＯＮＩは別関数
			//---------------------------------------------------------
			else if (readDataLen == 8 && !memcmp(readData,debugCommans[57],4))
			{
				moniMoniMoni();
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	アドレス指定読み込み
			//-----------------------------------------------------------------
			else if (readDataLen == 8 && !memcmp(readData,debugCommans[67],4))
			{
				uint32_t	adr;
				uint8_t		tmp;
				memcpy((uint8_t *)&adr,&readData[4],4);
//				for ( i = 0 ; i < 4 ; i++ )
//				{
//				  uu.cc[i]	=	*((uint8_t *)(adr + i));
//					uart_write(UART_USB_CH,uu.cc[i]);
//				}
				uu.ii[0]	=	*((uint32_t *)adr);
				tmp			=	uu.cc[0];
				uu.cc[0]	=	uu.cc[3];
				uu.cc[3]	=	tmp;
				tmp			=	uu.cc[1];
				uu.cc[1]	=	uu.cc[2];
				uu.cc[2]	=	tmp;
				for ( i = 0 ; i < 4 ; i++ )
					uart_write(UART_USB_CH,uu.cc[i]);
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	アドレス指定書き込み
			//-----------------------------------------------------------------
			else if (readDataLen == 12 && !memcmp(readData,debugCommans[68],4))
			{
				uint32_t	adr;
				uint32_t	dat;
				memcpy((uint8_t *)&adr,&readData[4],4);
				memcpy((uint8_t *)&dat,&readData[8],4);

				*((uint32_t *)(adr)) = dat;
				for ( i = 0 ; i < 4 ; i++ )
				{
					uu.cc[i]	=	*((uint8_t *)(adr + i));
					uart_write(UART_USB_CH,uu.cc[i]);
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	テストコマンドＵＤＰ送信
			//-----------------------------------------------------------------
			else if (readDataLen == 5 && !memcmp(readData,debugCommans[69],4))
			{
				switch (readData[4])
				{
				//	Ａポート
				case	1:
					g_testUdp0PaketSend(0);
					break;
				//	Ｂポート
				case	2:
					g_testUdp0PaketSend(1);
					break;
				default:
					break;
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	ＣＰＵ ＰＯＲＴ ＳＥＴ
			//-----------------------------------------------------------------
			else if (readDataLen == 5 && !memcmp(readData,debugCommans[70],4))
			{
				switch (readData[4])
				{
				case	1:
					//---------------------------------------------------------
					//--	ＰＨＹ０リセット
					//---------------------------------------------------------
					GPIO_WRITE(RIN_GPIO, P0B, 0x04, 1);	// PHY0_RESETn(P02) = 1
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x04, 0);	// PHY0_RESETn = 0
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x04, 1);	// PHY0_RESETn = 1
					break;
				case	2:
					//---------------------------------------------------------
					//--	ＰＨＹ１リセット
					//---------------------------------------------------------
					GPIO_WRITE(RIN_GPIO, P0B, 0x08, 1);	// PHY1_RESETn(P03) = 1
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x08, 0);	// PHY1_RESETn = 0
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x08, 1);	// PHY1_RESETn = 1
					break;
				case	3:

					g_fpgaRestart();
#if 0
					//---------------------------------------------------------
					//--	ＦＰＧＡリセット
					//---------------------------------------------------------
					GPIO_WRITE(RIN_GPIO, P0B, 0x10, 1);       // FPGA_RESETn = 1
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x10, 0);       // FPGA_RESETn = 0
					tslp_tsk(10);
					tslp_tsk(2000);
					for ( i = 0 ; i < BUSNO_MAX ; i++ )
					{
						xiSetUnitControl1(i,UC1_INI_1);
					}
					for ( i = 0 ; i < BUSNO_MAX ; i++ )
					{
						xiSetUnitControl1(i,UC1_INI_2);
					}
					//---------------------------------------------------------
					//--	ＩＰＴＸ初期化
					//---------------------------------------------------------
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	iptxInitial();
					//---------------------------------------------------------
					//--	ＩＰＲＸ初期化
					//---------------------------------------------------------
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	iprxInitial();
#endif
					break;
				case	4:
					//---------------------------------------------------------
					//--	トランシーバリセット
					//---------------------------------------------------------
					GPIO_WRITE(RIN_GPIO, P0B, 0x20, 1);       // FPGA_RESETn = 1
					tslp_tsk(10);
					GPIO_WRITE(RIN_GPIO, P0B, 0x20, 0);       // FPGA_RESETn = 0
#if 0
					GPIO_WRITE(RIN_GPIO, P0B, 0x20, 1);	// FPGA_RESETn(P05) = 1
#endif
					break;
				case	5:
					//---------------------------------------------------------
					//--	PHYリセット
					//---------------------------------------------------------
					LOG_W("phy1_Reset() is not supported");
					break;
				case	6:
					//---------------------------------------------------------
					//--	ETHリセット
					//---------------------------------------------------------
					LOG_W("eth_Reset2() is not supported");
					break;
				case	7:
					//---------------------------------------------------------
					//--	ETHリセット
					//---------------------------------------------------------
					LOG_W("eth_Reset3() is not supported");
					break;
				default:
					break;
				}
				uu.cc[0]	=	0;
				uart_write(UART_USB_CH,uu.cc[0]);
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	ＸＰ　ＯＵＴ書込み
			//-----------------------------------------------------------------
			else if (readDataLen == 6 && !memcmp(readData,debugCommans[51],4))
			{
				csz3SetXpOut(readData[4],readData[5]);
				for ( i = 0 ; i < 4 ; i++ )
				{
					uart_write(UART_USB_CH,(uint8_t)(*debugCommans[51] + i));
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	コンフィグレーション書込み（ＣＰＵ　アドレス）
			//-----------------------------------------------------------------
			else if (readDataLen == 40 && !memcmp(readData,debugCommans[62],4))
			{
//				memcpy((uint8_t *)&saveCpuaData + 1,&readData[4],sizeof(SAVE_CPUA_STR));
//				for ( i = 0 ; i < 4 ; i++ )
//				{
//					uart_write(UART_USB_CH,(uint8_t)(*debugCommans[62] + i));
//				}
//				g_writeSaveCpuData();
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	コンフィグレーション読込み（リスト）不要！！
			//-----------------------------------------------------------------
			else if (readDataLen == 6 && !memcmp(readData,debugCommans[63],4))
			{
				uu.cc[1]	=	readData[4];
				uu.cc[0]	=	readData[5];
				index		=	uu.ss[0];
				if (index <= 1023)
				{
					uu.cc[0]	=	0;
					uu.cc[1]	=	0;
					uu.cc[2]	=	0;
					uu.cc[3]	=	0;
					uu.cc[4]	=	0;
					uu.cc[5]	=	0;
					for ( i = 0 ; i < 6 ; i++ )
					{
						uart_write(UART_USB_CH,uu.cc[i]);
					}
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	コンフィグレーション書込み（リスト）不要！！
			//-----------------------------------------------------------------
			else if (readDataLen == 12 && !memcmp(readData,debugCommans[64],4))
			{
				uu.cc[1]	=	readData[4];
				uu.cc[0]	=	readData[5];
				index		=	uu.ss[0];
				if (index <= 1023)
				{
				}
				if (index == 9999)	//	フラッシュ保存依頼
				{
				}
				for ( i = 0 ; i < 4 ; i++ )
				{
					uart_write(UART_USB_CH,(uint8_t)(*debugCommans[64] + i));
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	基板固有情報書込み（不要！！）
			//-----------------------------------------------------------------
			else if (readDataLen == 39 && !memcmp(readData,debugCommans[67],4))
			{
//				memcpy((uint8_t *)&boardInfData,&readData[4],sizeof(BOARD_INF_STR));
//				boardInfData.initFlag	=	SETTING_BOARD_INIT_FLAG;
//				for ( i = 0 ; i < 4 ; i++ )
//				{
//					uart_write(UART_USB_CH,(uint8_t)(*debugCommans[67] + i));
//				}
//				g_writeBoardInfData();
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	光モジュールメモリダンプA0H
			//-----------------------------------------------------------------
			else if (readDataLen == 5 && !memcmp(readData,debugCommans[73],4))
			{
                debugOptMemDump(readData[4], 0, &uu.cc[0]);
				//	データ転送
				for (i = 0; i < 256; i++)
				{
					uart_write(UART_USB_CH, uu.cc[i]);
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	光モジュールメモリダンプA2H
			//-----------------------------------------------------------------
			else if (readDataLen == 5 && !memcmp(readData,debugCommans[74],4))
			{
                debugOptMemDump(readData[4], 1, &uu.cc[0]);
				//	データ転送
				for (i = 0; i < 256; i++)
				{
					uart_write(UART_USB_CH, uu.cc[i]);
				}
				readDataLen	=	0;
			}
			//-----------------------------------------------------------------
			//--	受信タイムアウトリセット
			//-----------------------------------------------------------------
			idlingTime	=	0;
		}
		//---------------------------------------------------------------------
		//--	受信データ無し
		//---------------------------------------------------------------------
		else
		{
			tslp_tsk(1);
			idlingTime++;
			//-----------------------------------------------------------------
			//--	２００ｍｓ以上受信無しで、受信バッファリセット
			//-----------------------------------------------------------------
			if (idlingTime >= 200)
			{
				readDataLen	=	0;
				idlingTime	=	0;
			}
			//-----------------------------------------------------------------
			//--	通信ＬＥＤ　ＯＦＦ
			//-----------------------------------------------------------------
			if (ledOnTime)	ledOnTime--;
//			if (!ledOnTime)	g_led_control(LED_NO_USB_SER,0);
		}
	}
}
//=============================================================================
//==	システム制御電文関数
//=============================================================================
static	uint32_t	systemControl(MSG_BSC_FORMAT *mf)
{
	uint32_t	writeLen	=	0;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	機器情報要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_INF_REQ:
		if (readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_INF_IFS_TO_TXRX)))
		{
			g_getFPGAVersion();
			writeDataI.data.infTxRxToIfs.mchData	=	modelInf[0];
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_INF_TXRX_TO_IFS);
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	ＭＴＸ制御電文関数
//=============================================================================
static	uint32_t	mtxControl(MSG_BSC_FORMAT *mf)
{
	uint32_t	i;
	uint16_t	si;
	uint32_t	writeLen	=	0;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ＭＴＸ切替要求（ＩＰ　ＲＸ用）
	//-------------------------------------------------------------------------
	case	CONT_CODE_MTX_CHG_REQ_IPRX:
		if (readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_MTX_CHG_IFS_TO_RX)))
		{
			//-----------------------------------------------------------------
			//--	Ｖ−ＩＮＴへ変更依頼
			//-----------------------------------------------------------------
//			wai_sem(ID_SEM_SAVE_DATA);
//			for ( i = 0 ; i < BUSNO_MAX ; i++ )
//			{
//				si	=	g_endianConv(mf->data.mtxChgIfsToRx.inputNo[i]) - 1;
//				//-----------------------------------------------------
//				//--	入力素材番号変更
//				//-----------------------------------------------------
//				if (si != commonData.rxCurrentInputNo[i])
//				{
//					commonData.rxCurrentInputNo[i]	=	si;
//					commonData.inputChange[i]		=	1;
//				}
//			}
//			sig_sem(ID_SEM_SAVE_DATA);
			//	ＦＰＧＡにて切替タイミング調整を行うので、ＣＰＵ側でのタイミング合わせは不要になった！！
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				si	=	g_endianConv(mf->data.mtxChgIfsToRx.inputNo[i]) - 1;
				//-------------------------------------------------------------
				//--	新素材は、ここで更新
				//--	入力素材番号変更（新素材）
				//-------------------------------------------------------------
				if (si != commonData.rxCurrentInputNo[i] && commonData.inputChange[i] != 1)
				{
					g_mtxChange(i,si);
					g_txLoopModeChange(i,si);
				}
			}
			//-----------------------------------------------------------------
			//--	応答データ作成
			//-----------------------------------------------------------------
			for ( i = 0 ; i < 4 ; i++ )
			{
				writeDataI.data.mtxChgRxToIfs.inputNo[i]	=	mf->data.mtxChgIfsToRx.inputNo[i];
			}
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_MTX_CHG_RX_TO_IFS);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＶＲＴ切替要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_VRT_CHG_REQ_IPTX:
		if (readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_VRT_CHG_IFS_TO_TX)))
		{
			wai_sem(ID_SEM_SAVE_DATA);
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				g_vrtChange(i,mf->data.vrtChgIfsToTx.vrtNo);
			}
			sig_sem(ID_SEM_SAVE_DATA);
			//-----------------------------------------------------------------
			//--	応答データ作成
			//-----------------------------------------------------------------
			writeDataI.data.vrtChgTxToIfs.vrtNo			=	mf->data.vrtChgIfsToTx.vrtNo;
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_VRT_CHG_TX_TO_IFS);
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	ＭＯＮＩシリアルモニター用関数
//=============================================================================
const uint8_t			*debugMoniCommans[] = {
								"----",		//	ＤＵＭＭＹ												Ｎｏ．０
								"RUC1",		//	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み					Ｎｏ．１
								"RUC2",		//	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み					Ｎｏ．２
								"RDEB",		//	ＤＥＢＵＧ　読み込み									Ｎｏ．３
								"ENCD",		//	ＴＩＣＯ　ＥＮＣＯＤＥ　１〜４　読み込み				Ｎｏ．４
								"VCNT",		//	ＶＩＤＥＯ　ＣＯＮＴ　読み込み							Ｎｏ．５
								"APES",		//	ＡＵＤＩＯ　ＰＥＳ　１、２　読み込み					Ｎｏ．６
								"ASTA",		//	ＡＵＤＩＯ　ＳＴＡＴＵＳ　１、２　読み込み				Ｎｏ．７
								"ANCC",		//	ＡＮＣ　ＣＯＮＴ　１、２　読み込み						Ｎｏ．８
								"ANCP",		//	ＡＮＣ　ＰＥＳ　１、２　読み込み						Ｎｏ．９
								"PIDN",		//	ＰＩＤ　１〜５　読み込み								Ｎｏ．１０
								"SMPT",		//	ＳＭＰＴＥ２０２２＿１＿２　読み込み					Ｎｏ．１１
								"RHA1",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み		Ｎｏ．１２
								"RHA2",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み		Ｎｏ．１３
								"RHA3",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み		Ｎｏ．１４
								"RHA4",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み		Ｎｏ．１５
								"RHA5",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み		Ｎｏ．１６
								"RHA6",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み		Ｎｏ．１７
								"RHA7",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み		Ｎｏ．１８
								"RHA8",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み		Ｎｏ．１９
								"RHA9",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み		Ｎｏ．２０
								"RHAA",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み	Ｎｏ．２１
								"RHAB",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み	Ｎｏ．２２
								"RHAC",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み	Ｎｏ．２３
								"RHAD",		//	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み	Ｎｏ．２４
								"    "		//	ＥＮＤ
};
static	void		moniMoniMoni(void)
{
	uint32_t	debugMoniCommandKind	=	0;
	int			i,ii,is,iu;
	//-------------------------------------------------------------------------
	//--	コマンド検索
	//-------------------------------------------------------------------------
	for ( i = 1 ; i < MAX_DEB_MONI_COMMAND ; i++ )
	{
		if (!memcmp(&readData[4],debugMoniCommans[i],4))
		{
			debugMoniCommandKind	=	i;
			break;
		}
	}
	//-------------------------------------------------------------------------
	//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み
	//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み
	//--	ＤＥＢＵＧ　読み込み
	//--	ＶＩＤＥＯ　ＣＯＮＴ　読み込み
	//--	ＳＭＰＴＥ２０２２＿１＿２　読み込み
	//-------------------------------------------------------------------------
	if (debugMoniCommandKind == 1 || debugMoniCommandKind == 2 ||
		debugMoniCommandKind == 3 || debugMoniCommandKind == 5 ||
		debugMoniCommandKind == 11)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			switch (debugMoniCommandKind)
			{
			case	1:	uu.ii[i]	=	xiMoniGetUnitControl1(i);	break;
			case	2:	uu.ii[i]	=	xiMoniGetUnitControl2(i);	break;
			case	3:	uu.ii[i]	=	xiMoniGetDebug(i);			break;
			case	5:	uu.ii[i]	=	xiMoniGetVideoCont(i);		break;
			case	11:	uu.ii[i]	=	xiMoniGetSmpte2022_1_2(i);	break;
			}
		}
		for ( i = 0 ; i < 16 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ　１〜４　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 4)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 4]		=	xiMoniGetTicoEncode1(i);
			uu.ii[i * 4 + 1]	=	xiMoniGetTicoEncode2(i);
			uu.ii[i * 4 + 2]	=	xiMoniGetTicoEncode3(i);
			uu.ii[i * 4 + 3]	=	xiMoniGetTicoEncode4(i);
		}
		for ( i = 0 ; i < 64 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＡＵＤＩＯ　ＰＥＳ　１、２　読み込み
	//--	ＡＵＤＩＯ　ＳＴＡＴＵＳ　１、２　読み込み
	//--	ＡＮＣ　ＣＯＮＴ　１、２　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 6 || debugMoniCommandKind == 7 ||
		debugMoniCommandKind == 8)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			switch (debugMoniCommandKind)
			{
			case	6:	uu.ii[i * 2]		=	xiMoniGetAudioPes1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAudioPes2(i);
						break;
			case	7:	uu.ii[i * 2]		=	xiMoniGetAudioStatus1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAudioStatus2(i);
						break;
			case	8:	uu.ii[i * 2]		=	xiMoniGetAncCont1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAncCont2(i);
						break;
			}
		}
		for ( i = 0 ; i < 32 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＡＮＣ　ＰＥＳ　ＩＮＦＯ１，２，３　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 9)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 3]		=	xiMoniGetAncPes1(i);
			uu.ii[i * 3 + 1]	=	xiMoniGetAncPes2(i);
			uu.ii[i * 3 + 2]	=	xiMoniGetAncData(i);
		}
		for ( i = 0 ; i < 48 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ　１〜５　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 10)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 5]		=	xiMoniGetPid1(i);
			uu.ii[i * 5 + 1]	=	xiMoniGetPid2(i);
			uu.ii[i * 5 + 2]	=	xiMoniGetPid3(i);
			uu.ii[i * 5 + 3]	=	xiMoniGetPid4(i);
			uu.ii[i * 5 + 4]	=	xiMoniGetPid5(i);
		}
		for ( i = 0 ; i < 80 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 12 || debugMoniCommandKind == 22 ||
			debugMoniCommandKind == 23 || debugMoniCommandKind == 24)
	{
		for ( i = 0 ; i < 2 ; i++ )
		{
			for ( ii = 0 ; ii < 4 ; ii++ )
			{
				switch (debugMoniCommandKind)
				{
				case	12:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl1(i,ii);	break;
				case	22:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl11(i,ii);	break;
				case	23:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl12(i,ii);	break;
				case	24:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl13(i,ii);	break;
				}
			}
		}
		for ( i = 0 ; i < 32 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind >= 13 && debugMoniCommandKind <= 21)
	{
		for ( i = 0 ; i < 2 ; i++ )
		{
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( is = 0 ; is < 2 ; is++ )
				{
					for ( ii = 0 ; ii < 4 ; ii++ )
					{
						switch (debugMoniCommandKind)
						{
						case	13:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl2(i,is,iu,ii);		break;
						case	14:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl3(i,is,iu,ii);		break;
						case	15:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl4(i,is,iu,ii);		break;
						case	16:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl5(i,is,iu,ii);		break;
						case	17:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl6(i,is,iu,ii);		break;
						case	18:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl7(i,is,iu,ii);		break;
						case	19:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl8(i,is,iu,ii);		break;
						case	20:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl9(i,is,iu,ii);		break;
						case	21:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl10(i,is,iu,ii);	break;
						}
					}
				}
			}
		}
		for ( i = 0 ; i < 128 ; i++ )
		{
			uart_write(UART_USB_CH,uu.cc[i]);
		}
	}
}

//=============================================================================
//==	光モジュールメモリダンプ
//=============================================================================
#define	SFPMEM_CD_ENABLE			0xC0000000		//	CORE_ENABLE:1
#define	SFPMEM_CD_START_SPF1		0xE400A000		//	CORE_ENABLE:1,SET,START,WRITE,address(A0)+write(0)
#define	SFPMEM_CD_START_SPF2		0xE400A200		//	CORE_ENABLE:1,SET,START,WRITE,address(A2)+write(0)
#define	SFPMEM_CD_READ				0xC4000000		//	CORE_ENABLE:1,SET,WRITE,read address(00)
//#define	SFPMEM_CD_STOP				0xE9000000		//	CORE_ENABLE:1,SET,STOP,READ,ACK:NACK
//#define	SFPMEM_CD_STOP				0xD8000000		//	CORE_ENABLE:1,SET,STOP,READ,ACK:NACK
#define	SFPMEM_CD_STOP				0xB9000000		//	CORE_ENABLE:1,SET,STOP,READ,ACK:NACK
#define	SFPMEM_CD_ACK				0xC8000000		//	CORE_ENABLE:1,SET,READ,ACK:ACK
//#define	SFPMEM_CD_RESET				0xE400FF00		//	CORE_ENABLE:1,SET,WRITE,FF
#define	SFPMEM_CD_RESET				0xECFF0000		//	CORE_ENABLE:1,SET,WRITE,FF

#define	SPFMEM_MASK_SET				0x40000000
#define	SPFMEM_MASK_ADDRREAD		0x00000100

#define	SFPMEM_ST_RXACK_NACK		0x00800000
#define	SFPMEM_ST_BUSY_START		0x00400000
#define	SFPMEM_ST_COREERR			0x00200000
#define	SFPMEM_ST_TRANSFER			0x00020000
#define	SFPMEM_ST_INTERRUPT			0x00010000

#define	SFPMEM_CD_ALLCLEAR			0x00000000

int16_t	optMemRegCheck(uint8_t adr, uint32_t sreg, uint32_t rreg);

//-----------------------------------------------------------------------------
//--	制御
//-----------------------------------------------------------------------------
void	debugOptMemDump(uint8_t moudle, uint8_t adr, uint8_t *bufa)
{
//	static uint8_t		bufa[256];
	static uint32_t	sreg;
	static uint32_t	rreg;
	uint32_t			i;
	int16_t				rtn;

#if 0
	sreg	=	SFPMEM_CD_RESET;
	//	データ書き込み
	xiSetSfpReg(moudle, sreg);
	//	データ読み込み
	rreg = xiGetSfpReg(moudle);
	tslp_tsk(100);

	//	CLEAR
	sreg	=	SFPMEM_CD_ALLCLEAR;				//	ALLCLEAR
	for (i = 0; i < 3; i++)
	{
		//	データ書き込み
		xiSetSfpReg(moudle, sreg);
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);

		tslp_tsk(100);
	}
#endif

	//	開始準備
	if (adr == 0)	//	A0H
	{
		sreg	=	SFPMEM_CD_START_SPF1;		//	CORE_ENABLE:1,SET,START,WRITE,address(A0)+write(0)
	}
	else			//	A2H
	{
		sreg	=	SFPMEM_CD_START_SPF2;		//	CORE_ENABLE:1,SET,START,WRITE,address(A2)+write(0)
	}
	//	データ書き込み
	xiSetSfpReg(moudle, sreg);
	while(1)
	{
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);
		//	データ判定
		rtn = optMemRegCheck(moudle, sreg, rreg);
		if (rtn == -1)							//	異常
		{
			goto ERREND;
		}
		else if (rtn == 1)						//	完了
		{
			break;
		}
	}

	//	データ読み込み開始
	sreg		=	SFPMEM_CD_READ;				//	CORE_ENABLE:1,WRITE,read address
	//	データ書き込み
	xiSetSfpReg(moudle, sreg);
	while(1)
	{
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);
		//	データ判定
		rtn = optMemRegCheck(moudle, sreg, rreg);
		if (rtn == -1)							//	異常
		{
			goto ERREND;
		}
		else if (rtn == 1)						//	完了
		{
			break;
		}
	}

	if (adr == 0)	//	A0H
	{
		sreg	=	SFPMEM_CD_START_SPF1 | SPFMEM_MASK_ADDRREAD;		//	CORE_ENABLE:1,SET,START,WRITE,address(A0)+read(1)
	}
	else			//	A2H
	{
		sreg	=	SFPMEM_CD_START_SPF2 | SPFMEM_MASK_ADDRREAD;		//	CORE_ENABLE:1,SET,START,WRITE,address(A2)+read(1)
	}
	//	データ書き込み
	xiSetSfpReg(moudle, sreg);
	while(1)
	{
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);
		//	データ判定
		rtn = optMemRegCheck(moudle, sreg, rreg);
		if (rtn == -1)							//	異常
		{
			goto ERREND;
		}
		else if (rtn == 1)						//	完了
		{
			break;
		}
	}

	for (i = 0; i < 255; i++)
	{
		sreg	=	SFPMEM_CD_ACK;				//	CORE_ENABLE:1,READ,ACK:ACK
		//	データ書き込み
		xiSetSfpReg(moudle, sreg);
		while(1)
		{
			//	データ読み込み
			rreg = xiGetSfpReg(moudle);
			//	データ判定
			rtn = optMemRegCheck(moudle, sreg, rreg);
			if (rtn == -1)						//	異常
			{
				goto ERREND;
			}
			else if (rtn == 1)					//	完了
			{
				break;
			}
		}
		bufa[i]	=	(uint8_t)(rreg & 0x000000FF);
    }
	sreg	=	SFPMEM_CD_STOP;					//	CORE_ENABLE:1,STOP,READ,ACK:NACK
	xiSetSfpReg(moudle, sreg);
	while(1)
	{
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);
		//	データ判定
		rtn = optMemRegCheck(moudle, sreg, rreg);
		if (rtn == -1)							//	異常
		{
			goto ERREND;
		}
		else if (rtn == 1)						//	完了
		{
			break;
		}
	}
	bufa[255]	=	(uint8_t)(rreg & 0x000000FF);

	//	データ転送
//	for (i = 0; i < 256; i++)
//	{
//		uart_write(UART_USB_CH, bufa[i]);
//	}

	sreg	=	SFPMEM_CD_RESET;
	//	データ書き込み
	xiSetSfpReg(moudle, sreg);
	//	データ読み込み
	rreg = xiGetSfpReg(moudle);
	tslp_tsk(100);

	//	CLEAR
	sreg	=	SFPMEM_CD_ALLCLEAR;				//	ALLCLEAR
	for (i = 0; i < 3; i++)
	{
		//	データ書き込み
		xiSetSfpReg(moudle, sreg);
		//	データ読み込み
		rreg = xiGetSfpReg(moudle);

		tslp_tsk(100);
	}
	return;

ERREND:
	//	データ転送
	for (i = 0; i < 256; i++)
	{
//		uart_write(UART_USB_CH, 0xFF);
		bufa[i] = 0xFF;
	}
	return;
}

//-----------------------------------------------------------------------------
//--	ステータスチェック
//-----------------------------------------------------------------------------
int16_t	optMemRegCheck(uint8_t moudle, uint32_t sr, uint32_t rr)
{
	static uint32_t	reg;
	static uint32_t	sreg;
	static uint32_t	rreg;

	sreg = sr;
	rreg = rr;

	//	コア異常 or BUSY:STOP
//	if ((rreg & SFPMEM_ST_COREERR) || (!(rreg & SFPMEM_ST_BUSY_START)) || (rreg & SFPMEM_ST_RXACK_NACK))
//	if ((rreg & SFPMEM_ST_COREERR) || (rreg & SFPMEM_ST_RXACK_NACK))
	if (rreg & SFPMEM_ST_RXACK_NACK)
	{
//		reg	=	SFPMEM_CD_STOP;						//	CORE_ENABLE:0,STOP,READ,ACK:NACK
		reg	=	SFPMEM_CD_ALLCLEAR;					//	ALLCLEAR
		xiSetSfpReg(moudle, reg);
		return -1;
	}

	//	転送中
	if (rreg & SFPMEM_ST_TRANSFER)
	{
		//	データ書き込み
		xiSetSfpReg(moudle, (sreg & ~SPFMEM_MASK_SET));	//	SET bit OFF
		return 0;
	}
	//	(割り込み要因あり or 転送完) and RxACK:NACK
	if (((rreg & SFPMEM_ST_INTERRUPT) || (!(rreg & SFPMEM_ST_TRANSFER))))
	{
		//	データ書き込み
		xiSetSfpReg(moudle, (sreg & ~SPFMEM_MASK_SET));	//	SET bit OFF
		return 1;
	}
	return 0;
}
