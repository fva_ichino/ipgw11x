//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	サイクルタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
static		uint8_t			cycleTXRXLed				=	0;
static		uint8_t			cycleTXRXLedInt				=	0;
static		uint8_t			ledIntervalSL				=	0;
static		uint32_t		oneMinCount					=	0;
static		uint32_t		alarmDetectStartWaitCount	=	0;

static		uint8_t			ledIntervalRB				=	0;
static		uint8_t			cycleRBLed					=	0;

//=============================================================================
//==	サイクルタスク関数
//=============================================================================
void	g_cycle_task(void)
{
	LOG_THREAD();

	uint8_t		i,ioff;
	uint16_t	errcnt,moni;
	uint32_t	staA,staB;
	uint8_t		srgPSel;
	uint16_t	srgST;
	uint16_t	x;
	uint8_t		mode[4] = { 0, 0, 0, 0 };
	uint8_t		mLed[4] = { 0, 0, 0, 0 };

	//-------------------------------------------------------------------------
	//--	ウォッチドッグ有効
	//-------------------------------------------------------------------------
	csz3SetWdtOnOff((uint8_t)commonData.wdtEnable); 
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	１００ｍｓ周期処理
		//---------------------------------------------------------------------
		tslp_tsk(100);
		//---------------------------------------------------------------------
		//--	開始直後アラーム検出抑止時間
		//---------------------------------------------------------------------
		if (alarmTrapDetectData.alarmDetectStartWait == 0 && commonData.unitStartFlag == 1)
		{
			alarmDetectStartWaitCount++;
			if (alarmDetectStartWaitCount >= 60)	//	6sec
			{
				alarmTrapDetectData.alarmDetectStartWait	=	1;
			}
		}
		//---------------------------------------------------------------------
		//--	フラッシュ保存
		//---------------------------------------------------------------------
		//	後部ＥＥＰＲＯＭ保存情報書き込み依頼フラグ
		if (commonData.writeSavePhysicalLayerEepromFlag)
		{
			g_writeSavePhysicalLayerEeprom();
			commonData.writeSavePhysicalLayerEepromFlag	=	0;
		}
		//	ＣＰＵ／ＶＩＤＥＯ保存情報書き込み依頼フラグ
		else if (commonData.writeSaveCpuVideoLayerFlag)
		{
			g_writeSaveCpuVideoLayer();
			commonData.writeSaveCpuVideoLayerFlag			=	0;
		}
		//	アラームマスク保存情報書き込み依頼フラグ
		else if (commonData.writeSaveAlarmMaskFlag)
		{
			g_writeSaveAlarmMask();
			commonData.writeSaveAlarmMaskFlag				=	0;
		}
		//	個別保存情報保存依頼フラグ
		else if (commonData.writeSaveIndividual)
		{
			g_writeSaveIndividual();
			commonData.writeSaveIndividual					=	0;
		}
		//	ＣＳＺ３保存情報フラグ
		else if (commonData.writeSaveCSZ3)
		{
			g_writeSaveCSZ3();
			commonData.writeSaveCSZ3						=	0;
		}
		//	系統情報保存情報フラグ
		else if (commonData.writeSaveSystemAB)
		{
			g_writeSaveSystemAB();
			commonData.writeSaveSystemAB					=	0;
		}
		//	SDICRC設定情報保存情報フラグ
		else if (commonData.writeSaveSdiIpMc)
		{
			g_writeSaveSdiIpMisInfl();
			commonData.writeSaveSdiIpMc					=	0;
		}

		//---------------------------------------------------------------------
		//--	接続要求受信済フラグの解除用カウンタ
		//---------------------------------------------------------------------
		for ( i = 0 ; i < 2 ; i++ )
		{
			if (commonData.connectRequest[i] == 1)
			{
				commonData.connectRequestLossCount[i]++;
				if (commonData.connectRequestLossCount[i] >= 400)	//	100ms×400=40sec
				{
					commonData.connectRequest[i]			=	0;
					commonData.connectRequestLossCount[i]	=	0;
				}
			}
		}
		//---------------------------------------------------------------------
		//--	映像信号ＬＥＤ
		//---------------------------------------------------------------------
		ioff	= (commonData.tx_or_rx == HDIPTXRX)?7:0;
		for ( i = 0 ; i < BUSNO_MAX ; i++ )
		{
			//-----------------------------------------------------------------
			//--	ＴＸ　ＲＸ＿ＭＯＤＥ＿ＬＯＣＫＥＤ
			//-----------------------------------------------------------------
			if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)
			{
				srgPSel	=	csz3GetSdiXpPsel(i);
				if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09 || srgPSel == 0x0C)
				{
					srgST	=	csz3GetSdiStatus(i);
					g_led_control((LED_NO_SDI_TOP + i + ioff),((srgST & 0x00ff) != 0x003F)?1:0);
				}
				else
				{
					xiGetRxMonitor1B(i,&errcnt,&moni);
					g_led_control((LED_NO_SDI_TOP + i + ioff),(moni & 0x4000)?1:0);
				}
			}
			//-----------------------------------------------------------------
			//--	ＲＸ　ＲＥＣＩＥＶＥ
			//-----------------------------------------------------------------
			mode[i]	=	0;
			if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)
			{
				mLed[i]	=	0;
				//	折り返し
				x	=	xiGetTxControlTxLoopMode(i);		//	自IP素材
				if (x == 0)
				{
					//	折り返し無
					mode[i]	=	0;
					staA	=	xiGetHeaderDelControl1(0,0,i);
					staB	=	xiGetHeaderDelControl1(1,0,i);
					g_led_control((LED_NO_SDI_TOP + i),((staA & 0x8000) || (staB & 0x8000))?1:0);
				}
				else
				{
					//	折り返し有
					mode[i]	=	1;
					//	SDI入力有り判定
					x -= 1;
					srgPSel	=	csz3GetSdiXpPsel(x);
					if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09 || srgPSel == 0x0C)
					{
						srgST	=	csz3GetSdiStatus(x);
						if ((srgST & 0x00ff) != 0x003F)
						{
							mLed[i]	=	1;
						}
					}
					else
					{
						xiGetRxMonitor1B(x,&errcnt,&moni);
						if ((moni & 0x4000))
						{
							mLed[i]	=	1;
						}
					}
				}
			}
		}
		//	折り返し有り時のLED制御
		ledIntervalRB++;
		if (ledIntervalRB > 5)
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				if (mode[i] == 1)
				{
					if (mLed[i] == 1)
					{
						g_led_control((LED_NO_SDI_TOP + i),cycleRBLed);
					}
					else
					{
						g_led_control((LED_NO_SDI_TOP + i),0);
					}
				}
			}
			ledIntervalRB	=	0;
			cycleRBLed		^=	1;
		}
		//---------------------------------------------------------------------
		//--	ＴＸ　ｏｒ　ＲＸ
		//---------------------------------------------------------------------
		cycleTXRXLed++;
		if (commonData.autoEqualizerConfig == 1)											cycleTXRXLedInt	=	10;
		else if (commonData.connectRequest[0] == 1 || commonData.connectRequest[1] == 1)	cycleTXRXLedInt	=	5;
		else																				cycleTXRXLedInt	=	2;
		if (cycleTXRXLed >= cycleTXRXLedInt)
		{
			//-----------------------------------------------------------------
			//--	通常時
			//-----------------------------------------------------------------
			if (commonData.autoEqualizerConfig == 0)
			{
				if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)
				{
					g_led_control(LED_NO_TX,ledIntervalSL);
				}
				ledIntervalSL	^=	1;
				if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)
				{
					g_led_control(LED_NO_RX,ledIntervalSL);
				}
			}
			//-----------------------------------------------------------------
			//--	ＥＱ　ＧＡＩＮ自動調整中
			//-----------------------------------------------------------------
			else
			{
				ledIntervalSL	^=	1;
				g_led_control(LED_NO_TX,ledIntervalSL);
				g_led_control(LED_NO_RX,ledIntervalSL);
			}
			cycleTXRXLed	=	0;
		}
		//---------------------------------------------------------------------
		//--	デバッグ用　ＴＥＳＴモード　３Ｇ／ＨＤ切替
		//--	ぱたぱたモード
		//---------------------------------------------------------------------
		/*
		test_3G_HD_count++;
		if (test_3G_HD_count >= 50)
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				saveTxMode[i]			=	(saveTxMode[i] == 0)?2:0;
			}
			test_3G_HD_count	=	0;
		}
		*/

		//---------------------------------------------------------------------
		//	SFP TX:ENABLE解除
		//---------------------------------------------------------------------
		for (i = 0; i < 2; i++)
		{
			if (commonData.sfpTxEnableCount[i] > 0)
			{
				commonData.sfpTxEnableCount[i]--;
				if (commonData.sfpTxEnableCount[i] == 0)
				{
					xiSetDemuxControl1TxEnable(i, 0);
				}
			}
		}
		//---------------------------------------------------------------------
		//--	１分周期
		//---------------------------------------------------------------------
		oneMinCount++;
		if (oneMinCount >= 600)
		{
			if (test_1Min_index == 9)
			{
				test_1Min_index	=	0;
			}
			else
			{
				test_1Min_index++;
			}
			test_1Min_count[test_1Min_index]	=	0;
			oneMinCount	=	0;
		}

		//---------------------------------------------------------------------
		//--	LINK DOWN 確認用
		//---------------------------------------------------------------------
		/* Link Down検出時のMACリセット(eth_Reset2相当)はドライバが実施するのでここでは何もしない */
	}
}
