//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	対ＦＰＧＡ関数群
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	FPGAアドレス定義
//=============================================================================
//-----------------------------------------------------------------------------
//--	ＣＳＺ３
//-----------------------------------------------------------------------------
volatile	uint32_t		CSZ3_STATUS0			=	0x00000000;
volatile	uint32_t		CSZ3_STATUS1			=	0x00000002;
volatile	uint32_t		CSZ3_REF_AD_VAL0		=	0x00000004;
volatile	uint32_t		CSZ3_REF_AD_VAL1		=	0x00000006;
volatile	uint32_t		CSZ3_FREE_AD_SET0		=	0x00000008;
volatile	uint32_t		CSZ3_FREE_AD_SET1		=	0x0000000A;
volatile	uint32_t		CSZ3_SRG_SET0			=	0x0000000C;
volatile	uint32_t		CSZ3_SRG_SET1			=	0x0000000E;
volatile    uint32_t        CSZ3_WDT_ONOFF          =   0x00000010;
volatile	uint32_t		CSZ3_SDI_HPHASE0		=	0x00000020;
volatile	uint32_t		CSZ3_SDI_HPHASE1		=	0x00000022;
volatile	uint32_t		CSZ3_SDI_VPHASE0		=	0x00000024;
volatile	uint32_t		CSZ3_SDI_VPHASE1		=	0x00000026;
volatile	uint32_t		CSZ3_SDI_VFMT			=	0x00000028;
volatile	uint32_t		CSZ3_SDI_TX_MODE		=	0x0000002A;
volatile	uint32_t		CSZ3_SDI_ST0			=	0x00000030;
volatile	uint32_t		CSZ3_SDI_ST1			=	0x00000032;
volatile	uint32_t		CSZ3_TEST0				=	0x000000A0;
volatile	uint32_t		CSZ3_TEST1				=	0x000000A2;
volatile	uint32_t		CSZ3_TEST2				=	0x000000A4;
volatile	uint32_t		CSZ3_TEST3				=	0x000000A6;
volatile	uint32_t		CSZ3_VER0				=	0x000000A8;
volatile	uint32_t		CSZ3_VER1				=	0x000000AA;
volatile	uint32_t		CSZ3_VER2				=	0x000000AC;
volatile	uint32_t		CSZ3_VER3				=	0x000000AE;
volatile	uint32_t		CSZ3_VER4				=	0x000000B0;
volatile	uint32_t		CSZ3_VER5				=	0x000000B2;
volatile	uint32_t		CSZ3_VER6				=	0x000000B4;
volatile	uint32_t		CSZ3_VER7				=	0x000000B6;
volatile	uint32_t		CSZ3_DIPSW7				=	0x000000B8;
volatile	uint32_t		CSZ3_TEMPL				=	0x000000BC;
volatile	uint32_t		CSZ3_TEMPH				=	0x000000BE;
volatile	uint32_t		CSZ3_XP_OUT				=	0x000000C0;
volatile	uint32_t		CSZ3_XP_PSEL			=	0x000000E0;
//-----------------------------------------------------------------------------
//--	ＸＬＩＮＫＸ
//-----------------------------------------------------------------------------
volatile	uint32_t		XI_UNIT_CONTROL1		=	0x00000000;
volatile	uint32_t		XI_UNIT_CONTROL2		=	0x00000004;
volatile	uint32_t		XI_FPGA_VERSION			=	0x00000008;
volatile	uint32_t		XI_DEBUG				=	0x0000000C;
volatile	uint32_t		XI_BOARD_REVISION		=	0x00000010;
volatile	uint32_t		XI_TEMPERATURE			=	0x00000014;
volatile	uint32_t		XI_DIP_SW				=	0x00000018;
volatile	uint32_t		XI_RX_MONITOR1			=	0x00000020;
volatile	uint32_t		XI_RX_MONITOR2			=	0x00000024;
volatile	uint32_t		XI_RX_MONITOR3			=	0x00000028;
volatile	uint32_t		XI_TX_CONTROL			=	0x00000040;
volatile	uint32_t		XI_VIDEO_STREAM_MONITOR	=	0x00000060;
volatile	uint32_t		XI_VIDEO_STREAM_MONITOR2=	0x00000064;
volatile	uint32_t		XI_HEADER_ADD_CONTROL1	=	0x00000080;
volatile	uint32_t		XI_HEADER_ADD_CONTROL2	=	0x00000084;
volatile	uint32_t		XI_HEADER_ADD_CONTROL3	=	0x00000088;
volatile	uint32_t		XI_HEADER_ADD_CONTROL4	=	0x0000008C;
volatile	uint32_t		XI_HEADER_ADD_CONTROL5	=	0x00000090;
volatile	uint32_t		XI_HEADER_ADD_CONTROL6	=	0x00000094;
volatile	uint32_t		XI_HEADER_ADD_CONTROL7	=	0x00000098;
volatile	uint32_t		XI_HEADER_ADD_CONTROL8	=	0x0000009C;
volatile	uint32_t		XI_HEADER_ADD_CONTROL9	=	0x000000A0;
volatile	uint32_t		XI_HEADER_ADD_CONTROL10	=	0x000000A4;
volatile	uint32_t		XI_HEADER_ADD_CONTROL11	=	0x000000A8;
volatile	uint32_t		XI_HEADER_ADD_CONTROL12	=	0x000000AC;
volatile	uint32_t		XI_HEADER_ADD_CONTROL13	=	0x000000B0;
volatile	uint32_t		XI_HEADER_DEL_CONTROL1	=	0x000000C0;
volatile	uint32_t		XI_HEADER_DEL_CONTROL2	=	0x000000C4;
volatile	uint32_t		XI_HEADER_DEL_CONTROL3	=	0x000000C8;
volatile	uint32_t		XI_HEADER_DEL_CONTROL4	=	0x000000CC;
volatile	uint32_t		XI_HEADER_DEL_CONTROL5	=	0x000000D0;
volatile	uint32_t		XI_HEADER_DEL_CONTROL6	=	0x000000D4;
volatile	uint32_t		XI_HEADER_DEL_CONTROL7	=	0x000000D8;
volatile	uint32_t		XI_HEADER_DEL_CONTROL8	=	0x000000DC;
volatile	uint32_t		XI_TX_FEC_CALCULATOROL	=	0x000000E0;
volatile	uint32_t		XI_RX_FEC_CALCULATOROL	=	0x000000E4;
volatile	uint32_t		XI_HEADER_DEL_CONTROL9	=	0x000000E8;
volatile	uint32_t		XI_DEMUX_CONTROL1		=	0x00001000;
volatile	uint32_t		XI_DEMUX_CONTROL2		=	0x00001004;
volatile	uint32_t		XI_DEMUX_CONTROL3		=	0x00001008;
volatile	uint32_t		XI_DEMUX_CONTROL4		=	0x0000100C;
volatile	uint32_t		XI_DEMUX_CONTROL5		=	0x00001010;
volatile	uint32_t		XI_DEMUX_CONTROL6		=	0x00001014;
volatile	uint32_t		XI_DEMUX_CONTROL7		=	0x00001018;
volatile	uint32_t		XI_DEMUX_CONTROL8		=	0x0000101C;
volatile	uint32_t		XI_SW12_FLAG			=	0x0000171C;

volatile	uint32_t		XI_IEEE1588_CONTROL1	=	0x00001800;
volatile	uint32_t		XI_IEEE1588_CONTROL2	=	0x00001804;
volatile	uint32_t		XI_IEEE1588_CONTROL3	=	0x00001808;
volatile	uint32_t		XI_IEEE1588_CONTROL4	=	0x0000180C;
volatile	uint32_t		XI_IEEE1588_CONTROL5	=	0x00001810;
volatile	uint32_t		XI_IEEE1588_CONTROL6	=	0x00001814;
volatile	uint32_t		XI_IEEE1588_CONTROL7	=	0x00001818;
volatile	uint32_t		XI_IEEE1588_CONTROL8	=	0x0000181C;
volatile	uint32_t		XI_IEEE1588_CONTROL9	=	0x00001820;
volatile	uint32_t		XI_IEEE1588_CONTROL10	=	0x00001824;
volatile	uint32_t		XI_IEEE1588_CONTROL11	=	0x00001828;
volatile	uint32_t		XI_IEEE1588_CONTROL12	=	0x0000182C;
volatile	uint32_t		XI_IEEE1588_CONTROL13	=	0x00001830;
volatile	uint32_t		XI_IEEE1588_CONTROL14	=	0x00001834;
volatile	uint32_t		XI_IEEE1588_CONTROL15	=	0x00001838;
volatile	uint32_t		XI_IEEE1588_CONTROL16	=	0x0000183C;
volatile	uint32_t		XI_IEEE1588_CONTROL17	=	0x00001840;
volatile	uint32_t		XI_IEEE1588_CONTROL18	=	0x00001844;
volatile	uint32_t		XI_IEEE1588_CONTROL19	=	0x00001848;

volatile	uint32_t		XI_L3SWITCH_CONTROL1	=	0x00004000;
volatile	uint32_t		XI_L3SWITCH_CONTROL2	=	0x00004004;
volatile	uint32_t		XI_L3SWITCH_CONTROL3	=	0x00004008;
volatile	uint32_t		XI_L3SWITCH_CONTROL4	=	0x0000400C;
volatile	uint32_t		XI_L3SWITCH_CONTROL5	=	0x00004010;
volatile	uint32_t		XI_L3SWITCH_CONTROL6	=	0x00004014;
volatile	uint32_t		XI_L3SWITCH_CONTROL7	=	0x00004018;
volatile	uint32_t		XI_L3SWITCH_CONTROL8	=	0x0000401C;
volatile	uint32_t		XI_L3SWITCH_CONTROL9	=	0x00004020;
volatile	uint32_t		XI_L3SWITCH_CONTROL10	=	0x00004024;
volatile	uint32_t		XI_L3SWITCH_CONTROL11	=	0x00004044;
volatile	uint32_t		XI_L3SWITCH_CONTROL12	=	0x00004048;
volatile	uint32_t		XI_L3SWITCH_CONTROL13	=	0x0000404C;
volatile	uint32_t		XI_L3SWITCH_CONTROL14	=	0x00004050;

volatile	uint32_t		XI_INFOR_REG			=	0x00007000;

volatile	uint32_t		XI_SFP1_REG		    	=	0x00007100;
volatile	uint32_t		XI_SFP2_REG		    	=	0x00007104;
//=============================================================================
//==	ローカル定義
//=============================================================================
#define		CSZ3_BUS_OFFSET		0x20
#define		XI_BUS_OFFSET		0x200
#define		XI_SW_OFFSET		0x100
#define		XI_IX_OFFSET		0x100
#define		XI_UPD_OFFSET		0x2000
#define		XI_CH_OFFSET		0x8000
#define		XI_REG_OFFSET       0x0004
//=============================================================================
//==	個別情報反映関数
//=============================================================================
void	applyIndividualData(void)
{
	uint8_t		ic,is,i;
	uint8_t		vMacAddress[6];
	uint32_t	temp32;
	uint8_t		temp8;
	//--	SYNC
	*((uint16_t *)(CSZ3_SRG_SET1))	=	saveIndividualSet.data.syncMode;
//	xiSetUnitControl2Sync(saveIndividualSet.data.syncMode);
	if (commonData.dipSw5 & 0x04)
	{
		xiSetUnitControl2Sync(0x00);
	}
	else
	{
		xiSetUnitControl2Sync(saveIndividualSet.data.syncMode);
	}
//	if ((saveIndividualSet.data.dipSw6 & 0x10) == 0)
//	{
//		*((uint16_t *)(CSZ3_SRG_SET1))	=	0x30;	//	REF_MOTHER
//	}
//	else
//	{
//		*((uint16_t *)(CSZ3_SRG_SET1))	=	0x20;	//	REF_REAR_CONNECTOR
//	}
	for ( i = 0 ; i < 4 ; i++ )
	{
		if ((commonData.dipSw4 & 0x04))	//	自走
		{
			csz3SetSdiXpPsel(i,saveCSZ3.data.fsInf.PresetMode[i]);
			csz3SetSdiHPhase(i,saveCSZ3.data.fsInf.HPhase[i]);
			csz3SetSdiVPhase(i,saveCSZ3.data.fsInf.VPhase[i]);
			csz3TxmodeSet(i,saveCSZ3.data.fsTxMode[i]);
			xiSetRxMonitor2(i,0x0F00);	//	Format VALID disable ,3G,SD,HD enable
		}
		else							//	他走
		{
			csz3SetSdiXpPsel(i,saveIndividualSet.data.fsXpSet[i]);
			csz3SetSdiHPhase(i,saveIndividualSet.data.fsHPhase[i]);
			csz3SetSdiVPhase(i,saveIndividualSet.data.fsVPhase[i]);
			csz3TxmodeSet(i,saveIndividualSet.data.fsTxMode[i]);
			xiSetHeaderAddControl1(i,saveIndividualSet.data.txVideoFormat[i]);
			xiSetTxControlFMT(i,saveIndividualSet.data.rxVideoFormat[i]);
//			switch (g_getSDIMode(0xA040))
			switch (g_getSDIMode(saveIndividualSet.data.txVideoFormat[i]))
			{
			case	0x01:		//	SD
				xiSetRxMonitor2(i,0x0200);	//	Format VALID enable ,SD enable
				break;
			case	0x02:		//	3G
				xiSetRxMonitor2(i,0x0400);	//	Format VALID enable ,3G enable
				break;
			case	0x00:		//	HD
			default:
				xiSetRxMonitor2(i,0x0100);	//	Format VALID enable ,HD enable
				break;
			}

			xiSetUnitControl2(i,0x00C2);	//	UDP/IP Header Add A,B,Triple-Rate SDI TX RESET Update SET
			xiSetUnitControl2(i,0x0000);	//	UDP/IP Header Add A,B,Triple-Rate SDI TX RESET Update RESET

		}
		csz3VfmtSet(i,saveIndividualSet.data.fsVideoForm[i]);
	}
	//--	Kintex：ＳＯＵＲＣＥ＿ＭＡＣ
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				temp32 = xiGetHeaderAddControl2(ic,is,1,i);
				temp8 = ((uint8_t)(temp32 >> 15)) & 0x01;
//				xiSetHeaderAddControl23(ic,is,i,savePhysicalLayerEeprom.data.videoMacAddress[ic],1,
				xiSetHeaderAddControl23(ic,is,i,savePhysicalLayerEeprom.data.videoMacAddress[ic],temp8,
					saveCpuVideoLayer.data.Extend[ic][i],
//					saveCpuVideoLayer.data.VLANFlag[ic][i],
					saveIndividualSet.data.vlan[i],	//	個別設定を使用する
//					saveCpuVideoLayer.data.TOSFlag[ic][i]);
					saveIndividualSet.data.tos[i]);	//	個別設定を使用する
				//	For MONI
				if (g_isBoard20X())
				{
						memcpy(vMacAddress,savePhysicalLayerEeprom.data.videoMacAddress[ic],6);
//						vMacAddress[5]	+=	0x02;
					temp32 = xiMoniGetHeaderAddControl2(ic,is,1,i);
					temp8 = ((uint8_t)(temp32 >> 15)) & 0x01;
//					xiMoniSetHeaderAddControl23(ic,is,i,vMacAddress,1,
					xiMoniSetHeaderAddControl23(ic,is,i,vMacAddress,temp8,
						saveCpuVideoLayer.data.Extend[ic][i],
//						saveCpuVideoLayer.data.VLANFlag[ic][i],
//						saveIndividualSet.data.moni_vlan[i],	//	個別設定を使用する
						saveIndividualSet.data.moniVlan[i],		//	個別設定を使用する
//						saveCpuVideoLayer.data.TOSFlag[ic][i]);
						saveIndividualSet.data.moni_tos[i]);	//	個別設定を使用する
				}
			}
		}
	}
	//--	Kintex：HEADER DEL CONTROL1 BUFFER
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( i = 0 ; i < BUSNO_MAX ; i++ )
		{
			xiSetHeaderDelControl1Buffer(ic,i,saveIndividualSet.data.redundBuf);
		}
	}
}
//=============================================================================
//==	ＣＳＺ３初期化関数
//=============================================================================
void	csz3Initial(void)
{
	uint8_t		setVal;
	uint8_t		i;
	uint16_t	setVal16;
	//-------------------------------------------------------------------------
	//--	ＡＤＣ＿Ｖｏｌｔａｇｅ（Ｉ）
	//-------------------------------------------------------------------------
	csz3SetADCVoltageSet(saveCSZ3.data.ADC_Vol);
	//-------------------------------------------------------------------------
	//--	Ｈ，ＶＰｈａｓｅ＆ＸＰ　Ｐｒｅｓｅｔ　Ｍｏｄｅ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		csz3SetSdiXpPsel(i,saveCSZ3.data.fsInf.PresetMode[i]);
		csz3SetSdiHPhase(i,saveCSZ3.data.fsInf.HPhase[i]);
		csz3SetSdiVPhase(i,saveCSZ3.data.fsInf.VPhase[i]);
	}
	//-------------------------------------------------------------------------
	//--	ＳＲＧ＿ＭＯＤＥ＿ＳＥＴ
	//-------------------------------------------------------------------------
	setVal	=	(commonData.tx_or_rx == HDIPTX)?0x01:0x00;
	*((uint16_t *)(CSZ3_SRG_SET0))	=	setVal;
	//-------------------------------------------------------------------------
	//--	ＳＹＮＣ　ＳＥＬ
	//-------------------------------------------------------------------------
	if ((commonData.dipSw6 & 0x80))	//	DipSw 有効時のみ
	{
		if ((commonData.dipSw6 & 0x10) == 0)
		{
			*((uint16_t *)(CSZ3_SRG_SET1))	=	0x30;	//	REF_MOTHER
		}
		else
		{
			*((uint16_t *)(CSZ3_SRG_SET1))	=	0x20;	//	REF_REAR_CONNECTOR
		}
	}
	//-------------------------------------------------------------------------
	//--	ＶＦＭＴ＆ＦＳ　Ｍｏｄｅ
	//-------------------------------------------------------------------------
	if ((commonData.dipSw7 & 0x80))	//	DipSw 有効時のみ
	{
		//	VFMT
		switch ((commonData.dipSw7 & 0x70))
		{
		case	0x70:	//	HD
			for ( i = 0 ; i < 4 ; i++ )	{	csz3VfmtSet(i,0x00);	}
			break;
		case	0x50:	//	SD
			for ( i = 0 ; i < 4 ; i++ )	{	csz3VfmtSet(i,0x10);	}
			break;
		case	0x10:	//	3G
			for ( i = 0 ; i < 4 ; i++ )	{	csz3VfmtSet(i,0x20);	}
			break;
		default:
			break;
		}
		//	FS Mode
		setVal16	=	0;
		if ((commonData.dipSw7 & 0x01))	setVal16	|=	0x0001;
//		if (!(commonData.dipSw7 & 0x02))	//	2Frは使用しない DEMUXはON固定
//		{
//			setVal16	|=	0x0102;
//		}
//		else
//		{
//			setVal16	|=	0x0302;
//		}
		setVal16	|=	0x10;
		if ((commonData.dipSw7 & 0x08))	setVal16	|=	0x0008;
		for ( i = 0 ; i < 4 ; i++ )	{	csz3TxmodeSet(i,setVal16);	};
	}
	else
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			csz3VfmtSet(i,saveIndividualSet.data.fsVideoForm[i]);
			if ((commonData.dipSw4 & 0x04))	//	自走
			{
				csz3TxmodeSet(i,saveCSZ3.data.fsTxMode[i]);
			}
			else							//	他走
			{
				csz3TxmodeSet(i,saveIndividualSet.data.fsTxMode[i]);
			}
		}
	}
}
//=============================================================================
//==	ＣＳＺ３　ＳＴＡＴＵＳ取得関数
//=============================================================================
uint16_t	csz3GetStatus(void)
{
	uint16_t	sta0	=	*((uint16_t *)(CSZ3_STATUS0));
	uint16_t	sta1	=	*((uint16_t *)(CSZ3_STATUS1));
	uint16_t	sta		=	(sta0 & 0x00ff) | ((sta1 & 0x00ff) << 8);
	return(sta);
}
//=============================================================================
//==	ＣＳＺ３　ＡＤＣ　ＶＯＬＴＡＧＥ　ＶＡＬ取得関数
//=============================================================================
uint16_t	csz3GetADCVoltageVal(void)
{
	uint16_t	v0	=	*((uint16_t *)(CSZ3_REF_AD_VAL0));
	uint16_t	v1	=	*((uint16_t *)(CSZ3_REF_AD_VAL1));
	uint16_t	val	=	(v0 & 0x00ff) | ((v1 & 0x0003) << 8);
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＡＤＣ　ＶＯＬＴＡＧＥ　ＳＥＴ取得関数
//=============================================================================
uint16_t	csz3GetADCVoltageSet(void)
{
	uint16_t	v0	=	*((uint16_t *)(CSZ3_FREE_AD_SET0));
	uint16_t	v1	=	*((uint16_t *)(CSZ3_FREE_AD_SET1));
	uint16_t	val	=	(v0 & 0x00ff) | ((v1 & 0x0003) << 8);
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＡＤＣ　ＶＯＬＴＡＧＥ　ＳＥＴ設定関数
//=============================================================================
void	csz3SetADCVoltageSet(uint16_t val)
{
	*((uint16_t *)(CSZ3_FREE_AD_SET0))	=	(val & 0x00ff);
	*((uint16_t *)(CSZ3_FREE_AD_SET1))	=	((val & 0x0300) >> 8);
}
//=============================================================================
//==	ＣＳＺ３　ＳＲＧ　ＳＥＴ取得関数
//=============================================================================
uint16_t	csz3GetSrgSet(void)
{
	uint16_t	sta0	=	*((uint16_t *)(CSZ3_SRG_SET0));
	uint16_t	sta1	=	*((uint16_t *)(CSZ3_SRG_SET1));
	uint16_t	sta		=	(sta0 & 0x00ff) | ((sta1 & 0x00ff) << 8);
	return(sta);
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＨＰＨＡＳＥ取得関数
//=============================================================================
uint16_t	csz3GetSdiHPhase(uint8_t busNo)
{
	uint16_t	v0	=	*((uint16_t *)(CSZ3_SDI_HPHASE0 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	v1	=	*((uint16_t *)(CSZ3_SDI_HPHASE1 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	val	=	(v0 & 0x00ff) | ((v1 & 0x000f) << 8);
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＨＰＨＡＳＥ設定関数
//=============================================================================
void	csz3SetSdiHPhase(uint8_t busNo,uint16_t val)
{
	*((uint16_t *)(CSZ3_SDI_HPHASE0 + CSZ3_BUS_OFFSET * busNo))	=	(val & 0x00ff);
	*((uint16_t *)(CSZ3_SDI_HPHASE1 + CSZ3_BUS_OFFSET * busNo))	=	((val & 0x0f00) >> 8);
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＶＰＨＡＳＥ取得関数
//=============================================================================
uint16_t	csz3GetSdiVPhase(uint8_t busNo)
{
	uint16_t	v0	=	*((uint16_t *)(CSZ3_SDI_VPHASE0 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	v1	=	*((uint16_t *)(CSZ3_SDI_VPHASE1 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	val	=	(v0 & 0x00ff) | ((v1 & 0x0007) << 8);
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＶＰＨＡＳＥ設定関数
//=============================================================================
void	csz3SetSdiVPhase(uint8_t busNo,uint16_t val)
{
	*((uint16_t *)(CSZ3_SDI_VPHASE0 + CSZ3_BUS_OFFSET * busNo))	=	(val & 0x00ff);
	*((uint16_t *)(CSZ3_SDI_VPHASE1 + CSZ3_BUS_OFFSET * busNo))	=	((val & 0x0700) >> 8);
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＸＰ　ＰＳＥＬ取得関数
//=============================================================================
uint8_t	csz3GetSdiXpPsel(uint8_t busNo)
{
	uint16_t	val	=	*((uint16_t *)(CSZ3_XP_PSEL + 2 * busNo));
	return((uint8_t)(val & 0xff));
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＸＰ　ＰＳＥＬ設定関数
//=============================================================================
void	csz3SetSdiXpPsel(uint8_t busNo,uint8_t val)
{
	*((uint16_t *)(CSZ3_XP_PSEL + 2 * busNo))	=	(uint16_t)val;
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＶＦＭＴ取得関数
//=============================================================================
uint16_t	csz3GetSdiVFmt(uint8_t busNo)
{
	uint16_t	val	=	*((uint16_t *)(CSZ3_SDI_VFMT + CSZ3_BUS_OFFSET * busNo));
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＶＦＭＴ設定関数
//=============================================================================
void	csz3VfmtSet(uint8_t busNo,uint16_t fmt)
{
	*((uint16_t *)(CSZ3_SDI_VFMT + CSZ3_BUS_OFFSET * busNo))	=	fmt;
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＴＸ＿ＭＯＤＥ取得関数
//=============================================================================
uint16_t	csz3GetSdiTxmode(uint8_t busNo)
{
	uint16_t	val	=	*((uint16_t *)(CSZ3_SDI_TX_MODE + CSZ3_BUS_OFFSET * busNo));
	return(val);
}
//=============================================================================
//==	ＣＳＺ３　ＴＸ＿ＭＯＤＥ設定関数
//=============================================================================
void	csz3TxmodeSet(uint8_t busNo,uint16_t mode)
{
	*((uint16_t *)(CSZ3_SDI_TX_MODE + CSZ3_BUS_OFFSET * busNo))	=	mode;
}
//=============================================================================
//==	ＣＳＺ３　ＳＤＩ　ＳＴＡＴＵＳ取得関数
//=============================================================================
uint16_t	csz3GetSdiStatus(uint8_t busNo)
{
	uint16_t	sta0	=	*((uint16_t *)(CSZ3_SDI_ST0 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	sta1	=	*((uint16_t *)(CSZ3_SDI_ST1 + CSZ3_BUS_OFFSET * busNo));
	uint16_t	sta		=	(sta0 & 0x00ff) | ((sta1 & 0x00ff) << 8);
	return(sta);
}
//=============================================================================
//==	ＣＳＺ３　ＴＥＳＴ取得関数
//=============================================================================
uint32_t	csz3GetTest()
{
	uint16_t	test0	=	*((uint16_t *)(CSZ3_TEST0));
	uint16_t	test1	=	*((uint16_t *)(CSZ3_TEST1));
	uint16_t	test2	=	*((uint16_t *)(CSZ3_TEST2));
	uint16_t	test3	=	*((uint16_t *)(CSZ3_TEST3));
	uint32_t	test	=	((uint32_t)test0 & 0x000000ff) |
							(((uint32_t)test1 & 0x000000ff) << 8) |
							(((uint32_t)test2 & 0x000000ff) << 16) |
							(((uint32_t)test3 & 0x000000ff) << 24);
	return(test);
}
//=============================================================================
//==	ＣＳＺ３　ＶＥＲＳＩＯＮ（ＦＰＧＡ２）取得関数
//=============================================================================
uint32_t	csz3GetVer2()
{
	uint16_t	ver0	=	*((uint16_t *)(CSZ3_VER0));
	uint16_t	ver1	=	*((uint16_t *)(CSZ3_VER1));
	uint16_t	ver2	=	*((uint16_t *)(CSZ3_VER2));
	uint16_t	ver3	=	*((uint16_t *)(CSZ3_VER3));
	uint32_t	ver	=	((uint32_t)ver0 & 0x000000ff) |
							(((uint32_t)ver1 & 0x000000ff) << 8) |
							(((uint32_t)ver2 & 0x000000ff) << 16) |
							(((uint32_t)ver3 & 0x000000ff) << 24);
	return(ver);
}
//=============================================================================
//==	ＣＳＺ３　ＶＥＲＳＩＯＮ（ＦＰＧＡ３）取得関数
//=============================================================================
uint32_t	csz3GetVer3()
{
	uint16_t	ver0	=	*((uint16_t *)(CSZ3_VER4));
	uint16_t	ver1	=	*((uint16_t *)(CSZ3_VER5));
	uint16_t	ver2	=	*((uint16_t *)(CSZ3_VER6));
	uint16_t	ver3	=	*((uint16_t *)(CSZ3_VER7));
	uint32_t	ver	=	((uint32_t)ver0 & 0x000000ff) |
							(((uint32_t)ver1 & 0x000000ff) << 8) |
							(((uint32_t)ver2 & 0x000000ff) << 16) |
							(((uint32_t)ver3 & 0x000000ff) << 24);
	return(ver);
}
//=============================================================================
//==	ＣＳＺ３　ＤＩＰ　ＳＷ７（ＦＰＧＡ３）取得関数
//=============================================================================
uint16_t	csz3GetDipSw7()
{
	uint16_t	sw	=	*((uint16_t *)(CSZ3_DIPSW7));
	return(sw);
}
//=============================================================================
//==	ＣＳＺ３　ＴＥＭＰ取得関数
//=============================================================================
uint16_t	csz3GetTemp()
{
	uint16_t	tmph	=	*((uint16_t *)(CSZ3_TEMPH));
	uint16_t	tmpl	=	*((uint16_t *)(CSZ3_TEMPL));
	uint16_t	tmp		=	((tmph & 0x00ff) << 8) | (tmpl & 0x00ff);
	return(tmp);
}
//=============================================================================
//==	ＣＳＺ３　ＸＰ＿ＯＵＴ取得関数
//=============================================================================
uint8_t	csz3GetXpOut(uint8_t index)
{
	uint16_t	out16	=	*((uint16_t *)(CSZ3_XP_OUT + index * 2));
	uint8_t		out	=	(uint8_t)(out16 & 0x00ff);
	return(out);
}
//=============================================================================
//==	ＣＳＺ３　ＸＰ＿ＯＵＴ設定関数
//=============================================================================
void	csz3SetXpOut(uint8_t index,uint8_t val)
{
	*((uint16_t *)(CSZ3_XP_OUT + index * 2))	=	(uint16_t)val;
}
//=============================================================================
//==	ＣＳＺ３　ＷＤＴ　ＯＮＯＦＦ設定関数
//=============================================================================
#define CSZ3_WDT_BITS		0x0001
void	csz3SetWdtOnOff(uint8_t onoff)
{
	if (onoff == 1)
	{
		*((uint16_t *)(CSZ3_WDT_ONOFF)) |= CSZ3_WDT_BITS;
	}
	else
	{
		*((uint16_t *)(CSZ3_WDT_ONOFF)) &= ~CSZ3_WDT_BITS;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　３２ｂｉｔｓ取得関数
//=============================================================================
uint32_t	xiGetRegister(uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.us[0]	=	*((uint16_t *)(adr));
	uu.us[1]	=	*((uint16_t *)(adr + 2));
	return(uu.ui);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiGetUnitControl1(uint8_t busNo)
{
	return(xiGetRegister(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ１設定関数
//=============================================================================
void	xiSetUnitControl1(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
	//	For MONI
//	if (g_isBoard20X())	xiMoniSetUnitControl1(busNo,data);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ１
//==	Ｓｔｒｅａｍ　Ｖｉｄｅｏ　Ｏｕｔ　＆
//==	Ｔｒｉｐｌｅ−Ｒａｔｅ　ＳＤＩ　ＴＸ
//==	＆　ＴＸ　ＡＬＬ
//==	ＯＮ／ＯＦＦ関数
//=============================================================================
void	xiSetUnitControl1OutOnOff(uint8_t busNo,uint8_t onoff)
{
	if (onoff == 1)
	{
//		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo + 2))	|=	UC_INIT_ENB;
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo + 2))	|=	(UC_INIT_ENB | UC_INIT_VAL_TX_EX);
	}
	else
	{
//		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo + 2))	&=	~UC_INIT_ENB;
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo + 2))	&=	~(UC_INIT_ENB | UC_INIT_VAL_TX_EX);
	}
	//	For MONI
//	if (g_isBoard20X())	xiMoniSetUnitControl1OutOnOff(busNo,onoff);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ１
//==	フォーマット不正時のリセットビット操作（ＴＸ）
//=============================================================================
#define	UC_FMT_FAIL_TX_BITS		0x00C0
void	xiSetResetWhenTxFormatFail(uint8_t busNo,BOOL fail)
{
	if (fail == TRUE)
	{
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo))	|=	UC_FMT_FAIL_TX_BITS;
	}
	else
	{
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo))	&=	~UC_FMT_FAIL_TX_BITS;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ１
//==	フォーマット不正時のリセットビット操作（ＲＸ）
//=============================================================================
#define	UC_FMT_FAIL_RX_BITS		0x000A
void	xiSetResetWhenRxFormatFail(uint8_t busNo,BOOL fail)
{
	if (fail == TRUE)
	{
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo))	|=	UC_FMT_FAIL_RX_BITS;
	}
	else
	{
		*((uint16_t *)(XI_UNIT_CONTROL1 + XI_BUS_OFFSET * busNo))	&=	~UC_FMT_FAIL_RX_BITS;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiGetUnitControl2(uint8_t busNo)
{
	return(xiGetRegister(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ２ＳＹＮＣ設定関数
//=============================================================================
void	xiSetUnitControl2Sync(uint8_t data)
{
//	*((uint16_t *)(XI_UNIT_CONTROL2 + 2))	&=	0xF0FF;		// 20210203
//	*((uint16_t *)(XI_UNIT_CONTROL2 + 2))	|=	((uint16_t)(data & 0xf0) << 4);
	uint16_t	us;
	us	=	*((uint16_t *)(XI_UNIT_CONTROL2 + 2));
	us	&=	0xF0FF;
	us	|=	((uint16_t)(data & 0xf0) << 4);
	*((uint16_t *)(XI_UNIT_CONTROL2 + 2))	=	us;
	//	For MONI
	if (g_isBoard20X())	xiMoniSetUnitControl2Sync(data);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ２設定関数
//=============================================================================
void	xiSetUnitControl2(uint8_t busNo,uint16_t data)
{
//	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		=	(data & 0x00FF);
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//$$	UNIT CONTROL2 No14 の DEMUX A Update は必ずONにすること！！
	//$$	FPGAが1つのDEMUXを4つにするために利用している！！
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		=	(data & 0x00FF) | 0x4000;
	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo + 2))	&=	~0x8000;	//	PK_CUT OFF
	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo + 2))	|=	0x4000;		//	FILTER_EN ON
//	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo + 2))	=	0x0000;		//	PK_CUT OFF & FILTER_EN OFF
	__NOP();
	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		=	0x4000;
	//	For MONI
	if (g_isBoard20X())	xiMoniSetUnitControl2(busNo,data);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＵＮＩＴ　ＣＯＮＴＲＯＬ２ ビットセット関数
//=============================================================================
void	xiBitSetUnitControl2(uint8_t busNo,uint8_t bit)
{
	*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		|=	(0x0001 << bit);
	//	For MONI
	if (g_isBoard20X())	xiMoniBitSetUnitControl2(busNo,bit);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＶＥＲＳＩＯＮ取得関数
//=============================================================================
uint32_t	xiGetFpgaVersion(void)
{
	return(xiGetRegister(XI_FPGA_VERSION));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＢＵＧ取得関数
//=============================================================================
uint32_t	xiGetDebug(uint8_t busNo)
{
	return(xiGetRegister(XI_DEBUG + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＢＯＡＲＤ　ＲＥＶＩＳＩＯＮ取得関数
//=============================================================================
uint32_t	xiGetBoardRevision(void)
{
	return(xiGetRegister(XI_BOARD_REVISION));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＥＭＰＥＲＡＴＵＲＥ取得関数
//=============================================================================
uint32_t	xiGetTemperature(void)
{
	return(xiGetRegister(XI_TEMPERATURE));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＩＰ　ＳＷ取得関数
//=============================================================================
uint32_t	xiGetDipSw(void)
{
	return(xiGetRegister(XI_DIP_SW));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＭＯＮＩＴＯＲ１取得関数
//=============================================================================
uint32_t	xiGetRxMonitor1(uint8_t busNo)
{
	return(xiGetRegister(XI_RX_MONITOR1 + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＭＯＮＩＴＯＲ１取得関数（分割取得）
//=============================================================================
void	xiGetRxMonitor1B(uint8_t busNo,uint16_t *errcnt,uint16_t *moni)
{
	*moni	=	*((uint16_t *)(XI_RX_MONITOR1 + XI_BUS_OFFSET * busNo));
	*errcnt	=	*((uint16_t *)(XI_RX_MONITOR1 + XI_BUS_OFFSET * busNo + 2));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＭＯＮＩＴＯＲ２取得関数
//=============================================================================
uint32_t	xiGetRxMonitor2(uint8_t busNo)
{
	return(xiGetRegister(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＭＯＮＩＴＯＲ２設定関数
//=============================================================================
void	xiSetRxMonitor2(uint8_t busNo,uint16_t data)
{
	uint16_t	cur	=	*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2));
	*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2))	=	data;
	if ((commonData.dipSw4 & 0x04))	//	自走モード
	{
		*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2))	|=	0x0100;
		*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2))	&=	~0x0E00;
	}
	else							//	他走モード
	{
		//	Reset Hi & lo when format changing.
		if ((cur & 0x0700) != (data & 0x0700))
		{
			*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2))	|=	0x0800;
			*((uint16_t *)(XI_RX_MONITOR2 + XI_BUS_OFFSET * busNo + 2))	&=	~0x0800;
		}
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＭＯＮＩＴＯＲ３取得関数
//=============================================================================
uint32_t	xiGetRxMonitor3(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_RX_MONITOR3 + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＣＯＮＴＲＯＬ取得関数
//=============================================================================
uint32_t	xiGetTxControl(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_TX_CONTROL + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＣＯＮＴＲＯＬ設定関数
//=============================================================================
void	xiSetTxControl(uint8_t busNo,uint16_t fmt)
{
	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo))		=	fmt;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＣＯＮＴＲＯＬ　ＦＭＴ設定関数
//=============================================================================
void	xiSetTxControlFMT(uint8_t busNo,uint16_t fmt)
{
	uint16_t	val;
	val	=	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo));
	val	&=	~0x000F;
	val	|=	(fmt & 0x000F);
	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo))		=	val;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＣＯＮＴＲＯＬ　ＴＸ ＬＯＯＰ ＭＯＤＥ設定関数
//=============================================================================
void	xiSetTxControlTxLoopMode(uint8_t busNo,uint16_t chdat)
{
	uint16_t	val;
	val	=	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo + 2));
	val	&=	~0x0007;
	val	|=	(chdat & 0x0007);
	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo + 2))		=	val;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＣＯＮＴＲＯＬ　ＴＸ ＬＯＯＰ ＭＯＤＥ取得関数
//=============================================================================
uint16_t	xiGetTxControlTxLoopMode(uint8_t busNo)
{
	uint16_t	val;
	val	=	*((uint16_t *)(XI_TX_CONTROL + XI_BUS_OFFSET * busNo + 2));
	val	&=	0x0007;

	return val;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ取得関数
//=============================================================================
uint32_t	xiGetVideoStreamMonitor(uint8_t busNo)
{
	return(xiGetRegister(XI_VIDEO_STREAM_MONITOR + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ２取得関数
//=============================================================================
uint32_t	xiGetVideoStreamMonitor2(uint8_t busNo)
{
	return(xiGetRegister(XI_VIDEO_STREAM_MONITOR2 + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl1(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL1 + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１設定関数
//==	（ＲＡＴＥ＆ＦＡＭＩＬＹ）
//=============================================================================
void	xiSetHeaderAddControl1(uint8_t busNo,uint16_t fmt)
{
	*((uint16_t *)(XI_HEADER_ADD_CONTROL1 + XI_BUS_OFFSET * busNo))		=	fmt;
	//	For MONI
	if (g_isBoard20X())	xiMoniSetHeaderAddControl1(busNo,fmt);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl2(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL2 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ３取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl3(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL3 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ２，３設定関数
//==	（ＳＯＵＲＣＥ　ＭＡＣ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetHeaderAddControl23(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr,uint8_t sel,uint8_t ext,uint8_t vlan,uint8_t tos)
{
	union
	{
		uint8_t		cc[4];
		uint16_t	us[2];
	}	uu;
	uu.cc[0]	=	0;
	uu.cc[1]	=	sel << 7;
	uu.cc[1]	|=	(ext << 6);
	uu.cc[1]	|=	(vlan << 5);
	uu.cc[1]	|=	(tos << 4);
	uu.cc[2]	=	*(adr + 1);
	uu.cc[3]	=	*(adr + 0);
	*((uint16_t *)(XI_HEADER_ADD_CONTROL2 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL2 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
	uu.cc[0]	=	*(adr + 5);
	uu.cc[1]	=	*(adr + 4);
	uu.cc[2]	=	*(adr + 3);
	uu.cc[3]	=	*(adr + 2);
	*((uint16_t *)(XI_HEADER_ADD_CONTROL3 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL3 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４，５設定関数
//==	（ＤＥＳＴＩＮＡＴＩＯＮ　ＭＡＣ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetHeaderAddControl45(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr)
{
	union
	{
		uint8_t		cc[4];
		uint16_t	us[2];
	}	uu;
	uu.cc[0]	=	*(adr + 1);
	uu.cc[1]	=	*(adr + 0);
	uu.cc[2]	=	0;
	uu.cc[3]	=	0;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	uu.cc[0]	=	*(adr + 5);
	uu.cc[1]	=	*(adr + 4);
	uu.cc[2]	=	*(adr + 3);
	uu.cc[3]	=	*(adr + 2);
	*((uint16_t *)(XI_HEADER_ADD_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＴＴＬ）
//=============================================================================
void	xiSetHeaderAddControl4TTL(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TTL,uint8_t moniTTL)
{
	union
	{
		uint8_t		cc[2];
		uint16_t	us[1];
	}	uu;
	uu.us[0]	=	*((uint16_t *)(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2));
	uu.cc[1]	=	TTL;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[0];
	//	For MONI
	if (g_isBoard20X())	xiMoniSetHeaderAddControl4TTL(chNo,swNo,busNo,moniTTL);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＴＯＳ）
//=============================================================================
void	xiSetHeaderAddControl4TOS(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TOS,uint8_t moniTOS)
{
	union
	{
		uint8_t		cc[2];
		uint16_t	us[1];
	}	uu;
	uu.us[0]	=	*((uint16_t *)(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2));
	uu.cc[0]	=	TOS;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[0];
	//	For MONI
	if (g_isBoard20X())	xiMoniSetHeaderAddControl4TOS(chNo,swNo,busNo,moniTOS);
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ６取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ６設定関数
//==	（ＳＯＵＲＣＥ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ７取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL7 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ７設定関数
//==	（ＤＥＳＴＩＮＡＴＩＯＮ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL7 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL7 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ８取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL8 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ８設定関数
//==	（ＳＯＵＲＣＥ＆ＤＥＳＴＩＮＡＴＩＯＮ　ＰＯＲＴ）
//=============================================================================
void	xiSetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t src,uint16_t dst)
{
	*((uint16_t *)(XI_HEADER_ADD_CONTROL8 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	dst;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL8 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	src;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ９取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL9 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ９設定関数
//==	（ＳＳＲＣ）
//=============================================================================
void	xiSetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	ssrc;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL9 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL9 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１０取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL10 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１０設定関数
//==	（ＶＬＡＮ）
//=============================================================================
void	xiSetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t vlan)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui		=	vlan;
	*((uint16_t *)(XI_HEADER_ADD_CONTROL10 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_HEADER_ADD_CONTROL10 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１１取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl11(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL11 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１２取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl12(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL12 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１３取得関数
//=============================================================================
uint32_t	xiGetHeaderAddControl13(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_ADD_CONTROL13 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl1(uint8_t chNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ１　ＲＥＣＩＶＥ＿ＯＦＦ設定関数
//=============================================================================
void		xiSetHeaderDelControl1ReciveOff(uint8_t chNo,uint8_t busNo,uint8_t val)
{
	uint16_t	us;
	if (!val)	//	OFF
	{
//		*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2))	|=	0x0004;
		us	=	*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2));
		us	&=	0x0007;
		us	|=	0x0004;
		*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2))	=	us;
	}
	else		//	ON
	{
//		*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2))	&=	~0x0004;
		us	=	*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2));
		us	&=	0x0003;
		*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2))	=	us;
	}
	//	Update
	if (!chNo)	//	A
	{
		*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		|=	0x0100;
		__NOP();
		*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		&=	~0x0100;
	}
	else		//	B
	{
		*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		|=	0x0200;
		__NOP();
		*((uint16_t *)(XI_UNIT_CONTROL2 + XI_BUS_OFFSET * busNo))		&=	~0x0200;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ１　ＢＵＦＦＥＲ設定関数
//=============================================================================
void		xiSetHeaderDelControl1Buffer(uint8_t chNo,uint8_t busNo,uint8_t val)
{
	uint16_t	cur	=	*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2));
	cur	&=	0xfffc;
	cur	|=	(val & 0x0003);
	*((uint16_t *)(XI_HEADER_DEL_CONTROL1 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + 2))	|=	cur;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl2(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL2 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ３取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl3(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL3 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ４取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl4(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL4 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl5(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL5 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl5_Reg(uint8_t chNo,uint8_t busNo,uint8_t regNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL5 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo + XI_REG_OFFSET * regNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ６取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl6(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL6 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ７取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl7(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL7 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ８取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl8(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL8 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ９取得関数
//=============================================================================
uint32_t	xiGetHeaderDelControl9(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_HEADER_DEL_CONTROL9 + XI_CH_OFFSET * chNo + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ取得関数
//=============================================================================
uint32_t	xiGetTxFecCalculatorol(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_TX_FEC_CALCULATOROL + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＴＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ設定関数
//=============================================================================
void		xiSetTxFecCalculatorol(uint8_t busNo,uint16_t fecL,uint8_t fecD,uint8_t opt)
{
	union
	{
		uint8_t		uc[4];
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.us[0]	=	fecL;
	uu.uc[2]	=	fecD;
	uu.uc[3]	=	opt;
	*((uint16_t *)(XI_TX_FEC_CALCULATOROL + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_TX_FEC_CALCULATOROL + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ取得関数
//=============================================================================
uint32_t	xiGetRxFecCalculatorol(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_RX_FEC_CALCULATOROL + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＲＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ設定関数
//=============================================================================
void		xiSetRxFecCalculatorol(uint8_t busNo,uint16_t fecL,uint8_t fecD,uint8_t opt)
{
	union
	{
		uint8_t		uc[4];
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.us[0]	=	fecL;
	uu.uc[2]	=	fecD;
	uu.uc[3]	=	opt;
	*((uint16_t *)(XI_TX_FEC_CALCULATOROL + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_TX_FEC_CALCULATOROL + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiGetDemuxControl1(uint8_t chNo,uint8_t upd)
{
	return(xiGetRegister(XI_DEMUX_CONTROL1 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiGetDemuxControl2(uint8_t chNo,uint8_t upd)
{
	return(xiGetRegister(XI_DEMUX_CONTROL2 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ１，２設定関数
//==	（ＭＡＣ）
//=============================================================================
void	xiSetDemuxControl12(uint8_t chNo,uint8_t *adr)
{
	union
	{
		uint8_t		cc[4];
		uint16_t	us[2];
	}	uu;
	uu.cc[0]	=	*(adr + 1);
	uu.cc[1]	=	*(adr + 0);
	uu.cc[2]	=	0;
	uu.cc[3]	=	0;
	*((uint16_t *)(XI_DEMUX_CONTROL1 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_DEMUX_CONTROL1 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
	uu.cc[0]	=	*(adr + 5);
	uu.cc[1]	=	*(adr + 4);
	uu.cc[2]	=	*(adr + 3);
	uu.cc[3]	=	*(adr + 2);
	*((uint16_t *)(XI_DEMUX_CONTROL2 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_DEMUX_CONTROL2 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ１
//==	（ＴＸ：ＥＮＡＢＬＥ）
//=============================================================================
#define SFP_TXENABE_ONOFF		0x1000
void	xiSetDemuxControl1TxEnable(uint8_t chNo,uint8_t onoff)
{
	if (onoff == 0)
	{
		*((uint16_t *)(XI_DEMUX_CONTROL1 + XI_CH_OFFSET * chNo + 2))	&=	~SFP_TXENABE_ONOFF;
	}
	else
	{
		*((uint16_t *)(XI_DEMUX_CONTROL1 + XI_CH_OFFSET * chNo + 2))	|=	SFP_TXENABE_ONOFF;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ３取得関数
//=============================================================================
uint32_t	xiGetDemuxControl3(uint8_t chNo,uint8_t upd)
{
	return(xiGetRegister(XI_DEMUX_CONTROL3 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ３設定関数
//==	（ＩＰ）
//=============================================================================
void	xiSetDemuxControl3(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_DEMUX_CONTROL3 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_DEMUX_CONTROL3 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ４取得関数
//=============================================================================
uint32_t	xiGetDemuxControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_DEMUX_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＦＩＲＥＷＡＬＬ＆ＤＥＳＴＩＮＡＴＩＯＮ　ＰＯＲＴ）
//=============================================================================
void	xiSetDemuxControl4(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t dst)
{
	*((uint16_t *)(XI_DEMUX_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	dst;
	*((uint16_t *)(XI_DEMUX_CONTROL4 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	0;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiGetDemuxControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_DEMUX_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ５設定関数
//==	（ＤＥＳＴＩＮＡＴＩＯＮ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetDemuxControl5(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_DEMUX_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_DEMUX_CONTROL5 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ６取得関数
//=============================================================================
uint32_t	xiGetDemuxControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_DEMUX_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_UPD_OFFSET * upd + XI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ６設定関数
//==	（ＳＳＲＣ）
//=============================================================================
void	xiSetDemuxControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	ssrc;
	*((uint16_t *)(XI_DEMUX_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_DEMUX_CONTROL6 + XI_CH_OFFSET * chNo + XI_SW_OFFSET * swNo + XI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ７取得関数
//=============================================================================
uint32_t	xiGetDemuxControl7(uint8_t chNo,uint8_t upd,uint8_t index)
{
	return(xiGetRegister(XI_DEMUX_CONTROL7 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd + XI_IX_OFFSET * index));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ７設定関数
//==	（ＵＤＰ　ＰＯＲＴ＆ＥＮＡＢＬＥ）
//=============================================================================
void	xiSetDemuxControl7(uint8_t chNo,uint8_t index,uint16_t ena,uint16_t pno)
{
	*((uint16_t *)(XI_DEMUX_CONTROL7 + XI_CH_OFFSET * chNo + XI_IX_OFFSET * index))		=	pno;
	*((uint16_t *)(XI_DEMUX_CONTROL7 + XI_CH_OFFSET * chNo + XI_IX_OFFSET * index + 2))	=	ena;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ８取得関数
//=============================================================================
uint32_t	xiGetDemuxControl8(uint8_t chNo,uint8_t upd,uint8_t index)
{
	return(xiGetRegister(XI_DEMUX_CONTROL8 + XI_CH_OFFSET * chNo + XI_UPD_OFFSET * upd + XI_IX_OFFSET * index));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＤＥＭＵＸ　ＣＯＮＴＲＯＬ８設定関数
//==	（ＴＣＰ　ＰＯＲＴ＆ＥＮＡＢＬＥ）
//=============================================================================
void	xiSetDemuxControl8(uint8_t chNo,uint8_t index,uint16_t ena,uint16_t pno)
{
	*((uint16_t *)(XI_DEMUX_CONTROL8 + XI_CH_OFFSET * chNo + XI_IX_OFFSET * index))		=	pno;
	*((uint16_t *)(XI_DEMUX_CONTROL8 + XI_CH_OFFSET * chNo + XI_IX_OFFSET * index + 2))	=	ena;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＳＷ１２ＦＬＡＧ（ＤＥＭＵＸ　ＣＯＮＴＲＯＬ８）設定関数
//==	ＳＷ１／２フラグセット
//=============================================================================
void	xiSetSw12Flag(uint8_t chNo,uint8_t ch,uint8_t val)
{
	static	uint16_t	onVal[4]	=	{	0x0001,	0x0002,	0x0004,	0x0008	};
	if (val == 1)
	{
		*((uint16_t *)(XI_SW12_FLAG + XI_CH_OFFSET * chNo))	|=	onVal[ch];
	}
	else
	{
		*((uint16_t *)(XI_SW12_FLAG + XI_CH_OFFSET * chNo))	&=	~onVal[ch];
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８初期値設定関数
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ１，２，３）
//=============================================================================
void	xiIEEE1588InitSet(uint8_t chNo,uint32_t init_h,uint32_t init_m,uint32_t init_l)
{
	*((uint32_t *)(XI_IEEE1588_CONTROL3 + XI_CH_OFFSET * chNo))	=	init_l;
	*((uint32_t *)(XI_IEEE1588_CONTROL2 + XI_CH_OFFSET * chNo))	=	init_m;
	*((uint32_t *)(XI_IEEE1588_CONTROL1 + XI_CH_OFFSET * chNo))	=	init_h | 0x80000000;
	*((uint32_t *)(XI_IEEE1588_CONTROL1 + XI_CH_OFFSET * chNo))	=	init_h;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８オフセット設定関数
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ１，２，３）
//=============================================================================
void	xiIEEE1588OffsetSet(uint8_t chNo,uint32_t offset_h,uint32_t offset_m,uint32_t offset_l)
{
	*((uint32_t *)(XI_IEEE1588_CONTROL3 + XI_CH_OFFSET * chNo))	=	offset_l;
	*((uint32_t *)(XI_IEEE1588_CONTROL2 + XI_CH_OFFSET * chNo))	=	offset_m;
	*((uint32_t *)(XI_IEEE1588_CONTROL1 + XI_CH_OFFSET * chNo))	=	offset_h | 0x40000000;
	*((uint32_t *)(XI_IEEE1588_CONTROL1 + XI_CH_OFFSET * chNo))	=	offset_h;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８加数値設定関数
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ４）
//=============================================================================
void	xiIEEE1588AddValueSet(uint8_t chNo,uint32_t add_val)
{
	*((uint32_t *)(XI_IEEE1588_CONTROL4 + XI_CH_OFFSET * chNo))	=	add_val;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８周期時刻値設定関数
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ５，６）
//=============================================================================
void	xiIEEE1588IntervalTimeSet(uint8_t chNo,uint32_t interval_h,uint32_t interval_l)
{
	*((uint32_t *)(XI_IEEE1588_CONTROL6 + XI_CH_OFFSET * chNo))	=	interval_l;
	*((uint32_t *)(XI_IEEE1588_CONTROL5 + XI_CH_OFFSET * chNo))	=	interval_h | 0x80000000;
	*((uint32_t *)(XI_IEEE1588_CONTROL5 + XI_CH_OFFSET * chNo))	=	interval_h;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８周期時刻初期値設定関数
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ７，８）
//=============================================================================
void	xiIEEE1588IntervalInitTimeSet(uint8_t chNo,uint32_t interval_h,uint32_t interval_l)
{
	*((uint32_t *)(XI_IEEE1588_CONTROL8 + XI_CH_OFFSET * chNo))	=	interval_l;
	*((uint32_t *)(XI_IEEE1588_CONTROL7 + XI_CH_OFFSET * chNo))	=	interval_h | 0x80000000;
	*((uint32_t *)(XI_IEEE1588_CONTROL7 + XI_CH_OFFSET * chNo))	=	interval_h;
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８送信タイムスタンプ値取得
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ９，１０，１１，１２）
//=============================================================================
void	xiIEEE1588TxTimeStampGet(uint8_t chNo,uint32_t *time_stamp)
{
	time_stamp[0]   = *((uint32_t *)(XI_IEEE1588_CONTROL12 + XI_CH_OFFSET * chNo));
	time_stamp[1]   = *((uint32_t *)(XI_IEEE1588_CONTROL11 + XI_CH_OFFSET * chNo));
	time_stamp[2]   = *((uint32_t *)(XI_IEEE1588_CONTROL10 + XI_CH_OFFSET * chNo));
	time_stamp[3]   = *((uint32_t *)(XI_IEEE1588_CONTROL9  + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８受信タイムスタンプ値取得
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ１３，１４，１５，１６）
//=============================================================================
void	xiIEEE1588RxTimeStampGet(uint8_t chNo,uint32_t *time_stamp)
{
	time_stamp[0]   = *((uint32_t *)(XI_IEEE1588_CONTROL16 + XI_CH_OFFSET * chNo));
	time_stamp[1]   = *((uint32_t *)(XI_IEEE1588_CONTROL15 + XI_CH_OFFSET * chNo));
	time_stamp[2]   = *((uint32_t *)(XI_IEEE1588_CONTROL14 + XI_CH_OFFSET * chNo));
	time_stamp[3]   = *((uint32_t *)(XI_IEEE1588_CONTROL13 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　IEEE１５８８システム時刻値取得
//==    （ＩＥＥＥ１５８８　ＣＯＮＴＲＯＬ１３，１４，１５，１６）
//=============================================================================
void	xiIEEE1588SystemTimeGet(uint8_t chNo,uint32_t *system_time)
{
	system_time[0]   = *((uint32_t *)(XI_IEEE1588_CONTROL19 + XI_CH_OFFSET * chNo));
	system_time[1]   = *((uint32_t *)(XI_IEEE1588_CONTROL18 + XI_CH_OFFSET * chNo));
	system_time[2]   = *((uint32_t *)(XI_IEEE1588_CONTROL17 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl1(void)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL1));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１設定関数
//=============================================================================
void	xiSetL3SwitchControl1(uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_L3SWITCH_CONTROL1))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL1 + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１　Ａ／Ｂ設定関数
//=============================================================================
void	xiSetL3SwitchControl1AB(uint8_t data)
{
	if (data)
	{
		*((uint16_t *)(XI_L3SWITCH_CONTROL1 + 2))		|=	0x0001;
	}
	else
	{
		*((uint16_t *)(XI_L3SWITCH_CONTROL1 + 2))		&=	~0x0001;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl2(void)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL2));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ２設定関数
//=============================================================================
void	xiSetL3SwitchControl2(uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_L3SWITCH_CONTROL2))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL2 + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ３取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl3(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL3 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ３設定関数
//==	（ＣＯＮＴ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetL3SwitchControl3(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL3 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL3 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ４取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl4(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL4 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＡＬＭ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetL3SwitchControl4(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL4 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL4 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl5(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL5 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ５設定関数
//==	（ＳＥＴ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetL3SwitchControl5(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL5 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL5 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ６取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl6(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL6 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ６設定関数
//==	（ＥＴＣ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiSetL3SwitchControl6(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL6 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL6 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ７取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl7(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL7 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ７設定関数
//==	（ＣＯＮＴ　ＩＰ　ＭＡＳＫ）
//=============================================================================
void	xiSetL3SwitchControl7(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL7 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL7 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ８取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl8(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL8 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ８設定関数
//==	（ＡＬＭ　ＩＰ　ＭＡＳＫ）
//=============================================================================
void	xiSetL3SwitchControl8(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL8 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL8 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ９取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl9(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL9 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ９設定関数
//==	（ＳＥＴ　ＩＰ　ＭＡＳＫ）
//=============================================================================
void	xiSetL3SwitchControl9(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL9 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL9 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１０取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl10(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL10 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１０設定関数
//==	（ＥＴＣ　ＩＰ　ＭＡＳＫ）
//=============================================================================
void	xiSetL3SwitchControl10(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL10 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL10 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１１取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl11(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL11 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１１設定関数
//==	（制御サーバー（裏）　ＩＰマスク）
//=============================================================================
void	xiSetL3SwitchControl11(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL11 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL11 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１２取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl12(void)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL12));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１３取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl13(uint8_t chNo)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL13 + XI_CH_OFFSET * chNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１３設定関数
//==	（制御サーバー（裏）　ＩＰ）
//=============================================================================
void	xiSetL3SwitchControl13(uint8_t chNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_L3SWITCH_CONTROL13 + XI_CH_OFFSET * chNo))		=	uu.us[0];
	*((uint16_t *)(XI_L3SWITCH_CONTROL13 + XI_CH_OFFSET * chNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　Ｌ３ＳＷＩＴＣＨ　ＣＯＮＴＲＯＬ１４取得関数
//=============================================================================
uint32_t	xiGetL3SwitchControl14(void)
{
	return(xiGetRegister(XI_L3SWITCH_CONTROL14));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＩＮＦＯＲ　ＲＥＧ取得関数
//=============================================================================
uint32_t	xiGetInfoReg(uint32_t idx)
{
	return(xiGetRegister(XI_INFOR_REG + 4 * idx));
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＳＦＰ　ＲＥＧ設定関数
//=============================================================================
void	xiSetSfpReg(uint32_t opt, uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_SFP1_REG + 4 * opt))		=	uu.us[0];
	*((uint16_t *)(XI_SFP1_REG + 4 * opt + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ　ＳＦＰ　ＲＥＧ取得関数
//=============================================================================
uint32_t	xiGetSfpReg(uint32_t opt)
{
	return(xiGetRegister(XI_SFP1_REG + 4 * opt));
}
