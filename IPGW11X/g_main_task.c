//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	メインタスク
//=============================================================================
#include	"IPGW11X_cfg.h"
#include	"interface.h"
#include	<limits.h> // INT_MAX
#include	<sys/mman.h> // mmap
#include	<fcntl.h>
#include	<unistd.h> 
#include	<errno.h>
//=============================================================================
//==	ローカル定義
//=============================================================================
#define STATION_REVISION_CODE		0x00		//	放送局（名古屋以外）	0x00
												//	拠点局（名古屋）		0x01
												//	拠点局（名古屋４Ｋ）	0x02

static	char	*monthStr[12]	=	{
	"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
};
static	uint8_t	rearBoardMatchTable[8][7]	=	{	//	0=OK	1=Missmatch
//	No		1		2		3		4		5		6
//	Dummy	IPT101	IPT101E	IPR101	IPR101E	IPT101S	IPT101ES
{	1,		0,		1,		1,		1,		0,		1	},		//	IPG101A Tx
{	1,		1,		1,		0,		1,		0,		1	},		//	IPG101A Rx
{	1,		0,		0,		1,		1,		0,		1	},		//	IPG101B Tx
{	1,		1,		1,		0,		0,		0,		1	},		//	IPG101B Rx
{	1,		0,		0,		1,		1,		0,		1	},		//	IPG101C Tx
{	1,		1,		1,		0,		0,		0,		1	},		//	IPG101C Rx
{	1,		1,		1,		1,		1,		0,		0	}		//	IPG102A
//	IPG103A以降 All OK
};
static	uint32_t	boardRevVal[]	=	{
	0x101A0000,		0x101B0000,		0x101C0000,
	0x102A0000,
	0x103A0000,
	0x104A0000,
	0x205A0000,
	0x206A0000,
	0x207A0000,
	0x208A0000,
	0x00000000	//	End
};
static	uint8_t	modelCode[]		=	{	//	機種コード
	0x09,	//	101A
	0x09,	//	101B
	0x09,	//	101C
	0x09,	//	102A
	0x09,	//	103A
	0x09,	//	104A
	0x0A,	//	205A
	0x0A,	//	206A
	0x0A,	//	207A
	0x0A	//	208A
};
static	uint8_t	kikiCodeBase[]	=	{	//	機器コード
	0x01,	//	101A
	0x01,	//	101B
	0x01,	//	101C
	0x03,	//	102A
	0x05,	//	103A
	0x07,	//	104A
	0x01,	//	205A
	0x03,	//	206A
	0x05,	//	207A
	0x07	//	208A
};
static	uint8_t	*frontBdNames[]	=	{
	"101A    ",		//	0
	"101B    ",		//	1
	"101C    ",		//	2
	"102A    ",		//	3
	"103A    ",		//	4
	"104A    ",		//	5
	"205A    ",		//	6
	"206A    ",		//	7
	"207A    ",		//	8
	"208A    "		//	9
};
static	uint8_t	*rearBdNames[]	=	{
	"        ",		//	0
	"IPT101  ",		//	1
	"IPT101E ",		//	2
	"IPR101  ",		//	3
	"IPR101E ",		//	4
	"IPT101S ",		//	5
	"IPT101ES"		//	6
};
//static	uint32_t	debi;
//static	uint8_t		deb_data[256];

//=============================================================================
//==	FPGA mmapアドレス補正
//=============================================================================

/** CS共通ブロックサイズ */
#define		CS_BLOCK_SIZE		0x02000000

/** CZ2(Kintex) 
 @note RTOSのXI(ＸＬＩＮＫＸ)から名称変更しない */
#define		XI_TOP_ADR			0x54000000

/** CS3(Trion)
 @note RTOSのCSZ3(ＣＳＺ３)から名称変更しない */
#define		CSZ3_TOP_ADR		0x56000000

#define		XI_MMAP_ADDR		m_xi_mmap
#define		CSZ3_MMAP_ADDR		m_csz3_mmap

//	ＸＬＩＮＫＸ
extern	volatile	uint32_t	XI_UNIT_CONTROL1;
extern	volatile	uint32_t	XI_UNIT_CONTROL2;
extern	volatile	uint32_t	XI_FPGA_VERSION;
extern	volatile	uint32_t	XI_DEBUG;
extern	volatile	uint32_t	XI_BOARD_REVISION;
extern	volatile	uint32_t	XI_TEMPERATURE;
extern	volatile	uint32_t	XI_DIP_SW;
extern	volatile	uint32_t	XI_RX_MONITOR1;
extern	volatile	uint32_t	XI_RX_MONITOR2;
extern	volatile	uint32_t	XI_RX_MONITOR3;
extern	volatile	uint32_t	XI_TX_CONTROL;
extern	volatile	uint32_t	XI_VIDEO_STREAM_MONITOR;
extern	volatile	uint32_t	XI_VIDEO_STREAM_MONITOR2;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL1;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL2;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL3;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL4;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL5;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL6;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL7;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL8;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL9;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL10;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL11;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL12;
extern	volatile	uint32_t	XI_HEADER_ADD_CONTROL13;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL1;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL2;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL3;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL4;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL5;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL6;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL7;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL8;
extern	volatile	uint32_t	XI_TX_FEC_CALCULATOROL;
extern	volatile	uint32_t	XI_RX_FEC_CALCULATOROL;
extern	volatile	uint32_t	XI_HEADER_DEL_CONTROL9;
extern	volatile	uint32_t	XI_DEMUX_CONTROL1;
extern	volatile	uint32_t	XI_DEMUX_CONTROL2;
extern	volatile	uint32_t	XI_DEMUX_CONTROL3;
extern	volatile	uint32_t	XI_DEMUX_CONTROL4;
extern	volatile	uint32_t	XI_DEMUX_CONTROL5;
extern	volatile	uint32_t	XI_DEMUX_CONTROL6;
extern	volatile	uint32_t	XI_DEMUX_CONTROL7;
extern	volatile	uint32_t	XI_DEMUX_CONTROL8;
extern	volatile	uint32_t	XI_SW12_FLAG;

extern	volatile	uint32_t	XI_IEEE1588_CONTROL1;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL2;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL3;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL4;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL5;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL6;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL7;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL8;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL9;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL10;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL11;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL12;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL13;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL14;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL15;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL16;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL17;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL18;
extern	volatile	uint32_t	XI_IEEE1588_CONTROL19;

extern	volatile	uint32_t	XI_L3SWITCH_CONTROL1;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL2;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL3;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL4;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL5;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL6;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL7;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL8;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL9;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL10;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL11;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL12;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL13;
extern	volatile	uint32_t	XI_L3SWITCH_CONTROL14;

extern	volatile	uint32_t	XI_INFOR_REG;

extern	volatile	uint32_t	XI_SFP1_REG;
extern	volatile	uint32_t	XI_SFP2_REG;

//	ＭＯＮＩ
extern	volatile	uint32_t	XI_MONI_UNIT_CONTROL1;
extern	volatile	uint32_t	XI_MONI_UNIT_CONTROL2;
extern	volatile	uint32_t	XI_MONI_DEBUG;
extern	volatile	uint32_t	XI_MONI_TICO_ENCODE1;
extern	volatile	uint32_t	XI_MONI_TICO_ENCODE2;
extern	volatile	uint32_t	XI_MONI_TICO_ENCODE3;
extern	volatile	uint32_t	XI_MONI_TICO_ENCODE4;
extern	volatile	uint32_t	XI_MONI_VIDEO_CONT;
extern	volatile	uint32_t	XI_MONI_AUDIO_PES1;
extern	volatile	uint32_t	XI_MONI_AUDIO_PES2;
extern	volatile	uint32_t	XI_MONI_AUDIO_STATUS1;
extern	volatile	uint32_t	XI_MONI_AUDIO_STATUS2;
extern	volatile	uint32_t	XI_MONI_ANC_CONT1;
extern	volatile	uint32_t	XI_MONI_ANC_CONT2;
extern	volatile	uint32_t	XI_MONI_ANC_PES1;
extern	volatile	uint32_t	XI_MONI_ANC_PES2;
extern	volatile	uint32_t	XI_MONI_ANC_DATA;
extern	volatile	uint32_t	XI_MONI_PID1;
extern	volatile	uint32_t	XI_MONI_PID2;
extern	volatile	uint32_t	XI_MONI_PID3;
extern	volatile	uint32_t	XI_MONI_PID4;
extern	volatile	uint32_t	XI_MONI_PID5;
extern	volatile	uint32_t	XI_MONI_SMPTE2022_1_2;
extern	volatile	uint32_t	XI_MONI_VIDEO_STREAM_MONITOR;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL1;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL2;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL3;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL4;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL5;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL6;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL7;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL8;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL9;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL10;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL11;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL12;
extern	volatile	uint32_t	XI_MONI_HEADER_ADD_CONTROL13;

//	ＣＳＺ３
extern	volatile	uint32_t	CSZ3_STATUS0;
extern	volatile	uint32_t	CSZ3_STATUS1;
extern	volatile	uint32_t	CSZ3_REF_AD_VAL0;
extern	volatile	uint32_t	CSZ3_REF_AD_VAL1;
extern	volatile	uint32_t	CSZ3_FREE_AD_SET0;
extern	volatile	uint32_t	CSZ3_FREE_AD_SET1;
extern	volatile	uint32_t	CSZ3_SRG_SET0;
extern	volatile	uint32_t	CSZ3_SRG_SET1;
extern  volatile    uint32_t    CSZ3_WDT_ONOFF;
extern	volatile	uint32_t	CSZ3_SDI_HPHASE0;
extern	volatile	uint32_t	CSZ3_SDI_HPHASE1;
extern	volatile	uint32_t	CSZ3_SDI_VPHASE0;
extern	volatile	uint32_t	CSZ3_SDI_VPHASE1;
extern	volatile	uint32_t	CSZ3_SDI_VFMT;
extern	volatile	uint32_t	CSZ3_SDI_TX_MODE;
extern	volatile	uint32_t	CSZ3_SDI_ST0;
extern	volatile	uint32_t	CSZ3_SDI_ST1;
extern	volatile	uint32_t	CSZ3_TEST0;
extern	volatile	uint32_t	CSZ3_TEST1;
extern	volatile	uint32_t	CSZ3_TEST2;
extern	volatile	uint32_t	CSZ3_TEST3;
extern	volatile	uint32_t	CSZ3_VER0;
extern	volatile	uint32_t	CSZ3_VER1;
extern	volatile	uint32_t	CSZ3_VER2;
extern	volatile	uint32_t	CSZ3_VER3;
extern	volatile	uint32_t	CSZ3_VER4;
extern	volatile	uint32_t	CSZ3_VER5;
extern	volatile	uint32_t	CSZ3_VER6;
extern	volatile	uint32_t	CSZ3_VER7;
extern	volatile	uint32_t	CSZ3_DIPSW7;
extern	volatile	uint32_t	CSZ3_TEMPL;
extern	volatile	uint32_t	CSZ3_TEMPH;
extern	volatile	uint32_t	CSZ3_XP_OUT;
extern	volatile	uint32_t	CSZ3_XP_PSEL;

static	int						m_mmap_fd;

static	uint8_t					*m_csz3_map_p;
static  volatile	uint32_t	m_csz3_mmap;

static	uint8_t					*m_xi_map_p;
static  volatile	uint32_t	m_xi_mmap;

// FPGA memmapアドレス補正処理
static	void	s_xiInitial(void);

//=============================================================================
//==	メインタスク関数
//=============================================================================
void	g_main_task(void)
{
	LOG_THREAD();

	uint8_t		i,ii,ic,doneSig;
	uint8_t		myNo,rearNo;
	uint32_t	boardRev,dipSw,mNo,dmx1,dmx2;
	ER			ercd;
	ER_RET		eret;
	uint8_t		rpVal	=	0;
	char		dateStr[16];
	char		timeStr[16];
	char		verTemp[4];
	//-------------------------------------------------------------------------
	//--	FPGA memmapアドレス補正処理(FPGAアクセス前に行う必要があるのでこの位置)
	//-------------------------------------------------------------------------
	s_xiInitial();
	//-------------------------------------------------------------------------
	//--	ＳＰＩ選択ポート初期化
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_GPIO, P0B, 0x02, 0);   //  SPI ROM S0
	GPIO_WRITE(RIN_RTPORT, RP2B, 0x01, 0);   //  SPI ROM S1
 	//-------------------------------------------------------------------------
	//--	ＦＰＧＡリセットポート初期化
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_GPIO, P0B, 0x10, 0);    // FPGA_RESETn(P04) = 0
	//-------------------------------------------------------------------------
	//--	LEDポート初期化
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_GPIO, P4B, 0xFF, 0);	//	PORT	40(LED0)	101Aのみ
									//			41(LED1
									//			42(LED2)
									//			43(LED3)
									//			45(LED4)
									//			46(LED5)
	GPIO_WRITE(RIN_GPIO, P5B, 0xFF, 0);	//	PORT	52(LED6)
									//			53(LED7)
									//			54(LED8)
									//			55(LED9)
									//			56(LED10)
									//			57(LED11)
	GPIO_WRITE(RIN_GPIO, P6B, 0x0C, 0);	//	PORT	62(LED12)
									//			63(LED13)
	//-------------------------------------------------------------------------
	//--	ＣＰＵ設定ディップスイッチポート初期化
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_GPIO, P7B, 0xFF, 1);
	//-------------------------------------------------------------------------
	//--	ＣＰＵ間バスポート初期化
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_RTPORT, RP0B, 0x80, 1);
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡ  ＳＦＰ初期化
	//-------------------------------------------------------------------------
	xiSetSfpReg(0, 0x00000000);
	xiSetSfpReg(1, 0x00000000);
	//-------------------------------------------------------------------------
	//--	ＬＥＤ全点灯
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 14 ; i++ )
	{
		g_led_control(i,1);
	}
	tslp_tsk(3000);
//debi = sizeof(CPU_VIDEO_LAYER_STR);
//debi =	sizeof(MSG485_CPU_VIDEO_CMD_RES);
	//-------------------------------------------------------------------------
	//--	ディップスイッチの読み込み
	//-------------------------------------------------------------------------
	commonData.dipSw4		=	GPIO_READ(RIN_GPIO, PIN7B, 0xFF);
	modelInf[0].dipSw[1]	=	commonData.dipSw4;
	/** @note Linuxでは未実装のディップスイッチ(dipSw2)
	 *  @attention EEPROM書込み許可の値(0x04)を設定してはおく(念のため)
	 */
	commonData.dipSw2		=	(0x20 >> 3);		//	AUX0=2bit AUX1=3bit
	modelInf[0].dipSw[0]	=	commonData.dipSw2;
	//--	自種別
	switch ((commonData.dipSw4 & 0x03))
	{
	case	0x03:	//	OFF,OFF
		commonData.tx_or_rx	=	HDIPTX;
		break;
	case	0x02:	//	OFF,ON
		commonData.tx_or_rx	=	HDIPRX;
		break;
	case	0x00:	//	ON,ON
		commonData.tx_or_rx	=	HDIPTXRX;
		break;
	default:		//	その他（他の種別が出てきたらそちらに使う）
		commonData.tx_or_rx	=	HDIPTXRX;
		break;
	}
	//--	ウォッチドッグ有効／無効
	commonData.wdtEnable	=	((commonData.dipSw4 & 0x40))?1:0;
	//--	設定経路（未定）
//	if ((commonData.dipSw4 & 0x80))	commonData.setting_InBand_or_485	=	SETTING_IN_BAND;
//	else							commonData.setting_InBand_or_485	=	SETTING_485;
	//-------------------------------------------------------------------------
	//--	機器情報セット
	//-------------------------------------------------------------------------
	//	コンパイル日時をバージョンにセット
	memset(dateStr,0,16);
	memcpy(dateStr,__DATE__,strlen(__DATE__));
	memset(timeStr,0,16);
	memcpy(timeStr,__TIME__,strlen(__TIME__));
	i						=	strlen(__DATE__);
	verTemp[0]		=	(dateStr[i - 2] - '0') * 10 + (dateStr[i - 1] - '0');
	for ( i = 0 ; i < 12 ; i++ )
	{
		if (!memcmp(dateStr,monthStr[i],3))
		{
			verTemp[1]		=	i + 1;
			break;
		}
	}
	if (dateStr[4] == ' ')	verTemp[2]		=	dateStr[5] - '0';
	else					verTemp[2]		=	(dateStr[4] - '0') * 10 + (dateStr[5] - '0');
	verTemp[3]		=	(timeStr[0] - '0') * 10 + (timeStr[1] - '0');
	//	１０進を１６進に直す
	for ( i = 0 ; i < 3 ; i++ )
	{
		modelInf[0].firmwareVer[i]	=	(verTemp[i] / 10) * 0x10 + (verTemp[i] % 10);
	}
	modelInf[0].firmwareVer[3]	=	((verTemp[3] / 3) << 4) + STATION_REVISION_CODE;
	//-------------------------------------------------------------------------
	//--	スロット番号取得
	//-------------------------------------------------------------------------
	commonData.slotNo	=	0;
	rpVal = GPIO_READ(RIN_RTPORT, RPIN3B, 0x40 | 0x08 | 0x10 | 0x04);
	if ((rpVal & 0x40))	commonData.slotNo	+=	8;
	if ((rpVal & 0x08))	commonData.slotNo	+=	4;
	if ((rpVal & 0x10))	commonData.slotNo	+=	2;
	if ((rpVal & 0x04))	commonData.slotNo	+=	1;
	//-------------------------------------------------------------------------
	//--	ＲＩＤ　ＲＡ＿ＯＵＴ取得
	//-------------------------------------------------------------------------
	i = GPIO_READ(RIN_GPIO, PIN3B, 0x0f << 2);
	commonData.RID	=	(i >> 2) & 0x0f;
	i = GPIO_READ(RIN_RTPORT, RPIN1B, 0x08);
	commonData.RA_OUT	=	(i & 0x08)?1:0;
	//-------------------------------------------------------------------------
	//--	ＥＥＰＲＯＭ（Ｉ２Ｃ）　ＣＨ０初期化
	//-------------------------------------------------------------------------
	eret	=	eep_init(I2C_EEPROM_CH,EEP_ADDRESS);
	if (eret != ER_OK)
	{
		return;
	}
	//-------------------------------------------------------------------------
	//--	後部基板ＥＥＰＲＯＭ　ＭＡＣアドレス情報読み込み
	//-------------------------------------------------------------------------
	commonData.saveDataCsmError	=	0;
	g_readSavePhysicalLayerEeprom();
	//	抜き差し時、MAC AddressがALL0になる場合（差し込み不良時）の対策（他走時のみ）
	if (!(commonData.dipSw4 & 0x04))
	{
		if (savePhysicalLayerEeprom.data.cpuMacAddress[0][0] != 0x00 ||
			savePhysicalLayerEeprom.data.cpuMacAddress[0][1] != 0x0A ||
			savePhysicalLayerEeprom.data.cpuMacAddress[0][2] != 0xC0 ||
			savePhysicalLayerEeprom.data.cpuMacAddress[1][0] != 0x00 ||
			savePhysicalLayerEeprom.data.cpuMacAddress[1][1] != 0x0A ||
			savePhysicalLayerEeprom.data.cpuMacAddress[1][2] != 0xC0)
		{
			GPIO_WRITE(RIN_RTPORT, RP0B, 0x80, 0);
			tslp_tsk(500);
			g_reset();	//	再起動！！
		}
	}
	memcpy(addressInf.macA,savePhysicalLayerEeprom.data.cpuMacAddress[0],6);
	memcpy(addressInf.macB,savePhysicalLayerEeprom.data.cpuMacAddress[1],6);
	//-------------------------------------------------------------------------
	//--	ＣＰＵ／ＶＩＤＥＯ情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveCpuVideoLayer();
//	vlanTagId	=	990;
//	vlanTagId	=	0xDE03;
//	vlanTagFlag	=	0;
	vlanTagId	=	(((uint16_t)saveCpuVideoLayer.data.cntVlanIdL << 8) | (uint16_t)saveCpuVideoLayer.data.cntVlanIdH);
	vlanTagFlag	=	saveCpuVideoLayer.data.cntVlanFlag;
	//-------------------------------------------------------------------------
	//--	アラームマスク情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveAlarmMask();
	//-------------------------------------------------------------------------
	//--	個別設定情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveIndividual();
	saveIndividualSetUndo	=	saveIndividualSet;
	//-------------------------------------------------------------------------
	//--	ＤＳ１００ＢＲレジスタ情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveDS100BR();
	//-------------------------------------------------------------------------
	//--	ＣＳＺ３保存情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveCSZ3();
	//-------------------------------------------------------------------------
	//--	系統情報保存情報読み込み
	//-------------------------------------------------------------------------
	g_readSaveSystemAB();
	//-------------------------------------------------------------------------
	//--	ＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御読み込み
	//-------------------------------------------------------------------------
	g_readSaveSdiIpMisInf();
	//-------------------------------------------------------------------------
	//--	共通変数初期化
	//-------------------------------------------------------------------------
	commonData.taskInit	=	0;
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		commonData.rxCurrentInputNo[i]	=	-1;
		commonData.inputChange[i]		=	0;
//		commonData.vrtSwTx[i]			=	0;
		commonData.vrtSwTx[i]			=	0xFF;
		commonData.vrtSwRx[i]			=	0;

		commonData.formatNgCoun[i]		=	0;
		commonData.txFormatNgCoun[i]	=	0;
	}
	commonData.A_or_B				=	0;
	commonData.boardKind			=	BK_102A;
	commonData.connectRequest[0]	=	0;
	commonData.connectRequest[1]	=	0;
	commonData.autoEqualizerConfig	=	0;
	for ( i = 0 ; i < 3 ; i++ )
	{
		commonData.fpgaTemp[i]	=	0;
		for ( ii = 0 ; ii < TEMP_MOV_AVR ; ii++ )
		{
			commonData.fpgaTempAvrBuf[i][ii]	=	0;
		}
		commonData.fpgaTempAvrCnt[i]	=	0;
		commonData.fpgaTempAvrWpt[i]	=	0;
	}
	commonData.fpgaResetDone	=	0;
	memset((uint8_t *)&alarmStatus,0,(uint32_t)sizeof(ALARM_STR));
	memset((uint8_t *)&alarmTrapDetectData,0,(uint32_t)sizeof(ALA_TRAP_DETECT_STR));
	memset((uint8_t *)&fwUpdateBuf,0,(uint32_t)sizeof(FW_UPDATE_BUF_STR));
	for ( i = 0 ; i < 2 ; i++ )
	{
		commonData.bcVRTrecFlag[i]		=	0;
		commonData.bcVRTsequenceNo[i]	=	0;
	}
	commonData.firstMTXChangeRec	=	0;
	commonData.startEnableFrom485	=	0;
	commonData.rs485monitor			=	0;

	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < BUSNO_MAX ; ii++ )
		{
			commonData.crcLossCoun[i][ii]	=	0;
		}
	}
	commonData.almPeriodCount	=	0;
	//-------------------------------------------------------------------------
	//--	初期アラームトラップ（正常、異常）出力用にフラグセット
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )
	{
		alarmStatus.txFault_C[i]	=	ALM_STATUS_INIT;
		alarmStatus.rxLos_C[i]		=	ALM_STATUS_INIT;
	}
	for ( i = 0 ; i < 4 ; i++ )
	{
		alarmStatus.sdiVideoStatusTx[i]	=	ALM_STATUS_INIT;
		alarmStatus.sdiVideoStatusRx[i]	=	ALM_STATUS_INIT;
		alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_INIT;
		alarmStatus.rtpFifoEmpty[i]		=	ALM_STATUS_INIT;
		alarmStatus.rtpFifoFull[i]		=	ALM_STATUS_INIT;

		alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_INIT;

		alarmBackupStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_INIT;
		alarmBackupStatus.ipMcMismatchRx[i]	=	ALM_STATUS_INIT;
		alarmTrapDetectData.sdiVideoFormatErrTx_mode[i]	=	0;
	}
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < 4 ; ii++ )
		{
			alarmStatus.udpFifoEmpty[i][ii]		=	ALM_STATUS_INIT;
			alarmStatus.udpFifoFull[i][ii]		=	ALM_STATUS_INIT;
			alarmStatus.packetCountError[i][ii]	=	ALM_STATUS_INIT;
			alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_INIT;

			alarmBackupStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_INIT;
			alarmTrapDetectData.sdiVideoCrcErrRx_mode[i][ii]	=	0;
		}
	}
	alarmStatus.refInFail		=	ALM_STATUS_INIT;
	alarmStatus.psStatusFail	=	ALM_STATUS_INIT;
	alarmStatus.tempFail		=	ALM_STATUS_INIT;
	alarmStatus.l1GLinkDown		=	ALM_STATUS_INIT;
	//-------------------------------------------------------------------------
	//--	疑似TRAP発生用テーブル初期化
	//-------------------------------------------------------------------------
	memset(&debug_alarm_pseudo, 0x00, sizeof(debug_alarm_pseudo));
	//-------------------------------------------------------------------------
	//--	ＴＥＳＴ用変数初期化
	//-------------------------------------------------------------------------
	saveTxMode[0]		=	0;	//	初期値ＨＤ
	saveTxMode[1]		=	0;	//	初期値ＨＤ
	saveTxMode[2]		=	0;	//	初期値ＨＤ
	saveTxMode[3]		=	2;	//	初期値ＨＤ
	//-------------------------------------------------------------------------
	//--	デバッグ用変数初期化
	//-------------------------------------------------------------------------
	test_3G_HD_count	=	0;
	test_1Min_index		=	0;
	for ( i = 0 ; i < 10 ; i++ )
	{
		test_1Min_count[i]	=	0;
	}
	memset((uint8_t *)&debugData,0,(uint32_t)sizeof(DEBUG_INF_STR));
	//-------------------------------------------------------------------------
	//--	ＬＥＤ全消灯
	//-------------------------------------------------------------------------
	tslp_tsk(500);
	for ( i = 0 ; i < 14 ; i++ )
	{
		g_led_control(i,0);
	}
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡに依存しないタスクスタート
	//-------------------------------------------------------------------------
	sta_tsk(ID_TASK_G_SERIAL,0);
	sta_tsk(ID_TASK_G_RS485,0);
	sta_tsk(ID_TASK_G_CYCLE,0);
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡ初期化完了待ち
	//-------------------------------------------------------------------------
	doneSig							=	DONE_102;
	commonData.fpgaDoneFailCount	=	0;
	while(1)
	{
		rpVal	=	GPIO_READ(RIN_RTPORT, RPIN2B, doneSig);
		if ((rpVal & doneSig) == doneSig)	break;
		tslp_tsk(1);
		if (commonData.fpgaDoneFailCount < FPGA_DONE_FAIL_WAIT)
		{
			commonData.fpgaDoneFailCount++;
			if (commonData.fpgaDoneFailCount >= FPGA_DONE_FAIL_WAIT)
			{
				if (!ALM_MASK_P_FPGA1_WAKEUP_FAIL)
				{
					alarmStatus.fpga1WakeUpFail_P	=	ALM_STATUS_DETECT;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＭＯＮＩ初期化
	//-------------------------------------------------------------------------
//	if (g_isBoard20X())
//	{
//		for ( i = 0 ; i < BUSNO_MAX ; i++ )
//		{
//			xiMoniSetUnitControl1(i,0x0000ffff);	//	リセットＯＮ
//		}
//	}
	//-------------------------------------------------------------------------
	//--	Ｋｉｎｔｅｘ　ＢＯＡＲＤ　ＲＥＶＶＩＳＩＯＮ読み込み
	//-------------------------------------------------------------------------
	tslp_tsk(150);		//	104を103と誤認する場合があったので待たせる
	boardRev	=	xiGetBoardRevision();
	for (i = 0 ; ; i++ )
	{
		if (boardRevVal[i] == 0x00000000)	break;
		if (boardRev == boardRevVal[i])
		{
			commonData.boardKind	=	i;
			break;
		}
	}
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡバージョン取得
	//-------------------------------------------------------------------------
	g_getFPGAVersion();
	//-------------------------------------------------------------------------
	//--	Ｋｉｎｔｅｘ　ＤＩＰ　ＳＷ読み込み
	//-------------------------------------------------------------------------
	dipSw	=	xiGetDipSw();
	commonData.dipSw5	=	(dipSw & 0x000000FF);
	commonData.dipSw10	=	((dipSw & 0x0000FF00) >> 8);
	if (!(commonData.dipSw5 & 0x80))	//	DipSw指示無効の場合、個別設定から上書き
	{
		commonData.dipSw5	=	(saveIndividualSet.data.dipSw5 & 0x7f);
		commonData.dipSw10	=	saveIndividualSet.data.dipSw10;
	}
	modelInf[0].dipSw[2]	=	commonData.dipSw5;
	//-------------------------------------------------------------------------
	//--	ボードインデックス（ロータリースイッチ）
	//-------------------------------------------------------------------------
	commonData.bdIndex	=	0;
	rpVal	=	GPIO_READ(RIN_GPIO, PIN2B, 0x10 | 0x08 | 0x04);
	if ((rpVal & 0x10))	commonData.bdIndex	+=	128;
	if ((rpVal & 0x08))	commonData.bdIndex	+=	64;
	if ((rpVal & 0x04))	commonData.bdIndex	+=	32;
	rpVal	=	GPIO_READ(RIN_GPIO, PIN1B, 0x08 | 0x04 | 0x01);
	if ((rpVal & 0x08))	commonData.bdIndex	+=	16;
	if ((rpVal & 0x04))	commonData.bdIndex	+=	8;
	if ((rpVal & 0x01))	commonData.bdIndex	+=	4;
	rpVal	=	GPIO_READ(RIN_GPIO, PIN0B, 0x80 | 0x40);
	if ((rpVal & 0x80))	commonData.bdIndex	+=	2;
	if ((rpVal & 0x40))	commonData.bdIndex	+=	1;
	if ((commonData.dipSw4 & 0x10))	commonData.bdIndex	+=	256;
	//-------------------------------------------------------------------------
	//--	ディップスイッチ値（ＳＷ６（ＦＰＧＡ２））取得
	//-------------------------------------------------------------------------
//	tslp_tsk(1000);
//	commonData.dipSw6	=	(uint8_t)((csz3GetStatus() & 0x0f00) >> 4);
//	if (!(commonData.dipSw6 & 0x80))	//	DipSw指示無効の場合、個別設定から上書き
//	{
//		commonData.dipSw6	=	(saveIndividualSet.data.dipSw6 & 0x7f);
//	}
//	modelInf[0].dipSw[3]	=	commonData.dipSw6;
	//-------------------------------------------------------------------------
	//--	ディップスイッチ値（ＳＷ７（ＦＰＧＡ３））取得
	//-------------------------------------------------------------------------
//	commonData.dipSw7	=	(uint8_t)(csz3GetDipSw7() & 0x00ff);
//	if (!(commonData.dipSw7 & 0x80))	//	DipSw指示無効の場合、個別設定から上書き
//	{
//		commonData.dipSw7	=	(saveIndividualSet.data.dipSw7 & 0x7f);
//	}
//	modelInf[0].dipSw[4]	=	commonData.dipSw7;
	//-------------------------------------------------------------------------
	//--	自ＣＰＵアドレス設定
	//-------------------------------------------------------------------------
	//	念のためA,B両方にセット（Aしか使わない）
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
//		gNET_ADR[ic].ipaddr			=	g_c4To32WithRSW_up1(saveCpuVideoLayer.data.cpuStartIpAddress);
		gNET_ADR[ic].ipaddr			=	g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.cpuStartIpAddress,ic,0);	//	for NHK
		gNET_ADR[ic].gateway		=	g_c4To32(saveCpuVideoLayer.data.cpuDefaultGateway);
		gNET_ADR[ic].mask			=	g_c4To32(saveCpuVideoLayer.data.cpuSubnetMask);
		for ( i = 0 ; i < 6 ; i++ )
		{
			gNET_DEV[ic].cfg.eth.mac[i]	=	savePhysicalLayerEeprom.data.cpuMacAddress[ic][i];
//			gNET_DEV[((ic == 0)?1:0)].cfg.eth.mac[i]	=	savePhysicalLayerEeprom.data.cpuMacAddress[ic][i];
		}
	}
	addressInf.ipA	=	gNET_ADR[0].ipaddr;
	addressInf.ipB	=	gNET_ADR[1].ipaddr;
	//-------------------------------------------------------------------------
	//--	ＳＮＭＰマネージャアドレス設定（アラームサーバー）
	//-------------------------------------------------------------------------
	snmp_mgrA.ipa	=	g_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[0]);
	snmp_mgrB.ipa	=	g_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[1]);

	// snmp
	IP4_ADDR(&snmp_ipA,	saveCpuVideoLayer.data.alarmServerIpAddress[0][0],
						saveCpuVideoLayer.data.alarmServerIpAddress[0][1],
	  					saveCpuVideoLayer.data.alarmServerIpAddress[0][2],
						saveCpuVideoLayer.data.alarmServerIpAddress[0][3]);
	IP4_ADDR(&snmp_ipB,	saveCpuVideoLayer.data.alarmServerIpAddress[1][0],
						saveCpuVideoLayer.data.alarmServerIpAddress[1][1],
	  					saveCpuVideoLayer.data.alarmServerIpAddress[1][2],
						saveCpuVideoLayer.data.alarmServerIpAddress[1][3]);
	//-------------------------------------------------------------------------
	//--	自コード情報設定
	//-------------------------------------------------------------------------
	//	局コード	マルチキャストアドレスの第２オクテットから
	myCode[0].stationCode	=	saveCpuVideoLayer.data.multicastStartIpAddress[0][1];
	myCode[0].modelCode		=	modelCode[commonData.boardKind];		//	機種コード
	myCode[0].machineCode	=	kikiCodeBase[commonData.boardKind];		//	機器コード
	myCode[0].machineNo[0]	=	((commonData.bdIndex & 0xff00) >> 8);	//	機器番号
	myCode[0].machineNo[1]	=	(commonData.bdIndex & 0xff);			//	機器番号
	myCode[1]	=	myCode[0];
	myCode[1].machineCode++;	//	B系は＋１
	//-------------------------------------------------------------------------
	//--	リアボード情報
	//-------------------------------------------------------------------------
	myNo	=	commonData.boardKind * 2 + 1;
	if (commonData.boardKind < BK_102A && commonData.tx_or_rx == HDIPRX)	myNo++;
	rearNo	=	0;
	if (commonData.RID == 0 && commonData.RA_OUT == 1)	rearNo	=	1;
	if (commonData.RID == 4 && commonData.RA_OUT == 1)	rearNo	=	2;
	if (commonData.RID == 1 && commonData.RA_OUT == 1)	rearNo	=	3;
	if (commonData.RID == 5 && commonData.RA_OUT == 1)	rearNo	=	4;
	if (commonData.RID == 0 && commonData.RA_OUT == 0)	rearNo	=	5;
	if (commonData.RID == 4 && commonData.RA_OUT == 0)	rearNo	=	6;
	//-------------------------------------------------------------------------
	//--	自機器情報設定
	//-------------------------------------------------------------------------
	modelInf[0].srcCodeNo	=	myCode[0];
	modelInf[0].tx_or_rx	=	commonData.tx_or_rx;
	memcpy(modelInf[0].setGVer,saveCpuVideoLayer.data.setVersion,4);
	memcpy(modelInf[0].setPVer,saveIndividualSet.data.setVersion,4);
	memcpy(modelInf[0].frontBdName,frontBdNames[commonData.boardKind],8);
	memcpy(modelInf[0].rearBdName,rearBdNames[rearNo],8);
	modelInf[0].rtSw		=	(commonData.bdIndex & 0xff);
	modelInf[1]				=	modelInf[0];
	modelInf[1].srcCodeNo	=	myCode[1];
	//-------------------------------------------------------------------------
	//--	ＬＷＩＰスタック初期化
	//-------------------------------------------------------------------------

	//	Creat and configure the EMAC intafece
	xipNet[0].xIpAddr.addr	=	g_c4To32((UB*)&gNET_ADR[0].ipaddr);
	xipNet[0].xNetMast.addr	=	g_c4To32((UB*)&gNET_ADR[0].mask);
	xipNet[0].xGateway.addr	=	g_c4To32((UB*)&gNET_ADR[0].gateway);

	xipNet[1].xIpAddr.addr	=	g_c4To32((UB*)&gNET_ADR[1].ipaddr);
	xipNet[1].xNetMast.addr	=	g_c4To32((UB*)&gNET_ADR[1].mask);
	xipNet[1].xGateway.addr	=	g_c4To32((UB*)&gNET_ADR[1].gateway);

	// 対通知先ＩＰアドレスＡ側
	dstNet[0].dst[0].addr	=	eth_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[0]);
	dstNet[0].dst[1].addr	=	eth_c4To32(saveCpuVideoLayer.data.settingServerIpAddress[0]);
	dstNet[0].dst[2].addr	=	eth_c4To32(saveCpuVideoLayer.data.managementServerIpAddress[0]);
	// 対通知先ＩＰアドレスＢ側
	dstNet[1].dst[0].addr	=	eth_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[1]);
	dstNet[1].dst[1].addr	=	eth_c4To32(saveCpuVideoLayer.data.settingServerIpAddress[1]);
	dstNet[1].dst[2].addr	=	eth_c4To32(saveCpuVideoLayer.data.managementServerIpAddress[1]);

	/** @note ＣＰＵ：スタートＩＰアドレスが設定されている時のみインターフェースを設定する */
	if (g_c4To32(saveCpuVideoLayer.data.cpuStartIpAddress) != 0) {
		// IPアドレス削除(不要？)
		interface_ipdel(CFG_IF_A_NAME, CFG_IF_A_DEFAULT_ADDRESS, CFG_IF_A_DEFAULT_MASK);
		interface_ipdel(CFG_IF_B_NAME, CFG_IF_B_DEFAULT_ADDRESS, CFG_IF_B_DEFAULT_MASK);

		// IPアドレス＋VLAN設定
		if (0 == vlanTagFlag) {
			interface_ipset(CFG_IF_A_NAME, xipNet[0]);
			interface_ipset(CFG_IF_B_NAME, xipNet[1]);

			// ルーティング設定
			interface_routingset(CFG_IF_A_NAME, dstNet[0]);
			interface_routingset(CFG_IF_B_NAME, dstNet[1]);
			// 初期ルーティング削除
			interface_routingdel(CFG_IF_A_NAME, xipNet[0]);
			interface_routingdel(CFG_IF_B_NAME, xipNet[1]);
			
		}
		else {
			interface_ipset_vlan(CFG_IF_A_NAME, xipNet[0], vlanTagId);
			interface_ipset_vlan(CFG_IF_B_NAME, xipNet[1], vlanTagId);

			// vlan用ルーティング設定
			interface_routingset_vlan(CFG_IF_A_NAME, dstNet[0], vlanTagId);
			interface_routingset_vlan(CFG_IF_B_NAME, dstNet[1], vlanTagId);

			// vlan初期ルーティング削除
			interface_routingdel_vlan(CFG_IF_A_NAME, xipNet[0], vlanTagId);
			interface_routingdel_vlan(CFG_IF_B_NAME, xipNet[1], vlanTagId);
		}
	}
	//-------------------------------------------------------------------------
	//--	Ｉ２Ｃ、ＣＨ１　初期化＆保存値書き込み
	//-------------------------------------------------------------------------
	g_writeDS100BRAll(1,1);
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡに依存するタスクスタート
	//-------------------------------------------------------------------------
	/** @note ＣＰＵ：スタートＩＰアドレスが設定されている時のみUDPスレッドを起動する */
	if (g_c4To32(saveCpuVideoLayer.data.cpuStartIpAddress) != 0) {
		//sta_tsk(ID_TASK_G_UDP_LOG,0);		// CPU使用率削減のため無効化
		/** @todo アップデートＵＤＰタスクは要検討
		sta_tsk(ID_TASK_G_UDP_UPD,0);
		*/
		sta_tsk(ID_TASK_G_UDP_CNT,0);
		sta_tsk(ID_TASK_G_UDP_MONI,0);
		sta_tsk(ID_TASK_G_SNMP,0);
		sta_tsk(ID_TASK_G_SNMP_SG,0);
	}

	sta_tsk(ID_TASK_G_1588,0);
	//sta_tsk(ID_TASK_G_LOG,0);				// CPU使用率削減のため無効化
	sta_tsk(ID_TASK_G_DEVICE,0);
	sta_tsk(ID_TASK_G_CYCLE_ALM,0);
	//-------------------------------------------------------------------------
	//--	ＳＷ６，７読み込み
	//-------------------------------------------------------------------------
	tslp_tsk(1000);
	g_dip_sw67_Read();
	//-------------------------------------------------------------------------
	//--	電源投入時の監視（Ｐ−ＢＩＴ）
	//-------------------------------------------------------------------------
	//--	パワーＯＮ　ｏｒ　ウォッチドッグ？
	//--			現状解らない
	//--	ＦＰＧＡコンフィグレーション正常？
	while (commonData.fpgaResetDone == 0)
	{
		tslp_tsk(10);
	}
	tslp_tsk(1000);
	//--	１０４起動不良対策
	mNo	=	xiGetDebug(0);
	if (mNo != MAGICNO_FPGA1)
	{
		GPIO_WRITE(RIN_RTPORT, RP0B, 0x80, 0);
		tslp_tsk(500);
		g_reset();	//	再起動！！
	}

	if (alarmStatus.fpga1WakeUpFail_P == 0 &&
		alarmStatus.fpga2WakeUpFail_P == 0 &&
		alarmStatus.fpga3WakeUpFail_P == 0)
	{
		//--	マジックナンバーチェック	ＦＰＧＡ１
		if (!ALM_MASK_P_FPGA1_MAGICNO_FAIL)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				mNo	=	xiGetDebug(i);
				if (mNo != MAGICNO_FPGA1)
				{
					alarmStatus.fpga1MagicNoFail_P	=	ALM_STATUS_DETECT;
				}
			}
		}
		//--	マジックナンバーチェック	ＦＰＧＡ１（ＭＯＮＩ）
		//--	ＭＯＮＩ側はチェックしない事になりました！
//		if (g_isBoard20X())
//		{
//			if (!ALM_MASK_P_FPGA1_MAGICNO_FAIL)
//			{
//				for ( i = 0 ; i < 4 ; i++ )
//				{
//					mNo	=	xiMoniGetDebug(i);
//					if (mNo != MAGICNO_FPGA1)
//					{
//						alarmStatus.fpga1MagicNoFail_P	=	ALM_STATUS_DETECT;
//					}
//				}
//			}
//		}
		//--	マジックナンバーチェック	ＦＰＧＡ２
		if (!ALM_MASK_P_FPGA2_MAGICNO_FAIL)
		{
			mNo	=	csz3GetTest();
			if ((mNo & 0x0000ffff) != ((MAGICNO_FPGA2_1 << 8) | MAGICNO_FPGA2_0))
			{
				alarmStatus.fpga2MagicNoFail_P	=	ALM_STATUS_DETECT;
			}
		}
		//--	ＴＸ＿ＦＡＵＬＴ　ＲＸ＿ＬＯＳ
		dmx1	=	xiGetDemuxControl1(0,0);
		if (!ALM_MASK_P_TX_FAULT_A_FAIL)
		{
			if ((dmx1 & 0x40000000))	alarmStatus.txFault_P[0]	=	ALM_STATUS_DETECT;
		}
		if (!ALM_MASK_P_RX_LOSS_A_FAIL)
		{
			if ((dmx1 & 0x80000000))	alarmStatus.rxLos_P[0]		=	ALM_STATUS_DETECT;
		}
		dmx2	=	xiGetDemuxControl1(1,0);
		if (!ALM_MASK_P_TX_FAULT_B_FAIL)
		{
			if ((dmx2 & 0x40000000))	alarmStatus.txFault_P[1]	=	ALM_STATUS_DETECT;
		}
		if (!ALM_MASK_P_RX_LOSS_B_FAIL)
		{
			if ((dmx2 & 0x80000000))	alarmStatus.rxLos_P[1]		=	ALM_STATUS_DETECT;
		}
		//--	リアボードとのマッチング検証
		if (!ALM_MASK_P_REAR_BOARD_MISSMATCH)
		{
			if (commonData.boardKind >= BK_103A)
			{
				alarmStatus.rearBoardMissMatch_P	=	0;	//	All OK
			}
			else
			{
				//alarmStatus.rearBoardMissMatch_P	=	rearBoardMatchTable[myNo][rearNo];
				alarmStatus.rearBoardMissMatch_P = 0;
				printf("myNo=%d, rearNo=%d\n", myNo, rearNo);
			}
		}
		//--	ディップスイッチミスマッチ
		if (!ALM_MASK_P_DIP_SWITCH_MISSMATCH)
		{
			//	FPGA1
//			if ((commonData.dipSw5	!=	(saveIndividualSet.data.dipSw5 & 0x7f)) ||
			if (((commonData.dipSw5 & 0xFB)	!=	(saveIndividualSet.data.dipSw5 & 0x7B)) ||
				(commonData.dipSw10	!=	saveIndividualSet.data.dipSw10) ||
			//	FPGA2
				(commonData.dipSw6	!=	(saveIndividualSet.data.dipSw6 & 0x7f)) ||
			//	FPGA3
				(commonData.dipSw7	!=	(saveIndividualSet.data.dipSw7 & 0x7f)))
			{
				alarmStatus.dipSwMissMatch_P	=	ALM_STATUS_DETECT;
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＭＯＮＩ初期化
	//-------------------------------------------------------------------------
//	if (g_isBoard20X())
//	{
//		while (1)	//	ＭＯＮＩ　ＦＰＧＡ初期化待ち
//		{
//			if (commonData.taskInit == 1)	break;
//			tslp_tsk(10);
//	  	}
//		for ( i = 0 ; i < BUSNO_MAX ; i++ )
//		{
//			xiMoniSetUnitControl1(i,0x00000000);	//	リセットＯＦＦ
//		}
//		for ( i = 0 ; i < BUSNO_MAX ; i++ )
//		{
//			xiMoniSetUnitControl1(i,0xffff0000);	//	動作開始
//		}
//	}
	if (g_isBoard20X())
	{
		for ( i = 0 ; i < BUSNO_MAX ; i++ )
		{
			xiMoniSetUnitControl1(i,0x00000000);
		}
		for ( i = 0 ; i < BUSNO_MAX ; i++ )
		{
			xiMoniSetUnitControl1(i,0x0000ffff);
		}
		for ( i = 0 ; i < BUSNO_MAX ; i++ )
		{
			if (!(commonData.dipSw4 & 0x04))	//	他走
			{
				xiMoniSetUnitControl1(i,0xfff70000);	//	Stream Video Out ON のみOFF
			}
			else								//	自走
			{
				xiMoniSetUnitControl1(i,0xffff0000);
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＣＰＵ　ＲＥＡＤＹ
	//-------------------------------------------------------------------------
	commonData.CPU_Ready	=	1;
	//-------------------------------------------------------------------------
	//--	自タスク消滅
	//-------------------------------------------------------------------------
	return;
}
//=============================================================================
//==	ＳＷ６、ＳＷ７読み込み
//=============================================================================
void	g_dip_sw67_Read(void)
{
	//-------------------------------------------------------------------------
	//--	ディップスイッチ値（ＳＷ６（ＦＰＧＡ２））取得
	//-------------------------------------------------------------------------
	commonData.dipSw6	=	(uint8_t)((csz3GetStatus() & 0x0f00) >> 4);
	if (!(commonData.dipSw6 & 0x80))	//	DipSw指示無効の場合、個別設定から上書き
	{
		commonData.dipSw6	=	(saveIndividualSet.data.dipSw6 & 0x7f);
	}
	modelInf[0].dipSw[3]	=	commonData.dipSw6;
	//-------------------------------------------------------------------------
	//--	ディップスイッチ値（ＳＷ７（ＦＰＧＡ３））取得
	//-------------------------------------------------------------------------
	commonData.dipSw7	=	(uint8_t)(csz3GetDipSw7() & 0x00ff);
	if (!(commonData.dipSw7 & 0x80))	//	DipSw指示無効の場合、個別設定から上書き
	{
		commonData.dipSw7	=	(saveIndividualSet.data.dipSw7 & 0x7f);
	}
	modelInf[0].dipSw[4]	=	commonData.dipSw7;
}

//=============================================================================
//==	FPGA memmapアドレス補正処理
//=============================================================================
void s_xiInitial(void) {
	int ret;

	/* mmap初期化設定 */
	m_mmap_fd			=	open("/dev/mem", O_RDWR | O_NDELAY);
	if (m_mmap_fd < 0) {
		LOG_E("open() failed: %d(%s)\n", errno, strerror(errno));
		return;
	}

	m_xi_map_p					=	(uint8_t*)mmap(NULL, CS_BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, m_mmap_fd, XI_TOP_ADR);
	if (MAP_FAILED == m_xi_map_p) {
		LOG_E("mmap() failed: %d(%s)\n", errno, strerror(errno));
		ret = close(m_mmap_fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return;
	}

	m_csz3_map_p					=	(uint8_t*)mmap(NULL, CS_BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, m_mmap_fd, CSZ3_TOP_ADR);
	if (MAP_FAILED == m_csz3_map_p) {
		LOG_E("mmap() failed: %d(%s)\n", errno, strerror(errno));
		ret = close(m_mmap_fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return;
	}

	/* ＦＰＧＡアドレスをｍｍａｐアドレスに補正処理追加 */
	m_xi_mmap	  			=	(volatile uint32_t)m_xi_map_p;
	m_csz3_mmap	  			=	(volatile uint32_t)m_csz3_map_p;

	XI_UNIT_CONTROL1			=	XI_UNIT_CONTROL1			+	XI_MMAP_ADDR;
	XI_UNIT_CONTROL2			=	XI_UNIT_CONTROL2			+	XI_MMAP_ADDR;
	XI_FPGA_VERSION				=	XI_FPGA_VERSION				+	XI_MMAP_ADDR;
	XI_DEBUG					=	XI_DEBUG					+	XI_MMAP_ADDR;
	XI_BOARD_REVISION			=	XI_BOARD_REVISION			+	XI_MMAP_ADDR;
	XI_TEMPERATURE				=	XI_TEMPERATURE				+	XI_MMAP_ADDR;
	XI_DIP_SW					=	XI_DIP_SW					+	XI_MMAP_ADDR;
	XI_RX_MONITOR1				=	XI_RX_MONITOR1				+	XI_MMAP_ADDR;
	XI_RX_MONITOR2				=	XI_RX_MONITOR2				+	XI_MMAP_ADDR;
	XI_RX_MONITOR3				=	XI_RX_MONITOR3				+	XI_MMAP_ADDR;
	XI_TX_CONTROL				=	XI_TX_CONTROL				+	XI_MMAP_ADDR;
	XI_VIDEO_STREAM_MONITOR		=	XI_VIDEO_STREAM_MONITOR		+	XI_MMAP_ADDR;
	XI_VIDEO_STREAM_MONITOR2	=	XI_VIDEO_STREAM_MONITOR2	+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL1		=	XI_HEADER_ADD_CONTROL1		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL2		=	XI_HEADER_ADD_CONTROL2		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL3		=	XI_HEADER_ADD_CONTROL3		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL4		=	XI_HEADER_ADD_CONTROL4		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL5		=	XI_HEADER_ADD_CONTROL5		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL6		=	XI_HEADER_ADD_CONTROL6		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL7		=	XI_HEADER_ADD_CONTROL7		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL8		=	XI_HEADER_ADD_CONTROL8		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL9		=	XI_HEADER_ADD_CONTROL9		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL10		=	XI_HEADER_ADD_CONTROL10		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL11		=	XI_HEADER_ADD_CONTROL11		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL12		=	XI_HEADER_ADD_CONTROL12		+	XI_MMAP_ADDR;
	XI_HEADER_ADD_CONTROL13		=	XI_HEADER_ADD_CONTROL13		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL1		=	XI_HEADER_DEL_CONTROL1		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL2		=	XI_HEADER_DEL_CONTROL2		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL3		=	XI_HEADER_DEL_CONTROL3		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL4		=	XI_HEADER_DEL_CONTROL4		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL5		=	XI_HEADER_DEL_CONTROL5		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL6		=	XI_HEADER_DEL_CONTROL6		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL7		=	XI_HEADER_DEL_CONTROL7		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL8		=	XI_HEADER_DEL_CONTROL8		+	XI_MMAP_ADDR;
	XI_TX_FEC_CALCULATOROL		=	XI_TX_FEC_CALCULATOROL		+	XI_MMAP_ADDR;
	XI_RX_FEC_CALCULATOROL		=	XI_RX_FEC_CALCULATOROL		+	XI_MMAP_ADDR;
	XI_HEADER_DEL_CONTROL9		=	XI_HEADER_DEL_CONTROL9		+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL1			=	XI_DEMUX_CONTROL1			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL2			=	XI_DEMUX_CONTROL2			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL3			=	XI_DEMUX_CONTROL3			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL4			=	XI_DEMUX_CONTROL4			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL5			=	XI_DEMUX_CONTROL5			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL6			=	XI_DEMUX_CONTROL6			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL7			=	XI_DEMUX_CONTROL7			+	XI_MMAP_ADDR;
	XI_DEMUX_CONTROL8			=	XI_DEMUX_CONTROL8			+	XI_MMAP_ADDR;
	XI_SW12_FLAG				=	XI_SW12_FLAG				+	XI_MMAP_ADDR;

	XI_IEEE1588_CONTROL1		=	XI_IEEE1588_CONTROL1		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL2		=	XI_IEEE1588_CONTROL2		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL3		=	XI_IEEE1588_CONTROL3		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL4		=	XI_IEEE1588_CONTROL4		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL5		=	XI_IEEE1588_CONTROL5		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL6		=	XI_IEEE1588_CONTROL6		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL7		=	XI_IEEE1588_CONTROL7		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL8		=	XI_IEEE1588_CONTROL8		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL9		=	XI_IEEE1588_CONTROL9		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL10		=	XI_IEEE1588_CONTROL10		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL11		=	XI_IEEE1588_CONTROL11		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL12		=	XI_IEEE1588_CONTROL12		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL13		=	XI_IEEE1588_CONTROL13		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL14		=	XI_IEEE1588_CONTROL14		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL15		=	XI_IEEE1588_CONTROL15		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL16		=	XI_IEEE1588_CONTROL16		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL17		=	XI_IEEE1588_CONTROL17		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL18		=	XI_IEEE1588_CONTROL18		+	XI_MMAP_ADDR;
	XI_IEEE1588_CONTROL19		=	XI_IEEE1588_CONTROL19		+	XI_MMAP_ADDR;

	XI_L3SWITCH_CONTROL1		=	XI_L3SWITCH_CONTROL1		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL2		=	XI_L3SWITCH_CONTROL2		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL3		=	XI_L3SWITCH_CONTROL3		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL4		=	XI_L3SWITCH_CONTROL4		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL5		=	XI_L3SWITCH_CONTROL5		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL6		=	XI_L3SWITCH_CONTROL6		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL7		=	XI_L3SWITCH_CONTROL7		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL8		=	XI_L3SWITCH_CONTROL8		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL9		=	XI_L3SWITCH_CONTROL9		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL10		=	XI_L3SWITCH_CONTROL10		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL11		=	XI_L3SWITCH_CONTROL11		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL12		=	XI_L3SWITCH_CONTROL12		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL13		=	XI_L3SWITCH_CONTROL13		+	XI_MMAP_ADDR;
	XI_L3SWITCH_CONTROL14		=	XI_L3SWITCH_CONTROL14		+	XI_MMAP_ADDR;

	XI_INFOR_REG				=	XI_INFOR_REG				+	XI_MMAP_ADDR;

	XI_SFP1_REG					=	XI_SFP1_REG					+	XI_MMAP_ADDR;
	XI_SFP2_REG					=	XI_SFP2_REG					+	XI_MMAP_ADDR;



	XI_MONI_UNIT_CONTROL1			=	XI_MONI_UNIT_CONTROL1			+	XI_MMAP_ADDR;
	XI_MONI_UNIT_CONTROL2			=	XI_MONI_UNIT_CONTROL2			+	XI_MMAP_ADDR;
	XI_MONI_DEBUG					=	XI_MONI_DEBUG					+	XI_MMAP_ADDR;
	XI_MONI_TICO_ENCODE1			=	XI_MONI_TICO_ENCODE1			+	XI_MMAP_ADDR;
	XI_MONI_TICO_ENCODE2			=	XI_MONI_TICO_ENCODE2			+	XI_MMAP_ADDR;
	XI_MONI_TICO_ENCODE3			=	XI_MONI_TICO_ENCODE3			+	XI_MMAP_ADDR;
	XI_MONI_TICO_ENCODE4			=	XI_MONI_TICO_ENCODE4			+	XI_MMAP_ADDR;
	XI_MONI_VIDEO_CONT				=	XI_MONI_VIDEO_CONT				+	XI_MMAP_ADDR;
	XI_MONI_AUDIO_PES1				=	XI_MONI_AUDIO_PES1				+	XI_MMAP_ADDR;
	XI_MONI_AUDIO_PES2				=	XI_MONI_AUDIO_PES2				+	XI_MMAP_ADDR;
	XI_MONI_AUDIO_STATUS1			=	XI_MONI_AUDIO_STATUS1			+	XI_MMAP_ADDR;
	XI_MONI_AUDIO_STATUS2			=	XI_MONI_AUDIO_STATUS2			+	XI_MMAP_ADDR;
	XI_MONI_ANC_CONT1				=	XI_MONI_ANC_CONT1				+	XI_MMAP_ADDR;
	XI_MONI_ANC_CONT2				=	XI_MONI_ANC_CONT2				+	XI_MMAP_ADDR;
	XI_MONI_ANC_PES1				=	XI_MONI_ANC_PES1				+	XI_MMAP_ADDR;
	XI_MONI_ANC_PES2				=	XI_MONI_ANC_PES2				+	XI_MMAP_ADDR;
	XI_MONI_ANC_DATA				=	XI_MONI_ANC_DATA				+	XI_MMAP_ADDR;
	XI_MONI_PID1					=	XI_MONI_PID1					+	XI_MMAP_ADDR;
	XI_MONI_PID2					=	XI_MONI_PID2					+	XI_MMAP_ADDR;
	XI_MONI_PID3					=	XI_MONI_PID3					+	XI_MMAP_ADDR;
	XI_MONI_PID4					=	XI_MONI_PID4					+	XI_MMAP_ADDR;
	XI_MONI_PID5					=	XI_MONI_PID5					+	XI_MMAP_ADDR;
	XI_MONI_SMPTE2022_1_2			=	XI_MONI_SMPTE2022_1_2			+	XI_MMAP_ADDR;
	XI_MONI_VIDEO_STREAM_MONITOR	=	XI_MONI_VIDEO_STREAM_MONITOR	+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL1		=	XI_MONI_HEADER_ADD_CONTROL1		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL2		=	XI_MONI_HEADER_ADD_CONTROL2		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL3		=	XI_MONI_HEADER_ADD_CONTROL3		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL4		=	XI_MONI_HEADER_ADD_CONTROL4		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL5		=	XI_MONI_HEADER_ADD_CONTROL5		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL6		=	XI_MONI_HEADER_ADD_CONTROL6		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL7		=	XI_MONI_HEADER_ADD_CONTROL7		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL8		=	XI_MONI_HEADER_ADD_CONTROL8		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL9		=	XI_MONI_HEADER_ADD_CONTROL9		+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL10	=	XI_MONI_HEADER_ADD_CONTROL10	+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL11	=	XI_MONI_HEADER_ADD_CONTROL11	+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL12	=	XI_MONI_HEADER_ADD_CONTROL12	+	XI_MMAP_ADDR;
	XI_MONI_HEADER_ADD_CONTROL13	=	XI_MONI_HEADER_ADD_CONTROL13	+	XI_MMAP_ADDR;



	CSZ3_STATUS0 				=	CSZ3_STATUS0				+	CSZ3_MMAP_ADDR;
	CSZ3_STATUS1 				=	CSZ3_STATUS1				+	CSZ3_MMAP_ADDR;
	CSZ3_REF_AD_VAL0 			=	CSZ3_REF_AD_VAL0			+	CSZ3_MMAP_ADDR;
	CSZ3_REF_AD_VAL1 			=	CSZ3_REF_AD_VAL1			+	CSZ3_MMAP_ADDR;
	CSZ3_FREE_AD_SET0 			=	CSZ3_FREE_AD_SET0			+	CSZ3_MMAP_ADDR;
	CSZ3_FREE_AD_SET1 			=	CSZ3_FREE_AD_SET1			+	CSZ3_MMAP_ADDR;
	CSZ3_SRG_SET0 				=	CSZ3_SRG_SET0				+	CSZ3_MMAP_ADDR;
	CSZ3_SRG_SET1 				=	CSZ3_SRG_SET1				+	CSZ3_MMAP_ADDR;
	CSZ3_WDT_ONOFF              =   CSZ3_WDT_ONOFF              +   CSZ3_MMAP_ADDR;
	CSZ3_SDI_HPHASE0 			=	CSZ3_SDI_HPHASE0			+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_HPHASE1 			=	CSZ3_SDI_HPHASE1			+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_VPHASE0 			=	CSZ3_SDI_VPHASE0			+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_VPHASE1 			=	CSZ3_SDI_VPHASE1			+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_VFMT 				=	CSZ3_SDI_VFMT				+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_TX_MODE 			=	CSZ3_SDI_TX_MODE			+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_ST0 				=	CSZ3_SDI_ST0				+	CSZ3_MMAP_ADDR;
	CSZ3_SDI_ST1 				=	CSZ3_SDI_ST1				+	CSZ3_MMAP_ADDR;
	CSZ3_TEST0 					=	CSZ3_TEST0					+	CSZ3_MMAP_ADDR;
	CSZ3_TEST1 					=	CSZ3_TEST1					+	CSZ3_MMAP_ADDR;
	CSZ3_TEST2 					=	CSZ3_TEST2					+	CSZ3_MMAP_ADDR;
	CSZ3_TEST3 					=	CSZ3_TEST3					+	CSZ3_MMAP_ADDR;
	CSZ3_VER0 					=	CSZ3_VER0					+	CSZ3_MMAP_ADDR;
	CSZ3_VER1 					=	CSZ3_VER1					+	CSZ3_MMAP_ADDR;
	CSZ3_VER2 					=	CSZ3_VER2					+	CSZ3_MMAP_ADDR;
	CSZ3_VER3 					=	CSZ3_VER3					+	CSZ3_MMAP_ADDR;
	CSZ3_VER4 					=	CSZ3_VER4					+	CSZ3_MMAP_ADDR;
	CSZ3_VER5 					=	CSZ3_VER5					+	CSZ3_MMAP_ADDR;
	CSZ3_VER6 					=	CSZ3_VER6					+	CSZ3_MMAP_ADDR;
	CSZ3_VER7 					=	CSZ3_VER7					+	CSZ3_MMAP_ADDR;
	CSZ3_DIPSW7 				=	CSZ3_DIPSW7					+	CSZ3_MMAP_ADDR;
	CSZ3_TEMPL 					=	CSZ3_TEMPL					+	CSZ3_MMAP_ADDR;
	CSZ3_TEMPH 					=	CSZ3_TEMPH					+	CSZ3_MMAP_ADDR;
	CSZ3_XP_OUT 				=	CSZ3_XP_OUT					+	CSZ3_MMAP_ADDR;
	CSZ3_XP_PSEL 				=	CSZ3_XP_PSEL				+	CSZ3_MMAP_ADDR;

	return;
}
