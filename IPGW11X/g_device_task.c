#define 	_GNU_SOURCE
//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	デバイスタスク
//=============================================================================
#include	"IPGW11X_cfg.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
/* V_INT監視時間(ns) */
#define V_INT_READ_INTERVAL (33367000L / 2)

static	uint16_t	enablePorts[2][6]	=	{
{
						UDP_PORT_R_A,	//	ＵＤＰポート（受）
						UDP_PORT_S_A,	//	ＵＤＰポート（送）
						161,			//	ＳＮＭＰ
						162,			//	ＳＮＭＰＴＲＡＰ
						UDP_MONI_PORT_R_A,	//	ＵＤＰモニター(DBG)ポート（受）
						UDP_MONI_PORT_S_A	//	ＵＤＰモニター(DBG)ポート（送）
},
{
						UDP_PORT_R_B,	//	ＵＤＰポート（受）
						UDP_PORT_S_B,	//	ＵＤＰポート（送）
						161,			//	ＳＮＭＰ
						162,			//	ＳＮＭＰＴＲＡＰ
						UDP_MONI_PORT_R_B,	//	ＵＤＰモニター(DBG)ポート（受）
						UDP_MONI_PORT_S_B	//	ＵＤＰモニター(DBG)ポート（送）
},
};
//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	void	iptxInitial(void);
static	void	iprxInitial(void);
static	void	ip20xInitial(void);
static 	void 	s_v_int_read_loop(void);
//=============================================================================
//==	デバイスタスク関数
//=============================================================================
void	g_device_task(void)
{
	LOG_THREAD();

	uint8_t		i,ic;
	uint32_t	viuc;
//	ER			ercd;
//	MSG_FORMAT	*mf;
	//-------------------------------------------------------------------------
	//--	ＣＳＺ３初期化
	//-------------------------------------------------------------------------
	csz3Initial();
	//-------------------------------------------------------------------------
	//--	ボードリセット
	//-------------------------------------------------------------------------
/*
	while (1)
	{
	  i = GPIO_READ(RIN_RTPORT, RP2B, 0x80);
	  if ((i & 0x80))	break;
	  tslp_tsk(1);
	}
*/
	if (commonData.boardKind >= BK_104A)	tslp_tsk(200);
	else									tslp_tsk(1000);
	if ((commonData.boardKind != BK_103A) && (commonData.boardKind != BK_104A) &&
		(commonData.boardKind != BK_205A) && (commonData.boardKind != BK_206A) &&
		(commonData.boardKind != BK_207A) && (commonData.boardKind != BK_208A))
	{
		GPIO_WRITE(RIN_GPIO, P7B, 0xBF, 1);
		GPIO_WRITE(RIN_RTPORT, RP0B, 0xFF, 0);
        tslp_tsk(10);
		/** @note 上で全ビットオフにしているので以下のみで良い */
		GPIO_WRITE(RIN_RTPORT, RP0B, 0x80, 1);
		GPIO_WRITE(RIN_GPIO, P7B, 0xFF, 1);
    }
    else
    {
		GPIO_WRITE(RIN_GPIO, P0B, 0x10, 1);       // FPGA_RESETn = 1
        tslp_tsk(10);
		GPIO_WRITE(RIN_GPIO, P0B, 0x10, 0);       // FPGA_RESETn = 0
        tslp_tsk(10);
    }
	commonData.fpgaResetDone	=	1;
	if (commonData.boardKind >= BK_104A)	tslp_tsk(500);
	else									tslp_tsk(2000);
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,UC1_INI_1);
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,UC1_INI_2);
	}
	//-------------------------------------------------------------------------
	//--	ＩＰＴＸ初期化
	//-------------------------------------------------------------------------
	if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	iptxInitial();
	//-------------------------------------------------------------------------
	//--	ＩＰＲＸ初期化
	//-------------------------------------------------------------------------
	if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	iprxInitial();
	//-------------------------------------------------------------------------
	//--	２０Ｘ初期化
	//-------------------------------------------------------------------------
	if (g_isBoard20X())	ip20xInitial();
	//-------------------------------------------------------------------------
	//--	ＵＤＰマスクセット
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( i = 0 ; i < 6 ; i++ )
		{
			xiSetDemuxControl7(ic,i,1,enablePorts[ic][i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	個別設定更新
	//-------------------------------------------------------------------------
	applyIndividualData();
	//-------------------------------------------------------------------------
	//--	Ｌ３ＳＷＩＴＣＨ初期化
	//-------------------------------------------------------------------------
	//	スイッチ
	xiSetL3SwitchControl1AB(saveSystemAB.data.unicast);
	//	ベンダー
	xiSetL3SwitchControl2(((saveCpuVideoLayer.data.vendorCode[0] << 16) |
							(saveCpuVideoLayer.data.vendorCode[1] << 8) |
							(saveCpuVideoLayer.data.vendorCode[2])));
	//	ＩＰアドレス＆マスク
	for ( i = 0 ; i < 2 ; i++ )
	{
		//	制御ＰＣ
		xiSetL3SwitchControl3(i,g_c4To32(saveCpuVideoLayer.data.managementServerIpAddress[i]));
		xiSetL3SwitchControl7(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpMask[i]));
		xiSetL3SwitchControl13(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpForSw[i]));
		xiSetL3SwitchControl11(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpMask[i]));
		//	アラームＰＣ
		xiSetL3SwitchControl4(i,g_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[i]));
		xiSetL3SwitchControl8(i,g_c4To32(saveCpuVideoLayer.data.almSvIpMask[i]));
		//	設定ＰＣ
		xiSetL3SwitchControl5(i,g_c4To32(saveCpuVideoLayer.data.settingServerIpAddress[i]));
		xiSetL3SwitchControl9(i,g_c4To32(saveCpuVideoLayer.data.setSvIpMask[i]));
		//	その他
		xiSetL3SwitchControl6(i,g_c4To32(saveCpuVideoLayer.data.etcSvIpAddress[i]));
		xiSetL3SwitchControl10(i,g_c4To32(saveCpuVideoLayer.data.etcSvIpMask[i]));
	}
	//-------------------------------------------------------------------------
	//--	設定更新＆更新フラグリセット
	//-------------------------------------------------------------------------
	switch (commonData.tx_or_rx)
	{
	case	HDIPTX:
		viuc	=	UC_INIT_VAL_TX;
		break;
	case	HDIPRX:
		viuc	=	UC_INIT_VAL_RX;
		break;
	case	HDIPTXRX:
		viuc	=	UC_INIT_VAL_TX | UC_INIT_VAL_RX;
		break;
	default:
		break;
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl2(i,viuc);
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl2(i,0x0000);
	}
	//-------------------------------------------------------------------------
	//--	マスク用ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		xiSetDemuxControl3(ic,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.cpuStartIpAddress,ic,0));	//	for NHK
//		xiSetDemuxControl3(ic,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.rxSrcStartIpAddress[ic],ic,0));	//	for NHK
	}
	//-------------------------------------------------------------------------
	//--	ＩＥＥＥ１５８８初期化
	//-------------------------------------------------------------------------
    xiIEEE1588InitSet(0, 0x00001234, 0x56789ABC, 0xDEF01234);
	xiIEEE1588OffsetSet(0, 0x00001234, 0x56789ABC, 0xDEF01234);
    xiIEEE1588AddValueSet(0, 0x00280000);
	xiIEEE1588IntervalTimeSet(0, 0x00000001, 0x00000000);
	xiIEEE1588IntervalInitTimeSet(0, 0x00000001, 0x00000000);
	//-------------------------------------------------------------------------
	//--	動作開始（ＩＣＦからの通信開始待ち）
	//-------------------------------------------------------------------------
	g_deviceStart();
	g_videoABSelect();
	if ((commonData.dipSw4 & 0x04))
	{
		commonData.startEnableFrom485	=	1;
		g_videoStart();
	}
	//-------------------------------------------------------------------------
	//--	初期化完了セット
	//-------------------------------------------------------------------------
	commonData.taskInit	=	1;
	//-------------------------------------------------------------------------
	//--	Ｖ＿ＩＮＴ読み込みループ
	//-------------------------------------------------------------------------
	s_v_int_read_loop();
	return;
}
//=============================================================================
//==	動作開始（ＵｎｉｔＣｏｎｔｒｏｌ１セット）
//=============================================================================
void	g_deviceStart(void)
{
	uint8_t		i;
	uint32_t	viuc,rstv;

	rstv	=	0;
	switch (commonData.tx_or_rx)
	{
	case	HDIPTX:
		viuc	=	UC_INIT_VAL_TX;
		break;
	case	HDIPRX:
		viuc	=	UC_INIT_VAL_RX;
		break;
	case	HDIPTXRX:
		viuc	=	UC_INIT_VAL_TX | UC_INIT_VAL_RX;
		break;
	default:
		break;
	}
	//--	！！！系統寄せは、ＵＮＩＴ　ＣＯＮＴＲＯＬではやらない！！
	//--	Ａ系運用
//	if (!saveSystemAB.data.verbose[0])
//	{
////		viuc	&=	(~0x0140);	//	(UDP/IP Header Del A) (UDP/IP Header Add A)=>OFF
//		viuc	&=	(~0x0100);	//	(UDP/IP Header Del A)=>OFF
//		rstv	|=	0x00000100;
//	}
//	//--	Ｂ系運用
//	if (!saveSystemAB.data.verbose[1])
//	{
////		viuc	&=	(~0x0280);	//	(UDP/IP Header Del B) (UDP/IP Header Add B)=>OFF
//		viuc	&=	(~0x0200);	//	(UDP/IP Header Del B)=>OFF
//		rstv	|=	0x00000200;
//	}
	//--	Stream Video Out マスク
	if (!(commonData.dipSw4 & 0x04))	//	他走のみヴィデオ出力開始は後回し
	{
//		viuc	&=	(~UC_INIT_ENB);
//		rstv	&=	(~UC_INIT_ENB);
		viuc	&=	(~(UC_INIT_ENB | UC_INIT_VAL_TX_EX));
		rstv	&=	(~(UC_INIT_ENB | UC_INIT_VAL_TX_EX));
	}
	//--	出力
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,(rstv | (viuc << 16)));
	}
}
//=============================================================================
//==	動作開始（ＵｎｉｔＣｏｎｔｒｏｌ１セット）（ＶＩＤＥＯ編）
//=============================================================================
void	g_videoStart(void)
{
	int		i;
    
	//--	出力
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1OutOnOff(i,1);
		if (g_isBoard20X())
		{
			xiMoniSetUnitControl1(i,0xffff0000);	//	Stream Video Out ON を含め ALL ON
		}
	}
	commonData.unitStartFlag	=	1;
}
//=============================================================================
//==	系統寄せ関数
//=============================================================================
void	g_videoABSelect(void)
{
	int		i,ii;
	//--	出力
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < BUSNO_MAX ; ii++ )
		{
			xiSetHeaderDelControl1ReciveOff(i,ii,saveSystemAB.data.verbose[i]);
		}
	}
}
//=============================================================================
//==	ＩＰＴＸ初期化関数
//=============================================================================
static	void	iptxInitial(void)
{
	uint8_t		i,ic,is;
	uint8_t		tmac[6];
	//-------------------------------------------------------------------------
	//--	Ｔｒｉｐｌｅ−Ｒａｔｅ　ＳＤＩ　ＲＸ　ＯＮ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,UC1_INI_1);
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＭＡＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl23(ic,is,i,savePhysicalLayerEeprom.data.videoMacAddress[ic],1,
					saveCpuVideoLayer.data.Extend[ic][i],
//					saveCpuVideoLayer.data.VLANFlag[ic][i],
					saveIndividualSet.data.vlan[i],	//	個別設定を使用する
//					saveCpuVideoLayer.data.TOSFlag[ic][i]);
					saveIndividualSet.data.tos[i]);	//	個別設定を使用する
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＭＡＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
//				g_multiCastIpToMac(g_c4To32WithRSW_up4(saveCpuVideoLayer.data.multicastStartIpAddress[ic],i),tmac);
//				g_multiCastIpToMac(g_c4To32WithIndex_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[ic],i),tmac);		//	for NHK
				g_multiCastIpToMac(g_c4To32WithRSW_MC_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[ic],ic,i),tmac);	//	for NHK
				xiSetHeaderAddControl45(ic,is,i,tmac);
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
//				xiSetHeaderAddControl6(ic,is,i,g_c4To32WithRSW_up1(saveCpuVideoLayer.data.txSrcStartIpAddress[ic]));
				xiSetHeaderAddControl6(ic,is,i,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.txSrcStartIpAddress[ic],ic,1));	//	for NHK
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
//				xiSetHeaderAddControl7(ic,is,i,g_c4To32WithRSW_up4(saveCpuVideoLayer.data.multicastStartIpAddress[ic],i));
				xiSetHeaderAddControl7(ic,is,i,g_c4To32WithRSW_MC_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[ic],ic,i));	//	for NHK
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＰＯＲＴ、ＤＥＳＴＩＮＡＴＩＯＮ＿ＰＯＲＴ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl8(ic,is,i,
					g_c2To16(saveCpuVideoLayer.data.txSrcPortNo[ic * 8 + is * 4 + i]),
					g_c2To16(saveCpuVideoLayer.data.txDstPortNo[ic]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＳＲＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl9(ic,is,i,g_c4To32(saveCpuVideoLayer.data.txSsrc[i]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＴＴＬ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl4TTL(ic,is,i,saveCpuVideoLayer.data.txTtl[i],saveCpuVideoLayer.data.moni.txTtl[i]);
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＴＯＳ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl4TOS(ic,is,i,saveCpuVideoLayer.data.TOS[ic][i],saveCpuVideoLayer.data.moni.TOS[ic][i]);
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＦＥＣ　Ｌ、ＦＥＣ　Ｄ、Ｏｐｔｉｏｎｓ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetTxFecCalculatorol(i,
			g_c2To16(saveCpuVideoLayer.data.txFecL[i]),
			saveCpuVideoLayer.data.txFecD[i],
			saveCpuVideoLayer.data.txOptions[i]);
	}
	//-------------------------------------------------------------------------
	//--	ＶＬＡＮ　ＴＡＧ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetHeaderAddControl10(ic,is,i,g_c4To32(saveCpuVideoLayer.data.VLANTag[ic * 4 + i]));
			}
		}
	}
}
//=============================================================================
//==	ＩＰＲＸ初期化関数
//=============================================================================
static	void	iprxInitial(void)
{
	uint8_t		i,ic,is;
	uint32_t	dsip;
	ER			eret;
	//-------------------------------------------------------------------------
	//--	ＭＡＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		xiSetDemuxControl12(ic,savePhysicalLayerEeprom.data.videoMacAddress[ic]);
	}
	//-------------------------------------------------------------------------
	//--	ＩＰ（マスク用に流用）したので、ここでは設定しない。
	//-------------------------------------------------------------------------
//	for ( ic = 0 ; ic < 2 ; ic++ )
//	{
//		xiSetDemuxControl3(ic,g_c4To32WithRSW_up1(saveCpuVideoLayer.data.rxSrcStartIpAddress[ic]));
//		xiSetDemuxControl3(ic,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.rxSrcStartIpAddress[ic],ic,1));	//	for NHK
//	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＰＯＲＴ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetDemuxControl4(ic,is,i,g_c2To16(saveCpuVideoLayer.data.txDstPortNo[ic * 8 + is * 4 + i]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
//				dsip	=	g_c4To32WithRSW_up4(saveCpuVideoLayer.data.multicastStartIpAddress[ic],i);
				dsip	=	g_c4To32WithIndex_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[ic],i);	//	for NHK
				xiSetDemuxControl5(ic,is,i,dsip);
				/** @note マルチキャストへの参加はネットワークドライバが行う */
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＳＲＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiSetDemuxControl6(ic,is,i,g_c4To32(saveCpuVideoLayer.data.txSsrc[i]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＦＥＣ　Ｌ、ＦＥＣ　Ｄ、Ｏｐｔｉｏｎｓ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetRxFecCalculatorol(i,
			g_c2To16(saveCpuVideoLayer.data.rxFecL[i]),
			saveCpuVideoLayer.data.rxFecD[i],
			saveCpuVideoLayer.data.rxOptions[i]);
	}
}
//=============================================================================
//==	２０Ｘ初期化関数
//=============================================================================
//--	TICO ENCODE1
#define	TICO_E_WIDTH		960
#define	TICO_E_HEIGHT		540
//--	TICO ENCODE2
#define	TICO_E_LVL_WEIGHTS	0x80000000
#define	TICO_E_PLAY_MODE	0x10000000
#define	TICO_E_VID_DEPTH10	0x0A000000
#define	TICO_E_VID_DEPTH8	0x08000000
#define	TICO_E_NBR			0x00000000
#define	TOCO_E_VLD			0x00000080
#define	TICO_E_HLVLS		0x00000050
#define	TICO_E_MODE			0x00000004
#define	TICO_E_COMP			0x00000003
//--	AUDIO PES2
#define	AUDIO_PES2_VAL		0x00130000
//--	ANC CONT1
#define	ANC_CONT1_VAL		0x0813080B
//--	ANC CONT2
#define	ANC_CONT2_VAL		0x0A460A3E
//--	PID1
#define	PID1_VAL			0x30012100
//--	PID2
#define	PID2_VAL			0x00003011
//--	PID3
#define	PID3_VAL			0x31013100
//--	PID4
#define	PID4_VAL			0x3FD13FD0
//--	PID5
#define	PID5_VAL			0x3FD33FD2
//--	SMPTE2022_1_2
#define	SMPTE2022_1_2_VAL	0x00004007

static	void	ip20xInitial(void)
{
	uint8_t		i,ic,is;
	uint8_t		vMacAddress[6];
	uint8_t		tmac[6];
	uint8_t		mcIpAddress[4];
	uint32_t	data;
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ１
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetTicoEncode1(i,TICO_E_HEIGHT,TICO_E_WIDTH);
	}
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ２
	//-------------------------------------------------------------------------
	data	=	0;
	data	|=	TICO_E_VID_DEPTH10;
	data	|=	TICO_E_NBR;
//	data	|=	TOCO_E_VLD;		UNIT CONTROL1:Triple-Rate SDI RX ONに連動！！ここではOFF
	data	|=	TICO_E_HLVLS;
	data	|=	TICO_E_MODE;
	data	|=	TICO_E_COMP;
	data	|=	TICO_E_PLAY_MODE;
	if ((commonData.dipSw10 & 0x20))	data	|=	0x08000000;	//	8bits
	else								data	|=	0x0A000000;	//	10bits
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetTicoEncode2(i,data);
	}
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ３
	//-------------------------------------------------------------------------
	switch ((commonData.dipSw10 & 0xC0))
	{
	case	0x00:	//	1/6
		data	=	216000;
		break;
	case	0x80:	//	1/7
		data	=	185142;
		break;
	case	0x40:	//	1/8
		data	=	162000;
		break;
	case	0xC0:	//	1/5
		data	=	259200;
		break;
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetTicoEncode3(i,data);
	}
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ４（未使用）
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetTicoEncode4(i,0);
	}
	//-------------------------------------------------------------------------
	//--	ＶＩＤＥＯ　ＣＯＮＴ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetVideoCont(i,0);
	}
	//-------------------------------------------------------------------------
	//--	ＡＵＤＩＯ　ＰＥＳ２
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetAudioPes2(i,AUDIO_PES2_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＡＮＣ　ＣＯＮＴ１
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetAncCont1(i,ANC_CONT1_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＡＮＣ　ＣＯＮＴ２
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetAncCont2(i,ANC_CONT2_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ１
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetPid1(i,PID1_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ２
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetPid2(i,PID2_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ３
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetPid3(i,PID3_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ４
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetPid4(i,PID4_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ５
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetPid5(i,PID5_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＳＭＰＴＥ２０２２＿１＿２
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiMoniSetSmpte2022_1_2(i,SMPTE2022_1_2_VAL);
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＭＡＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		memcpy(vMacAddress,savePhysicalLayerEeprom.data.videoMacAddress[ic],6);
//		vMacAddress[5]	+=	0x02;
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				//-------------------------------------------------------------
				//--	ＭＡＣ　ＡＤＤＲＥＳＳ
				//-------------------------------------------------------------
				xiMoniSetHeaderAddControl23(ic,is,i,savePhysicalLayerEeprom.data.videoMacAddress[ic],1,
					saveCpuVideoLayer.data.Extend[ic][i],
//					saveCpuVideoLayer.data.VLANFlag[ic][i],
//					saveIndividualSet.data.moni_vlan[i],	//	個別設定を使用する
					saveIndividualSet.data.moniVlan[i],		//	個別設定を使用する
//					saveCpuVideoLayer.data.TOSFlag[ic][i]);
					saveIndividualSet.data.moni_tos[i]);	//	個別設定を使用する
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＭＡＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		memcpy(mcIpAddress,saveCpuVideoLayer.data.multicastStartIpAddress[ic],4);
		mcIpAddress[2]	+=	20;
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				//-------------------------------------------------------------
				//--	ＭＡＣ　ＡＤＤＲＥＳＳ
				//-------------------------------------------------------------
//				g_multiCastIpToMac(g_c4To32WithIndex_NHK(mcIpAddress,i),tmac);		//	for NHK
				g_multiCastIpToMac((g_c4To32WithRSW_MC_NHK(mcIpAddress,ic,i) + 0x1400),tmac);		//	for NHK
				xiMoniSetHeaderAddControl45(ic,is,i,tmac);
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiMoniSetHeaderAddControl6(ic,is,i,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.txSrcStartIpAddress[ic],ic,1));	//	for NHK
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＤＥＳＴＩＮＡＴＩＯＮ＿ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		memcpy(mcIpAddress,saveCpuVideoLayer.data.multicastStartIpAddress[ic],4);
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiMoniSetHeaderAddControl7(ic,is,i,(g_c4To32WithRSW_MC_NHK(mcIpAddress,ic,i) + 0x1400));	//	for NHK No3 octet +20
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＯＵＲＣＥ＿ＰＯＲＴ、ＤＥＳＴＩＮＡＴＩＯＮ＿ＰＯＲＴ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiMoniSetHeaderAddControl8(ic,is,i,
					g_c2To16(saveCpuVideoLayer.data.moni.txSrcPortNo[ic * 8 + is * 4 + i]),
					g_c2To16(saveCpuVideoLayer.data.moni.txDstPortNo[ic]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＳＲＣ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiMoniSetHeaderAddControl9(ic,is,i,
				g_c4To32(saveCpuVideoLayer.data.moni.txSsrc[i]));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＶＬＡＮ　ＴＡＧ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( is = 0 ; is < 2 ; is++ )
		{
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				xiMoniSetHeaderAddControl10(ic,is,i,g_c4To32(saveCpuVideoLayer.data.moni.VLANTag[ic * 4 + i]));
			}
		}
	}
}
//=============================================================================
//==	ＦＰＧＡ　再スタート関数
//=============================================================================
void g_fpgaRestart()
{
	uint8_t		i,ic;
	uint32_t	viuc;
//	ER			ercd;
	if ((commonData.boardKind != BK_103A) && (commonData.boardKind != BK_104A) &&
		(commonData.boardKind != BK_205A) && (commonData.boardKind != BK_206A) &&
		(commonData.boardKind != BK_207A) && (commonData.boardKind != BK_208A))
	{
		GPIO_WRITE(RIN_GPIO, P7B, 0xBF, 1);
		GPIO_WRITE(RIN_RTPORT, RP0B, 0xFF, 0);
        tslp_tsk(10);
		/** @note 上で全ビットオフにしているので以下のみで良い */
		GPIO_WRITE(RIN_RTPORT, RP0B, 0x80, 1);
		GPIO_WRITE(RIN_GPIO, P7B, 0xFF, 1);
    }
    else
    {
		GPIO_WRITE(RIN_GPIO, P0B, 0x10, 1);       // FPGA_RESETn = 1
        tslp_tsk(10);
		GPIO_WRITE(RIN_GPIO, P0B, 0x10, 0);       // FPGA_RESETn = 0
        tslp_tsk(10);
    }
	tslp_tsk(2000);
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,UC1_INI_1);
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl1(i,UC1_INI_2);
	}
	//-------------------------------------------------------------------------
	//--	ＩＰＴＸ初期化
	//-------------------------------------------------------------------------
	if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	iptxInitial();
	//-------------------------------------------------------------------------
	//--	ＩＰＲＸ初期化
	//-------------------------------------------------------------------------
	if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	iprxInitial();
	//-------------------------------------------------------------------------
	//--	ＵＤＰマスクセット
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		for ( i = 0 ; i < 6 ; i++ )
		{
			xiSetDemuxControl7(ic,i,1,enablePorts[ic][i]);
		}
	}
	//-------------------------------------------------------------------------
	//--	設定更新＆更新フラグリセット
	//-------------------------------------------------------------------------
	switch (commonData.tx_or_rx)
	{
	case	HDIPTX:
		viuc	=	UC_INIT_VAL_TX;
		break;
	case	HDIPRX:
		viuc	=	UC_INIT_VAL_RX;
		break;
	case	HDIPTXRX:
		viuc	=	UC_INIT_VAL_TX | UC_INIT_VAL_RX;
		break;
	default:
		break;
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl2(i,viuc);
	}
	for ( i = 0 ; i < BUSNO_MAX ; i++ )
	{
		xiSetUnitControl2(i,0x0000);
	}
	//-------------------------------------------------------------------------
	//--	マスク用ＩＰ
	//-------------------------------------------------------------------------
	for ( ic = 0 ; ic < 2 ; ic++ )
	{
		xiSetDemuxControl3(ic,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.cpuStartIpAddress,ic,0));	//	for NHK
//		xiSetDemuxControl3(ic,g_c4To32WithRSW_NHK(saveCpuVideoLayer.data.rxSrcStartIpAddress[ic],ic,0));	//	for NHK
	}
	//-------------------------------------------------------------------------
	//--	ＩＥＥＥ１５８８初期化
	//-------------------------------------------------------------------------
#if 0
    xiIEEE1588InitSet(0, 0x00001234, 0x56789ABC, 0xDEF01234);
	xiIEEE1588OffsetSet(0, 0x00001234, 0x56789ABC, 0xDEF01234);
    xiIEEE1588AddValueSet(0, 0x00280000);
	xiIEEE1588IntervalTimeSet(0, 0x00000001, 0x00000000);
	xiIEEE1588IntervalInitTimeSet(0, 0x00000001, 0x00000000);
#endif
	//-------------------------------------------------------------------------
	//--	Ｌ３ＳＷＩＴＣＨ初期化
	//-------------------------------------------------------------------------
	//	スイッチ
	xiSetL3SwitchControl1AB(saveSystemAB.data.unicast);
	//	ベンダー
	xiSetL3SwitchControl2(((saveCpuVideoLayer.data.vendorCode[0] << 16) |
							(saveCpuVideoLayer.data.vendorCode[1] << 8) |
							(saveCpuVideoLayer.data.vendorCode[2])));
	//	ＩＰアドレス＆マスク
	for ( i = 0 ; i < 2 ; i++ )
	{
		//	制御ＰＣ
		xiSetL3SwitchControl3(i,g_c4To32(saveCpuVideoLayer.data.managementServerIpAddress[i]));
		xiSetL3SwitchControl7(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpMask[i]));
		xiSetL3SwitchControl13(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpForSw[i]));
		xiSetL3SwitchControl11(i,g_c4To32(saveCpuVideoLayer.data.cntSvIpMask[i]));
		//	アラームＰＣ
		xiSetL3SwitchControl4(i,g_c4To32(saveCpuVideoLayer.data.alarmServerIpAddress[i]));
		xiSetL3SwitchControl8(i,g_c4To32(saveCpuVideoLayer.data.almSvIpMask[i]));
		//	設定ＰＣ
		xiSetL3SwitchControl5(i,g_c4To32(saveCpuVideoLayer.data.settingServerIpAddress[i]));
		xiSetL3SwitchControl9(i,g_c4To32(saveCpuVideoLayer.data.setSvIpMask[i]));
		//	その他
		xiSetL3SwitchControl6(i,g_c4To32(saveCpuVideoLayer.data.etcSvIpAddress[i]));
		xiSetL3SwitchControl10(i,g_c4To32(saveCpuVideoLayer.data.etcSvIpMask[i]));
	}
}
//=============================================================================
//==	Ｖ＿ＩＮＴ読み込みループ
//=============================================================================
static void s_v_int_read_loop(void) {
	const unsigned int offset = GPIO_OFFSET_V_INT;
	struct timespec request, remain;
	struct timespec start, end;
	int	onoff;

	/* V_INT_READ_INTERVAL 周期 */
	while (1) {
		/* 処理開始時間 */
		clock_gettime(CLOCK_MONOTONIC, &start);
		
		/* デフォルト値 */
		onoff = 0;
		/* V-INT読み込み処理 */
		com_gpio_read(CFG_APL_NAME, offset, &g_gpiod_lines[offset], &onoff);
		/* 値が1の場合にV_INT処理を呼び出す */
		if (1 == onoff) {
			g_v_int();
		}

		/* 処理終了時間 */
		clock_gettime(CLOCK_MONOTONIC, &end);

		/* スリープ時間算出 */
		if ((end.tv_sec - start.tv_sec == 0L) && (end.tv_nsec - start.tv_nsec < V_INT_READ_INTERVAL)) {
			request.tv_nsec = V_INT_READ_INTERVAL - (end.tv_nsec - start.tv_nsec);	
		}
		else {
			/** @note 常にここに来るようならスリープ処理自体不要？ */
			// continue;
			request.tv_nsec = 0L;
		}
		request.tv_sec = 0;

		/* とりあえず第二引数は指定しておくが、割り込みなどが発生しても再スリープはしない */
		nanosleep(&request, &remain);
	}

	return;
}