//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	共通関数ファイル
//=============================================================================
#include	"IPGW11X_cfg.h"
#include	"IPGW11X_gpio.h"
#include	"run_cmd.h"
//=============================================================================
//==	共通変数域
//=============================================================================
#define		TXRX_TEMP_DET_VAL		850		//	異常検出温度（８５℃）
uint16_t	TXRX_TEMP_DET;					//	異常検出温度

MSG_BSC_CODE_NO						myCode[2];					//	自コード情報 [0]A [1]B
SAVE_PHYSICAL_LAYER_EEPROM_STR		savePhysicalLayerEeprom;	//	後部基板ＥＥＰＲＯＭ　ＭＡＣアドレス情報
SAVE_CPU_VIDEO_LAYER_STR			saveCpuVideoLayer;			//	ＣＰＵ／ＶＩＤＥＯ情報
SAVE_ALARM_MASK_STR					saveAlarmMask;				//	アラームマスク情報
SAVE_INDIVIDUAL_SET_STR				saveIndividualSet;			//	個別設定
SAVE_INDIVIDUAL_SET_STR				saveIndividualSetUndo;		//	個別設定（Ｕｎｄｏ用）
SAVE_DS100BR_REG_STR				saveDS100BR;				//	ＤＳ１００ＢＲレジスタ情報
SAVE_CSZ3_REG_STR					saveCSZ3;					//	ＣＳＺ３保存情報
SAVE_SYSTEM_AB						saveSystemAB;				//	系統情報保存情報
SAVE_SDIIPMISMATCH_STR				saveSdiIpMisInf;			//	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報
FREERUN_CAL_STR						autoADCVolData;				//	フリーラン周波数ＡＤＣ　ＶＯＬ記録データバッファ
uint8_t								DS100BR_RegisterAddress[11]	=	//	ＤＳ１００ＢＲレジスタアドレス
{	0x06,	0x07,	0x08,	0x0F,	0x10,	0x11,	0x16,	0x17,	0x18,	0x25,	0x2D	};
uint8_t								DS100BR_EQ_LEVEL_Values[16]	=	//	ＤＳ１００ＢＲ　ＥＱ設定値テーブル
{	0x00,	0x01,	0x02,	0x03,	0x07,	0x15,	0x0B,	0x0F,	0x55,	0x1F,	0x2F,
	0x3F,	0xAA,	0x7F,	0xBF,	0xFF	};
AUTO_EQUALIZER_CONF_STR				autoEQConfData;				//	ＥＱ　ＧＡＩＮ自動調整データバッファ
UH						net_dev_nos[2]	=	{	NET_DEVICE_NUM_A,	NET_DEVICE_NUM_B	};
COMMON_STR				commonData;						//	共通変数
MSG_BSC_MCH_DATA		modelInf[2]	=	{				//	機種情報  [0]A [1]B
{
{	0x00,				//	局コード
	0x00,				//	機器コード
	0x00,				//	機器コード
{	0x00,0x00		},	//	機器番号（ロータリースイッチ）
					},
{	0,	0,	0,	0	},	//	ソフトウェアバージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ１バージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ２バージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ３バージョン
{	0,	0,	0,	0	},	//	基本設定バージョン
{	0,	0,	0,	0	},	//	個別設定バージョン
	0x00,				//	ＴＸ　ＲＸ
{	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00	},	//	フロント型名
{	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00	},	//	リア型名
	0x00,										//	ロータリースイッチ
{	0x00,0x00,0x00,0x00,0x00	},				//	ディップスイッチ
{	0x00,0x00,0x00,0x00	},						//	３Ｇ−ＨＤ
{	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	}	//	ＡＵＸ
},
{
{	0x00,				//	局コード
	0x00,				//	機器コード
	0x00,				//	機器コード
{	0x00,0x00		},	//	機器番号（ロータリースイッチ）
					},
{	0,	0,	0,	0	},	//	ソフトウェアバージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ１バージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ２バージョン
{	0,	0,	0,	0	},	//	ＦＰＧＡ３バージョン
{	0,	0,	0,	0	},	//	基本設定バージョン
{	0,	0,	0,	0	},	//	個別設定バージョン
	0x00,				//	ＴＸ　ＲＸ
{	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00	},	//	フロント型名
{	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00	},	//	リア型名
	0x00,										//	ロータリースイッチ
{	0x00,0x00,0x00,0x00,0x00	},				//	ディップスイッチ
{	0x00,0x00,0x00,0x00	},						//	３Ｇ−ＨＤ
{	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0	}	//	ＡＵＸ
}
};
MSG_IPG_ADDRESS_INF		addressInf;						//	アドレス情報
ALARM_STR				alarmStatus;					//	アラーム状態管理
ALARM_BACKUP_STR		alarmBackupStatus;				//	アラーム状態管理（周期監視用）
ALA_TRAP_DETECT_STR		alarmTrapDetectData;			//	アラームトラップ検出用情報
DEBUG_INF_STR			debugData;						//	デバッグデータ
uint32_t				saveTxMode[BUSNO_MAX];			//	テストパターンＳＤＩモード
//--	デバッグ用
uint32_t				test_3G_HD_count;				//	ＴＥＳＴ　３Ｇ　ＨＤ　自動切り替え用
uint32_t				test_1Min_count[10];			//	ＴＥＳＴ　１分周期のカウンタ
uint32_t				test_1Min_index;				//	ＴＥＳＴ　１分周期のカウンタの添え字
uint8_t					snmpTrapMoniBuf[SNMP_TRAP_MONI_MAX][128];//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用
uint32_t				snmpTrapMoniWptr	=	0;		//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用
uint32_t				snmpTrapMoniRptr	=	0;		//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用

struct	ip_addr 		snmp_ipA;						//	snmp IPAddress A
struct	ip_addr 		snmp_ipB;						//	snmp IPAddress B

//	ファームウェアアップデート用
FW_UPDATE_BUF_STR		fwUpdateBuf;

//　疑似TRAP発生有無（テスト用）
BEBUG_ALARM_PSEUDO_STR	debug_alarm_pseudo;
//=============================================================================
//==	２０Ｘ判定
//=============================================================================
BOOL	g_isBoard20X(void)
{
	BOOL	ret	=	FALSE;
	if (commonData.boardKind == BK_205A ||
		commonData.boardKind == BK_206A ||
		commonData.boardKind == BK_207A ||
		commonData.boardKind == BK_208A)
	{
		ret	=	TRUE;
	}
	return(ret);
}
//=============================================================================
//==	ＢＣ対応判定
//=============================================================================
BOOL	g_isBoardBC(void)
{
	BOOL	ret	=	FALSE;
	if (commonData.boardKind == BK_102A ||
		commonData.boardKind == BK_205A ||
		commonData.boardKind == BK_206A ||
		commonData.boardKind == BK_207A ||
		commonData.boardKind == BK_208A)
	{
		ret	=	TRUE;
	}
	return(ret);
}
//=============================================================================
//==	ＦＰＧＡバージョン取得
//=============================================================================
void	g_getFPGAVersion(void)
{
	uint32_t	fpgaVer;

	fpgaVer	=	xiGetFpgaVersion();
	modelInf[0].FPGA1Ver[0]	=	(fpgaVer >> 24) & 0xff;
	modelInf[0].FPGA1Ver[1]	=	(fpgaVer >> 16) & 0xff;
	modelInf[0].FPGA1Ver[2]	=	(fpgaVer >> 8) & 0xff;
	modelInf[0].FPGA1Ver[3]	=	(fpgaVer >> 0) & 0xff;
	fpgaVer	=	csz3GetVer2();
	modelInf[0].FPGA2Ver[0]	=	(fpgaVer >> 24) & 0xff;
	modelInf[0].FPGA2Ver[1]	=	(fpgaVer >> 16) & 0xff;
	modelInf[0].FPGA2Ver[2]	=	(fpgaVer >> 8) & 0xff;
	modelInf[0].FPGA2Ver[3]	=	(fpgaVer >> 0) & 0xff;
	fpgaVer	=	csz3GetVer3();
	modelInf[0].FPGA3Ver[0]	=	(fpgaVer >> 24) & 0xff;
	modelInf[0].FPGA3Ver[1]	=	(fpgaVer >> 16) & 0xff;
	modelInf[0].FPGA3Ver[2]	=	(fpgaVer >> 8) & 0xff;
	modelInf[0].FPGA3Ver[3]	=	(fpgaVer >> 0) & 0xff;
}
//=============================================================================
//==	ＤＳ１００ＢＲレジスタ読み込み
//=============================================================================
uint8_t	g_readDS100BR(uint8_t radr)
{
	uint8_t	ret	=	0;
	ds100br_read(radr, &ret, 1);
	return(ret);
}
//=============================================================================
//==	ＤＳ１００ＢＲレジスタ書き込み
//=============================================================================
void	g_writeDS100BR(uint8_t wadr,uint8_t val)
{
	ds100br_write(wadr, &val, 1);
}
//=============================================================================
//==	ＤＳ１００ＢＲレジスタ全書き込み
//=============================================================================
void	g_writeDS100BRAll(uint8_t init,uint8_t all)
{
	//-------------------------------------------------------------------------
	//--	ＤＳ１００ＢＲ非実装基板は何もしない
	//-------------------------------------------------------------------------
	if (commonData.boardKind == BK_101A)	return;
	if (commonData.boardKind == BK_101B)	return;
	if (commonData.boardKind == BK_102A)	return;
	//-------------------------------------------------------------------------
	//--	初期化を伴う場合
	//-------------------------------------------------------------------------
	if (init)
	{
		ds100br_init();
	}
	//-------------------------------------------------------------------------
	//--	レジスタ書き込み
	//-------------------------------------------------------------------------
	if (all)
	{
		g_writeDS100BR(DS100BR_RegisterAddress[0],saveDS100BR.data.SlaveRegisterControl);
		g_writeDS100BR(DS100BR_RegisterAddress[1],saveDS100BR.data.DigitalResetAndControl);
		g_writeDS100BR(DS100BR_RegisterAddress[2],saveDS100BR.data.PinOverrride);
		g_writeDS100BR(DS100BR_RegisterAddress[4],saveDS100BR.data.CHA_Control1);
		g_writeDS100BR(DS100BR_RegisterAddress[5],saveDS100BR.data.CHA_Control2);
		g_writeDS100BR(DS100BR_RegisterAddress[7],saveDS100BR.data.CHB_Control1);
		g_writeDS100BR(DS100BR_RegisterAddress[8],saveDS100BR.data.CHB_Control2);
		g_writeDS100BR(DS100BR_RegisterAddress[9],saveDS100BR.data.CHA_VODControl);
		g_writeDS100BR(DS100BR_RegisterAddress[10],saveDS100BR.data.CHB_VODControl);
	}
	g_writeDS100BR(DS100BR_RegisterAddress[3],saveDS100BR.data.CHA_EQSetting);
	g_writeDS100BR(DS100BR_RegisterAddress[6],saveDS100BR.data.CHB_EQSetting);
}
//=============================================================================
//==	ＥＥＰＲＯＭ　ＭＡＣアドレス情報読み込み
//=============================================================================
void	g_readSavePhysicalLayerEeprom(void)
{
	uint8_t	csm;
	eep_read(EEP_ADDRESS,(uint8_t *)&savePhysicalLayerEeprom,(uint32_t)sizeof(SAVE_PHYSICAL_LAYER_EEPROM_STR));
	csm	=	g_calcCS((uint8_t *)&savePhysicalLayerEeprom.data,0,((uint32_t)sizeof(PHYSICAL_LAYER_EEPROM_STR)));
	if (csm != savePhysicalLayerEeprom.csm)
	{
		commonData.saveDataCsmError	=	1;
	}
}
//=============================================================================
//==	ＥＥＰＲＯＭ　ＭＡＣアドレス情報書き込み
//=============================================================================
void	g_writeSavePhysicalLayerEeprom(void)
{
	savePhysicalLayerEeprom.csm	=	g_calcCS((uint8_t *)&savePhysicalLayerEeprom.data,0,((uint32_t)sizeof(PHYSICAL_LAYER_EEPROM_STR)));
	eep_write(EEP_ADDRESS,(uint8_t *)&savePhysicalLayerEeprom,(uint32_t)sizeof(SAVE_PHYSICAL_LAYER_EEPROM_STR));
}
//=============================================================================
//==	ＣＰＵ／ＶＩＤＥＯ情報読み込み
//=============================================================================
void	g_readSaveCpuVideoLayer(void)
{
	uint8_t	csm;
	setting_read((uint8_t *)&saveCpuVideoLayer,SETTING_DATA_CPU_VIDEO_ADR,(uint32_t)sizeof(SAVE_CPU_VIDEO_LAYER_STR));
	csm	=	g_calcCS((uint8_t *)&saveCpuVideoLayer.data,0,((uint32_t)sizeof(CPU_VIDEO_LAYER_STR)));
	if (csm != saveCpuVideoLayer.csm)
	{
		commonData.saveDataCsmError	=	1;
	}
}
//=============================================================================
//==	ＣＰＵ／ＶＩＤＥＯ情報書き込み
//=============================================================================
void	g_writeSaveCpuVideoLayer(void)
{
	saveCpuVideoLayer.csm	=	g_calcCS((uint8_t *)&saveCpuVideoLayer.data,0,((uint32_t)sizeof(CPU_VIDEO_LAYER_STR)));
	// setting_erase(SETTING_DATA_CPU_VIDEO_ADR);
	setting_write((uint8_t *)&saveCpuVideoLayer,SETTING_DATA_CPU_VIDEO_ADR,(uint32_t)sizeof(SAVE_CPU_VIDEO_LAYER_STR));
}
//=============================================================================
//==	アラームマスク情報読み込み
//=============================================================================
void	g_readSaveAlarmMask(void)
{
	uint8_t	csm;
	setting_read((uint8_t *)&saveAlarmMask,SETTING_DATA_ALARM_MASK_ADR,(uint32_t)sizeof(SAVE_ALARM_MASK_STR));
	csm	=	g_calcCS((uint8_t *)&saveAlarmMask.data,0,((uint32_t)sizeof(ALARM_MASK_STR)));
	if (csm != saveAlarmMask.csm)
	{
		commonData.saveDataCsmError	=	1;
	}
}
//=============================================================================
//==	アラームマスク情報書き込み
//=============================================================================
void	g_writeSaveAlarmMask(void)
{
	saveAlarmMask.csm	=	g_calcCS((uint8_t *)&saveAlarmMask.data,0,((uint32_t)sizeof(ALARM_MASK_STR)));
	// setting_erase(SETTING_DATA_ALARM_MASK_ADR);
	setting_write((uint8_t *)&saveAlarmMask,SETTING_DATA_ALARM_MASK_ADR,(uint32_t)sizeof(SAVE_ALARM_MASK_STR));
}
//=============================================================================
//==	個別設定情報読み込み
//=============================================================================
void	g_readSaveIndividual(void)
{
	uint8_t	csm,i;
	setting_read((uint8_t *)&saveIndividualSet,SETTING_DATA_INDIVIDUAL_SET_ADR,(uint32_t)sizeof(SAVE_INDIVIDUAL_SET_STR));
	csm	=	g_calcCS((uint8_t *)&saveIndividualSet.data,0,((uint32_t)sizeof(INDIVIDUAL_SET_STR)));
	if (csm != saveIndividualSet.csm ||
		!memcmp(saveIndividualSet.data.setVersion,"\xff\xff\xff\xff",4))
	{
		memset((uint8_t *)&saveIndividualSet,0,(uint32_t)sizeof(SAVE_INDIVIDUAL_SET_STR));
		for ( i = 0 ; i < 4 ; i++ )
		{
			saveIndividualSet.data.fsXpSet[i]	=	0x0A;
		}
//		commonData.saveDataCsmError	=	1;	//	ここではアラームをつけない！！暫定！！
	}
}
//=============================================================================
//==	個別設定情報書き込み
//=============================================================================
void	g_writeSaveIndividual(void)
{
	//	機器情報に、バージョン情報を反映
	memcpy(modelInf[0].setPVer,saveIndividualSet.data.setVersion,4);
	memcpy(modelInf[1].setPVer,saveIndividualSet.data.setVersion,4);
	//	保存
	saveIndividualSet.csm	=	g_calcCS((uint8_t *)&saveIndividualSet.data,0,((uint32_t)sizeof(INDIVIDUAL_SET_STR)));
	// setting_erase(SETTING_DATA_INDIVIDUAL_SET_ADR);
	setting_write((uint8_t *)&saveIndividualSet,SETTING_DATA_INDIVIDUAL_SET_ADR,(uint32_t)sizeof(SAVE_INDIVIDUAL_SET_STR));
}
//=============================================================================
//==	ＤＳ１００ＢＲレジスタ情報読み込み
//=============================================================================
void	g_readSaveDS100BR(void)
{
	uint8_t	csm;
	setting_read((uint8_t *)&saveDS100BR,SETTING_DATA_DS100BR_REG,(uint32_t)sizeof(SAVE_DS100BR_REG_STR));
	csm	=	g_calcCS((uint8_t *)&saveDS100BR.data,0,((uint32_t)sizeof(DS100BR_REG_STR)));
	if (csm != saveDS100BR.csm)
	{
		//	デフォルト書き込み
		memset((uint8_t *)&saveDS100BR,0,sizeof(SAVE_DS100BR_REG_STR));
		saveDS100BR.data.SlaveRegisterControl		=	0x18;
		saveDS100BR.data.DigitalResetAndControl		=	0x01;
		saveDS100BR.data.PinOverrride				=	0x04;
		saveDS100BR.data.CHA_EQSetting				=	0x00;
		saveDS100BR.data.CHA_Control1				=	0x00;
		saveDS100BR.data.CHA_Control2				=	0x00;
		saveDS100BR.data.CHB_EQSetting				=	0x00;
		saveDS100BR.data.CHB_Control1				=	0xAD;
		saveDS100BR.data.CHB_Control2				=	0x00;
		saveDS100BR.data.CHA_VODControl				=	0x00;
		saveDS100BR.data.CHB_VODControl				=	0x00;
		g_writeSaveDS100BR();
	}
}
//=============================================================================
//==	ＤＳ１００ＢＲレジスタ情報書き込み
//=============================================================================
void	g_writeSaveDS100BR(void)
{
	saveDS100BR.csm	=	g_calcCS((uint8_t *)&saveDS100BR.data,0,((uint32_t)sizeof(DS100BR_REG_STR)));
	// setting_erase(SETTING_DATA_DS100BR_REG);
	setting_write((uint8_t *)&saveDS100BR,SETTING_DATA_DS100BR_REG,(uint32_t)sizeof(SAVE_DS100BR_REG_STR));
}
//=============================================================================
//==	ＣＳＺ３保存情報読み込み
//=============================================================================
void	g_readSaveCSZ3(void)
{
	uint8_t	csm;
	uint8_t	i;
	setting_read((uint8_t *)&saveCSZ3,SETTING_DATA_CSZ3_ADC_VOL,(uint32_t)sizeof(SAVE_CSZ3_REG_STR));
	csm	=	g_calcCS((uint8_t *)&saveCSZ3.data,0,((uint32_t)sizeof(CSZ3_REG_STR)));
	if (csm != saveCSZ3.csm)
	{
		//	デフォルト書き込み
		memset((uint8_t *)&saveCSZ3,0,sizeof(SAVE_CSZ3_REG_STR));
		saveCSZ3.data.ADC_Vol	=	0x01E0;
		for ( i = 0 ; i < 4 ; i++ )
		{
			saveCSZ3.data.fsInf.PresetMode[i]	=	0;
			saveCSZ3.data.fsInf.HPhase[i]		=	0;
			saveCSZ3.data.fsInf.VPhase[i]		=	0;
			saveCSZ3.data.fsTxMode[i]			=	0x10;
		}
		g_writeSaveCSZ3();
	}
}
//=============================================================================
//==	ＣＳＺ３保存情報書き込み
//=============================================================================
void	g_writeSaveCSZ3(void)
{
	saveCSZ3.csm	=	g_calcCS((uint8_t *)&saveCSZ3.data,0,((uint32_t)sizeof(CSZ3_REG_STR)));
	// setting_erase(SETTING_DATA_CSZ3_ADC_VOL);
	setting_write((uint8_t *)&saveCSZ3,SETTING_DATA_CSZ3_ADC_VOL,(uint32_t)sizeof(SAVE_CSZ3_REG_STR));
}
//=============================================================================
//==	系統情報保存情報読み込み
//=============================================================================
void	g_readSaveSystemAB(void)
{
	uint8_t	csm;
	setting_read((uint8_t *)&saveSystemAB,SETTING_DATA_SYSTEM_AB_ADR,(uint32_t)sizeof(SAVE_SYSTEM_AB));
	csm	=	g_calcCS((uint8_t *)&saveSystemAB.data,0,((uint32_t)sizeof(SYSTEM_AB_STR)));
	if (csm != saveSystemAB.csm ||
		saveSystemAB.data.verbose[0] > 0x01 ||
		saveSystemAB.data.verbose[1] > 0x01 ||
		saveSystemAB.data.unicast > 0x01)
	{
		//	デフォルト書き込み
		memset((uint8_t *)&saveSystemAB,0,sizeof(SAVE_SYSTEM_AB));
		saveSystemAB.data.verbose[0]	=	0x01;
		saveSystemAB.data.verbose[1]	=	0x01;
		g_writeSaveSystemAB();
	}
}
//=============================================================================
//==	系統情報保存情報書き込み
//=============================================================================
void	g_writeSaveSystemAB(void)
{
	saveSystemAB.csm	=	g_calcCS((uint8_t *)&saveSystemAB.data,0,((uint32_t)sizeof(SYSTEM_AB_STR)));
	// setting_erase(SETTING_DATA_SYSTEM_AB_ADR);
	setting_write((uint8_t *)&saveSystemAB,SETTING_DATA_SYSTEM_AB_ADR,(uint32_t)sizeof(SAVE_SYSTEM_AB));
}
//=============================================================================
//==	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御読み込み
//=============================================================================
void	g_readSaveSdiIpMisInf(void)
{
	uint16_t i;
	uint8_t	csm;
	uint8_t	flg = 0;
	
	setting_read((uint8_t *)&saveSdiIpMisInf,SETTING_DATA_SDIIPMIS_ADR,(uint32_t)sizeof(SAVE_SDIIPMISMATCH_STR));
	csm	=	g_calcCS((uint8_t *)&saveSdiIpMisInf.data,0,((uint32_t)sizeof(SDIIPMISMATCH_STR)));
	if ((csm != saveSdiIpMisInf.csm1) || (saveSdiIpMisInf.mode != 0x0001))
	{
		//	デフォルト書き込み
		memset((uint8_t *)&saveSdiIpMisInf,0,(uint32_t)sizeof(SAVE_SDIIPMISMATCH_STR));
		saveSdiIpMisInf.data.periodTime	=	1;						//	監視周期
		saveSdiIpMisInf.data.sdiVideoFormatErrTxTime	=	10;		//	ＳＤＩＥＲＲ監視時間
		saveSdiIpMisInf.data.sdiVideoFormatErrTxCount	=	5;		//	ＳＤＩＥＲＲ連続回数
		saveSdiIpMisInf.data.sdiVideoCrcErrRxTime		=	10;		//	ＩＰＥＲＲ監視時間
		saveSdiIpMisInf.data.sdiVideoCrcErrRxCount		=	10;		//	ＩＰＥＲＲ連続回数
		saveSdiIpMisInf.data.MismatchTime				=	10;		//	ＩＰＭＩＳＭＡＴＣＨ監視時間
		saveSdiIpMisInf.data.MismatchErrCount			=	5;		//	ＩＰＭＩＳＭＡＴＣＨ連続回数
		for(i = 0; i < 4; i++)
		{
			saveSdiIpMisInf.data.sdiVideoFormatErrTxThres[i]	=	5;		//	ＳＤＩＥＲＲ閾値
			saveSdiIpMisInf.data.sdiVideoCrcErrRxThres[i]		=	10;		//	ＩＰＥＲＲ閾値
			saveSdiIpMisInf.data.MismatchThres[i]				=	2;		//	ＩＰＭＩＳＭＡＴＣＨ閾値
		}
		flg	=	1;
//		g_writeSaveSdiIpMisInfl();
	}

	csm	=	g_calcCS((uint8_t *)&saveSdiIpMisInf.data1,0,((uint32_t)sizeof(OTHER_INFO_STR)));
	if (csm != saveSdiIpMisInf.csm2)
	{
		memset((uint8_t *)&saveSdiIpMisInf.data1,0,(uint32_t)sizeof(OTHER_INFO_STR));
		saveSdiIpMisInf.data1.tempeThres	=	TXRX_TEMP_DET_VAL;
		flg	=	1;
	}
	if (saveSdiIpMisInf.data1.tempeThres == 0)
	{
		saveSdiIpMisInf.data1.tempeThres	=	TXRX_TEMP_DET_VAL;
		flg	=	1;
	}
	TXRX_TEMP_DET	=	saveSdiIpMisInf.data1.tempeThres;

	if (flg)
	{
		g_writeSaveSdiIpMisInfl();
	}
}
//=============================================================================
//==	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御書き込み
//=============================================================================
void	g_writeSaveSdiIpMisInfl(void)
{
	saveSdiIpMisInf.mode = 0x0001;
	saveSdiIpMisInf.csm1	=	g_calcCS((uint8_t *)&saveSdiIpMisInf.data,0,((uint32_t)sizeof(SDIIPMISMATCH_STR)));
	saveSdiIpMisInf.csm2	=	g_calcCS((uint8_t *)&saveSdiIpMisInf.data1,0,((uint32_t)sizeof(OTHER_INFO_STR)));
	// setting_erase(SETTING_DATA_SDIIPMIS_ADR);
	setting_write((uint8_t *)&saveSdiIpMisInf,SETTING_DATA_SDIIPMIS_ADR,(uint32_t)sizeof(SAVE_SDIIPMISMATCH_STR));
}
//=============================================================================
//==	ＦＳ機能情報エンディアン変換
//=============================================================================
void	g_changeEndFsInf(FSINF_STR *src,FSINF_STR *dst)
{
	uint8_t	i;
	for ( i = 0 ; i < 4 ; i++ )
	{
		dst->PresetMode[i]	=	src->PresetMode[i];
		dst->HPhase[i]		=	g_endianConv(src->HPhase[i]);
		dst->VPhase[i]		=	g_endianConv(src->VPhase[i]);
	}
}
//=============================================================================
//==	LED点灯／消灯関数
//=============================================================================
void	g_led_control(uint8_t no,uint8_t onoff)
{
	/* 初期化値に意味はない */ 
	unsigned int offset = GPIO_OFFSET_MIN;
	switch (no)
	{
 	case	0:	//	LED0
		offset = GPIO_OFFSET_CPU_LED0;
		break;
	case	1:	//	LED1
		offset = GPIO_OFFSET_CPU_LED1;
		break;
	case	2:	//	LED2
		offset = GPIO_OFFSET_CPU_LED2;
		break;
	case	3:	//	LED3
		offset = GPIO_OFFSET_CPU_LED3;
		break;
	case	4:	//	LED4
		offset = GPIO_OFFSET_CPU_LED4;
		break;
	case	5:	//	LED5
		offset = GPIO_OFFSET_CPU_LED5;
		break;
	case	6:	//	LED6
		offset = GPIO_OFFSET_CPU_LED6;
		break;
	case	7:	//	LED7
		offset = GPIO_OFFSET_CPU_LED7;
		break;
	case	8:	//	LED8
		offset = GPIO_OFFSET_CPU_LED8;
		break;
	case	9:	//	LED9
		offset = GPIO_OFFSET_CPU_LED9;
		break;
	case	10:	//	LED10
		offset = GPIO_OFFSET_CPU_LED10;
		break;
	case	11:	//	LED11
		offset = GPIO_OFFSET_CPU_LED11;
		break;
	case	12:	//	LED12
		offset = GPIO_OFFSET_CPU_LED12;
		break;
	case	13:	//	LED13
		offset = GPIO_OFFSET_CPU_LED13;
		break;
	default:
		break;
	}

	/* 未対応のLED(ここには来ない) */
	if (GPIO_OFFSET_MIN == offset) {
		LOG_E("unsupported no: %u, onoff: %u\n", no, onoff);
		return;
	}

	// LOG_D("no: %u, offset: %u, onoff: %u\n", no, offset, onoff);
	com_gpio_write(CFG_APL_NAME, offset, &g_gpiod_lines[offset], (int)onoff);
}
//=============================================================================
//==	チェックサム計算関数
//=============================================================================
uint8_t	g_calcCS(uint8_t *data,uint32_t offset,uint32_t len)
{
	uint8_t	ret	=	0;
	uint32_t	i;
	for ( i = offset ; i < (offset + len) ; i++ )
	{
		ret	+=	*(data + i);
	}
	ret	=	~ret;
	return(ret);
}
//=============================================================================
//==	マルチキャストＩＰアドレスからＭＡＣアドレスを作成
//=============================================================================
void		g_multiCastIpToMac(uint32_t ip,uint8_t *mac)
{
	*(mac + 0)		=	MC_MAC_AD1;
	*(mac + 1)		=	MC_MAC_AD2;
	*(mac + 2)		=	MC_MAC_AD3;
	*(mac + 3)		=	((ip & 0x7F0000) >> 16);
	*(mac + 4)		=	((ip & 0xFF00) >> 8);
	*(mac + 5)		=	(ip & 0xFF);
}
//=============================================================================
//==	エンディアン変換関数
//=============================================================================
uint16_t	g_endianConv(uint16_t src)
{
	union
	{
		uint8_t		cc[2];
		uint16_t	ss;
	}	uu1;
	union
	{
		uint8_t		cc[2];
		uint16_t	ss;
	}	uu2;
	uu1.ss	=	src;
	uu2.cc[0]	=	uu1.cc[1];
	uu2.cc[1]	=	uu1.cc[0];
	return(uu2.ss);
}
//=============================================================================
//==	ＩＰアドレス整形（uint8_t[4]⇒uint32_t)
//=============================================================================
uint32_t	g_c4To32(uint8_t *c)
{
	uint32_t	ret	=	0;
	ret	=	((uint32_t)(*(c + 0)) << 24) +
			((uint32_t)(*(c + 1)) << 16) +
			((uint32_t)(*(c + 2)) << 8) +
			(uint32_t)(*(c + 3));
	return(ret);
}
//=============================================================================
//==	ＩＰアドレス自動計算（uint8_t[4]⇒uint32_t)
//==	2017/05/18〜NHKの仕様(ロータリースイッチ１００までしか対応しない）
//==	A_or_B 0:A 1:B
//==	C_or_V 0:Control 1:Video
//=============================================================================
#define		NO4_OCTER_MAX	248
uint32_t	g_c4To32WithRSW_NHK(uint8_t *c,uint8_t A_or_B,uint8_t C_or_V)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	sidx	=	commonData.bdIndex;
	uint32_t	N4		=	ret & 0x000000ff;
	uint32_t	N3		=	ret & 0x0000ff00;
	uint32_t	N3up	=	0;
	ret		=	ret & 0xffff0000;
	N4		=	N4 + sidx * 4;
	if (C_or_V)	N4	=	N4 + 2;
	if (A_or_B)	N4	=	N4 + 1;
	N3up	=	(N4 - 1) / NO4_OCTER_MAX;
	N4		=	(N4 - 1) % NO4_OCTER_MAX + 1;
	if ((N3 + N3up * 0x100) <= 0x0000ff00)
	{
		N3	=	N3 + N3up * 0x100;
	}
	else
	{
		N3	=	0x0000ff00;
	}
	ret	=	ret | N3 | N4;
	return(ret);
}
/*
uint32_t	g_c4To32WithRSW_NHK(uint8_t *c,uint8_t A_or_B,uint8_t C_or_V)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	sidx	=	commonData.bdIndex + 1;
	uint32_t	N4		=	0;
	ret	=	ret & 0xffffff00;
	if (sidx <= 12)
	{
		N4	=	201 + (sidx - 1) * 4;
	}
	else if (sidx <= 74)
	{
		N4	=	1 + (sidx - 13) * 4;
		ret	=	ret + 0x100;
	}
	else if (sidx <= 100)
	{
		N4	=	1 + (sidx - 75) * 4;
		ret	=	ret + 0x200;
	}
	ret	=	ret | N4;
	if (C_or_V)	ret	=	ret + 2;
	if (A_or_B)	ret	=	ret + 1;
	return(ret);
}
*/
//=============================================================================
//==	マルチキャストアドレス自動計算（uint8_t[4]⇒uint32_t)
//==	2017/05/18〜NHKの仕様(素材番号４００までしか対応しない）
//==	2020/04/21〜NHKの仕様(素材番号４００以上も対応）
//==	sno：素材番号
//=============================================================================
uint32_t	g_c4To32WithIndex_NHK(uint8_t *c,uint16_t sno)
{
#if 0
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	N4		=	0;
	ret	=	ret & 0xffffff00;
	if (sno < 200)
	{
		N4	=	1 + sno;
	}
	else if (sno < 400)
	{
		N4	=	1 + (sno - 200);
		ret	=	ret + 0x100;
	}
	else
	{
		N4	=	1 + (sno - 400);
		ret	=	ret + 0x200;
	}
	ret	=	ret | N4;
	return(ret);
#else
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	N3		=	0;
	uint32_t	N4		=	0;

	N3	=	(ret & 0x0000ff00) >> 8;
	ret	=	ret & 0xffff0000;
	N3	=	N3 + (sno / 200);
	if (N3 > 255) N3 = 255;
	N4	=	(sno % 200) + 1;
	ret	=	ret | N4 | (N3 << 8);
	return(ret);
#endif	
}
//=============================================================================
//==	マルチキャストアドレス自動計算（uint8_t[4]⇒uint32_t)（ロータリーＳＷ）
//==	2017/05/18〜NHKの仕様(素材番号４００までしか対応しない）
//==	ab ：A or B
//==	bno：BUS No
//=============================================================================
uint32_t	g_c4To32WithRSW_MC_NHK(uint8_t *c,uint16_t ab,uint16_t bno)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	N3		=	0;
	uint32_t	N4		=	0;
	uint32_t	bn		=	commonData.bdIndex * 4;
	ret	=	ret & 0xffff0000;
	N3	=	(bn / 200);
	if (ab)	N3	=	N3 + 10;
	N4	=	(bn % 200) + 1 + bno;
	ret	=	ret | N4 | (N3 << 8);
	return(ret);
}
//=============================================================================
//==	ＩＰアドレス自動計算(up1)（uint8_t[4]⇒uint32_t)
//=============================================================================
uint32_t	g_c4To32WithRSW_up1(uint8_t *c)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	sidx	=	commonData.bdIndex;
	//	最終桁
	ret	=	(ret & 0xffffff00) + (sidx % 200) + 1;
	//	３桁目
	ret	+=	(0x100 * (sidx / 200));
	//	ＲＸは、三桁目＋４
	if (commonData.tx_or_rx == HDIPRX)
	{
		ret	+=	0x400;
	}
	return(ret);
}
//=============================================================================
//==	ＩＰアドレス自動計算(up4)（uint8_t[4]⇒uint32_t)
//=============================================================================
uint32_t	g_c4To32WithRSW_up4(uint8_t *c,uint8_t idx)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	sidx	=	commonData.bdIndex * 4 + idx;
	//	最終桁
	ret	=	(ret & 0xffffff00) + (sidx % 200) + 1;
	//	３桁目
	ret	+=	(0x100 * (sidx / 200));
	return(ret);
}
//=============================================================================
//==	ＩＰアドレス自動計算(upn)（uint8_t[4]⇒uint32_t)
//=============================================================================
uint32_t	g_c4To32WithRSW_upN(uint8_t *c,uint32_t idx)
{
	uint32_t	ret		=	g_c4To32(c);
	uint32_t	sidx	=	idx;
	//	最終桁
	ret	=	(ret & 0xffffff00) + (sidx % 200) + 1;
	//	３桁目
	ret	+=	(0x100 * (sidx / 200));
	return(ret);
}
//=============================================================================
//==	ポート番号整形（uint8_t[2]⇒uint16_t)
//=============================================================================
uint16_t	g_c2To16(uint8_t *c)
{
	uint16_t	ret	=	0;
	ret	=	((uint16_t)(*(c + 0)) << 8) +
			(uint16_t)(*(c + 1));
	return(ret);
}
//=============================================================================
//--	管理制御棚インデックスを求める（Ａ＝０，Ｂ＝１，−１＝ＥＲＲ）
//=============================================================================
int32_t		g_IsManagementServerIndex(T_NODE *nd)
{
	int32_t	ret	=	-1;
	int32_t	i;

	for ( i = 0 ; i < 2 ; i++ )
	{
		if (saveCpuVideoLayer.data.managementServerIpAddress[i][0] == ((nd->ipa & 0xff000000) >> 24) &&
			saveCpuVideoLayer.data.managementServerIpAddress[i][1] == ((nd->ipa & 0x00ff0000) >> 16) &&
			saveCpuVideoLayer.data.managementServerIpAddress[i][2] == ((nd->ipa & 0x0000ff00) >> 8) &&
			saveCpuVideoLayer.data.managementServerIpAddress[i][3] == ((nd->ipa & 0x000000ff) >> 0))
		{
			ret	=	i;
		}
	}
	return(ret);
}
//=============================================================================
//==	ＡＤＣ１６ビットを温度実数に変換（有効ビットは下位８ビット）
//=============================================================================
double		g_ADC16ToDoubleTemp(uint16_t adc)
{
	uint16_t	adc8	=	(adc & 0x00ff);
	double	adcv	=	(double)adc8;
	double	temp	=	adcv * 503.975 / 256.0 - 273.15;
	return(temp);
}
//=============================================================================
//==	自己リセット
//=============================================================================
void		g_reset(void)
{
#if 0
	// run_cmd("reboot");
#else
	LOG_W("%s unsupported\n", __func__);
#endif
}
