#include <sys/socket.h>
#include <arpa/inet.h>

#include <errno.h>
#include <string.h>

#include "device.h" // DEVICE_FD_INIT_VAL
#include "g_common.h" /* SNMP_PORT_R, SNMP_PORT_S, UDP_MAX_LEN */

#include "ethernetif.h"
#include "log.h"

/*============================================================================*/
/* D E F I N E                                                                */
/*============================================================================*/

/* udp, snmp, udp_moniの各種ソケット最大数 */
#define ETHERNETIF_SOCK_NUM_MAX 2

/** @note 送受信バッファサイズを変更する必要がある場合に0以外の値を設定 */
#define ETHERNETIF_SOCK_RCV_BUF_SIZE 0
#define ETHERNETIF_SOCK_SND_BUF_SIZE 0

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

XIP_NET_T  xipNet[2];
LWII_XIP_T dstNet[2]	=	{	{ 0,0,0,0,0,0,0,0,0,0 },
								{ 0,0,0,0,0,0,0,0,0,0 }	};
UH vlanTagId = 0;
UB vlanTagFlag = 0;

/* ソケットのファイルディスクリプタ配列 */
static int s_udp_fd_array[ETHERNETIF_SOCK_NUM_MAX] = {DEVICE_FD_INIT_VAL, DEVICE_FD_INIT_VAL};
static int s_snmp_fd_array[ETHERNETIF_SOCK_NUM_MAX] = {DEVICE_FD_INIT_VAL, DEVICE_FD_INIT_VAL};
static int s_udp_moni_fd_array[ETHERNETIF_SOCK_NUM_MAX] = {DEVICE_FD_INIT_VAL, DEVICE_FD_INIT_VAL};

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

void udp_raw_init_port0(struct ip_addr *addr, u16_t port);
void udp_raw_init_port1(struct ip_addr *addr, u16_t port);
u32_t udp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
void udp_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos);
void snmp_raw_init_port0(struct ip_addr *addr, u16_t port);
void snmp_raw_init_port1(struct ip_addr *addr, u16_t port);
u32_t snmp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
// UDP MONITOR
void udp_moni_raw_init_port0(struct ip_addr *addr, u16_t port);
void udp_moni_raw_init_port1(struct ip_addr *addr, u16_t port);
u32_t udp_moni_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
void udp_moni_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static int s_socket_create_inet_udp(const struct ip_addr *addr,
									const u16_t port,
									const int rcv_buf_size,
									const int snd_buf_size);

static void s_socket_init(	const u8_t id,
							int *fd_array,
							const struct ip_addr *addr,
							const u16_t port,
							const int rcv_buf_size,
							const int snd_buf_size);

static u32_t s_socket_recv_mnt(	const int *fd_array,
								u8_t *dp,
								u8_t *id,
								struct ip_addr *addr,
								u16_t *port);

static void s_socket_send(	const u8_t id,
							const int *fd_array,
							u8_t *dp,
							u32_t len,
							struct ip_addr *addr,
							u16_t port,
							u16_t srcport,
							u8_t tos);

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

void udp_raw_init_port0(struct ip_addr *addr, u16_t port) {
	s_socket_init(0, s_udp_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}

void udp_raw_init_port1(struct ip_addr *addr, u16_t port) {
	s_socket_init(1, s_udp_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}

u32_t udp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port) {
	return s_socket_recv_mnt(s_udp_fd_array, dp, id, addr, port);
}

void udp_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos) {
	/* SNMP */
	if (SNMP_PORT_R == srcport && SNMP_PORT_S == port) {
		s_socket_send(id, s_snmp_fd_array, dp, len, addr, port, srcport, tos);
	}
	else {
		s_socket_send(id, s_udp_fd_array, dp, len, addr, port, srcport, tos);
	}
	return;
}

void snmp_raw_init_port0(struct ip_addr *addr, u16_t port) {
	s_socket_init(0, s_snmp_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}

void snmp_raw_init_port1(struct ip_addr *addr, u16_t port) {
	s_socket_init(1, s_snmp_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}

u32_t snmp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port) {
	return s_socket_recv_mnt(s_snmp_fd_array, dp, id, addr, port);
}

void udp_moni_raw_init_port0(struct ip_addr *addr, u16_t port) {
	s_socket_init(0, s_udp_moni_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}
void udp_moni_raw_init_port1(struct ip_addr *addr, u16_t port) {
	s_socket_init(1, s_udp_moni_fd_array, addr, port, ETHERNETIF_SOCK_RCV_BUF_SIZE, ETHERNETIF_SOCK_SND_BUF_SIZE);
	return;
}

u32_t udp_moni_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port) {
	return s_socket_recv_mnt(s_udp_moni_fd_array, dp, id, addr, port);	 
}

void udp_moni_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos) {
	s_socket_send(id, s_udp_moni_fd_array, dp, len, addr, port, srcport, tos);
	return;
}

/*============================================================================*/

/**
 * @brief
 * 
 * @param addr 
 * @param port 
 * @param rcv_buf_size 
 * @param snd_buf_size 
 * @return int 
 * @retval < 0	失敗
 * @retval 他	ソケットのファイルディスクリプタ
 *  
 */
int s_socket_create_inet_udp(	const struct ip_addr *addr,
								const u16_t port,
								const int rcv_buf_size,
								const int snd_buf_size) {
	int ret;
	int fd;
	/* TOS値は全ソケットで固定 */
	int ip_tos = RAW_SEND_PRI;
	struct sockaddr_in sa;

	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (fd < 0) {
		LOG_E("socket() failed: errno = %d(%s)\n", errno, strerror(errno));
		return -1;
	}

	/* 受信バッファサイズを設定 */
	if (0 < rcv_buf_size) {
		ret = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, &rcv_buf_size, sizeof(int));
		if (ret < 0) {
			LOG_E("setsockopt(SO_RCVBUF: %d) failed: errno = %d(%s)\n", rcv_buf_size, errno, strerror(errno));
			ret = close(fd);
			if (0 != ret) {
				/* クローズ失敗時に行うことを記載する */
			}
			return -1;
		}
	}

	/* 送信バッファサイズを設定 */
	if (0 < snd_buf_size) {
		ret = setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &snd_buf_size, sizeof(int));
		if (ret < 0) {
			LOG_E("setsockopt(SO_SNDBUF: %d) failed: errno = %d(%s)\n", snd_buf_size, errno, strerror(errno));
			ret = close(fd);
			if (0 != ret) {
				/* クローズ失敗時に行うことを記載する */
			}
			return -1;
		}
	}

	/* TOS値設定 */
	ret = setsockopt(fd, IPPROTO_IP, IP_TOS, &ip_tos, sizeof(ip_tos));
	if (ret < 0) {
		LOG_E("setsockopt(IP_TOS: %d) failed: errno = %d(%s)\n", ip_tos, errno, strerror(errno));
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return -1;
	}

	/* sockaddr_in領域をクリア */
	memset(&sa, 0x00, sizeof(sa));

	/* ソケットのアドレス情報を設定 */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = addr->addr;

	/* バインド設定 */
	ret = bind(fd, (struct sockaddr *)&sa, sizeof(sa));
	if (ret < 0) {
		LOG_E("bind(0x%08X, %u) failed: errno = %d(%s)\n", ntohl(addr->addr), port, errno, strerror(errno));
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return -1;
	}

	return fd;
}

void s_socket_init(	const u8_t id,
					int *fd_array,
					const struct ip_addr *addr,
					const u16_t port,
					const int rcv_buf_size,
					const int snd_buf_size) {
	int fd;

	fd = s_socket_create_inet_udp(addr, port, rcv_buf_size, snd_buf_size);
	if (fd < 0) {
		/* エラーログは出力済み */
		return;
	}
	fd_array[id] = fd;

	LOG_TRACE("[Socket] fd = %d, addr = 0x%08X, port = %u, id = %u\n", fd, ntohl(addr->addr), port, id);	
	return;
}

u32_t s_socket_recv_mnt(const int *fd_array,
						u8_t *dp,
						u8_t *id,
						struct ip_addr *addr,
						u16_t *port) {
	u32_t len = 0;
	int ret;
	int sock_num;
	int fd;
	int max_fd = DEVICE_FD_INIT_VAL;
	fd_set fds;
	ssize_t rcv_len;
	struct sockaddr_in from;
	socklen_t addrlen = (socklen_t)sizeof(from);

	/* ソケット集合設定 */
	FD_ZERO(&fds);
	for (sock_num = 0; sock_num < ETHERNETIF_SOCK_NUM_MAX; sock_num++) {
		/* 未作成ではないソケットのみ */
		if (DEVICE_FD_INIT_VAL != fd_array[sock_num]) {
			FD_SET(fd_array[sock_num], &fds);
			if (max_fd < fd_array[sock_num]) {
				max_fd = fd_array[sock_num];
			}
		}
	}
	if (DEVICE_FD_INIT_VAL == max_fd) {
		/* s_socket_init()でログ出力済み */
		return 0;
	}

	ret = select(max_fd + 1, &fds, NULL, NULL, NULL);
	if (ret < 0) {
		LOG_E("select(%d) failed: %d(%s)\n", max_fd, errno, strerror(errno));
		return 0;
	}
	else if (0 == ret) {
		/* ここには来ない(タイムアウトは未設定) */
		LOG_W("select(%d) timeout\n", max_fd);
		return 0;
	}

	/* ソケットファイルディスクリプタ検索 */
	fd = DEVICE_FD_INIT_VAL;
	for (sock_num = 0; sock_num < ETHERNETIF_SOCK_NUM_MAX; sock_num++) {
		/* 未作成ではないソケットのみ */
		if (DEVICE_FD_INIT_VAL != fd_array[sock_num]) {
			if (FD_ISSET(fd_array[sock_num], &fds)) {
				fd = fd_array[sock_num];
				break;
			}
		}
	}
	/* ここには来ない(対象ソケットで受信できなかった場合) */
	if (DEVICE_FD_INIT_VAL == fd) {
		LOG_W("FD_ISSET() not match\n");
		return 0;
	}

	rcv_len = recvfrom(fd, dp, UDP_MAX_LEN, 0, (struct sockaddr *)&from, &addrlen);
	if (rcv_len < 0) {
		LOG_E("recvfrom(%d) failed: %d(%s)\n", fd, errno, strerror(errno));
		return 0;
	}

	/* 受信ソケット番号(0 or 1)、送信元アドレス、送信元L4ポート番号設定 */
	addr->addr = from.sin_addr.s_addr;
	*id = (u8_t)sock_num;
	*port = ntohs(from.sin_port);
	/* 受信メッセージサイズ */
	len = (u32_t)rcv_len;

	/* 受信完了 */
	return len;					
}

/**
 * @brief 
 * 
 * @param id 
 * @param fd_array 
 * @param dp 
 * @param len 
 * @param addr 
 * @param port 
 * @param srcport 
 * @param tos 無視
 * 
 * @attention tos値の指定が全ソケットの送信で同じのため、
 *            この関数では設定しない
 */
void s_socket_send(	const u8_t id,
					const int *fd_array,
					u8_t *dp,
					u32_t len,
					struct ip_addr *addr,
					u16_t port,
					u16_t srcport,
					u8_t tos) {
	int fd;
	ssize_t snd_len;
	struct sockaddr_in to;
	socklen_t addrlen = (socklen_t)sizeof(to);

	memset(&to, 0x00, sizeof(to));
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = addr->addr;
	to.sin_port = htons(port);
	addrlen = sizeof(to);

	/** @note idが0 or 1であることは、呼び出し元で保証 */
#if 0
	/* A/B系の判定 */
	if (((u8_t)0) != id && ((u8_t)1) != id) {
		LOG_E("Unexpected parameter: id = %u\n", id);
		return;
	}
#endif

	fd = fd_array[id];
	if (DEVICE_FD_INIT_VAL == fd) {
		/* s_socket_init()でログ出力済み */
		return;
	}

	snd_len = sendto(fd, dp, len, 0, (struct sockaddr *)&to, addrlen);
	if (snd_len < 0) {
		LOG_E("sendto(%d -> 0x%08X, %u) failed: %d(%s)\n", fd, ntohl(addr->addr), port, errno, strerror(errno));
		return;
	}

	/* 送信完了 */
	return;
}