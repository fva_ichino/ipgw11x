#include	"IPGW11X_gpio.h"
#include	"IPGW11X_cfg.h" // CFG_APL_NAME

//=============================================================================
//==	テーブル定義
//=============================================================================

/** RTOS RIN_GPIOのポート番号とビットからLinux GPIOのLine Offsetに変換するテーブル
 * @note PNB -> のコメントは、RTOSの内容
 */
static const gpio_rin_table_t RIN_GPIO_TBL[RIN_GPIO_PORT_MAX] = {
//	gpio_rin_table_t
//		offsets[CHAR_BIT],
//		(NONE)
	{
		{
			// P00 -> V_INT
			GPIO_OFFSET_V_INT,
			// P01 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P02-> CPU IO0
			GPIO_OFFSET_CPU_IO0,
			// P03-> CPU IO1
			GPIO_OFFSET_CPU_IO1,
			// P04-> CPU IO2
			GPIO_OFFSET_CPU_IO2,
			// P05-> CPU IO3
			GPIO_OFFSET_CPU_IO3,
			// P06-> RSW0
			GPIO_OFFSET_CPU_RSW0,
			// P07-> RSW1
			GPIO_OFFSET_CPU_RSW1,
		},
	},
	{
		{
			// P10 -> RSW2
			GPIO_OFFSET_CPU_RSW2,
			// P11 -> PWRGD3
			GPIO_OFFSET_PWRGD3,
			// P12 -> RSW3
			GPIO_OFFSET_CPU_RSW3,
			// P13 -> RSW4
			GPIO_OFFSET_CPU_RSW4,
			// P14 -> SMSCK
			GPIO_OFFSET_PRESERVED_0,
			// P15 -> SMSO
			GPIO_OFFSET_PRESERVED_0,
			// P16 -> SMSI
			GPIO_OFFSET_PRESERVED_0,
			// P17 -> SMCSZ
			GPIO_OFFSET_PRESERVED_0,
		},
	},
	{
		{
			// P20 -> RXD485
			GPIO_OFFSET_PRESERVED_0,
			// P21 -> TXD485
			GPIO_OFFSET_PRESERVED_0,
			// P22 -> RSW5
			GPIO_OFFSET_CPU_RSW5,
			// P23 -> RSW6
			GPIO_OFFSET_CPU_RSW6,
			// P24 -> RSW7
			GPIO_OFFSET_CPU_RSW7,
			// P25 -> WDTOUTZ
			GPIO_OFFSET_PRESERVED_0,
			// P26 -> FAN ALM1
			GPIO_OFFSET_FAN_ALM1,
			// P27 -> CPU LED14
			GPIO_OFFSET_CPU_LED14,
		},
	},
	{
		{
			// P30 -> RXD USB	
			GPIO_OFFSET_PRESERVED_0,
			// P31 -> TXD USB
			GPIO_OFFSET_PRESERVED_0,
			// P32 -> RID0
			GPIO_OFFSET_R_ID0,
			// P33 -> RID1
			GPIO_OFFSET_R_ID1,
			// P34 -> RID2
			GPIO_OFFSET_R_ID2,
			// P35 -> RID3
			GPIO_OFFSET_R_ID3,
			// P36 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P37 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
		},
	},
	{
		{
			// P40 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P41 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P42 -> CPU LED1
			GPIO_OFFSET_CPU_LED1,
			// P43 -> CPU LED2
			GPIO_OFFSET_CPU_LED2,
			// P44 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P45 -> CPU LED3
			GPIO_OFFSET_CPU_LED3,
			// P46 -> CPU LED4
			GPIO_OFFSET_CPU_LED4,
			// P47 -> CPU LED5
			GPIO_OFFSET_CPU_LED5,
		},
	},
	{
		{
			// P50 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P51 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P52 -> CPU LED6
			GPIO_OFFSET_CPU_LED6,
			// P53 -> CPU LED7
			GPIO_OFFSET_CPU_LED7,
			// P54 -> CPU LED8
			GPIO_OFFSET_CPU_LED8,
			// P55 -> CPU LED9
			GPIO_OFFSET_CPU_LED9,
			// P56 -> CPU LED10
			GPIO_OFFSET_CPU_LED10,
			// P57 -> CPU LED11
			GPIO_OFFSET_CPU_LED11,
		},
	},
	{
		{
			// P60 -> EEPROM SCL
			GPIO_OFFSET_PRESERVED_0,
			// P61 -> EEPROM SDA
			GPIO_OFFSET_PRESERVED_0,
			// P62 -> CPU LED12
			GPIO_OFFSET_CPU_LED12,
			// P63 -> CPU LED13
			GPIO_OFFSET_CPU_LED13,
			// P64 -> FPGA3 DONE
			GPIO_OFFSET_FPGA3_DONE,
			// P65 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P66 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// P67 -> SET MODE
			GPIO_OFFSET_PRESERVED_0,
		},
	},
	{
		{
			// P70 -> CPU DSW0
			GPIO_OFFSET_CPU_DSW0,
			// P71 -> CPU DSW1
			GPIO_OFFSET_CPU_DSW1,
			// P72 -> CPU DSW2
			GPIO_OFFSET_CPU_DSW2,
			// P73 -> CPU DSW3
			GPIO_OFFSET_CPU_DSW3,
			// P74 -> CPU DSW4
			GPIO_OFFSET_CPU_DSW4,
			// P75 -> CPU DSW5
			GPIO_OFFSET_CPU_DSW5,
			// P76 -> CPU DSW6
			GPIO_OFFSET_CPU_DSW6,
			// P77 -> CPU DSW7
			GPIO_OFFSET_CPU_DSW7,
		},
	},
};

/** RTOS RIN_RTPORTのポート番号とビットからLinux GPIOのLine Offsetに変換するテーブル 
 * @note RPNB -> のコメントは、RTOSの内容
 */
static const gpio_rin_table_t RIN_RTPORT_TBL[RIN_RTPORT_PORT_MAX] = {
//	gpio_rin_table_t
//		offsets[CHAR_BIT],
//		(NONE)
	{
		{
			// RP00 -> EQ-GAIN SCL
			GPIO_OFFSET_PRESERVED_0,
			// RP01 -> EQ-GAIN SDA
			GPIO_OFFSET_PRESERVED_0,
			// RP02 -> CPU OUT3
			GPIO_OFFSET_PRESERVED_0,
			// RP03 -> CPU OUT4
			GPIO_OFFSET_PRESERVED_0,
			// RP04 -> CPU OUT1
			GPIO_OFFSET_PRESERVED_0,
			// RP05 -> CPU IN3
			GPIO_OFFSET_PRESERVED_0,
			// RP06 -> CPU OUT2
			GPIO_OFFSET_PRESERVED_0,
			// RP07 -> PRG_B_FPGA1
			GPIO_OFFSET_PRESERVED_0,
		},
	},
	{
		{
			// RP10 -> CPU IN4
			GPIO_OFFSET_PRESERVED_0,
			// RP11 -> SYS ALM A
			GPIO_OFFSET_SYS_ALM_A,
			// RP12 -> SYS ALM B
			GPIO_OFFSET_SYS_ALM_B,
			// RP13 -> RA OUT
			GPIO_OFFSET_RA_OUT,
			// RP14 -> CPU IN1
			GPIO_OFFSET_PRESERVED_0,
			// RP15 -> CPU MODE0, CPU M DSW3: 書込み許可で固定 
			GPIO_OFFSET_PRESERVED_1,
			// RP16 -> CPU MODE1, CPU M DSW4: リアユニットで固定
			GPIO_OFFSET_PRESERVED_0,
			// RP17 -> 485EN
			GPIO_OFFSET_PRESERVED_0,
		},
	},
	{
		{
			// RP20 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// RP21 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// RP22 -> (NONE)
			GPIO_OFFSET_PRESERVED_0,
			// RP23 -> ALM OUT
			GPIO_OFFSET_ALM_OUT,
			// RP24 -> FPGA2 DONE
			GPIO_OFFSET_FPGA2_DONE,
			// RP25 -> FPGA1 DONE
			GPIO_OFFSET_FPGA1_DONE,
			// RP26 -> CPU IO4
			GPIO_OFFSET_CPU_IO4,
			// RP27 -> CPU IO5
			GPIO_OFFSET_CPU_IO5,
		},
	},
	{
		{
			// RP30 -> CPU IO6	
			GPIO_OFFSET_CPU_IO6,
			// RP31 -> CPU IO7
			GPIO_OFFSET_CPU_IO7,
			// RP32 -> ID0
			GPIO_OFFSET_ID0,
			// RP33 -> ID2
			GPIO_OFFSET_ID2,
			// RP34 -> ID1
			GPIO_OFFSET_ID1,
			// RP35 -> CPU LED0
			GPIO_OFFSET_CPU_LED0,
			// RP36 -> ID3
			GPIO_OFFSET_ID3,
			// RP37 -> CPU IN2
			GPIO_OFFSET_PRESERVED_0,
		},
	},
};

/* マクロ関数用テーブル */
const gpio_rin_table_t *g_gpio_rin_tbl[RIN_TYPE_MAX] = {
	RIN_GPIO_TBL,
	RIN_RTPORT_TBL,
};

/** @todo この初期化指定で全部NULLになるのか */
struct gpiod_line *g_gpiod_lines[GPIO_OFFSET_MAX] = {NULL};
/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

int gpio_read(const unsigned int offset) {
	int onoff = 0;	/** @note デフォルト値は0 */

	/* 存在するオフセット値の場合 */
	if (offset < GPIO_OFFSET_MAX) {
		/** @note エラー時はonoff値は変更されない(デフォルト値のまま) */
		com_gpio_read(CFG_APL_NAME, offset, &g_gpiod_lines[offset], &onoff);
		return onoff;
	}

	/** @note RTOS互換仕様 */
	switch (offset) {
		/* 1を返す */
		case GPIO_OFFSET_PRESERVED_1:
			onoff = 1;
			break;

		/* デフォルト値を返すため、何もしない */
		case GPIO_OFFSET_PRESERVED_0:
 			break;

		/* フェールセーフ */
		default:
			LOG_W("Unexpected parameter: %u\n", offset);
			break;
	}

	return onoff;
}

bool gpio_write(const unsigned int offset, const int onoff) {
	bool ok = true;
	/* 存在するオフセット値の場合 */
	if (offset < GPIO_OFFSET_MAX) {
		ok = com_gpio_write(CFG_APL_NAME, offset, &g_gpiod_lines[offset], onoff);
		return ok;
	}

	/** @note RTOS互換仕様 */
	switch (offset) {
		/* 何もせず、trueを返す */
		case GPIO_OFFSET_PRESERVED_1:
		case GPIO_OFFSET_PRESERVED_0:
 			break;

		/* フェールセーフ */
		default:
			ok = false;
			LOG_W("Unexpected parameter: %u\n", offset);
			break;
	}

	return ok;
}
