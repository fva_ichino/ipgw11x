//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	シリアルタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル定義
//=============================================================================

//=============================================================================
//==	外部変数定義
//=============================================================================
extern	const uint8_t		*debugCommans[];
extern	const uint8_t		*debugMoniCommans[];
//=============================================================================
//==	グローバル定義
//=============================================================================
static uint8_t				udp_moni_readData[UDP_MAX_LEN];
static uint32_t				udp_moni_readDataLen;
static MSG_BSC_FORMAT		udp_moni_writeData;
static uint32_t				udp_moni_writeDataLen;
//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	uint32_t	g_udpmonitorControl(MSG_BSC_FORMAT *mf,int32_t AorB);
static	uint32_t	g_udpmonitor(MSG_BSC_MONITOR_SG *monitor, uint8_t *data);
static	uint32_t	g_udpmonitor_moni(MSG_BSC_MONITOR_SG *monitor, uint8_t *data);
static	uint32_t	g_udpmonitor_write(MSG_BSC_MONITOR_SG *monitor, uint8_t *data);
static	union
		{
			uint8_t		cc[512];
			uint16_t	ss[256];
			uint32_t	ii[128];
		}	uu;

#define		MAX_DEB_WRITE_COMMAND			5
const uint8_t			*debugWriteCommans[] = {
								"    ",		//															Ｎｏ．０
								"SSCM",		//	ＳＤＩ_ＥＲＲ／ＣＲＣ／ＭＣ　エラー閾値　設定			Ｎｏ．１
								"GSCM",		//	ＳＤＩ_ＥＲＲ／ＣＲＣ／ＭＣ　エラー閾値　取得			Ｎｏ．２
								"STMP",		//	温度監視閾値　設定										Ｎｏ．３
								"GTMP",		//	温度監視閾値　取得										Ｎｏ．４
								"    "		//	ＥＮＤ
							};

// ボードステート（パンダの互換性）
// １以上で意味を持たす
static	uint32_t		mfvaBoardState = 0;
//=============================================================================
//==	制御ＵＤＰタスク関数
//=============================================================================
void	g_udp_monitor_task(void)
{
	LOG_THREAD();

	struct ip_addr	addr;
	UB				portId;
	UH				port;
	MSG_BSC_FORMAT	*mf;
	int32_t			msIndex;
	//-------------------------------------------------------------------------
	//--	初期化完了待ち
	//-------------------------------------------------------------------------
	while (1)
	{
		if (commonData.taskInit == 1)	break;
		tslp_tsk(10);
  	}
	//-------------------------------------------------------------------------
	//--	初期化
	//-------------------------------------------------------------------------
    udp_moni_raw_init_port0(&xipNet[0].xIpAddr,UDP_MONI_PORT_R_A);
    udp_moni_raw_init_port1(&xipNet[1].xIpAddr,UDP_MONI_PORT_R_B);
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	受信待ち
		//---------------------------------------------------------------------
		udp_moni_readDataLen = udp_moni_raw_recv_mnt(&udp_moni_readData[0],&portId,&addr,&port);
//		udp_moni_readDataLen = udp_raw_recv_mnt(&udp_moni_readData[0],&portId,&addr,&port);
		if (udp_moni_readDataLen == 0)
		{
			continue;
        }
		mf = (MSG_BSC_FORMAT*)&udp_moni_readData[0];

		//	管理制御棚判定用Ｉ／Ｆ作成
		msIndex			=	(port < UDP_MONI_PORT_R_B)?0:1;	//	これが正解！
		if (msIndex == -1)	continue;
		//---------------------------------------------------------------------
		//--	受信データ解析
		//---------------------------------------------------------------------
		if (udp_moni_readDataLen > (uint32_t)sizeof(MSG_BSC_HEADER))
		{
			udp_moni_writeDataLen	=	0;
			//-----------------------------------------------------------------
			//--	モニター電文（Ｄｅｂｕｇ）
			//-----------------------------------------------------------------
			if (mf->dataHead.itemCode == ITEM_CODE_UDPMONITOR)
			{
				udp_moni_writeDataLen	=	g_udpmonitorControl(mf,msIndex);
			}
			//-----------------------------------------------------------------
			//--	応答送信
			//-----------------------------------------------------------------
			if (udp_moni_writeDataLen != 0)
			{
				//-------------------------------------------------------------
				//--	応答ヘッダー作成
				//-------------------------------------------------------------
				udp_moni_writeData.head.len				=	g_endianConv(udp_moni_writeDataLen - sizeof(MSG_BSC_HEADER) - 2);
				udp_moni_writeData.head.id				=	0x00;
				udp_moni_writeData.head.srcCodeNo		=	myCode[msIndex];
				udp_moni_writeData.head.dstCodeNo		=	mf->head.srcCodeNo;
				udp_moni_writeData.head.total			=	0x01;
				udp_moni_writeData.head.no				=	0x01;
				udp_moni_writeData.head.sequenceNo		=	mf->head.sequenceNo;
				udp_moni_writeData.head.result			=	0x00;
				udp_moni_writeData.head.AUX[0]			=	0x00;
				udp_moni_writeData.head.AUX[1]			=	0x00;
				udp_moni_writeData.head.AUX[2]			=	0x00;
				udp_moni_writeData.head.AUX[3]			=	0x00;
				udp_moni_writeData.dataHead.itemCode	=	mf->dataHead.itemCode;
				udp_moni_writeData.dataHead.controlCode	=	CONT_CODE_UDPMONITOR_RES;
				//-------------------------------------------------------------
				//--	送信
				//-------------------------------------------------------------
				udp_moni_raw_send((uint8_t *)&udp_moni_writeData,udp_moni_writeDataLen,&addr,
							 ((msIndex == 0)?UDP_MONI_PORT_S_A:UDP_MONI_PORT_S_B),
							 portId,
							 ((msIndex == 0)?UDP_MONI_PORT_R_A:UDP_MONI_PORT_R_B),RAW_SEND_PRI);
			}
		}
	}
}
//=============================================================================
//==	システム制御電文関数
//=============================================================================
static	uint32_t	g_udpmonitorControl(MSG_BSC_FORMAT *mf,int32_t AorB)
{
	uint32_t	writeLen	=	0;
	uint32_t	dataLen = 0;
	uint32_t	len = 0;
	//-------------------------------------------------------------------------
	//--	デバックモニター
	//-------------------------------------------------------------------------
	if (udp_moni_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
								 (uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								 (uint32_t)sizeof(MSG_BSC_MONITOR_SG)))
	{
		memcpy(&udp_moni_writeData.data.dbgMonitor.command[0], &mf->data.dbgMonitor.command[0], 4);
		len = sizeof(udp_moni_writeData.data.dbgMonitor.data);
		memset(&udp_moni_writeData.data.dbgMonitor.data[0], 0x00, len);
		dataLen	=	g_udpmonitor(&mf->data.dbgMonitor, &udp_moni_writeData.data.dbgMonitor.data[0]);
		if (dataLen > 0)
		{
			udp_moni_writeData.data.dbgMonitor.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_MONITOR_SG) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_MONITOR_SG);
		}
	}
	return(writeLen);
}
//=============================================================================
//==	デバッグモニター関数
//=============================================================================
static	uint32_t	g_udpmonitor(MSG_BSC_MONITOR_SG *monitor, uint8_t *data)
{
	uint32_t	i,ii,iu,is,brev;
	uint32_t	debugCommandKind	=	0;
	uint32_t	moniReturnLen = 0;
	uint32_t	idx;

	//-------------------------------------------------------------------------
	//--	デバッグコマンド対応
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//--	ディップスイッチ　読み込み
	//-------------------------------------------------------------------------
	if (!memcmp(monitor->command,debugCommans[0],3))
	{
		data[0] = commonData.dipSw4;
		data[1] = commonData.bdIndex;
		data[2] = commonData.slotNo;
		data[3] = commonData.RID;
		data[4] = commonData.RA_OUT;
		return 5;
	}
	//-------------------------------------------------------------------------
	//--	ＸＩＬＩＮＸレジスタ読み込み
	//--	ＣＳＺ３レジスタ読み込み
	//--	コンフィグレーション
	//-------------------------------------------------------------------------
	debugCommandKind	=	0;
	for ( i = 1 ; i < MAX_DEB_COMMAND ; i++ )
	{
		if (!memcmp(monitor->command,debugCommans[i],4))
		{
			debugCommandKind	=	i;
			break;
		}
	}
	//-------------------------------------------------------------------------
	//--	パラメタ有コマンドはここではスルー
	//-------------------------------------------------------------------------
	if (debugCommandKind == 51 ||
		debugCommandKind == 62 || debugCommandKind == 63 ||
		debugCommandKind == 64 || debugCommandKind == 67 ||
		debugCommandKind == 68 || debugCommandKind == 69 ||
		debugCommandKind == 70)
	{
		return 0;
	}
	//-------------------------------------------------------------------------
	//--	ボードステート検索対応
	//-------------------------------------------------------------------------
	if (debugCommandKind == 0 && !memcmp(monitor->command,"sFVA",4))
	{
		memcpy(uu.cc,(char*)&mfvaBoardState,4);
		memcpy(data, &uu.cc[0], 4);
		return 4;
	}
	//-------------------------------------------------------------------------
	//--	デバッグコマンド受信
	//-------------------------------------------------------------------------
	if (debugCommandKind != 0)
	{
		//---------------------------------------------------------------------
		//--	ＦＰＧＡ　ＤＡＴＡ　取得
		//---------------------------------------------------------------------
		if (debugCommandKind == 81)
		{
			uint32_t	adr;
			uint32_t	off;
			uint32_t	num;
			memcpy((uint8_t *)&adr,&monitor->data[0],4);
			memcpy((uint8_t *)&off,&monitor->data[4],4);
			memcpy((uint8_t *)&num,&monitor->data[8],4);

			for (i = 0; (i < num) && (i < 5); i++)
			{
				uu.ii[0]	=	*((uint32_t *)adr);
				memcpy(&data[i * 4], &uu.cc[0], 4);
				adr += off;
			}
			return (4 * num);
		}
		//---------------------------------------------------------------------
		//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み
		//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み
		//--	ＤＥＢＵＧ　読み込み
		//--	ＲＸ　ＭＯＮＩＴＯＲ　１　読み込み
		//--	ＲＸ　ＭＯＮＩＴＯＲ　２　読み込み
		//--	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ　読み込み
		//--	ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ２　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 3 || debugCommandKind == 53)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				switch (debugCommandKind)
				{
				case	3:	uu.ii[i]	=	xiGetDebug(i);					break;
				case	53:	uu.ii[i]	=	xiGetVideoStreamMonitor2(i);	break;
				default:													break;
				}
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		else if (debugCommandKind == 1 || debugCommandKind == 2 ||
				  debugCommandKind == 4 || debugCommandKind == 5 ||
				  debugCommandKind == 8)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				switch (debugCommandKind)
				{
				case	1:	uu.ii[i]	=	xiGetUnitControl1(i);			break;
				case	2:	uu.ii[i]	=	xiGetUnitControl2(i);			break;
				case	4:	uu.ii[i]	=	xiGetRxMonitor1(i);				break;
				case	5:	uu.ii[i]	=	xiGetRxMonitor2(i);				break;
				case	8:	uu.ii[i]	=	xiGetVideoStreamMonitor(i);		break;
				default:													break;
				}
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＲＸ　ＭＯＮＩＴＯＲ　３　読み込み
		//--	ＴＸ　ＣＯＮＴＲＯＬ　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み
		//--	ＴＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み
		//--	ＲＸ　ＦＥＣ　ＣＡＬＣＵＬＡＴＯＲＯＬ　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　２　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　３　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　４　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 9  || debugCommandKind == 20 ||
				 debugCommandKind == 21 || debugCommandKind == 30 ||
				 debugCommandKind == 31 || debugCommandKind == 32 ||
				 debugCommandKind == 33 || debugCommandKind == 34 ||
				 debugCommandKind == 35)
		{
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					switch (debugCommandKind)
					{
					case	9:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl1(iu,ii);	break;
					case	20:	uu.ii[iu * 4 + ii]	=	xiGetTxFecCalculatorol(iu,ii);	break;
					case	21:	uu.ii[iu * 4 + ii]	=	xiGetRxFecCalculatorol(iu,ii);	break;
					case	30:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl11(iu,ii);	break;
					case	31:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl12(iu,ii);	break;
					case	32:	uu.ii[iu * 4 + ii]	=	xiGetHeaderAddControl13(iu,ii);	break;
					case	33:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl2(iu,ii);	break;
					case	34:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl3(iu,ii);	break;
					case	35:	uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl4(iu,ii);	break;
					default:															break;
					}
				}
			}
			memcpy(data, &uu.cc[0], 32);
			return 32;
		}
		else if (debugCommandKind == 6 || debugCommandKind == 7)
		{
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					switch (debugCommandKind)
					{
					case	6:	uu.ii[iu * 4 + ii]	=	xiGetRxMonitor3(iu,ii);			break;
					case	7:	uu.ii[iu * 4 + ii]	=	xiGetTxControl(iu,ii);			break;
					default:															break;
					}
				}
			}
			memcpy(data, &uu.cc[0], 32);
			return 32;
		}
		//---------------------------------------------------------------------
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　５　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　６　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　７　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　８　読み込み
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　９　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 36)
		{
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					uu.ii[iu * 4 + ii]	=	xiGetHeaderDelControl5(iu,ii);
				}
			}
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					uu.ii[8 + iu * 4 + ii]	=	xiGetHeaderDelControl6(iu,ii);
				}
			}
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					uu.ii[16 + iu * 4 + ii]	=	xiGetHeaderDelControl7(iu,ii);
				}
			}
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					uu.ii[24 + iu * 4 + ii]	=	xiGetHeaderDelControl8(iu,ii);
				}
			}
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					uu.ii[32 + iu * 4 + ii]	=	xiGetHeaderDelControl9(iu,ii);
				}
			}
			memcpy(data, &uu.cc[0], 160);
			return 160;
		}
		//---------------------------------------------------------------------
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み
		//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　４　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　５　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　６　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 10 || debugCommandKind == 11 ||
				 debugCommandKind == 12 || debugCommandKind == 13 ||
				 debugCommandKind == 14 || debugCommandKind == 15 ||
				 debugCommandKind == 16 || debugCommandKind == 17 ||
				 debugCommandKind == 18 || debugCommandKind == 25 ||
				 debugCommandKind == 26 || debugCommandKind == 27)
		{
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( iu = 0 ; iu < 2 ; iu++ )
				{
					for ( is = 0 ; is < 2 ; is++ )
					{
						for ( ii = 0 ; ii < 4 ; ii++ )
						{
							switch (debugCommandKind)
							{
							case	10:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl2(i,is,iu,ii);		break;
							case	11:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl3(i,is,iu,ii);		break;
							case	12:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl4(i,is,iu,ii);		break;
							case	13:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl5(i,is,iu,ii);		break;
							case	14:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl6(i,is,iu,ii);		break;
							case	15:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl7(i,is,iu,ii);		break;
							case	16:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl8(i,is,iu,ii);		break;
							case	17:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl9(i,is,iu,ii);		break;
							case	18:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetHeaderAddControl10(i,is,iu,ii);	break;
							case	25:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl4(i,is,iu,ii);			break;
							case	26:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl5(i,is,iu,ii);			break;
							case	27:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiGetDemuxControl6(i,is,iu,ii);			break;
							default:																						break;
							}
						}
					}
				}
			}
			memcpy(data, &uu.cc[0], 128);
			return 128;
		}
		//---------------------------------------------------------------------
		//--	ＨＥＡＤＥＲ　ＤＥＬ　ＣＯＮＴＲＯＬ　１　読み込み
		//--	ＩＮＦＯＲ　ＲＥＧ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 19 || debugCommandKind == 40)
		{
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( iu = 0 ; iu < 2 ; iu++ )
				{
					for ( ii = 0 ; ii < 4 ; ii++ )
					{
						switch (debugCommandKind)
						{
						case	19:	uu.ii[i * 8 + iu * 4 + ii]	=	xiGetHeaderDelControl1(i,iu,ii);		break;
						case	40:	uu.ii[i * 8 + iu * 4 + ii]	=	xiGetInfoReg((i * 8 + iu * 4 + ii));	break;
						default:																			break;
						}
					}
				}
			}
			memcpy(data, &uu.cc[0], 64);
			return 64;
		}
		//---------------------------------------------------------------------
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　１　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　２　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　３　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 22 || debugCommandKind == 23 ||
				 debugCommandKind == 24)
		{
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( iu = 0 ; iu < 2 ; iu++ )
				{
					switch (debugCommandKind)
					{
					case	22:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl1(i,iu);		break;
					case	23:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl2(i,iu);		break;
					case	24:	uu.ii[i * 2 + iu]	=	xiGetDemuxControl3(i,iu);		break;
					default:															break;
					}
				}
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　７　読み込み
		//--	ＤＥＭＵＸ　ＣＯＮＴＲＯＬ　８　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 28 || debugCommandKind == 29)
		{
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( iu = 0 ; iu < 2 ; iu++ )
				{
					for ( ii = 0 ; ii < 8 ; ii++ )
					{
						switch (debugCommandKind)
						{
						case	28:	uu.ii[i * 16 + iu * 8 + ii]	=	xiGetDemuxControl7(i,iu,ii);		break;
						case	29:	uu.ii[i * 16 + iu * 8 + ii]	=	xiGetDemuxControl8(i,iu,ii);		break;
						default:																		break;
						}
					}
				}
			}
			memcpy(data, &uu.cc[0], 128);
			return 128;
		}
		//---------------------------------------------------------------------
		//--	Ｌ３ＳＷＩＴＣＨ　１〜１４　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 37)
		{
			uu.ii[0]	=	xiGetL3SwitchControl1();
			uu.ii[1]	=	xiGetL3SwitchControl2();
			uu.ii[2]	=	xiGetL3SwitchControl3(0);
			uu.ii[3]	=	xiGetL3SwitchControl3(1);
			uu.ii[4]	=	xiGetL3SwitchControl4(0);
			uu.ii[5]	=	xiGetL3SwitchControl4(1);
			uu.ii[6]	=	xiGetL3SwitchControl5(0);
			uu.ii[7]	=	xiGetL3SwitchControl5(1);
			uu.ii[8]	=	xiGetL3SwitchControl6(0);
			uu.ii[9]	=	xiGetL3SwitchControl6(1);
			uu.ii[10]	=	xiGetL3SwitchControl7(0);
			uu.ii[11]	=	xiGetL3SwitchControl7(1);
			uu.ii[12]	=	xiGetL3SwitchControl8(0);
			uu.ii[13]	=	xiGetL3SwitchControl8(1);
			uu.ii[14]	=	xiGetL3SwitchControl9(0);
			uu.ii[15]	=	xiGetL3SwitchControl9(1);
			uu.ii[16]	=	xiGetL3SwitchControl10(0);
			uu.ii[17]	=	xiGetL3SwitchControl10(1);
			uu.ii[18]	=	xiGetL3SwitchControl11(0);
			uu.ii[19]	=	xiGetL3SwitchControl11(1);
			uu.ii[20]	=	xiGetL3SwitchControl12();
			uu.ii[21]	=	xiGetL3SwitchControl13(0);
			uu.ii[22]	=	xiGetL3SwitchControl13(1);
			uu.ii[23]	=	xiGetL3SwitchControl14();
			memcpy(data, &uu.cc[0], 96);
			return 96;
		}
		//---------------------------------------------------------------------
		//--	ＢＯＡＲＤ　ＴＥＭＰ　ＳＷＩＴＣＨ読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 38)
		{
			uu.ii[0]	=	xiGetBoardRevision();
			uu.ii[1]	=	xiGetTemperature();
			uu.ii[2]	=	xiGetDipSw();
			memcpy(data, &uu.cc[0], 12);
			return 12;
		}
		//---------------------------------------------------------------------
		//--	ＩＥＥＥ１５８８　１〜１９　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 39)
		{
			for ( i = 0 ; i < 19 ; i++ )
			{
				for ( ii = 0 ; ii < 2 ; ii++ )
				{
					uu.ii[i * 2 + ii]	=	xiGetRegister((0x18001800 + i * 4 + ii * 0x8000));
				}
			}
			memcpy(data, &uu.cc[0], 152);
			return 152;
		}
		//---------------------------------------------------------------------
		//--	ＳＴＡＴＵＳ　＆　ＴＥＭＰ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 41)
		{
			uu.ss[0]	=	csz3GetStatus();
			uu.ss[1]	=	csz3GetTemp();
			uu.ss[1]	=	((uu.ss[1] & 0xff00) >> 8);//	MSB 8ビットのみ
			uu.ss[2]	=	csz3GetDipSw7();
			uu.ss[2]	=	(uu.ss[2] & 0x00ff);
			memcpy(data, &uu.cc[0], 6);
			return 6;
		}
		//---------------------------------------------------------------------
		//--	ＡＤＣ　ＶＯＬＴＡＧＥ　ＶＡＬ　読み込み
		//--	ＡＤＣ　ＶＯＬＴＡＧＥ　ＳＥＴ　読み込み
		//--	ＳＲＧ　ＳＥＴ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 42 ||
				 debugCommandKind == 43 || debugCommandKind == 44)
		{
			switch (debugCommandKind)
			{
			case	42:	uu.ss[0]	=	csz3GetADCVoltageVal();	break;
			case	43:	uu.ss[0]	=	csz3GetADCVoltageSet();	break;
			case	44:	uu.ss[0]	=	csz3GetSrgSet();		break;
			default:											break;
			}
			memcpy(data, &uu.cc[0], 2);
			return 2;
		}
		//---------------------------------------------------------------------
		//--	ＳＤＩ　ＨＰＨＡＳＥ　読み込み
		//--	ＳＤＩ　ＶＰＨＡＳＥ　読み込み
		//--	ＳＤＩ　ＶＦＭＴ　読み込み
		//--	ＳＤＩ　ＳＴＡＴＵＳ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 45 || debugCommandKind == 46 ||
				 debugCommandKind == 47 || debugCommandKind == 48)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				switch (debugCommandKind)
				{
				case	45:	uu.ss[i]	=	csz3GetSdiHPhase(i);	break;
				case	46:	uu.ss[i]	=	csz3GetSdiVPhase(i);	break;
				case	47:	uu.ss[i]	=	csz3GetSdiVFmt(i);		break;
				case	48:	uu.ss[i]	=	csz3GetSdiStatus(i);	break;
				default:											break;
				}
			}
			memcpy(data, &uu.cc[0], 8);
			return 8;
		}
		//---------------------------------------------------------------------
		//--	ＴＥＳＴ　＆　ＴＸ＿ＭＯＤＥ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 49)
		{
			uu.ii[0]	=	csz3GetTest();
			for ( i = 0 ; i < 4 ; i++ )
			{
				uu.cc[4 + i]	=	(csz3GetSdiTxmode(i) & 0xff);
			}
			memcpy(data, &uu.cc[0], 8);
			return 8;
		}
		//---------------------------------------------------------------------
		//--	ＳＤＩ　ＰＳＥＴ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 52)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				uu.cc[i]	=	csz3GetSdiXpPsel(i);
			}
			memcpy(data, &uu.cc[0], 4);
			return 4;
		}
		//---------------------------------------------------------------------
		//--	ＸＰ　ＯＵＴ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 50)
		{
			for ( i = 0 ; i < 16 ; i++ )
			{
				uu.cc[i]	=	csz3GetXpOut(i);
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	アラームステータス　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 58)
		{
			g_alarmStatus2unitInf(uu.cc,0);
			memcpy(data, &uu.cc[0], 20);
			return 20;
		}
		//---------------------------------------------------------------------
		//--	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 59)
		{
			if (snmpTrapMoniWptr == snmpTrapMoniRptr)
			{
				memset(uu.cc,0,128);
			}
			else
			{
				memcpy(uu.cc,snmpTrapMoniBuf[snmpTrapMoniRptr],128);
				snmpTrapMoniRptr++;
				if (snmpTrapMoniRptr >= SNMP_TRAP_MONI_MAX)	snmpTrapMoniRptr	=	0;
			}
			memcpy(data, &uu.cc[0], 128);
			return 128;
		}
		//---------------------------------------------------------------------
		//--	ＧＰＩＯ読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 60)
		{
			uu.cc[0]	=	GPIO_READ(RIN_GPIO, PIN0B, 0xFF);
			uu.cc[1]	=	GPIO_READ(RIN_GPIO, PIN1B, 0xFF);
			uu.cc[2]	=	GPIO_READ(RIN_GPIO, PIN2B, 0xFF);
			uu.cc[3]	=	GPIO_READ(RIN_GPIO, PIN3B, 0xFF);
			uu.cc[4]	=	GPIO_READ(RIN_GPIO, PIN4B, 0xFF);
			uu.cc[5]	=	GPIO_READ(RIN_GPIO, PIN5B, 0xFF);
			uu.cc[6]	=	GPIO_READ(RIN_GPIO, PIN6B, 0xFF);
			uu.cc[7]	=	GPIO_READ(RIN_GPIO, PIN7B, 0xFF);
			uu.cc[8]	=	GPIO_READ(RIN_RTPORT, RPIN0B, 0xFF);
			uu.cc[9]	=	GPIO_READ(RIN_RTPORT, RPIN1B, 0xFF);
			uu.cc[10]	=	GPIO_READ(RIN_RTPORT, RPIN2B, 0xFF);
			uu.cc[11]	=	GPIO_READ(RIN_RTPORT, RPIN3B, 0xFF);
			memcpy(data, &uu.cc[0], 12);
			return 12;
		}
		//---------------------------------------------------------------------
		//--	ＣＰＵ　アドレス読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 61)
		{
			memcpy(&uu.cc[0],(uint8_t *)gNET_DEV[0].cfg.eth.mac,6);
			memcpy(&uu.cc[6],(uint8_t *)&gNET_ADR[0].ipaddr,4);
			memcpy(&uu.cc[10],(uint8_t *)&gNET_ADR[0].mask,4);
			memcpy(&uu.cc[14],(uint8_t *)&gNET_ADR[0].gateway,4);
			memcpy(&uu.cc[18],(uint8_t *)gNET_DEV[1].cfg.eth.mac,6);
			memcpy(&uu.cc[24],(uint8_t *)&gNET_ADR[1].ipaddr,4);
			memcpy(&uu.cc[28],(uint8_t *)&gNET_ADR[1].mask,4);
			memcpy(&uu.cc[32],(uint8_t *)&gNET_ADR[1].gateway,4);
			brev	=	xiGetBoardRevision();
			memcpy(&uu.cc[36],(uint8_t *)&brev,4);
			memcpy(data, &uu.cc[0], 40);
			return 40;
		}
		//---------------------------------------------------------------------
		//--	機器情報読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 65)
		{
			g_getFPGAVersion();
			memcpy(uu.cc,(uint8_t *)&modelInf[0],sizeof(MSG_BSC_MCH_DATA));
			memcpy(data, &uu.cc[0], 72);
			return 72;
		}
		//---------------------------------------------------------------------
		//--	デバッグ情報読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 66)
		{
			memcpy(uu.cc,(uint8_t *)&debugData,sizeof(DEBUG_INF_STR));
			memcpy(data, &uu.cc[0], 128);
			return 128;
		}
		//---------------------------------------------------------------------
		//--	ＧＰＩＯ読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 71)
		{
			/** @note ETHの状態は別で確認 */
			uu.ii[0]	=	0;
			uu.ii[1]	=	0;
			memcpy(data, &uu.cc[0], 8);
			return 8;
		}
		//---------------------------------------------------------------------
		//--	アドレス指定読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 67)
		{
			uint32_t	adr;
			uint8_t		tmp;
			memcpy((uint8_t *)&adr,&monitor->data[4],4);
			uu.ii[0]	=	*((uint32_t *)adr);
			tmp			=	uu.cc[0];
			uu.cc[0]	=	uu.cc[3];
			uu.cc[3]	=	tmp;
			tmp			=	uu.cc[1];
			uu.cc[1]	=	uu.cc[2];
			uu.cc[2]	=	tmp;
			memcpy(data, &uu.cc[0], 4);
			return 4;
		}
		//---------------------------------------------------------------------
		//--	ＤＯＷＮ　ＣＯＮＶＥＲＴＯＲ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 75)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetDownConvertor(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＧＡＭＭＡ　１　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 76)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetGamma1(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＧＡＭＭＡ　２　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 77)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetGamma2(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＦＳ　１　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 78)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetFs1(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＦＳ　２　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 79)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetFs2(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	ＰＡＹＬＯＡＤ　読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 80)
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
//				uu.ii[i]	=	xiGetPayload(i);
				uu.ii[i]	=	0;
			}
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	光モジュールメモリダンプA0H
		//---------------------------------------------------------------------
		else if (debugCommandKind == 73)
		{
            debugOptMemDump(monitor->data[0], 0, &uu.cc[0]);
			memcpy(data, &uu.cc[0], 256);
			return 256;
		}
		//---------------------------------------------------------------------
		//--	光モジュールメモリダンプA2H
		//---------------------------------------------------------------------
		else if (debugCommandKind == 74)
		{
            debugOptMemDump(monitor->data[0], 1, &uu.cc[0]);
			memcpy(data, &uu.cc[0], 256);
			return 256;
		}
		//---------------------------------------------------------------------
		//--	書き込み処理
		//---------------------------------------------------------------------
		//---------------------------------------------------------------------
		//--	疑似ＴＲＡＰ情報書き込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 82)
		{
			idx = 0;
			debug_alarm_pseudo.alaram_test_OnOff = monitor->data[idx];
			idx++;
			for ( i = 0 ; i < 4 ; i++ )
			{
				debug_alarm_pseudo.sdiVideoCrcErrOnOff[i] = monitor->data[idx];
				idx++;
			}
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					debug_alarm_pseudo.sdiVideoCrcErrRxOnOff[i][ii] = monitor->data[idx];
					idx++;
				}
			}
			for ( i = 0 ; i < 4 ; i++ )
			{
				debug_alarm_pseudo.ipMcMismatchRxOnOff[i] = monitor->data[idx];
				idx++;
			}
			uu.ii[i]	=	1;
			memcpy(data, &uu.cc[0], 16);
			return 16;
		}
		//---------------------------------------------------------------------
		//--	疑似ＴＲＡＰ情報読み込み
		//---------------------------------------------------------------------
		else if (debugCommandKind == 83)
		{
			idx = 0;
			data[idx] = debug_alarm_pseudo.alaram_test_OnOff;
			idx++;
			for ( i = 0 ; i < 4 ; i++ )
			{
				data[idx] = debug_alarm_pseudo.sdiVideoCrcErrOnOff[i];
				idx++;
			}
			for ( i = 0 ; i < 2 ; i++ )
			{
				for ( ii = 0 ; ii < 4 ; ii++ )
				{
					data[idx] = debug_alarm_pseudo.sdiVideoCrcErrRxOnOff[i][ii];
					idx++;
				}
			}
			for ( i = 0 ; i < 4 ; i++ )
			{
				data[idx] = debug_alarm_pseudo.ipMcMismatchRxOnOff[i];
				idx++;
			}
			return 17;
		}
		/*===================================================================*/
		//---------------------------------------------------------------------
		//--	設定関数
		//---------------------------------------------------------------------
		else if (debugCommandKind == 84)
		{
			moniReturnLen = g_udpmonitor_write(monitor, data);
			return moniReturnLen;
		}
		/*===================================================================*/
		/*===================================================================*/
		//---------------------------------------------------------------------
		//--	ＭＯＮＩは別関数
		//---------------------------------------------------------------------
		else if (debugCommandKind == 57)
		{
			moniReturnLen = g_udpmonitor_moni(monitor, data);
			return moniReturnLen;
		}
		/*===================================================================*/
	}
	return 0;
}
//=============================================================================
//==	ＭＯＮＩシリアルモニター用関数
//=============================================================================
static	uint32_t	g_udpmonitor_moni(MSG_BSC_MONITOR_SG *monitor, uint8_t *data)
{
	uint32_t	debugMoniCommandKind	=	0;
	int			i,ii,is,iu;
	//-------------------------------------------------------------------------
	//--	コマンド検索
	//-------------------------------------------------------------------------
	for ( i = 1 ; i < MAX_DEB_MONI_COMMAND ; i++ )
	{
		if (!memcmp(&monitor->data[0],debugMoniCommans[i],4))
		{
			debugMoniCommandKind	=	i;
			break;
		}
	}
	//-------------------------------------------------------------------------
	//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　１　読み込み
	//--	ＵＮＩＴ　ＣＯＮＴＲＯＬ　２　読み込み
	//--	ＤＥＢＵＧ　読み込み
	//--	ＶＩＤＥＯ　ＣＯＮＴ　読み込み
	//--	ＳＭＰＴＥ２０２２＿１＿２　読み込み
	//-------------------------------------------------------------------------
	if (debugMoniCommandKind == 1 || debugMoniCommandKind == 2 ||
		debugMoniCommandKind == 3 || debugMoniCommandKind == 5 ||
		debugMoniCommandKind == 11)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			switch (debugMoniCommandKind)
			{
			case	1:	uu.ii[i]	=	xiMoniGetUnitControl1(i);	break;
			case	2:	uu.ii[i]	=	xiMoniGetUnitControl2(i);	break;
			case	3:	uu.ii[i]	=	xiMoniGetDebug(i);			break;
			case	5:	uu.ii[i]	=	xiMoniGetVideoCont(i);		break;
			case	11:	uu.ii[i]	=	xiMoniGetSmpte2022_1_2(i);	break;
			}
		}
		memcpy(data, &uu.cc[0], 16);
		return 16;
	}
	//-------------------------------------------------------------------------
	//--	ＴＩＣＯ　ＥＮＣＯＤＥ　１〜４　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 4)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 4]		=	xiMoniGetTicoEncode1(i);
			uu.ii[i * 4 + 1]	=	xiMoniGetTicoEncode2(i);
			uu.ii[i * 4 + 2]	=	xiMoniGetTicoEncode3(i);
			uu.ii[i * 4 + 3]	=	xiMoniGetTicoEncode4(i);
		}
		memcpy(data, &uu.cc[0], 64);
		return 64;
	}
	//-------------------------------------------------------------------------
	//--	ＡＵＤＩＯ　ＰＥＳ　１、２　読み込み
	//--	ＡＵＤＩＯ　ＳＴＡＴＵＳ　１、２　読み込み
	//--	ＡＮＣ　ＣＯＮＴ　１、２　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 6 || debugMoniCommandKind == 7 ||
			 debugMoniCommandKind == 8)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			switch (debugMoniCommandKind)
			{
			case	6:	uu.ii[i * 2]		=	xiMoniGetAudioPes1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAudioPes2(i);
						break;
			case	7:	uu.ii[i * 2]		=	xiMoniGetAudioStatus1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAudioStatus2(i);
						break;
			case	8:	uu.ii[i * 2]		=	xiMoniGetAncCont1(i);
						uu.ii[i * 2 + 1]	=	xiMoniGetAncCont2(i);
						break;
			}
		}
		memcpy(data, &uu.cc[0], 32);
		return 32;
	}
	//-------------------------------------------------------------------------
	//--	ＡＮＣ　ＰＥＳ　ＩＮＦＯ１，２，３　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 9)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 3]		=	xiMoniGetAncPes1(i);
			uu.ii[i * 3 + 1]	=	xiMoniGetAncPes2(i);
			uu.ii[i * 3 + 2]	=	xiMoniGetAncData(i);
		}
		memcpy(data, &uu.cc[0], 48);
		return 48;
	}
	//-------------------------------------------------------------------------
	//--	ＰＩＤ　１〜５　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 10)
	{
		for ( i = 0 ; i < 4 ; i++ )
		{
			uu.ii[i * 5]		=	xiMoniGetPid1(i);
			uu.ii[i * 5 + 1]	=	xiMoniGetPid2(i);
			uu.ii[i * 5 + 2]	=	xiMoniGetPid3(i);
			uu.ii[i * 5 + 3]	=	xiMoniGetPid4(i);
			uu.ii[i * 5 + 4]	=	xiMoniGetPid5(i);
		}
		memcpy(data, &uu.cc[0], 80);
		return 80;
	}
	//-------------------------------------------------------------------------
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１１　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１２　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１３　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 12 || debugMoniCommandKind == 22 ||
			 debugMoniCommandKind == 23 || debugMoniCommandKind == 24)
	{
		for ( i = 0 ; i < 2 ; i++ )
		{
			for ( ii = 0 ; ii < 4 ; ii++ )
			{
				switch (debugMoniCommandKind)
				{
				case	12:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl1(i,ii);	break;
				case	22:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl11(i,ii);	break;
				case	23:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl12(i,ii);	break;
				case	24:	uu.ii[i * 4 + ii]	=	xiMoniGetHeaderAddControl13(i,ii);	break;
				}
			}
		}
		memcpy(data, &uu.cc[0], 32);
		return 32;
	}
	//-------------------------------------------------------------------------
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　２　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　３　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　４　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　５　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　６　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　７　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　８　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　９　読み込み
	//--	ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ　１０　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind >= 13 && debugMoniCommandKind <= 21)
	{
		for ( i = 0 ; i < 2 ; i++ )
		{
			for ( iu = 0 ; iu < 2 ; iu++ )
			{
				for ( is = 0 ; is < 2 ; is++ )
				{
					for ( ii = 0 ; ii < 4 ; ii++ )
					{
						switch (debugMoniCommandKind)
						{
						case	13:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl2(i,is,iu,ii);		break;
						case	14:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl3(i,is,iu,ii);		break;
						case	15:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl4(i,is,iu,ii);		break;
						case	16:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl5(i,is,iu,ii);		break;
						case	17:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl6(i,is,iu,ii);		break;
						case	18:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl7(i,is,iu,ii);		break;
						case	19:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl8(i,is,iu,ii);		break;
						case	20:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl9(i,is,iu,ii);		break;
						case	21:	uu.ii[i * 16 + iu * 8 + is * 4 + ii]	=	xiMoniGetHeaderAddControl10(i,is,iu,ii);	break;
						}
					}
				}
			}
		}
		memcpy(data, &uu.cc[0], 128);
		return 128;
	}

	return 0;
}
//=============================================================================
//==	デバッグモニターライト関数
//=============================================================================
static	uint32_t	g_udpmonitor_write(MSG_BSC_MONITOR_SG *monitor, uint8_t *data)
{
	uint32_t	debugMoniCommandKind	=	0;
	int			i;
	uint16_t	tmp16;
	//-------------------------------------------------------------------------
	//--	コマンド検索
	//-------------------------------------------------------------------------
	for ( i = 1 ; i < MAX_DEB_WRITE_COMMAND ; i++ )
	{
		if (!memcmp(&monitor->data[0],debugWriteCommans[i],4))
		{
			debugMoniCommandKind	=	i;
			break;
		}
	}
	//-------------------------------------------------------------------------
	//--	ＳＤＩ_ＥＲＲ／ＣＲＣ／ＭＣ 　エラー閾値　設定
	//-------------------------------------------------------------------------
	if (debugMoniCommandKind == 1)
	{
		memcpy((uint8_t *)&saveSdiIpMisInf.data, &monitor->data[4], (uint32_t)sizeof(SDIIPMISMATCH_STR));

		memcpy(data, &monitor->data[0], 4);
		commonData.writeSaveSdiIpMc = 1;
		return 4;
	}
	//-------------------------------------------------------------------------
	//--	ＳＤＩ_ＥＲＲ／ＣＲＣ／ＭＣ 　エラー閾値　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 2)
	{
		memcpy(&data[0], &monitor->data[0], 4);
		memcpy(&data[4], (uint8_t *)&saveSdiIpMisInf.data, (uint32_t)sizeof(SDIIPMISMATCH_STR));

		return (sizeof(SDIIPMISMATCH_STR) + 4);
	}
	//-------------------------------------------------------------------------
	//--	環境設定  書き込み
	//-------------------------------------------------------------------------
	if (debugMoniCommandKind == 3)
	{
		//	温度監視閾値
		memcpy((uint8_t *)&tmp16, &monitor->data[4], (uint32_t)sizeof(tmp16));
		saveSdiIpMisInf.data1.tempeThres	=	tmp16;
		TXRX_TEMP_DET	=	saveSdiIpMisInf.data1.tempeThres;
		//	折り返し有無
//		saveSdiIpMisInf.data1.orikaesiMode = monitor->data[6];

		memcpy(data, &monitor->data[0], 4);
		commonData.writeSaveSdiIpMc = 1;
		return 4;
	}
	//-------------------------------------------------------------------------
	//--	環境設定　読み込み
	//-------------------------------------------------------------------------
	else if (debugMoniCommandKind == 4)
	{
		//	温度監視閾値
		memcpy(&data[0], &monitor->data[0], 4);
		tmp16	=	saveSdiIpMisInf.data1.tempeThres;
		memcpy(&data[4], (uint8_t *)&tmp16, (uint32_t)sizeof(tmp16));
		//	折り返し有無
		data[6] = saveSdiIpMisInf.data1.orikaesiMode;

		return (sizeof(SDIIPMISMATCH_STR) + 4);
	}
	return 0;
}