//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	サイクルタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル定義
//=============================================================================

//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	void	alarmTrapCycle_TX(void);
static	void	alarmTrapCycle_RX(void);
//=============================================================================
//==	サイクルタスク関数
//=============================================================================
void	g_cycle_alarm_task(void)
{
	LOG_THREAD();

	uint32_t	i,ii;
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	１００ｍｓ周期処理
		//---------------------------------------------------------------------
		tslp_tsk(100);
		//---------------------------------------------------------------------
		//--	ＴＲＡＰ出力アラーム監視
		//---------------------------------------------------------------------
		if (alarmTrapDetectData.alarmDetectStartWait == 1)
		{
			//-----------------------------------------------------------------
			//　疑似TRAP発生有無（テスト用）
			//-----------------------------------------------------------------
			if (debug_alarm_pseudo.alaram_test_OnOff == 0x01)
			{
				for ( i = 0 ; i < 4 ; i++ )
				{
					if (debug_alarm_pseudo.sdiVideoCrcErrOnOff[i] == 0x01)
						alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_DETECT;	//	発生
					else
						alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_NON;		//	回復
				}
				for ( i = 0 ; i < 2 ; i++ )
				{
					for ( ii = 0 ; ii < 4 ; ii++ )
					{
						if (debug_alarm_pseudo.sdiVideoCrcErrRxOnOff[i][ii] == 0x01)
							alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_DETECT;	//	発生
						else
							alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_NON;		//	回復
					}
				}
				for ( i = 0 ; i < 4 ; i++ )
				{
					if (debug_alarm_pseudo.ipMcMismatchRxOnOff[i] == 0x01)
						alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_DETECT;	//	発生
					else
						alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_NON;		//	回復
				}
				continue;
			}
			//-----------------------------------------------------------------

			//	100ms固定に変更
//			commonData.almPeriodCount++;
//			if (commonData.almPeriodCount >= saveSdiIpMisInf.data.periodTime)
//			{
				commonData.almPeriodCount	=	0;
				if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	alarmTrapCycle_TX();
				if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	alarmTrapCycle_RX();
//			}
		}
	}
}

//=============================================================================
//==	ＴＸ　アラームトラップ検出
//=============================================================================
#define SDI_ERR_PACKE_THRESHOLD		500
static	void	alarmTrapCycle_TX(void)
{
	uint8_t		i;
	uint32_t	dat32a;
	uint32_t	dat32b;
	uint32_t	dat32c;
	uint32_t	dat32d;
	//-------------------------------------------------------------------------
	//--	ＳＤＩ　ＥＲＲ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		if ((i == 0 && !ALM_MASK_C_VIDEO1_CRCERR_FAIL) ||
			(i == 1 && !ALM_MASK_C_VIDEO2_CRCERR_FAIL) ||
			(i == 2 && !ALM_MASK_C_VIDEO3_CRCERR_FAIL) ||
			(i == 3 && !ALM_MASK_C_VIDEO4_CRCERR_FAIL))
		{
			if (saveSdiIpMisInf.data.sdiVideoFormatErrTxTime > 0)
			{
				//	出力フォーマット異常検出カウント
				dat32a	=	xiGetVideoStreamMonitor(i);
				if ((dat32a & 0x00080000) == 0x00000000)
				{
					commonData.txFormatNgCoun[i]++;
				}

				//	SDI ERR 監視時間経過?
				alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[i]++;
				if (alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[i] >= saveSdiIpMisInf.data.sdiVideoFormatErrTxTime)
				{
					//	監視時間クリア
					alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[i]	=	0;
					//	カウンタ差分判定
					if (commonData.formatNgCoun[i] >= commonData.txFormatNgCoun[i])
						dat32b	=	commonData.formatNgCoun[i] - commonData.txFormatNgCoun[i];
					else
						dat32b	=	commonData.txFormatNgCoun[i] - commonData.formatNgCoun[i];
					//	パケットカウンタ取得
					dat32c	=	xiGetHeaderDelControl2(0,i);
					dat32d	=	xiGetHeaderDelControl2(1,i);
					//	閾値越え かつ パケット閾値以上有り
					if ((dat32b >= (uint32_t)(saveSdiIpMisInf.data.sdiVideoFormatErrTxThres[i])) &&
						((dat32c >= SDI_ERR_PACKE_THRESHOLD) || (dat32d >= SDI_ERR_PACKE_THRESHOLD)))
					{
						//	SDI ERR	連続回数
						alarmTrapDetectData.sdiVideoFormatErrTx_Count[i]++;
						if (alarmTrapDetectData.sdiVideoFormatErrTx_Count[i] >= saveSdiIpMisInf.data.sdiVideoFormatErrTxCount)
						{
							//	発生
							alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_DETECT;
							//	カウンタクリア
							alarmTrapDetectData.sdiVideoFormatErrTx_Count[i]	=	0;
						}
					}
					else
					{
						//	回復
						alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_NON;
						//	カウンタクリア
						alarmTrapDetectData.sdiVideoFormatErrTx_Count[i]	=	0;
					}
					//	データ退避
					commonData.formatNgCoun[i]	=	commonData.txFormatNgCoun[i];
				}
			}
			else
			{
				//	回復
				alarmStatus.sdiVideoCrcErr[i]	=	ALM_STATUS_NON;
				//	カウンタクリア
				alarmTrapDetectData.sdiVideoFormatErrTx_Count[i]	=	0;
				//	データ退避クリア
				commonData.formatNgCoun[i]	=	0;
			}
		}
	}
}

//=============================================================================
//==	ＲＸ　アラームトラップ検出
//=============================================================================
static	void	alarmTrapCycle_RX(void)
{
	uint8_t		i,ii;
	uint32_t	dat32a;
	uint32_t	dat32b;
	uint32_t	ddiff;
	//-------------------------------------------------------------------------
	//--	ＩＰ　ＣＲＣＥＲＲ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < 4 ; ii++ )
		{
			if ((i == 0 && ii == 0 && !ALM_MASK_C_VIDEO1_CRCERR_A_FAIL) ||
				(i == 0 && ii == 1 && !ALM_MASK_C_VIDEO2_CRCERR_A_FAIL) ||
				(i == 0 && ii == 2 && !ALM_MASK_C_VIDEO3_CRCERR_A_FAIL) ||
				(i == 0 && ii == 3 && !ALM_MASK_C_VIDEO4_CRCERR_A_FAIL) ||
				(i == 1 && ii == 0 && !ALM_MASK_C_VIDEO1_CRCERR_B_FAIL) ||
				(i == 1 && ii == 1 && !ALM_MASK_C_VIDEO2_CRCERR_B_FAIL) ||
				(i == 1 && ii == 2 && !ALM_MASK_C_VIDEO3_CRCERR_B_FAIL) ||
				(i == 1 && ii == 3 && !ALM_MASK_C_VIDEO4_CRCERR_B_FAIL))
			{
				//	IP CRCERR 監視 かつ 折り返し？
				if ((saveSdiIpMisInf.data.sdiVideoCrcErrRxTime > 0) && (xiGetTxControlTxLoopMode(ii) == 0))
				{
					//	IP CRCERR 監視時間経過?
					alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[i][ii]++;
					if (alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[i][ii] >= saveSdiIpMisInf.data.sdiVideoCrcErrRxTime)
					{
						//	監視時間クリア
						alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[i][ii]	=	0;
						//	カウンタ差分判定
						dat32a	=	xiGetHeaderDelControl7(i,ii);
						if (commonData.crcLossCoun[i][ii] >= dat32a)
							dat32b	=	commonData.crcLossCoun[i][ii] - dat32a;
						else
							dat32b	=	dat32a - commonData.crcLossCoun[i][ii];
						//	閾値判定
						if (dat32b >= (uint32_t)(saveSdiIpMisInf.data.sdiVideoCrcErrRxThres[ii]))
						{
							//	IP CRCERR 連続回数
							alarmTrapDetectData.sdiVideoCrcErrRx_Count[i][ii]++;
							if (alarmTrapDetectData.sdiVideoCrcErrRx_Count[i][ii] >= saveSdiIpMisInf.data.sdiVideoCrcErrRxCount)
							{
								//	発生
								alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_DETECT;
								//	カウンタクリア
								alarmTrapDetectData.sdiVideoCrcErrRx_Count[i][ii]	=	0;
							}
						}
						else
						{
							//	回復
							alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_NON;
							//	カウンタクリア
							alarmTrapDetectData.sdiVideoCrcErrRx_Count[i][ii]	=	0;
						}
						//	データ退避
						commonData.crcLossCoun[i][ii]	=	dat32a;
					}
				}
				else
				{
					//	回復
					alarmStatus.sdiVideoCrcErrRx[i][ii]	=	ALM_STATUS_NON;
					//	カウンタクリア
					alarmTrapDetectData.sdiVideoCrcErrRx_Count[i][ii]	=	0;
					//	データ退避クリア
					commonData.crcLossCoun[i][ii]	=	0;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		if ((i == 0 && !ALM_MASK_C_IP_MC_MISMATCH1_FAIL) ||
			(i == 1 && !ALM_MASK_C_IP_MC_MISMATCH2_FAIL) ||
			(i == 2 && !ALM_MASK_C_IP_MC_MISMATCH3_FAIL) ||
			(i == 3 && !ALM_MASK_C_IP_MC_MISMATCH4_FAIL))
		{
			//	IP MISMATCH 監視 かつ 折り返し？
			if ((saveSdiIpMisInf.data.MismatchTime > 0) && (xiGetTxControlTxLoopMode(i) == 0))
			{
				//	IP MISMATCH 判定
				dat32a	=	xiGetHeaderDelControl2(0,i);
				dat32b	=	xiGetHeaderDelControl2(1,i);
				//	データ補正
				if (dat32a >= dat32b)	ddiff	=	dat32a - dat32b;
				else					ddiff	=	dat32b - dat32a;
				//	IP MISMATCH？				
				if (ddiff >= SDI_ERR_PACKE_THRESHOLD)
				{
					//	差異常カウントアップ
					alarmTrapDetectData.ipMcMismatchRxErr_Count[i]++;
				}
				//	IP MISMATCH	監視時間経過?
				alarmTrapDetectData.ipMcMismatchRxTime_Count[i]++;
				if (alarmTrapDetectData.ipMcMismatchRxTime_Count[i] >= saveSdiIpMisInf.data.MismatchTime)
				{
					//	監視時間クリア
					alarmTrapDetectData.ipMcMismatchRxTime_Count[i]	=	0;
					//	IP MISMATCH？				
					if (alarmTrapDetectData.ipMcMismatchRxErr_Count[i] >= saveSdiIpMisInf.data.MismatchThres[i])
					{
						//	IP MISMATCH	連続回数
						alarmTrapDetectData.ipMcMismatchRx_Count[i]++;
						if (alarmTrapDetectData.ipMcMismatchRx_Count[i] >= saveSdiIpMisInf.data.MismatchErrCount)
						{
							if (alarmStatus.ipMcMismatchRx[i] != ALM_STATUS_DETECT)
							{
								//	発生
								alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_DETECT;
							}
							//	カウンタクリア
							alarmTrapDetectData.ipMcMismatchRx_Count[i]	=	0;
						}
					}
					else
					{
						if (alarmStatus.ipMcMismatchRx[i] != ALM_STATUS_NON)
						{
							//	回復
							alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_NON;
						}
						//	カウンタクリア
						alarmTrapDetectData.ipMcMismatchRx_Count[i]	=	0;
					}
					//	差異常カウントクリア
					alarmTrapDetectData.ipMcMismatchRxErr_Count[i]	=	0;
				}
			}
			else
			{
				//	回復
				alarmStatus.ipMcMismatchRx[i]	=	ALM_STATUS_NON;
				//	カウンタクリア
				alarmTrapDetectData.ipMcMismatchRx_Count[i]	=	0;
				//	差異常カウントクリア
				alarmTrapDetectData.ipMcMismatchRxErr_Count[i]	=	0;
			}
		}
	}
}
