#include "IPGW11X_cfg.h"
#include <unistd.h> // sleep
#include <limits.h> // INT_MAX

int main(int argc, char *argv[]) {
	/** @note argv[0] = "/mnt/mmc/ipgw11x/ipgw11x"となる想定 */

	/***************************************************/
	/* g_main_task()の呼び出し前に初期化が必要な処理を行う */
	/***************************************************/

	/* SYSLOGにプロセス名を指定して初期化 */
	log_init(CFG_APL_NAME);
	/* 以降からSYSLOG出力が有効になる */
	LOG_D("main\n");

	/* スレッドテーブルを登録 */
	kernel_reg_thread_table(g_cfg_tskid_thread_manage_table);
	/* ミューテックスのテーブルを登録 */
	kernel_reg_mutex_table(g_cfg_semid_mutex_manage_table);
	/* セッティングのテーブルを登録 */
	setting_reg_table(g_cfg_setting_table);

	/* GPIO初期化 */
	GPIO_INIT();

	/*****************/
	/* 他スレッド起動 */
	/*****************/

	g_main_task();

	/******************************/
	/* メインスレッドは終了させない */
	/******************************/
	
	while (1) {
		sleep(INT_MAX);
	}

	/***********************************/
	/* 終了処理を行うべきだが、実装しない */
	/***********************************/

	LOG_E("Process Exit\n");

	/* SYSLOG終了 */
	// log_close();

	/* 想定外の終了だが、ステータスは0とする */
	return 0;
} 