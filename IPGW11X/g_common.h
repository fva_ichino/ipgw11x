//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	共通ヘッダーファイル
//=============================================================================
#include	<itron.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	"string.h"
#include	"kernel.h"
#include	"kernel_id.h"
#include	"net_hdr.h"
#include	"setting.h"
#include	"eeprom.h"
#include	"ethernetif.h"
#include	<time.h>
#include	"IPGW11X_ds100br.h"
#include	"IPGW11X_gpio.h"
#include	"log.h" // SYSLOG
#include	"dbg_timer.h" // DBG_TIMER_*

//=============================================================================
//==	  セッティング
//=============================================================================
/** @todo 削除予定 */
#if 0
	#define		SETTING_INIT_FLAG					0x5A
	#define		SETTING_BOARD_INIT_FLAG			0x89
#endif

#define		SETTING_DATA_CPU_VIDEO_ADR			0x00350000
#define		SETTING_DATA_ALARM_MASK_ADR		0x00360000
#define		SETTING_DATA_INDIVIDUAL_SET_ADR	0x00390000
#define		SETTING_DATA_DS100BR_REG			0x00370000
#define		SETTING_DATA_CSZ3_ADC_VOL			0x00380000
#define		SETTING_DATA_SYSTEM_AB_ADR			0x003A0000
#define		SETTING_DATA_DOWNCON_ADR			0x003B0000
#define		SETTING_DATA_SDIIPMIS_ADR			0x00320000

/** @todo 削除予定 */
#if 0
	#define		SETTING_PRG_BACKUP					0x003C0000	//	アップデートバックアップバッファ(0x003C0000〜0x003FFFFF)
	#define		SETTING_PRG_SECSIZE				0x00010000	//	64K
#endif
//=============================================================================
//==	EEPROM(I2C)
//=============================================================================
#define		I2C_EEPROM_CH				0	//	P60,P61

/** @todo 削除予定(FRONT未使用) */
#if 0
	#define		EEP_ADDRESS_FRONT			0
	#define		EEP_ADDRESS_REAR			1
	#define		EEP_ADDRESS					((commonData.dipSw2 & 0x08)?EEP_ADDRESS_FRONT:EEP_ADDRESS_REAR)
#endif

/** @note Linux仕様(リアユニットで固定) */
#define		EEP_ADDRESS_REAR			1
#define		EEP_ADDRESS					EEP_ADDRESS_REAR
#define		EEP_DATA_CPUA_ADR			0x00000000
//=============================================================================
//==	CSZ3(I2C)
//=============================================================================
#define		I2C_DS100BR_CH				1	//	RP0,RP1
#define		I2C_DS100BR_SLAVE_ADR		0x58
//=============================================================================
//==	UART
//=============================================================================
#define		UART_USB_CH					1
#define		UART_485_CH					0
//=============================================================================
//==	デバイス番号
//=============================================================================
#define		NET_DEVICE_NUM_A			1
#define		NET_DEVICE_NUM_B			2
//=============================================================================
//==	ＬＥＤ番号定義
//=============================================================================
#define		LED_NO_SDI_TOP				0
#define		LED_NO_REF_IN				4
#define		LED_NO_TX					5
#define		LED_NO_RX					6
//#define		LED_NO_USB_SER				7
//#define		LED_NO_485					8
#define		LED_NO_UDP_CNT				11
#define		LED_NO_MISSMATCH			12
#define		LED_NO_TOT_ALM				13
//=============================================================================
//==	TX RX定義
//=============================================================================
#define		HDIPTX						0
#define		HDIPRX						1
#define		HDIPTXRX					2
//=============================================================================
//==	基板種類
//=============================================================================
#define		BK_101A						0
#define		BK_101B						1
#define		BK_101C						2
#define		BK_102A						3
#define		BK_103A						4
#define		BK_104A						5
#define		BK_205A						6
#define		BK_206A						7
#define		BK_207A						8
#define		BK_208A						9
#define		BK_405A						10
#define		BK_406A						11
#define		BK_407A						12
#define		BK_408A						13
#define		BK_ICF						50
//=============================================================================
//==	DONE信号定義
//=============================================================================
#define		DONE_101					0x30
#define		DONE_102					0x20
//=============================================================================
//==	設定経路定義
//=============================================================================
//#define		SETTING_IN_BAND				0
//#define		SETTING_485					1
//=============================================================================
//==	ビデオフォーマット定義
//=============================================================================
#define		VFMT_3G						0
#define		VFMT_SD						1
#define		VFMT_HD1					2
#define		VFMT_HD2					3
//=============================================================================
//==	初期化時ＵＮＩＴ　ＣＯＮＴＲＯＬ１、２の値
//==		ＵＮＩＴ　ＣＯＮＴＲＯＬ１の上位と
//==		ＵＮＩＴ　ＣＯＮＴＲＯＬ２の下位は同じ値
//=============================================================================
#define		UC_INIT_VAL_TX				(uint32_t)0x34d5
#define		UC_INIT_VAL_TX_EX			(uint32_t)0x04d5	//	VRT
#define		UC_INIT_VAL_RX				(uint32_t)0xcb2A
#define		UC_INIT_ENB					(uint32_t)0x000A
#define		UC_INIT_VAL_TX_CHG			(uint32_t)0x0000
#define		UC_INIT_VAL_RX_CHG			(uint32_t)0x0000
#define		TX_CONT_TEST_3G				(uint16_t)0x0246
#define		SRG_TEST_3G					(uint16_t)0x0020
#define		TX_CONT_TEST_HD				(uint16_t)0x0224
#define		SRG_TEST_HD					(uint16_t)0x0000
#define		UC1_INI_1					(uint32_t)0x00030000
#define		UC1_INI_2					(uint32_t)0x00000FFC
//=============================================================================
//==	ＥＥＰＲＯＭ　ＩＮＨＩＢＩＴ
//==	ＳＷ１２　ＥＮＡＢＬＥ
//=============================================================================

/** @todo 削除予定(Dipsw2未実装) */
#if 0
	#define		EEPROM_NOT_INHIBIT			(commonData.dipSw2 & 0x04)
#endif

/** @note Linux仕様(書込み許可で固定) */
#define		EEPROM_NOT_INHIBIT			1
//=============================================================================
//==	ＭＴＸ切替時　ＵＮＩＴ　ＣＯＮＴＲＯＬ２　ビット操作定義
//=============================================================================
#define		UC2_DEMUX_B_UPDATE(a)				xiBitSetUnitControl2(a,15)
#define		UC2_DEMUX_A_UPDATE(a)				xiBitSetUnitControl2(a,14)
#define		UC2_MUX_B_UPDATE(a)					xiBitSetUnitControl2(a,13)
#define		UC2_MUX_A_UPDATE(a)					xiBitSetUnitControl2(a,12)
#define		UC2_RX_FEC_UPDATE(a)				xiBitSetUnitControl2(a,11)
#define		UC2_TX_EFC_UPDATE(a)				xiBitSetUnitControl2(a,10)
#define		UC2_UDPIP_HEADER_DEL_B_UPDATE(a)	xiBitSetUnitControl2(a,9)
#define		UC2_UDPIP_HEADER_DEL_A_UPDATE(a)	xiBitSetUnitControl2(a,8)
#define		UC2_UDPIP_HEADER_ADD_B_UPDATE(a)	xiBitSetUnitControl2(a,7)
#define		UC2_UDPIP_HEADER_ADD_A_UPDATE(a)	xiBitSetUnitControl2(a,6)
#define		UC2_RTP_HEADER_DEL_UPDATE(a)		xiBitSetUnitControl2(a,5)
#define		UC2_RTP_HEADER_ADD_UPDATE(a)		xiBitSetUnitControl2(a,4)
#define		UC2_STREAM_VIDEO_OUT_UPDATE(a)		xiBitSetUnitControl2(a,3)
#define		UC2_STREAM_VIDEI_IN_UPDATE(a)		xiBitSetUnitControl2(a,2)
#define		UC2_TRIPLE_RATE_SDI_TX_UPDATE(a)	xiBitSetUnitControl2(a,1)
#define		UC2_TRIPLE_RATE_SDI_RX_UPDATE(a)	xiBitSetUnitControl2(a,0)
//=============================================================================
//==	最大、最小値定義
//=============================================================================
#define		BUSNO_MAX					4
#define		FPGA_DONE_FAIL_WAIT			5000	//	ＦＰＧＡ　ＤＯＮＥ不正待ち時間（ｍｓ）
#define		BOARD_SERIAL_NO_LEN			32
#define		FREERUN_CAL_BUF_MAX			10
#define		SNMP_SEND_TMO				5000
//#define		SNMP_TRAP_MONI_MAX			32
#define		SNMP_TRAP_MONI_MAX			128
#define		RAW_SEND_PRI				saveCpuVideoLayer.data.cntTOS
#define		UDP_MAX_LEN					1472
#define		TEMP_MOV_AVR				40		//	温度移動平均個数
//=============================================================================
//==	ポート番号定義
//=============================================================================
#define		UDP_PORT_R_A				60000	//	ＵＤＰポート（受）
#define		UDP_PORT_S_A				60010	//	ＵＤＰポート（送）
#define		UDP_PORT_R_B				60100	//	ＵＤＰポート（受）
#define		UDP_PORT_S_B				60110	//	ＵＤＰポート（送）
// MONITOR
#define		UDP_MONI_PORT_R_A			61000	//	ＵＤＰモニター(DBG)ポート（受）
#define		UDP_MONI_PORT_S_A			61010	//	ＵＤＰモニター(DBG)ポート（送）
#define		UDP_MONI_PORT_R_B			61100	//	ＵＤＰモニター(DBG)ポート（受）
#define		UDP_MONI_PORT_S_B			61110	//	ＵＤＰモニター(DBG)ポート（送）
//=============================================================================
//==	ＳＮＭＰポート番号定義
//=============================================================================
#define		SNMP_PORT_R					161		//	ＳＮＭＰポート（受）
#define		SNMP_PORT_S					162		//	ＳＮＭＰポート（送）
//=============================================================================
//==	マルチキャストＭＡＣアドレス
//=============================================================================
#define		MC_MAC_AD1					0x01
#define		MC_MAC_AD2					0x00
#define		MC_MAC_AD3					0x5E
//=============================================================================
//==	サブネットマスク＆ゲートウェイ
//=============================================================================
#define		TXRX_SUBNET_MASK			0xFFFF0000	//	255.255.0.0
#define		INPUT_LIST_GATEWAY			0xFE000101	//	239.0.1.1
//=============================================================================
//==	通信コード
//=============================================================================
#define		COM_CODE_DUM				0xFF
#define		COM_CODE_STX				0x02
#define		COM_CODE_ETX				0x03
//=============================================================================
//==	マジックナンバー
//=============================================================================
#define		MAGICNO_FPGA1				0x12345678
#define		MAGICNO_FPGA2_0				0xAA
#define		MAGICNO_FPGA2_1				0x55
//=============================================================================
//==	ＩＰＧ、ＩＣＦ共通ヘッダーファイル
//=============================================================================
#include	"g_ipgicfcommon.h"
//=============================================================================
//==	項目コード定義（インバンド用）
//=============================================================================
#define		ITEM_CODE_SYSTEM_CONTROL	0x00	//	システム制御
#define		ITEM_CODE_MTX_CONTROL		0x10	//	ＭＴＸ制御
//=============================================================================
//==	制御コード定義（インバンド用）
//=============================================================================
//--	システム制御コマンド
#define		CONT_CODE_HEALTH_REQ		0x72	//	ヘルスチェック
#define		CONT_CODE_INF_REQ			0x74	//	機器情報要求
#define		CONT_CODE_CONNECT_REQ		0x78	//	接続要求
//--	ＭＴＸ制御コマンド
#define		CONT_CODE_MTX_CHG_REQ_IPRX	0x21	//	ＭＴＸ切替要求
#define		CONT_CODE_MTX_CHG_NOTI		0x23	//	ＭＴＸ切替通知
#define		CONT_CODE_VRT_CHG_REQ_IPTX	0x24	//	ＶＲＴ切替要求
#define		CONT_CODE_VRT_CHG_NOTI		0x25	//	ＶＲＴ切替通知
#define		CONT_CODE_RX_SDI_OUT		0x26	//	ＳＤＩ出力映像系統情報
//=============================================================================
//==	アラーム状態定義
//=============================================================================
#define		ALM_STATUS_NON				0
#define		ALM_STATUS_DETECT			1
#define		ALM_STATUS_INIT				0xFF
//=============================================================================
//==	アラームマスクビット定義
//=============================================================================
//	P-BIT
#define		ALM_MASK_P_CPU_WAKEUP_FAIL			(saveAlarmMask.data.P_BIT[0] & 0x80)
#define		ALM_MASK_P_FPGA1_WAKEUP_FAIL		(saveAlarmMask.data.P_BIT[0] & 0x40)
#define		ALM_MASK_P_FPGA1_MAGICNO_FAIL		(saveAlarmMask.data.P_BIT[0] & 0x20)
#define		ALM_MASK_P_FPGA2_WAKEUP_FAIL		(saveAlarmMask.data.P_BIT[0] & 0x10)
#define		ALM_MASK_P_FPGA2_MAGICNO_FAIL		(saveAlarmMask.data.P_BIT[0] & 0x08)
#define		ALM_MASK_P_FPGA3_WAKEUP_FAIL		(saveAlarmMask.data.P_BIT[0] & 0x04)
#define		ALM_MASK_P_TX_FAULT_A_FAIL			(saveAlarmMask.data.P_BIT[0] & 0x02)
#define		ALM_MASK_P_TX_FAULT_B_FAIL			(saveAlarmMask.data.P_BIT[0] & 0x01)
#define		ALM_MASK_P_RX_LOSS_A_FAIL			(saveAlarmMask.data.P_BIT[1] & 0x80)
#define		ALM_MASK_P_RX_LOSS_B_FAIL			(saveAlarmMask.data.P_BIT[1] & 0x40)
#define		ALM_MASK_P_REAR_BOARD_MISSMATCH		(saveAlarmMask.data.P_BIT[1] & 0x20)
#define		ALM_MASK_P_DIP_SWITCH_MISSMATCH		(saveAlarmMask.data.P_BIT[1] & 0x10)
//	C-BIT
#define		ALM_MASK_C_TX_FAULT_A_FAIL			(saveAlarmMask.data.C_BIT[0] & 0x80)
#define		ALM_MASK_C_TX_FAULT_B_FAIL			(saveAlarmMask.data.C_BIT[0] & 0x40)
#define		ALM_MASK_C_RX_LOSS_A_FAIL			(saveAlarmMask.data.C_BIT[0] & 0x20)
#define		ALM_MASK_C_RX_LOSS_B_FAIL			(saveAlarmMask.data.C_BIT[0] & 0x10)
#define		ALM_MASK_C_VIDEO1_STATUS_FAIL		(saveAlarmMask.data.C_BIT[0] & 0x08)
#define		ALM_MASK_C_VIDEO1_CRCERR_FAIL		(saveAlarmMask.data.C_BIT[0] & 0x04)
#define		ALM_MASK_C_VIDEO2_STATUS_FAIL		(saveAlarmMask.data.C_BIT[0] & 0x02)
#define		ALM_MASK_C_VIDEO2_CRCERR_FAIL		(saveAlarmMask.data.C_BIT[0] & 0x01)
#define		ALM_MASK_C_VIDEO3_STATUS_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x80)
#define		ALM_MASK_C_VIDEO3_CRCERR_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x40)
#define		ALM_MASK_C_VIDEO4_STATUS_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x20)
#define		ALM_MASK_C_VIDEO4_CRCERR_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x10)
#define		ALM_MASK_C_RTP1_FIFO_EMPTY_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x04)
#define		ALM_MASK_C_RTP2_FIFO_EMPTY_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x02)
#define		ALM_MASK_C_RTP3_FIFO_EMPTY_FAIL		(saveAlarmMask.data.C_BIT[1] & 0x01)
#define		ALM_MASK_C_RTP4_FIFO_EMPTY_FAIL		(saveAlarmMask.data.C_BIT[2] & 0x80)
#define		ALM_MASK_C_RTP1_FIFO_FULL_FAIL		(saveAlarmMask.data.C_BIT[2] & 0x40)
#define		ALM_MASK_C_RTP2_FIFO_FULL_FAIL		(saveAlarmMask.data.C_BIT[2] & 0x20)
#define		ALM_MASK_C_RTP3_FIFO_FULL_FAIL		(saveAlarmMask.data.C_BIT[2] & 0x10)
#define		ALM_MASK_C_RTP4_FIFO_FULL_FAIL		(saveAlarmMask.data.C_BIT[2] & 0x08)
#define		ALM_MASK_C_UDP_FIFO_EMPTY1_A_FAIL	(saveAlarmMask.data.C_BIT[2] & 0x04)
#define		ALM_MASK_C_UDP_FIFO_EMPTY2_A_FAIL	(saveAlarmMask.data.C_BIT[2] & 0x02)
#define		ALM_MASK_C_UDP_FIFO_EMPTY3_A_FAIL	(saveAlarmMask.data.C_BIT[2] & 0x01)
#define		ALM_MASK_C_UDP_FIFO_EMPTY4_A_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x80)
#define		ALM_MASK_C_UDP_FIFO_EMPTY1_B_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x40)
#define		ALM_MASK_C_UDP_FIFO_EMPTY2_B_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x20)
#define		ALM_MASK_C_UDP_FIFO_EMPTY3_B_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x10)
#define		ALM_MASK_C_UDP_FIFO_EMPTY4_B_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x08)
#define		ALM_MASK_C_UDP_FIFO_FULL1_A_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x04)
#define		ALM_MASK_C_UDP_FIFO_FULL2_A_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x02)
#define		ALM_MASK_C_UDP_FIFO_FULL3_A_FAIL	(saveAlarmMask.data.C_BIT[3] & 0x01)
#define		ALM_MASK_C_UDP_FIFO_FULL4_A_FAIL	(saveAlarmMask.data.C_BIT[4] & 0x80)
#define		ALM_MASK_C_UDP_FIFO_FULL1_B_FAIL	(saveAlarmMask.data.C_BIT[4] & 0x40)
#define		ALM_MASK_C_UDP_FIFO_FULL2_B_FAIL	(saveAlarmMask.data.C_BIT[4] & 0x20)
#define		ALM_MASK_C_UDP_FIFO_FULL3_B_FAIL	(saveAlarmMask.data.C_BIT[4] & 0x10)
#define		ALM_MASK_C_UDP_FIFO_FULL4_B_FAIL	(saveAlarmMask.data.C_BIT[4] & 0x08)
#define		ALM_MASK_C_PACKET_COUNT1_A_FAIL		(saveAlarmMask.data.C_BIT[4] & 0x04)
#define		ALM_MASK_C_PACKET_COUNT2_A_FAIL		(saveAlarmMask.data.C_BIT[4] & 0x02)
#define		ALM_MASK_C_PACKET_COUNT3_A_FAIL		(saveAlarmMask.data.C_BIT[4] & 0x01)
#define		ALM_MASK_C_PACKET_COUNT4_A_FAIL		(saveAlarmMask.data.C_BIT[5] & 0x80)
#define		ALM_MASK_C_PACKET_COUNT1_B_FAIL		(saveAlarmMask.data.C_BIT[5] & 0x40)
#define		ALM_MASK_C_PACKET_COUNT2_B_FAIL		(saveAlarmMask.data.C_BIT[5] & 0x20)
#define		ALM_MASK_C_PACKET_COUNT3_B_FAIL		(saveAlarmMask.data.C_BIT[5] & 0x10)
#define		ALM_MASK_C_PACKET_COUNT4_B_FAIL		(saveAlarmMask.data.C_BIT[5] & 0x08)
#define		ALM_MASK_C_REF_IN_FAIL				(saveAlarmMask.data.C_BIT[5] & 0x04)
#define		ALM_MASK_C_PS_STATUS_FAIL			(saveAlarmMask.data.C_BIT[5] & 0x02)
#define		ALM_MASK_C_TEMP_FAIL				(saveAlarmMask.data.C_BIT[5] & 0x01)
#define		ALM_MASK_C_1G_LINK_DOWN_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x80)
//	追加
#define		ALM_MASK_C_VIDEO1_CRCERR_A_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x40)
#define		ALM_MASK_C_VIDEO2_CRCERR_A_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x20)
#define		ALM_MASK_C_VIDEO3_CRCERR_A_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x10)
#define		ALM_MASK_C_VIDEO4_CRCERR_A_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x08)
#define		ALM_MASK_C_VIDEO1_CRCERR_B_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x04)
#define		ALM_MASK_C_VIDEO2_CRCERR_B_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x02)
#define		ALM_MASK_C_VIDEO3_CRCERR_B_FAIL		(saveAlarmMask.data.C_BIT[6] & 0x01)
#define		ALM_MASK_C_VIDEO4_CRCERR_B_FAIL		(saveAlarmMask.data.C_BIT[7] & 0x80)
#define		ALM_MASK_C_IP_MC_MISMATCH1_FAIL		(saveAlarmMask.data.C_BIT[7] & 0x40)
#define		ALM_MASK_C_IP_MC_MISMATCH2_FAIL		(saveAlarmMask.data.C_BIT[7] & 0x20)
#define		ALM_MASK_C_IP_MC_MISMATCH3_FAIL		(saveAlarmMask.data.C_BIT[7] & 0x10)
#define		ALM_MASK_C_IP_MC_MISMATCH4_FAIL		(saveAlarmMask.data.C_BIT[7] & 0x08)
//=============================================================================
//==	モニターコマンド数
//=============================================================================
#define		MAX_DEB_COMMAND			85
#define		MAX_DEB_MONI_COMMAND	25
//=============================================================================
//==	構造体定義
//=============================================================================
#pragma pack(1)
//-----------------------------------------------------------------------------
//--	後部基板ＥＥＰＲＯＭ　ＭＡＣアドレス情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			cpuMacAddress[2][6];			//	ＣＰＵ：ＭＡＣアドレス
	uint8_t			videoMacAddress[2][6];			//	ＶＩＤＥＯ：ＭＡＣアドレス
}	PHYSICAL_LAYER_EEPROM_STR;
typedef	struct
{
	PHYSICAL_LAYER_EEPROM_STR	data;
	uint8_t						csm;				//	チェックサム
}	SAVE_PHYSICAL_LAYER_EEPROM_STR;
//-----------------------------------------------------------------------------
//--	ＣＰＵ／ＶＩＤＥＯレイヤ保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	CPU_VIDEO_LAYER_STR			data;
	uint8_t						csm;				//	チェックサム
}	SAVE_CPU_VIDEO_LAYER_STR;
//-----------------------------------------------------------------------------
//--	アラームマスク保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	ALARM_MASK_STR				data;
	uint8_t						csm;				//	チェックサム
}	SAVE_ALARM_MASK_STR;
//-----------------------------------------------------------------------------
//--	個別設定情報
//-----------------------------------------------------------------------------
typedef	struct
{
	INDIVIDUAL_SET_STR			data;
	uint8_t						csm;				//	チェックサム
}	SAVE_INDIVIDUAL_SET_STR;
//-----------------------------------------------------------------------------
//--	ＤＳ１００ＢＲレジスタ保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			SlaveRegisterControl;
	uint8_t			DigitalResetAndControl;
	uint8_t			PinOverrride;
	uint8_t			CHA_EQSetting;
	uint8_t			CHA_Control1;
	uint8_t			CHA_Control2;
	uint8_t			CHB_EQSetting;
	uint8_t			CHB_Control1;
	uint8_t			CHB_Control2;
	uint8_t			CHA_VODControl;
	uint8_t			CHB_VODControl;
	uint8_t			rsv[52];
}	DS100BR_REG_STR;
typedef	struct
{
	DS100BR_REG_STR				data;
	uint8_t						csm;				//	チェックサム
}	SAVE_DS100BR_REG_STR;
//-----------------------------------------------------------------------------
//--	ＣＳＺ３保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			PresetMode[4];
	uint16_t		HPhase[4];
	uint16_t		VPhase[4];
}	FSINF_STR;
typedef	struct
{
	uint16_t		ADC_Vol;
	FSINF_STR		fsInf;
	uint8_t			fsTxMode[4];
	uint8_t			rsv[37];
}	CSZ3_REG_STR;
typedef	struct
{
	CSZ3_REG_STR				data;
	uint8_t						csm;				//	チェックサム
}	SAVE_CSZ3_REG_STR;
//-----------------------------------------------------------------------------
//--	系統情報保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	SYSTEM_AB_STR				data;
	uint8_t						csm;				//	チェックサム
}	SAVE_SYSTEM_AB;
//-----------------------------------------------------------------------------
//--	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t			periodTime;
	uint16_t			sdiVideoFormatErrTxTime;		//	ＳＤＩＥＲＲ監視時間
	uint16_t			sdiVideoFormatErrTxThres[4];	//	ＳＤＩＥＲＲ閾値
	uint16_t			sdiVideoFormatErrTxCount;		//	ＳＤＩＥＲＲ連続回数
	uint16_t			sdiVideoCrcErrRxTime;			//	ＩＰＥＲＲ監視時間
	uint16_t			sdiVideoCrcErrRxThres[4];		//	ＩＰＥＲＲ閾値
	uint16_t			sdiVideoCrcErrRxCount;			//	ＩＰＥＲＲ連続回数
	uint16_t			MismatchTime;					//	ＩＰＭＩＳＭＡＴＣＨ監視時間
	uint16_t			MismatchThres[4];				//	ＩＰＭＩＳＭＡＴＣＨ閾値
	uint16_t			MismatchErrCount;				//	ＩＰＭＩＳＭＡＴＣＨ連続回数
}	SDIIPMISMATCH_STR;

typedef	struct
{
	uint16_t					tempeThres;				//	温度監視閾値
	uint8_t						orikaesiMode;			//	折り返し有無
	uint8_t						aux[211];				//	リザーブ
}	OTHER_INFO_STR;
//-----------------------------------------------------------------------------
//--	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御保存情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t					mode;					//	設定可否
	SDIIPMISMATCH_STR			data;					//	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報
	OTHER_INFO_STR				data1;					//	温度監視閾値・他
	uint8_t						csm2;					//	チェックサム
	uint8_t						csm1;					//	チェックサム
}	SAVE_SDIIPMISMATCH_STR;
//-----------------------------------------------------------------------------
//--	ＲＳ４８５用
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//--	電文別定義（フッターを含めること）
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//--	後部ＥＥＰＲＯＭ保存情報読込応答、書込要求共通
//-----------------------------------------------------------------------------
typedef	struct
{
	PHYSICAL_LAYER_EEPROM_STR	data;
	MSG485_FOOTER				foot;
}	MSG485_PL_EEPROM_CMD_RES;
//-----------------------------------------------------------------------------
//--	ＤＳ１００ＢＲレジスタ保存情報読込応答、書込要求共通
//-----------------------------------------------------------------------------
typedef	struct
{
	DS100BR_REG_STR				data;
	MSG485_FOOTER				foot;
}	MSG485_DS100BR_REG_CMD_RES;
//-----------------------------------------------------------------------------
//--	ＦＳ機能情報読込応答、書込要求共通
//-----------------------------------------------------------------------------
typedef	struct
{
	FSINF_STR					data;
	MSG485_FOOTER				foot;
}	MSG485_FSINF_REG_CMD_RES;
//-----------------------------------------------------------------------------
//--	フリーラン周波数ＡＤＣ　ＶＯＬ記録開始応答
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t						result;
	MSG485_FOOTER				foot;
}	MSG485_FREERUN_CAL_RES;
//-----------------------------------------------------------------------------
//--	ＦＳ：ＴＸ＿ＭＯＤＥ読込応答、書込要求共通
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t						data[4];
	MSG485_FOOTER				foot;
}	MSG485_TX_MODE_REG_CMD_RES;
//-----------------------------------------------------------------------------
//--	ユニット情報
//-----------------------------------------------------------------------------
typedef	struct
{
	MSG_CPUSNMPALM_DATA			data;
	MSG485_FOOTER				foot;
}	MSG485_UNIT_INF_CMD_RES;
//-----------------------------------------------------------------------------
//--	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報読込応答、書込要求共通
//-----------------------------------------------------------------------------
typedef	struct
{
	SDIIPMISMATCH_STR			data;
	MSG485_FOOTER				foot;
}	MSG485_SDIIPMIS_INF_CMD_RES;
//-----------------------------------------------------------------------------
//--	総合
//-----------------------------------------------------------------------------
typedef	struct
{
	MSG485_HEADER		head;			//	ヘッダー部
	MSG485_DATA_COMMON	dataCommon;		//	データ部共通
	union								//	データ部
	{
		MSG485_FOOTER					foot;				//	情報部無しの場合の終端
		MSG485_PL_EEPROM_CMD_RES		plEepromCmdRes;		//	後部ＥＥＰＲＯＭ保存情報読込応答、書込要求共通
		MSG485_CPU_VIDEO_CMD_RES		cpuVideoCmdRes;		//	ＣＰＵ／ＶＩＤＥＯレイヤ読込応答、書込要求共通
		MSG485_ALARM_MASK_CMD_RES		alarmMaskCmdRes;	//	アラームマスク情報読込応答、書込要求共通
		MSG485_DS100BR_REG_CMD_RES		ds100brRegCmdRes;	//	ＤＳ１００ＢＲレジスタ保存情報読込応答、書込要求共通
		MSG485_FSINF_REG_CMD_RES		fsInfCmdRes;		//	ＦＳ機能情報読込応答、書込要求共通
		MSG485_TX_MODE_REG_CMD_RES		txModeCmdRes;		//	ＦＳ：ＴＸ＿ＭＯＤＥ読込応答、書込要求共通
		MSG485_FREERUN_CAL_RES			freerunCal;			//	フリーラン周波数ＡＤＣ　ＶＯＬ記録開始応答
        MSG485_DEVICE_INF_CMD_RES		deviceInfCmdRes;	//	機器情報要求応答
		MSG485_ADDRESS_INF_CMD_RES		addressInfCmdRes;	//	アドレス情報要求応答
		MSG485_UNIT_INF_CMD_RES			unitInfCmdRes;		//	ユニット情報要求応答
		MSG485_READ_INDIVIDUAL_CMD_RES	indInfCmdRes;		//	個別情報読込応答、書込み要求共通
		MSG485_FREE_ADC_WRITE_CMD_REQ	adcVolWriteCmd;		//	ＦＲＥＥ　ＡＤＣ書込要求
		MSG485_FREE_ADC_WRITE_CMD_RES	adcVolWriteRes;		//	ＦＲＥＥ　ＡＤＣ書込要求応答
		MSG485_IPG_RESET_CMD_RES		ipgReset;			//	ＩＰＧ　リセット要求
		MSG485_VERBOSE_STAT_CMD_RES		verboseCmdRes;		//	映像冗長系切り替え要求、情報要求応答共通
		MSG485_UNICAST_STAT_CMD_RES		unicastCmdRes;		//	ユニキャスト通信経路切り替え要求、情報要求応答共通
		MSG485_SDIIPMIS_INF_CMD_RES		sdiIpMisCmdRes;		//	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報読込応答、書込要求共通
		MSG485_IPG_SFP_RESET_REQ		ipgSfpResetReq;		//	ＩＰＧ−ＳＦＰモジュールリセット
	}					data;
}	MSG485_FORMAT;
//-----------------------------------------------------------------------------
//--	インバンド用
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//--	電文ヘッダー
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t			len;			//	電文長
	uint8_t				id;				//	通信ＩＤ（0x00固定）
	MSG_BSC_CODE_NO		srcCodeNo;		//	送信元　機器コード
	MSG_BSC_CODE_NO		dstCodeNo;		//	送信先　機器コード
	uint8_t				total;			//	電文総数（0x01固定）
	uint8_t				no;				//	電文番号（0x01固定）
	uint16_t			sequenceNo;		//	シーケンス番号
	uint8_t				result;			//	応答結果（0x00固定）
	uint8_t				AUX[4];			//	予備
}	MSG_BSC_HEADER;
//-----------------------------------------------------------------------------
//--	電文データ部ヘッダー
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				itemCode;		//	項目コード
	uint8_t				controlCode;	//	制御コード
}	MSG_BSC_DATA_HEADER;
//-----------------------------------------------------------------------------
//--	ヘルスチェック
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				AUX;			//	0x00
	uint16_t			checkData;
}	MSG_BSC_HEALTH_IFS_TO_TXRX;
//-----------------------------------------------------------------------------
//--	ヘルスチェック応答
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				AUX;			//	0x00
	uint16_t			checkData;
}	MSG_BSC_HELTH_IFS_TO_TXRX;
//-----------------------------------------------------------------------------
//--	機器情報要求
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				AUX;			//	0x00
	uint16_t			checkData;
}	MSG_BSC_INF_IFS_TO_TXRX;
//-----------------------------------------------------------------------------
//--	機器情報要求応答
//-----------------------------------------------------------------------------
typedef	struct
{
	MSG_BSC_MCH_DATA	mchData;		//	機器情報
	uint16_t			checkData;
}	MSG_BSC_INF_TXRX_TO_IFS;
//-----------------------------------------------------------------------------
//--	接続要求
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				AUX;			//	0x00
	uint16_t			checkData;
}	MSG_BSC_CONNECT_IFS_TO_TXRX;
//-----------------------------------------------------------------------------
//--	接続要求応答
//-----------------------------------------------------------------------------
//	機器情報要求応答と同じ
//-----------------------------------------------------------------------------
//--	ＭＴＸ切替要求
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t			inputNo[4];		//	入力素材番号（ＢＵＳ１〜４）
	uint16_t			checkData;
}	MSG_BSC_MTX_CHG_IFS_TO_RX;
//-----------------------------------------------------------------------------
//--	ＭＴＸ切替通知
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t			inputNo[4];		//	入力素材番号（ＢＵＳ１〜４）
	uint16_t			checkData;
}	MSG_BSC_MTX_CHG_RX_TO_IFS;
//-----------------------------------------------------------------------------
//--	ＶＲＴ切替要求
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				vrtNo;			//	ＶＲＴ番号（０〜１）
	uint16_t			checkData;
}	MSG_BSC_VRT_CHG_IFS_TO_TX;
//-----------------------------------------------------------------------------
//--	ＶＲＴ切替通知
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				vrtNo;			//	ＶＲＴ番号（０〜１）
	uint16_t			checkData;
}	MSG_BSC_VRT_CHG_TX_TO_IFS;
//-----------------------------------------------------------------------------
//--	ＳＤＩ出力映像系統情報要求
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t			checkData;
}	MSG_BSC_RX_SDI_OUT_ALS_TO_RX;
//-----------------------------------------------------------------------------
//--	ＳＤＩ出力映像系統情報応答
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				AB;				//	出力系統
	uint16_t			inputNo;		//	出力素材番号
}	RX_SDI_OUT_STR;
typedef	struct
{
	RX_SDI_OUT_STR		sdi[BUSNO_MAX];
	uint16_t			checkData;
}	MSG_BSC_RX_SDI_OUT_RX_TO_ALS;
//-----------------------------------------------------------------------------
//--	モニター電文
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t				command[4];
	uint8_t				data[508];
	uint16_t			checkData;
}	MSG_BSC_MONITOR_SG;
//-----------------------------------------------------------------------------
//--	総合
//-----------------------------------------------------------------------------
typedef	struct
{
	MSG_BSC_HEADER		head;			//	ヘッダー
	MSG_BSC_DATA_HEADER	dataHead;		//	データ部ヘッダー
	union								//	データ部
	{
		//---------------------------------------------------------------------
		//--	システム制御電文
		//---------------------------------------------------------------------
		//--	ヘルスチェック
		MSG_BSC_HEALTH_IFS_TO_TXRX		healthIfsToTxRx;
		//--	ヘルスチェック応答
		MSG_BSC_HELTH_IFS_TO_TXRX		healthTxRxToIfs;
		//--	機器情報要求
		MSG_BSC_INF_IFS_TO_TXRX			infIfsToTxRx;
		//--	機器情報要求応答
		MSG_BSC_INF_TXRX_TO_IFS			infTxRxToIfs;
		//--	接続要求
		MSG_BSC_CONNECT_IFS_TO_TXRX		connectIfsToTxRx;
		//--	接続要求応答
		MSG_BSC_INF_TXRX_TO_IFS			connectTxRxToIfs;
		//---------------------------------------------------------------------
		//--	ＭＴＸ制御電文
		//---------------------------------------------------------------------
		//--	ＭＴＸ切替要求
		MSG_BSC_MTX_CHG_IFS_TO_RX		mtxChgIfsToRx;
		//--	ＭＴＸ切替通知
		MSG_BSC_MTX_CHG_RX_TO_IFS		mtxChgRxToIfs;
		//--	ＶＲＴ切替要求
		MSG_BSC_VRT_CHG_IFS_TO_TX		vrtChgIfsToTx;
		//--	ＶＲＴ切替通知
		MSG_BSC_VRT_CHG_TX_TO_IFS		vrtChgTxToIfs;
		//--	ＳＤＩ出力映像系統情報要求
		MSG_BSC_RX_SDI_OUT_ALS_TO_RX	rxSdiOutAlsToRx;
		//--	ＳＤＩ出力映像系統情報応答
		MSG_BSC_RX_SDI_OUT_RX_TO_ALS	rxSdiOutRxToAls;
		//---------------------------------------------------------------------
		//--	ファームウェアアップデート電文
		//---------------------------------------------------------------------
		//--	ファイル転送開始要求
		MSG_BSC_UPD_FST_SET_TO_IPG		updFstSetToIpg;
		//--	ファイル転送開始応答
		MSG_BSC_UPD_FST_IPG_TO_SET		updFstIpgToSet;
		//--	ブロック転送要求
		MSG_BSC_UPD_BLS_SET_TO_IPG		updBlsSetToIpg;
		//--	ブロック転送応答
		MSG_BSC_UPD_BLS_IPG_TO_SET		updBlsIpgToSet;
		//--	書き込み開始要求
		MSG_BSC_UPD_PGO_SET_TO_IPG		updPgoSetToIpg;
		//--	書き込み開始応答
		MSG_BSC_UPD_PGO_IPG_TO_SET		updPgoIpgToSet;
		//--	書き込み状況問い合わせ
		MSG_BSC_UPD_PST_SET_TO_IPG		updPstSetToIpg;
		//--	書き込み状況応答
		MSG_BSC_UPD_PST_IPG_TO_SET		updPstIpgToSet;
		//--	リセット要求
		MSG_BSC_UPD_RST_SET_TO_IPG		updRstSetToIpg;
		//--	その他情報
		MSG_BSC_UPD_ETC_COMMON			updEtcCommon;
		//---------------------------------------------------------------------
		//--	モニター電文
		//---------------------------------------------------------------------
		MSG_BSC_MONITOR_SG				dbgMonitor;
	}					data;
}	MSG_BSC_FORMAT;
//-----------------------------------------------------------------------------
//--	共通変数
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			CPU_Ready;						//	ＦＰＧＡ準備完了で１
	uint8_t			startEnableFrom485;				//	通信開始フラグ（ＩＣＦからの許可待ちフラグ）
	uint8_t			firstMTXChangeRec;				//	初期ＸＰ位置受信済フラグ
	uint8_t			unitStartFlag;					//	ユニットスタートフラグ
	uint8_t			taskInit;						//	初期処理完了フラグ
	uint8_t			dipSw2;							//	ディップスイッチ値（ＳＷ２）AUX0=0bit AUX11=1bit	0=ON
	uint8_t			dipSw4;							//	ディップスイッチ値（ＳＷ４）						0=ON
	uint8_t			dipSw5;							//	ディップスイッチ値（ＳＷ５（ＦＰＧＡ１））
	uint8_t			dipSw6;							//	ディップスイッチ値（ＳＷ６（ＦＰＧＡ２））			0=ON
	uint8_t			dipSw7;							//	ディップスイッチ値（ＳＷ７（ＦＰＧＡ１））
	uint8_t			dipSw10;						//	ディップスイッチ値（ＳＷ１０（ＦＰＧＡ１−２））
	uint8_t			RID;							//	リアユニットID
	uint8_t			RA_OUT;							//	リアユニットRA_OUT
	uint8_t			boardKind;						//	基板種類 （101A等）
	uint8_t			tx_or_rx;						//	HDIPTX or HDIPRX or HDIPTXRX
	uint8_t			saveDataCsmError;				//	保存情報チェックサムエラーフラグ
	uint8_t			A_or_B;							//	使用チャンネル（Ａ　ｏｒ　Ｂ）
	uint8_t			wdtEnable;						//	ウォッチドッグ有効
	uint8_t			setting_InBand_or_485;			//	設定経路選択
	uint8_t			slotNo;							//	スロット番号
	uint32_t		bdIndex;						//	ボード番号
	uint32_t		fpgaDoneFailCount;				//	ＦＰＧＡ　ＤＯＮＥ監視カウンタ
	uint16_t		fpgaTemp[3];					//	ＦＰＧＡ　温度
	uint32_t		fpgaTempAvrBuf[3][TEMP_MOV_AVR];//	ＦＰＧＡ　温度移動平均バッファ
	uint16_t		fpgaTempAvrCnt[3];				//	ＦＰＧＡ　温度移動平均カウンタ
	uint16_t		fpgaTempAvrWpt[3];				//	ＦＰＧＡ　温度移動平均書込み位置
	int16_t			rxCurrentInputNo[BUSNO_MAX];	//	現状入力素材番号
	uint8_t			inputChange[BUSNO_MAX];			//	入力素材変更フラグ
	uint8_t			vrtSwTx[BUSNO_MAX];				//	ＴＸ ＶＲＴーＳＷ（１／２）
	uint8_t			vrtSwRx[BUSNO_MAX];				//	ＲＸ ＶＲＴーＳＷ（１／２）
	uint8_t			connectRequest[2];				//	接続要求受信済フラグ（Ａ，Ｂ）
	uint32_t		connectRequestLossCount[2];		//	接続要求受信済解除カウンタ（Ａ，Ｂ）
	uint8_t			autoADCVolConfig;				//	ＡＤＣ　ＶＯＬ記録フラグ
	uint8_t			autoEqualizerConfig;			//	イコライザー自動調整フラグ
	uint8_t			vIntSnmpProc;					//	Ｖ−ＩＮＴ　ＳＮＭＰ　ＴＲＡＰ用フラグ
    uint8_t         vrtSwTxDone[BUSNO_MAX][2];  	//  TX　ＶＲＴ−切り替え完了
	uint8_t			writeSavePhysicalLayerEepromFlag;	//	後部ＥＥＰＲＯＭ保存情報書き込み依頼フラグ
	uint8_t			writeSaveCpuVideoLayerFlag;		//	ＣＰＵ／ＶＩＤＥＯ保存情報書き込み依頼フラグ
	uint8_t			writeSaveAlarmMaskFlag;			//	アラームマスク保存情報書き込み依頼フラグ
	uint8_t			writeSaveAlarmMaskClean;		//	アラームマスク保存情報更新・状態整理依頼
	uint8_t			writeSaveIndividual;			//	個別保存情報保存依頼フラグ
	uint8_t			writeSaveCSZ3;					//	ＣＳＺ３保存情報フラグ
	uint8_t			writeSaveSystemAB;				//	系統情報保存フラグ
	uint8_t			fpgaResetDone;					//	ＦＰＧＡリセット終了フラグ
	uint8_t			bcVRTrecFlag[2];				//  ブロードキャストＶＲＴ切替電文受信フラグ
	uint16_t		bcVRTsequenceNo[2];				//  ブロードキャストＶＲＴ切替電文シーケンス番号（３回応答回避用）
	uint8_t			rs485monitor;					//	ＲＳ４８５受信監視
	uint32_t		crcLossCoun[2][4];				//	計測ロスカウンタ
	uint32_t		formatNgCoun[4];				//	計測ＴＸ　ＦＯＲＭＳＡＴ　ＮＧカウンタ
	uint32_t		txFormatNgCoun[4];				//	TX FORMAT NGカウンタ
	uint16_t		almPeriodCount;					//	アラーム周期監視カウンタ
	uint8_t			writeSaveSdiIpMc;				//	SDICRC設定情報保存依頼フラグ
	uint16_t		sfpTxEnableCount[2];			//	SFP TX:ENABLE OFF以降カウント
}	COMMON_STR;
//-----------------------------------------------------------------------------
//--	フリーラン周波数キャリブレーション機能情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint16_t		readBuffer[FREERUN_CAL_BUF_MAX];
	uint16_t		readCount;
}	FREERUN_CAL_STR;
//-----------------------------------------------------------------------------
//--	ケーブルイコライザートレーニング機能情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			stepNo;								//	処理番号
	uint8_t			currentLevel;						//	レベル番号
	uint32_t		v_count;							//	Ｖカウンタ
	uint32_t		lossCountBase_A[4];					//	ロスカウンタ計測開始時値（Ａ）
	uint32_t		lossCountBase_B[4];					//	ロスカウンタ計測開始時値（Ｂ）
	uint32_t		lossCounTot_A[16];					//	計測ロスカウンタ（４つ合計値）（Ａ）
	uint32_t		lossCounTot_B[16];					//	計測ロスカウンタ（４つ合計値）（Ｂ）
}	AUTO_EQUALIZER_CONF_STR;
//-----------------------------------------------------------------------------
//--	アラーム状態管理
//-----------------------------------------------------------------------------
typedef	struct
{
	//	P-BIT
	uint8_t			cpuWakeUpFail_P;				//	ＣＰＵ異常起動
	uint8_t			fpga1WakeUpFail_P;				//	ＦＰＧＡ１　ＤＯＮＥ不正
	uint8_t			fpga1MagicNoFail_P;				//	ＦＰＧＡ１　マジックナンバーエラー
	uint8_t			fpga2WakeUpFail_P;				//	ＦＰＧＡ２　ＤＯＮＥ不正
	uint8_t			fpga2MagicNoFail_P;				//	ＦＰＧＡ２　マジックナンバーエラー
	uint8_t			fpga3WakeUpFail_P;				//	ＦＰＧＡ３　ＤＯＮＥ不正
	uint8_t			txFault_P[2];					//	ＴＸ＿ＦＡＵＬＴ
	uint8_t			rxLos_P[2];						//	ＲＸ＿ＬＯＳ
	uint8_t			rearBoardMissMatch_P;			//	リアボード不適合
	uint8_t			dipSwMissMatch_P;				//	ディップスイッチミスマッチ
	//	C-BIT
	uint8_t			txFault_C[2];					//	ＴＸ＿ＦＡＵＬＴ
	uint8_t			rxLos_C[2];						//	ＲＸ＿ＬＯＳ
	uint8_t			sdiVideoStatusTx[4];			//	ＴＸ　Ｖｉｄｅｏ　Ｓｔａｔｕｓ
	uint8_t			sdiVideoStatusRx[4];			//	ＲＸ　Ｖｉｄｅｏ　Ｓｔａｔｕｓ
	uint8_t			sdiVideoCrcErr[4];				//	ＴＸ　Ｖｉｄｅｏ　ＣＲＣ　Ｅｒｒｏｒ
					//	IEEE1588 受信あり　未実装
	uint8_t			rtpFifoEmpty[4];				//	ＲＴＰ　ＦＩＦＯ　ＥＭＰＴＹ
	uint8_t			rtpFifoFull[4];					//	ＲＴＰ　ＦＩＦＯ　ＦＵＬＬ
	uint8_t			udpFifoEmpty[2][4];				//	ＵＤＰ　ＦＩＦＯ　ＥＭＰＴＹ
	uint8_t			udpFifoFull[2][4];				//	ＵＤＰ　ＦＩＦＯ　ＦＵＬＬ
	uint8_t			packetCountError[2][4];			//	受信パケット監視
	uint8_t			refInFail;						//	ＲＥＦ＿ＩＮ
	uint8_t			psStatusFail;					//	ＰＳ　Ｓｔａｔｕｓ
	uint8_t			tempFail;						//	温度異常
	uint8_t			l1GLinkDown;					//	１Ｇ　ＬＩＮＫ　ＤＯＷＮ

	uint8_t			sdiVideoCrcErrRx[2][4];			//	ＲＸ　ＩＰ　ＣＲＣＥＲＲ
	uint8_t			ipMcMismatchRx[4];				//	ＲＸ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
}	ALARM_STR;

typedef	struct
{
	uint8_t			sdiVideoCrcErr[4];				//	ＴＸ　Ｖｉｄｅｏ　ＣＲＣ　Ｅｒｒｏｒ
	uint8_t			sdiVideoCrcErrRx[2][4];			//	ＲＸ　ＩＰ　ＣＲＣＥＲＲ
	uint8_t			ipMcMismatchRx[4];				//	ＲＸ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
}	ALARM_BACKUP_STR;
//-----------------------------------------------------------------------------
//--	アラームトラップ検出用情報
//-----------------------------------------------------------------------------
typedef	struct
{
	uint32_t		alarmDetectStartWait;			//	起動直後アラーム検出抑止フラグ
	//	C-BIT
	uint32_t		txFault_C_Count[2];				//	ＴＸ＿ＦＡＵＬＴカウンタ
	uint32_t		rxLos_C_Count[2];				//	ＲＸ＿ＬＯＳカウンタ
	uint32_t		sdiVideoStatusTx_Count[4];		//	ＴＸ　Ｖｉｄｅｏ　Ｓｔａｔｕｓカウンタ
	uint32_t		sdiVideoStatusRx_Count[4];		//	ＲＸ　Ｖｉｄｅｏ　Ｓｔａｔｕｓカウンタ
	uint32_t		sdiVideoCrcErr_Count[4];		//	ＴＸ　Ｖｉｄｅｏ　ＣＲＣ　Ｅｒｒｏｒカウンタ
					//	IEEE1588 受信あり　未実装
	uint32_t		rtpFifoEmpty_Count[4];			//	ＲＴＰ　ＦＩＦＯ　ＥＭＰＴＹ
	uint32_t		rtpFifoFull_Count[4];			//	ＲＴＰ　ＦＩＦＯ　ＦＵＬＬ
	uint32_t		udpFifoEmpty_Count[2][4];		//	ＵＤＰ　ＦＩＦＯ　ＥＭＰＴＹ
	uint32_t		udpFifoFull_Count[2][4];		//	ＵＤＰ　ＦＩＦＯ　ＦＵＬＬ
	uint32_t		packetCountError_Count[2][4];	//	受信パケット監視
	uint32_t		refInFail_Count;				//	ＲＥＦ＿ＩＮ
	uint32_t		psStatusFail_Count;				//	ＰＳ　Ｓｔａｔｕｓ
	uint32_t		tempFail_Count;					//	温度異常
	uint32_t		l1GLinkDown_Count;				//	１Ｇ　ＬＩＮＫ　ＤＯＷＮ

	uint32_t		sdiVideoFormatErrTxTime_Count[4];	//	ＴＸ　ＳＤＩ　ＥＲＲ　監視時間
	uint32_t		sdiVideoFormatErrTx_Count[4];		//	ＴＸ　ＳＤＩ　ＥＲＲ
	uint32_t		sdiVideoCrcErrRxTime_Count[2][4];	//	ＲＸ　ＩＰ　ＣＲＣＥＲＲ　監視時間
	uint32_t		sdiVideoCrcErrRx_Count[2][4];		//	ＲＸ　ＩＰ　ＣＲＣＥＲＲ
	uint32_t		ipMcMismatchRxTime_Count[4];		//	ＲＣ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ　監視時間
	uint32_t		ipMcMismatchRx_Count[4];			//	ＲＣ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
	uint32_t		ipMcMismatchRxErr_Count[4];			//	ＲＣ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ

	uint8_t			sdiVideoFormatErrTx_mode[4];
	uint8_t			sdiVideoCrcErrRx_mode[2][4];

}	ALA_TRAP_DETECT_STR;
//-----------------------------------------------------------------------------
//--	デバッグ情報（シリアルモニタでみれる値群）
//-----------------------------------------------------------------------------
typedef	struct
{
	uint32_t	data[32];
}	DEBUG_INF_STR;
//-----------------------------------------------------------------------------
//--	疑似TRAP発生（一時テスト用）
//-----------------------------------------------------------------------------
typedef	struct
{
	uint8_t			alaram_test_OnOff;					//	アラーム発生テスト
	uint8_t			sdiVideoCrcErrOnOff[4];				//	ＴＸ　Ｖｉｄｅｏ　ＣＲＣ　Ｅｒｒｏｒ
	uint8_t			sdiVideoCrcErrRxOnOff[2][4];		//	ＲＸ　ＩＰ　ＣＲＣＥＲＲ
	uint8_t			ipMcMismatchRxOnOff[4];				//	ＲＸ　ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
}	BEBUG_ALARM_PSEUDO_STR;

//=============================================================================
//==	参照定義
//=============================================================================
extern		MSG_BSC_CODE_NO						myCode[2];					//	自コード情報 [0]A [1]B
extern		SAVE_PHYSICAL_LAYER_EEPROM_STR		savePhysicalLayerEeprom;	//	後部基板ＥＥＰＲＯＭ　ＭＡＣアドレス情報
extern		SAVE_CPU_VIDEO_LAYER_STR			saveCpuVideoLayer;			//	ＣＰＵ／ＶＩＤＥＯ情報
extern		SAVE_ALARM_MASK_STR					saveAlarmMask;				//	アラームマスク情報
extern		SAVE_INDIVIDUAL_SET_STR				saveIndividualSet;			//	個別設定
extern		SAVE_INDIVIDUAL_SET_STR				saveIndividualSetUndo;		//	個別設定（Ｕｎｄｏ用）
extern		SAVE_DS100BR_REG_STR				saveDS100BR;				//	ＤＳ１００ＢＲレジスタ情報
extern		SAVE_CSZ3_REG_STR					saveCSZ3;					//	ＣＳＺ３保存情報
extern		SAVE_SYSTEM_AB						saveSystemAB;				//	系統情報保存情報
extern		SAVE_SDIIPMISMATCH_STR				saveSdiIpMisInf;			//	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報
extern		FREERUN_CAL_STR						autoADCVolData;				//	フリーラン周波数ＡＤＣ　ＶＯＬ記録データバッファ
extern		uint8_t								DS100BR_RegisterAddress[11];//	ＤＳ１００ＢＲレジスタアドレス
extern		uint8_t								DS100BR_EQ_LEVEL_Values[16];//	ＤＳ１００ＢＲ　ＥＱ設定値テーブル
extern		AUTO_EQUALIZER_CONF_STR				autoEQConfData;				//	ＥＱ　ＧＡＩＮ自動調整データバッファ
extern		UH									net_dev_nos[2];				//	ネットワークデバイス番号
extern		COMMON_STR							commonData;					//	共通変数
extern		MSG_BSC_MCH_DATA					modelInf[2];				//	機種情報  [0]A [1]B
extern		MSG_IPG_ADDRESS_INF					addressInf;					//	アドレス情報
extern		ALARM_STR							alarmStatus;				//	アラーム状態管理
extern		ALARM_BACKUP_STR					alarmBackupStatus;			//	アラーム状態管理（周期監視用）
extern		ALA_TRAP_DETECT_STR					alarmTrapDetectData;		//	アラームトラップ検出用情報
extern		DEBUG_INF_STR						debugData;					//	デバッグデータ
extern		uint32_t							saveTxMode[BUSNO_MAX];		//	テストパターンＳＤＩモード
extern		uint32_t							test_3G_HD_count;			//	ＴＥＳＴ　３Ｇ　ＨＤ　自動切り替え用
extern		uint32_t							test_1Min_count[10];		//	ＴＥＳＴ　１分周期のカウンタ
extern		uint32_t							test_1Min_index;			//	ＴＥＳＴ　１分周期のカウンタの添え字
extern		T_NET_ADR gNET_ADR[2];											//	ネットワークアドレス情報
extern		T_NET_DEV gNET_DEV[2];											//	ネットワークデバイス情報
extern		T_NODE	snmp_mgrA;												//	ＳＮＭＰマネージャ情報（Ａ）
extern		T_NODE	snmp_mgrB;												//	ＳＮＭＰマネージャ情報（Ｂ）

extern		uint8_t								snmpTrapMoniBuf[SNMP_TRAP_MONI_MAX][128];//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用
extern		uint32_t							snmpTrapMoniWptr;			//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用
extern		uint32_t							snmpTrapMoniRptr;			//	ＳＮＭＰ　ＴＲＡＰ　ＭＯＮＩＴＯＲ用

extern		struct ip_addr				 		snmp_ipA;					//	snmp IPAddress A
extern		struct ip_addr 						snmp_ipB;					//	snmp IPAddress B

//	ファームウェアアップデート用
extern		FW_UPDATE_BUF_STR					fwUpdateBuf;
//=============================================================================
//==	共通関数
//=============================================================================
extern		void		g_dip_sw67_Read(void);
extern		void		g_deviceStart(void);
extern		void		g_videoStart(void);
extern		void		g_videoABSelect(void);
extern		BOOL		g_isBoard20X(void);
extern		BOOL		g_isBoardBC(void);
extern		void		g_getFPGAVersion(void);
extern		uint8_t		g_readDS100BR(uint8_t radr);
extern		void		g_writeDS100BR(uint8_t radr,uint8_t val);
extern		void		g_writeDS100BRAll(uint8_t init,uint8_t all);
extern		void		g_readSavePhysicalLayerEeprom(void);
extern		void		g_writeSavePhysicalLayerEeprom(void);
extern		void		g_readSaveCpuVideoLayer(void);
extern		void		g_writeSaveCpuVideoLayer(void);
extern		void		g_readSaveAlarmMask(void);
extern		void		g_writeSaveAlarmMask(void);
extern		void		g_readSaveIndividual(void);
extern		void		g_writeSaveIndividual(void);
extern		void		g_readSaveDS100BR(void);
extern		void		g_writeSaveDS100BR(void);
extern		void		g_readSaveCSZ3(void);
extern		void		g_writeSaveCSZ3(void);
extern		void		g_readSaveSystemAB(void);
extern		void		g_writeSaveSystemAB(void);
extern		void		g_readSaveSdiIpMisInf(void);
extern		void		g_writeSaveSdiIpMisInfl(void);
extern		void		g_changeEndFsInf(FSINF_STR *src,FSINF_STR *dst);
extern		void		g_led_control(uint8_t no,uint8_t onoff);
extern		uint8_t		g_calcCS(uint8_t *data,uint32_t offset,uint32_t len);
extern		void		g_multiCastIpToMac(uint32_t ip,uint8_t *mac);
extern		uint16_t	g_endianConv(uint16_t src);
extern		uint32_t	g_c4To32(uint8_t *c);
extern		uint32_t	g_c4To32WithRSW_NHK(uint8_t *c,uint8_t A_or_B,uint8_t C_or_V);
extern		uint32_t	g_c4To32WithIndex_NHK(uint8_t *c,uint16_t sno);
extern		uint32_t	g_c4To32WithRSW_MC_NHK(uint8_t *c,uint16_t ab,uint16_t bno);
extern		uint32_t	g_c4To32WithRSW_up1(uint8_t *c);
extern		uint32_t	g_c4To32WithRSW_up4(uint8_t *c,uint8_t idx);
extern		uint32_t	g_c4To32WithRSW_upN(uint8_t *c,uint32_t idx);
extern		uint16_t	g_c2To16(uint8_t *c);
extern		int32_t		g_IsManagementServerIndex(T_NODE *nd);
extern		double		g_ADC16ToDoubleTemp(uint16_t adc);
extern		void		g_reset(void);
//=============================================================================
//==	制御コマンド共通関数
//=============================================================================
extern		void		g_mtxChange(uint8_t busNo,uint16_t matNo);
extern		void		g_vrtChange(uint8_t busNo,uint8_t sw);
extern		uint8_t		g_getSDIMode(uint16_t rfm);
extern		void		g_txLoopModeChange(uint8_t busNo, uint16_t matNo);
//=============================================================================
//==	シリアル通信、ＲＳ４８５共通関数
//=============================================================================
extern		uint32_t	g_rs485Setting(uint32_t rLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd);
extern		uint32_t	g_rs485SystemControl(uint32_t rLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd);
extern		void		g_rs485ResponseMakeCommon(uint32_t flag,uint32_t wLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd);
extern		void		g_alarmStatus2unitInf(uint8_t *unitInf,uint8_t tx_or_rx);
//=============================================================================
//==	ＵＤＰ通信
//=============================================================================
extern		uint32_t	g_updateControl(MSG_BSC_FORMAT *mf,int32_t AorB);
extern		uint32_t	g_updateControlUsb(MSG_BSC_FORMAT *mf);		//	## USB UPDATE ##
//=============================================================================
//==	ＳＮＭＰ関数
//=============================================================================
extern		void		g_snmpSendTrap(uint8_t *ent_oidHex,int len,int element,int txrx);
//=============================================================================
//==	対ＦＰＧＡ関数
//=============================================================================
extern		void		applyIndividualData(void);
extern		void		csz3Initial(void);
extern		uint16_t	csz3GetStatus(void);
extern		uint16_t	csz3GetADCVoltageVal(void);
extern		uint16_t	csz3GetADCVoltageSet(void);
extern		void		csz3SetADCVoltageSet(uint16_t val);
extern		uint16_t	csz3GetSrgSet(void);
extern		uint16_t	csz3GetSdiHPhase(uint8_t busNo);
extern		void		csz3SetSdiHPhase(uint8_t busNo,uint16_t val);
extern		uint16_t	csz3GetSdiVPhase(uint8_t busNo);
extern		void		csz3SetSdiVPhase(uint8_t busNo,uint16_t val);
extern		uint8_t		csz3GetSdiXpPsel(uint8_t busNo);
extern		void		csz3SetSdiXpPsel(uint8_t busNo,uint8_t val);
extern		uint16_t	csz3GetSdiVFmt(uint8_t busNo);
extern		void		csz3VfmtSet(uint8_t busNo,uint16_t fmt);
extern		uint16_t	csz3GetSdiTxmode(uint8_t busNo);
extern		void		csz3TxmodeSet(uint8_t busNo,uint16_t mode);
extern		uint16_t	csz3GetSdiStatus(uint8_t busNo);
extern		uint32_t	csz3GetTest(void);
extern		uint32_t	csz3GetVer2(void);
extern		uint32_t	csz3GetVer3(void);
extern		uint16_t	csz3GetDipSw7(void);
extern		uint16_t	csz3GetTemp(void);
extern		uint8_t		csz3GetXpOut(uint8_t index);
extern		void		csz3SetXpOut(uint8_t index,uint8_t val);
extern		void		csz3SetWdtOnOff(uint8_t onoff);
extern		uint32_t	xiGetRegister(uint32_t adr);
extern		uint32_t	xiGetUnitControl1(uint8_t busNo);
extern		void		xiSetUnitControl1(uint8_t busNo,uint32_t data);
extern		void		xiSetUnitControl1OutOnOff(uint8_t busNo,uint8_t onoff);
extern		uint32_t	xiGetUnitControl2(uint8_t busNo);
extern		void		xiSetUnitControl2Sync(uint8_t data);
extern		void		xiSetUnitControl2(uint8_t busNo,uint16_t data);
extern		void		xiBitSetUnitControl2(uint8_t busNo,uint8_t bit);
extern		void		xiSetResetWhenTxFormatFail(uint8_t busNo,BOOL fail);
extern		void		xiSetResetWhenRxFormatFail(uint8_t busNo,BOOL fail);
extern		uint32_t	xiGetDebug(uint8_t busNo);
extern		uint32_t	xiGetFpgaVersion(void);
extern		uint32_t	xiGetBoardRevision(void);
extern		uint32_t	xiGetTemperature(void);
extern		uint32_t	xiGetDipSw(void);
extern		uint32_t	xiGetRxMonitor1(uint8_t busNo);
extern		void		xiGetRxMonitor1B(uint8_t busNo,uint16_t *errcnt,uint16_t *moni);
extern		uint32_t	xiGetRxMonitor2(uint8_t busNo);
extern		void		xiSetRxMonitor2(uint8_t busNo,uint16_t data);
extern		uint32_t	xiGetRxMonitor3(uint8_t upd,uint8_t busNo);
extern		uint32_t	xiGetTxControl(uint8_t upd,uint8_t busNo);
extern		void		xiSetTxControl(uint8_t busNo,uint16_t fmt);
extern		void		xiSetTxControlFMT(uint8_t busNo,uint16_t fmt);
extern		uint32_t	xiGetVideoStreamMonitor(uint8_t busNo);
extern		uint32_t	xiGetVideoStreamMonitor2(uint8_t busNo);
extern		uint32_t	xiGetHeaderAddControl1(uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl1(uint8_t busNo,uint16_t fmt);
extern		uint32_t	xiGetHeaderAddControl2(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		uint32_t	xiGetHeaderAddControl3(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl23(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr,uint8_t sel,uint8_t ext,uint8_t vlan,uint8_t tos);
extern		uint32_t	xiGetHeaderAddControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		uint32_t	xiGetHeaderAddControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl45(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr);
extern		void		xiSetHeaderAddControl4TTL(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TTL,uint8_t moniTTL);
extern		void		xiSetHeaderAddControl4TOS(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TOS,uint8_t moniTOS);
extern		uint32_t	xiGetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr);
extern		uint32_t	xiGetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr);
extern		uint32_t	xiGetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t src,uint16_t dst);
extern		uint32_t	xiGetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc);
extern		uint32_t	xiGetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t vlan);
extern		uint32_t	xiGetHeaderAddControl11(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderAddControl12(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderAddControl13(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl1(uint8_t chNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetHeaderDelControl1ReciveOff(uint8_t chNo,uint8_t busNo,uint8_t val);
extern		void		xiSetHeaderDelControl1Buffer(uint8_t chNo,uint8_t busNo,uint8_t val);
extern		uint32_t	xiGetHeaderDelControl2(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl3(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl4(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl5(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl6(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl7(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl8(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl9(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiGetHeaderDelControl5_Reg(uint8_t chNo,uint8_t busNo,uint8_t regNo);
extern		uint32_t	xiGetTxFecCalculatorol(uint8_t upd,uint8_t busNo);
extern		void		xiSetTxFecCalculatorol(uint8_t busNo,uint16_t fecL,uint8_t fecD,uint8_t opt);
extern		uint32_t	xiGetRxFecCalculatorol(uint8_t upd,uint8_t busNo);
extern		void		xiSetRxFecCalculatorol(uint8_t busNo,uint16_t fecL,uint8_t fecD,uint8_t opt);
extern		uint32_t	xiGetDemuxControl1(uint8_t chNo,uint8_t upd);
extern		uint32_t	xiGetDemuxControl2(uint8_t chNo,uint8_t upd);
extern		void		xiSetDemuxControl12(uint8_t chNo,uint8_t *adr);
extern		void		xiSetDemuxControl1TxEnable(uint8_t chNo,uint8_t onoff);
extern		uint32_t	xiGetDemuxControl3(uint8_t chNo,uint8_t upd);
extern		void		xiSetDemuxControl3(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetDemuxControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetDemuxControl4(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t dst);
extern		uint32_t	xiGetDemuxControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetDemuxControl5(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr);
extern		uint32_t	xiGetDemuxControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiSetDemuxControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc);
extern		uint32_t	xiGetDemuxControl7(uint8_t chNo,uint8_t upd,uint8_t index);
extern		void		xiSetDemuxControl7(uint8_t chNo,uint8_t index,uint16_t ena,uint16_t pno);
extern		uint32_t	xiGetDemuxControl8(uint8_t chNo,uint8_t upd,uint8_t index);
extern		void		xiSetDemuxControl8(uint8_t chNo,uint8_t index,uint16_t ena,uint16_t pno);
extern		void		xiSetSw12Flag(uint8_t chNo,uint8_t ch,uint8_t val);
extern		void		xiIEEE1588InitSet(uint8_t chNo,uint32_t init_h,uint32_t init_m,uint32_t init_l);
extern		void		xiIEEE1588OffsetSet(uint8_t chNo,uint32_t offset_h,uint32_t offset_m,uint32_t offset_l);
extern		void		xiIEEE1588AddValueSet(uint8_t chNo,uint32_t add_val);
extern		void		xiIEEE1588IntervalTimeSet(uint8_t chNo,uint32_t interval_h,uint32_t interval_l);
extern		void		xiIEEE1588IntervalInitTimeSet(uint8_t chNo,uint32_t interval_h,uint32_t interval_l);
extern		void		xiIEEE1588TxTimeStampGet(uint8_t chNo,uint32_t *time_stamp);
extern		void		xiIEEE1588RxTimeStampGet(uint8_t chNo,uint32_t *time_stamp);
extern		void		xiIEEE1588SystemTimeGet(uint8_t chNo,uint32_t *system_time);
extern		uint32_t	xiGetL3SwitchControl1(void);
extern		void		xiSetL3SwitchControl1(uint32_t data);
extern		void		xiSetL3SwitchControl1AB(uint8_t data);
extern		uint32_t	xiGetL3SwitchControl2(void);
extern		void		xiSetL3SwitchControl2(uint32_t data);
extern		uint32_t	xiGetL3SwitchControl3(uint8_t chNo);
extern		void		xiSetL3SwitchControl3(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl4(uint8_t chNo);
extern		void		xiSetL3SwitchControl4(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl5(uint8_t chNo);
extern		void		xiSetL3SwitchControl5(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl6(uint8_t chNo);
extern		void		xiSetL3SwitchControl6(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl7(uint8_t chNo);
extern		void		xiSetL3SwitchControl7(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl8(uint8_t chNo);
extern		void		xiSetL3SwitchControl8(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl9(uint8_t chNo);
extern		void		xiSetL3SwitchControl9(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl10(uint8_t chNo);
extern		void		xiSetL3SwitchControl10(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl11(uint8_t chNo);
extern		void		xiSetL3SwitchControl11(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl12(void);
extern		uint32_t	xiGetL3SwitchControl13(uint8_t chNo);
extern		void		xiSetL3SwitchControl13(uint8_t chNo,uint32_t adr);
extern		uint32_t	xiGetL3SwitchControl14(void);
extern		uint32_t	xiGetInfoReg(uint32_t idx);
extern		void		xiSetTxControlTxLoopMode(uint8_t busNo,uint16_t mode);
extern		uint16_t	xiGetTxControlTxLoopMode(uint8_t busNo);

extern		void		xiSetSfpReg(uint32_t opt, uint32_t data);
extern		uint32_t	xiGetSfpReg(uint32_t opt);

//=============================================================================
//==	対ＦＰＧＡ関数（ＭＯＮＩ）
//=============================================================================
extern		uint32_t	xiMoniGetRegister(uint32_t adr);
extern		uint32_t	xiMoniGetUnitControl1(uint8_t busNo);
extern		void		xiMoniSetUnitControl1(uint8_t busNo,uint32_t data);
extern		void		xiMoniSetUnitControl1OutOnOff(uint8_t busNo,uint8_t onoff);
extern		void		xiMoniSetResetWhenTxFormatFail(uint8_t busNo,BOOL fail);
extern		uint32_t	xiMoniGetUnitControl2(uint8_t busNo);
extern		void		xiMoniSetUnitControl2Sync(uint8_t data);
extern		void		xiMoniSetUnitControl2(uint8_t busNo,uint16_t data);
extern		void		xiMoniBitSetUnitControl2(uint8_t busNo,uint8_t bit);
extern		uint32_t	xiMoniGetDebug(uint8_t busNo);
extern		uint32_t	xiMoniGetTicoEncode1(uint8_t busNo);
extern		void		xiMoniSetTicoEncode1(uint8_t busNo,uint16_t height,uint16_t width);
extern		uint32_t	xiMoniGetTicoEncode2(uint8_t busNo);
extern		void		xiMoniSetTicoEncode2(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetTicoEncode3(uint8_t busNo);
extern		void		xiMoniSetTicoEncode3(uint8_t busNo,uint32_t bgt);
extern		uint32_t	xiMoniGetTicoEncode4(uint8_t busNo);
extern		void		xiMoniSetTicoEncode4(uint8_t busNo,uint32_t weight);
extern		uint32_t	xiMoniGetVideoCont(uint8_t busNo);
extern		void		xiMoniSetVideoCont(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetAudioPes1(uint8_t busNo);
extern		uint32_t	xiMoniGetAudioPes2(uint8_t busNo);
extern		void		xiMoniSetAudioPes2(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetAudioStatus1(uint8_t busNo);
extern		uint32_t	xiMoniGetAudioStatus2(uint8_t busNo);
extern		uint32_t	xiMoniGetAncCont1(uint8_t busNo);
extern		void		xiMoniSetAncCont1(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetAncCont2(uint8_t busNo);
extern		void		xiMoniSetAncCont2(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetAncPes1(uint8_t busNo);
extern		uint32_t	xiMoniGetAncPes2(uint8_t busNo);
extern		uint32_t	xiMoniGetAncData(uint8_t busNo);
extern		uint32_t	xiMoniGetPid1(uint8_t busNo);
extern		void		xiMoniSetPid1(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetPid2(uint8_t busNo);
extern		void		xiMoniSetPid2(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetPid3(uint8_t busNo);
extern		void		xiMoniSetPid3(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetPid4(uint8_t busNo);
extern		void		xiMoniSetPid4(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetPid5(uint8_t busNo);
extern		void		xiMoniSetPid5(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetSmpte2022_1_2(uint8_t busNo);
extern		void		xiMoniSetSmpte2022_1_2(uint8_t busNo,uint32_t data);
extern		uint32_t	xiMoniGetVideoStreamMonitor(uint8_t busNo);
extern		uint32_t	xiMoniGetHeaderAddControl1(uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl1(uint8_t busNo,uint16_t fmt);
extern		uint32_t	xiMoniGetHeaderAddControl2(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		uint32_t	xiMoniGetHeaderAddControl3(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl23(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr,uint8_t sel,uint8_t ext,uint8_t vlan,uint8_t tos);
extern		uint32_t	xiMoniGetHeaderAddControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		uint32_t	xiMoniGetHeaderAddControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl45(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr);
extern		void		xiMoniSetHeaderAddControl4TTL(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TTL);
extern		void		xiMoniSetHeaderAddControl4TOS(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TOS);
extern		uint32_t	xiMoniGetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr);
extern		uint32_t	xiMoniGetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr);
extern		uint32_t	xiMoniGetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t src,uint16_t dst);
extern		uint32_t	xiMoniGetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc);
extern		uint32_t	xiMoniGetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo);
extern		void		xiMoniSetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t sdioa_vlan_0);
extern		uint32_t	xiMoniGetHeaderAddControl11(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiMoniGetHeaderAddControl12(uint8_t chNo,uint8_t busNo);
extern		uint32_t	xiMoniGetHeaderAddControl13(uint8_t chNo,uint8_t busNo);
/*---------------------------------------------------------------------------
	LINK DOWN 確認用
  ---------------------------------------------------------------------------*/
extern		void g_testUdp0PaketSend(uint16_t port);
/*---------------------------------------------------------------------------
	疑似TRAP発生有無（テスト用）
  ---------------------------------------------------------------------------*/
extern		BEBUG_ALARM_PSEUDO_STR	debug_alarm_pseudo;
/*---------------------------------------------------------------------------
	温度監視閾値
  ---------------------------------------------------------------------------*/
extern		uint16_t	TXRX_TEMP_DET;					//	異常検出温度

/*---------------------------------------------------------------------------
	光モジュール（SFP）読み込み
  ---------------------------------------------------------------------------*/
extern		void		debugOptMemDump(uint8_t moudle, uint8_t adr, uint8_t *bufa);


