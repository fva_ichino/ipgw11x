//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	ＲＳ４８５タスク
//=============================================================================
#include	"g_common.h"
#include	"uart.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
#define		SER_MAX_LEN		2048
#define		TX_FIFO_EMPTY	(1 << 2)
static		uint8_t			readData[SER_MAX_LEN];
static		uint32_t		readDataLen;
static		MSG485_FORMAT	writeData;
static		uint32_t		writeDataLen;
//=============================================================================
//==	ローカル関数定義
//=============================================================================
//=============================================================================
//==	ＲＳ４８５タスク関数
//=============================================================================
void	g_rs485_task(void)
{
	LOG_THREAD();

	uint32_t		i;
	uint32_t		idlingTime	=	0;
	uint8_t			rbuf;
	uint8_t			csm;
	MSG485_FORMAT	*mf;
	readDataLen		=	0;
	//-------------------------------------------------------------------------
	//--	ＵＡＲＴ初期化
	//-------------------------------------------------------------------------
	uart_init(UART_485_CH);
	//-------------------------------------------------------------------------
	//--	受信モードでスタート
	//-------------------------------------------------------------------------
	GPIO_WRITE(RIN_RTPORT, RP1B, 0x80, 0);
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	受信データ有り
		//---------------------------------------------------------------------
		if (uart_read(UART_485_CH,&rbuf) == 1)
		{
			//-----------------------------------------------------------------
			//--	受信長をチェックしてバッファへ
			//-----------------------------------------------------------------
			if (readDataLen >= SER_MAX_LEN)
			{
				readDataLen	=	0;
			}
			readData[readDataLen]	=	rbuf;
			readDataLen++;
			if (readDataLen >= (uint32_t)sizeof(MSG485_HEADER))
			{
				mf	=	(MSG485_FORMAT *)&readData[0];
				//-------------------------------------------------------------
				//--	受信完結？
				//-------------------------------------------------------------
				if (readDataLen ==
						((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)mf->head.length +
						(uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
				{
					//---------------------------------------------------------
					//--	電文解析
					//---------------------------------------------------------
					writeDataLen	=	0;
					//---------------------------------------------------------
					//--	送信元がＩＣＦ？
					//---------------------------------------------------------
					if (mf->head.srcID != ID485_ICF)
					{
						readDataLen	=	0;
						continue;
					}
					//---------------------------------------------------------
					//--	自分宛かどうか
					//---------------------------------------------------------
					if (mf->head.dstID != (commonData.slotNo + 1) && mf->head.dstID != ID485_BCAST)
					{
						readDataLen	=	0;
						continue;
					}
					//---------------------------------------------------------
					//--	ＳＴＸ、ＥＴＸ、チェックサムチェック
					//---------------------------------------------------------
					csm	=	g_calcCS(readData,1,(readDataLen - sizeof(MSG485_FOOTER) - 1));
					if (mf->head.startOfText != COM_CODE_STX ||
						readData[readDataLen - 1] != COM_CODE_ETX ||
						csm != readData[readDataLen - sizeof(MSG485_FOOTER)])
					{
						readDataLen	=	0;
						continue;
					}
					//---------------------------------------------------------
					//--	設定電文
					//---------------------------------------------------------
					if (mf->dataCommon.itemCode == ITEM_CODE_SETTING)
					{
						writeDataLen	=	g_rs485Setting(readDataLen,mf,&writeData);
					}
					//---------------------------------------------------------
					//--	システム制御電文
					//---------------------------------------------------------
                    else if (mf->dataCommon.itemCode == ITEM_CODE_SYSTEMC)
                    {
						writeDataLen	=	g_rs485SystemControl(readDataLen,mf,&writeData);
                    }
					//---------------------------------------------------------
					//--	応答送信
					//---------------------------------------------------------
					if (writeDataLen != 0)
					{
						GPIO_WRITE(RIN_RTPORT, RP1B, 0x80, 1);	//	送信モードへ
						//-----------------------------------------------------
						//--	送信データ整形
						//-----------------------------------------------------
						g_rs485ResponseMakeCommon(0,writeDataLen,mf,&writeData);
						//-----------------------------------------------------
						//--	送信処理
						//-----------------------------------------------------
						for ( i = 0 ; i < writeDataLen ; i++ )
						{
							uart_write(UART_485_CH,*((uint8_t *)&writeData + i));
						}
						tslp_tsk(1);
						GPIO_WRITE(RIN_RTPORT, RP1B, 0x80, 0);	//	受信モードへ
						//-----------------------------------------------------
						//--	ＩＰＧ　リセット
						//-----------------------------------------------------
						if (mf->dataCommon.itemCode == ITEM_CODE_SETTING &&
							mf->dataCommon.controlCode == CONT_CODE_IPG_RESET)
						{
							tslp_tsk(10);
							g_reset();
						}
						//-----------------------------------------------------
						//--	ＩＣＦはリセットを認識できないので
						//--	なにかやり取りを行ったらＯＫとする
						//-----------------------------------------------------
#if 0	//	処理場所移動
						if (commonData.startEnableFrom485 == 0)
						{
							commonData.startEnableFrom485	=	1;
							if (commonData.firstMTXChangeRec == 1)
							{
								g_videoStart();
							}
						}
#endif
					}
					//---------------------------------------------------------
					//--	受信バッファリセット
					//---------------------------------------------------------
					readDataLen	=	0;
				}
			}
			//-----------------------------------------------------------------
			//--	受信タイムアウトリセット
			//-----------------------------------------------------------------
			idlingTime	=	0;
		}
		//---------------------------------------------------------------------
		//--	受信データ無し
		//---------------------------------------------------------------------
		else
		{
			tslp_tsk(1);
			idlingTime++;
			//-----------------------------------------------------------------
			//--	２０ｍｓ以上受信無しで、受信バッファリセット
			//-----------------------------------------------------------------
			if (idlingTime >= 20)
			{
				readDataLen	=	0;
				idlingTime	=	0;
			}
		}
	}
}
//=============================================================================
//==	設定電文関数
//=============================================================================
uint32_t	g_rs485Setting(uint32_t rLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd)
{
	uint8_t		i;
	uint32_t	writeLen	=	0;
	FSINF_STR	tempFsInf;
	switch (mf->dataCommon.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ユニット情報
	//-------------------------------------------------------------------------
	case	CONT_CODE_UNIT_INFO:
		if (commonData.rs485monitor == 1)		//	ＲＳ４８５開始判定
		{
			if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
			{
				//-------------------------------------------------------------
				//--	アラーム情報設定
				//-------------------------------------------------------------
				//	ＴＸ
				memset(&wd->data.unitInfCmdRes.data.cpuSnmpAlmTx[0],0x00,sizeof(wd->data.unitInfCmdRes.data.cpuSnmpAlmTx));
				if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_alarmStatus2unitInf(&wd->data.unitInfCmdRes.data.cpuSnmpAlmTx[0],0);
				//	ＲＸ
				memset(&wd->data.unitInfCmdRes.data.cpuSnmpAlmRx[0],0x00,sizeof(wd->data.unitInfCmdRes.data.cpuSnmpAlmRx));
				//	温度
				if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_alarmStatus2unitInf(&wd->data.unitInfCmdRes.data.cpuSnmpAlmRx[0],1);
				for ( i = 0 ; i < 3 ; i++ )
				{
					wd->data.unitInfCmdRes.data.temperature[i]	=	commonData.fpgaTemp[i];
				}
				//	ＡＤＣ
				wd->data.unitInfCmdRes.data.ADC_Vol	=	saveCSZ3.data.ADC_Vol;

				writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_UNIT_INF_CMD_RES);
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	後部ＥＥＰＲＯＭ保存情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_PL_EEPROM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.plEepromCmdRes,(uint8_t *)&savePhysicalLayerEeprom.data,(uint32_t)sizeof(PHYSICAL_LAYER_EEPROM_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_PL_EEPROM_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	後部ＥＥＰＲＯＭ保存情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_PL_EEPROM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_PL_EEPROM_CMD_RES)))
		{
			if (EEPROM_NOT_INHIBIT)
			{
				memcpy((uint8_t *)&savePhysicalLayerEeprom.data,(uint8_t *)&mf->data.plEepromCmdRes,(uint32_t)sizeof(PHYSICAL_LAYER_EEPROM_STR));
				commonData.writeSavePhysicalLayerEepromFlag	=	1;
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＣＰＵ／ＶＩＤＥＯ保存情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_CPU_VIDEO:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.cpuVideoCmdRes,(uint8_t *)&saveCpuVideoLayer.data,(uint32_t)sizeof(CPU_VIDEO_LAYER_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_CPU_VIDEO_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＣＰＵ／ＶＩＤＥＯ保存情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_CPU_VIDEO:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_CPU_VIDEO_CMD_RES)))
		{
			memcpy((uint8_t *)&saveCpuVideoLayer.data,(uint8_t *)&mf->data.cpuVideoCmdRes,(uint32_t)sizeof(CPU_VIDEO_LAYER_STR));
			commonData.writeSaveCpuVideoLayerFlag	=	1;
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	アラームマスク保存情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_ALARM_MASK:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.alarmMaskCmdRes,(uint8_t *)&saveAlarmMask.data,(uint32_t)sizeof(ALARM_MASK_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_ALARM_MASK_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	アラームマスク保存情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_ALARM_MASK:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_ALARM_MASK_CMD_RES)))
		{
			memcpy((uint8_t *)&saveAlarmMask.data,(uint8_t *)&mf->data.alarmMaskCmdRes,(uint32_t)sizeof(ALARM_MASK_STR));
			commonData.writeSaveAlarmMaskFlag	=	1;
			commonData.writeSaveAlarmMaskClean	=	1;
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	個別保存情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_INDIV_ITEM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.indInfCmdRes,(uint8_t *)&saveIndividualSet.data,(uint32_t)sizeof(INDIVIDUAL_SET_STR));
			//	endian change
			for ( i = 0 ; i < 4 ; i++ )
			{
				wd->data.indInfCmdRes.data.fsHPhase[i]	=	g_endianConv(saveIndividualSet.data.fsHPhase[i]);
				wd->data.indInfCmdRes.data.fsVPhase[i]	=	g_endianConv(saveIndividualSet.data.fsVPhase[i]);
				wd->data.indInfCmdRes.data.txVideoFormat[i]	=	g_endianConv(saveIndividualSet.data.txVideoFormat[i]);
				wd->data.indInfCmdRes.data.rxVideoFormat[i]	=	g_endianConv(saveIndividualSet.data.rxVideoFormat[i]);
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_READ_INDIVIDUAL_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	個別保存情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_INDIV_ITEM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_READ_INDIVIDUAL_CMD_RES)))
		{
			saveIndividualSetUndo	=	saveIndividualSet;
			memcpy((uint8_t *)&saveIndividualSet.data,(uint8_t *)&mf->data.indInfCmdRes,(uint32_t)sizeof(INDIVIDUAL_SET_STR));
			//	endian change
			for ( i = 0 ; i < 4 ; i++ )
			{
				saveIndividualSet.data.fsHPhase[i]	=	g_endianConv(mf->data.indInfCmdRes.data.fsHPhase[i]);
				saveIndividualSet.data.fsVPhase[i]	=	g_endianConv(mf->data.indInfCmdRes.data.fsVPhase[i]);
				saveIndividualSet.data.txVideoFormat[i]	=	g_endianConv(mf->data.indInfCmdRes.data.txVideoFormat[i]);
				saveIndividualSet.data.rxVideoFormat[i]	=	g_endianConv(mf->data.indInfCmdRes.data.rxVideoFormat[i]);
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
			applyIndividualData();
		}
		break;
	//-------------------------------------------------------------------------
	//--	個別保存情報保存
	//-------------------------------------------------------------------------
	case	CONT_CODE_SAVE_INDIV_ITEM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
			for ( i = 0 ; i < 4 ; i++ )
			{
				saveCSZ3.data.fsInf.PresetMode[i]	=	saveIndividualSet.data.fsXpSet[i];
				saveCSZ3.data.fsInf.HPhase[i]		=	saveIndividualSet.data.fsHPhase[i];
				saveCSZ3.data.fsInf.VPhase[i]		=	saveIndividualSet.data.fsVPhase[i];
			}
			commonData.writeSaveIndividual	=	1;
			commonData.writeSaveCSZ3		=	1;
		}
		break;
	//-------------------------------------------------------------------------
	//--	個別保存情報キャンセル
	//-------------------------------------------------------------------------
	case	CONT_CODE_CANCEL_INDIV_ITEM:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
//			saveIndividualSet	=	saveIndividualSetUndo;
			g_readSaveIndividual();	//	S-ROMから再読み込みが正解
			applyIndividualData();
		}
		break;
	//-------------------------------------------------------------------------
	//--	通信開始要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_COMM_START_REQ:
		if (commonData.rs485monitor == 1)		//	ＲＳ４８５開始判定
		{
			if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
			{
				writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
				if (commonData.startEnableFrom485 == 0)
				{
					commonData.startEnableFrom485	=	1;
					if (commonData.firstMTXChangeRec == 1)
					{
						g_videoStart();
					}
				}
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	映像冗長系切り替え
	//-------------------------------------------------------------------------
	case	CONT_CODE_VERBOSE_CHANGE:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_VERBOSE_STAT_CMD_RES)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
			if (saveSystemAB.data.verbose[0] != mf->data.verboseCmdRes.stat[0] ||
				saveSystemAB.data.verbose[1] != mf->data.verboseCmdRes.stat[1])
			{
				saveSystemAB.data.verbose[0]	=	mf->data.verboseCmdRes.stat[0];
				saveSystemAB.data.verbose[1]	=	mf->data.verboseCmdRes.stat[1];
				g_videoABSelect();
//				g_deviceStart();
//				g_videoStart();
				commonData.writeSaveSystemAB	=	1;
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	映像冗長系切り替え情報
	//-------------------------------------------------------------------------
	case	CONT_CODE_VERBOSE_INF:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_VERBOSE_STAT_CMD_RES);
			wd->data.verboseCmdRes.stat[0]	=	saveSystemAB.data.verbose[0];
			wd->data.verboseCmdRes.stat[1]	=	saveSystemAB.data.verbose[1];
		}
		break;
	//-------------------------------------------------------------------------
	//--	ユニキャスト通信経路切り替え
	//-------------------------------------------------------------------------
	case	CONT_CODE_UNICAST_CHANGE:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_UNICAST_STAT_CMD_RES)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
			if (saveSystemAB.data.unicast != mf->data.unicastCmdRes.AB)
			{
				saveSystemAB.data.unicast	=	mf->data.unicastCmdRes.AB;
				xiSetL3SwitchControl1AB(saveSystemAB.data.unicast);
				commonData.writeSaveSystemAB	=	1;
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	ユニキャスト通信経路切り替え情報
	//-------------------------------------------------------------------------
	case	CONT_CODE_UNICAST_INF:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_UNICAST_STAT_CMD_RES);
			wd->data.unicastCmdRes.AB	=	saveSystemAB.data.unicast;
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＤＳ１００ＢＲレジスタ保存情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_DS100BR_REG:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.ds100brRegCmdRes,(uint8_t *)&saveDS100BR.data,(uint32_t)sizeof(DS100BR_REG_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_DS100BR_REG_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＤＳ１００ＢＲレジスタ保存情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_DS100BR_REG:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_DS100BR_REG_CMD_RES)))
		{
			memcpy((uint8_t *)&saveDS100BR.data,(uint8_t *)&mf->data.ds100brRegCmdRes,(uint32_t)sizeof(DS100BR_REG_STR));
			g_writeSaveDS100BR();
			g_writeDS100BRAll(0,1);
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＤＳ１００ＢＲＧＡＩＮ自動調整開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_AUTO_EQ_START:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			//-----------------------------------------------------------------
			//--	イコライザー自動調整開始
			//-----------------------------------------------------------------
			if (commonData.autoEqualizerConfig == 0)
			{
				memset((uint8_t*)&autoEQConfData,0,(uint32_t)sizeof(AUTO_EQUALIZER_CONF_STR));
				commonData.autoEqualizerConfig	=	1;
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	フリーラン周波数ＡＤＣ　ＶＯＬ記録開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_FREERUN_ADC_VOL:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			if (commonData.autoADCVolConfig == 0 && (csz3GetStatus() & 0x0080))
			{
				memset((uint8_t*)&autoADCVolData,0,(uint32_t)sizeof(FREERUN_CAL_STR));
				commonData.autoADCVolConfig	=	1;
				wd->data.freerunCal.result	=	0;
			}
			else
			{
				wd->data.freerunCal.result	=	1;
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FREERUN_CAL_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＦＳ機能情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_FSINF:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			g_changeEndFsInf(&saveCSZ3.data.fsInf,&tempFsInf);
			memcpy((uint8_t *)&wd->data.fsInfCmdRes.data,(uint8_t *)&tempFsInf,(uint32_t)sizeof(FSINF_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FSINF_REG_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＦＳ機能情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_FSINF:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FSINF_REG_CMD_RES)))
		{
			memcpy((uint8_t *)&tempFsInf,(uint8_t *)&mf->data.fsInfCmdRes.data,(uint32_t)sizeof(FSINF_STR));
			g_changeEndFsInf(&tempFsInf,&saveCSZ3.data.fsInf);
			g_writeSaveCSZ3();		//	保存
			for ( i = 0 ; i < 4 ; i++ )
			{
				csz3SetSdiXpPsel(i,saveCSZ3.data.fsInf.PresetMode[i]);
				csz3SetSdiHPhase(i,saveCSZ3.data.fsInf.HPhase[i]);
				csz3SetSdiVPhase(i,saveCSZ3.data.fsInf.VPhase[i]);
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＦＳ：ＴＸ＿ＭＯＤＥ読込
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_TX_MODE:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				wd->data.txModeCmdRes.data[i]	=	(csz3GetSdiTxmode(i) & 0x00ff);
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_TX_MODE_REG_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＦＳ：ＴＸ＿ＭＯＤＥ書込
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_TX_MODE:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_TX_MODE_REG_CMD_RES)))
		{
			for ( i = 0 ; i < 4 ; i++ )
			{
				csz3TxmodeSet(i,mf->data.txModeCmdRes.data[i]);
				saveCSZ3.data.fsTxMode[i]	=	mf->data.txModeCmdRes.data[i];
			}
			g_writeSaveCSZ3();		//	保存
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	フリーラン周波数ＡＤＣ　書込要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_FREE_ADC_WRITE:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FREE_ADC_WRITE_CMD_REQ)))
		{
			if (commonData.autoADCVolConfig == 0)
			{
				if (mf->data.adcVolWriteCmd.vol <= 0x03FF)
				{	//	一時的に書き換える。保存はしない。
					saveCSZ3.data.ADC_Vol	=	mf->data.adcVolWriteCmd.vol;
					csz3SetADCVoltageSet(saveCSZ3.data.ADC_Vol);
				}
			}
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報読み込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_READ_SDIIPMIS:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			memcpy((uint8_t *)&wd->data.sdiIpMisCmdRes.data,(uint8_t *)&saveSdiIpMisInf.data,(uint32_t)sizeof(SDIIPMISMATCH_STR));
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_SDIIPMIS_INF_CMD_RES);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＳＤＩ／ＩＰ／ＭＩＳＭＡＴＣＨ制御情報書き込み
	//-------------------------------------------------------------------------
	case	CONT_CODE_WRITE_SDIIPMIS:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_SDIIPMIS_INF_CMD_RES)))
		{
			memcpy((uint8_t *)&saveSdiIpMisInf.data,(uint8_t *)&mf->data.sdiIpMisCmdRes.data,(uint32_t)sizeof(SDIIPMISMATCH_STR));
			g_writeSaveSdiIpMisInfl();		//	保存
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＩＰＧ−ＳＦＰモジュールリセット
	//-------------------------------------------------------------------------
	case	CONT_CODE_IPGSFP_RESET:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_IPG_SFP_RESET_REQ)))
		{
			xiSetDemuxControl1TxEnable(mf->data.ipgSfpResetReq.AorB, 0x01);
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);

			commonData.sfpTxEnableCount[mf->data.ipgSfpResetReq.AorB]	=	50;	// (100ms * 50 = 5s)
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＩＰＧ　リセット
	//-------------------------------------------------------------------------
	case	CONT_CODE_IPG_RESET:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER)))
		{
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER);
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	システム制御電文関数
//=============================================================================
uint32_t	g_rs485SystemControl(uint32_t rLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd)
{
	uint32_t	writeLen	=	0;
	switch (mf->dataCommon.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	機器情報要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_DEVICE_INFO:
		if (rLen == ((uint32_t)sizeof(MSG485_HEADER) +
					 (uint32_t)sizeof(MSG485_DATA_COMMON) +
					 (uint32_t)sizeof(MSG485_DEVICE_INF_CMD_REQ)))
		{
			g_getFPGAVersion();
			wd->data.deviceInfCmdRes.mchData	=	modelInf[0];
			wd->data.deviceInfCmdRes.CPU_Ready	=	commonData.CPU_Ready;
			//	ＩＣＦにだけで有効／無効ビットを教える
			if ((xiGetDipSw() & 0x00000080))	wd->data.deviceInfCmdRes.mchData.dipSw[2]	|=	0x80;	//	DipSw5
			if ((csz3GetStatus() & 0x0800))		wd->data.deviceInfCmdRes.mchData.dipSw[3]	|=	0x80;	//	DipSw6
			if ((csz3GetDipSw7() & 0x0080))		wd->data.deviceInfCmdRes.mchData.dipSw[4]	|=	0x80;	//	DipSw7
			writeLen	=	(uint32_t)sizeof(MSG485_HEADER) +
							(uint32_t)sizeof(MSG485_DATA_COMMON) +
							(uint32_t)sizeof(MSG485_DEVICE_INF_CMD_RES);

			commonData.rs485monitor	=	1;			//	ＲＳ４８５受信開始
		}
		break;
	//-------------------------------------------------------------------------
	//--	アドレス情報要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_ADDDRESS_INF:
		if (commonData.rs485monitor == 1)		//	ＲＳ４８５開始判定
		{
			if (rLen == ((uint32_t)sizeof(MSG485_HEADER) +
						 (uint32_t)sizeof(MSG485_DATA_COMMON) +
						 (uint32_t)sizeof(MSG485_ADDRESS_INF_CMD_REQ)))
			{
				wd->data.addressInfCmdRes.adrData	=	addressInf;
				writeLen	=	(uint32_t)sizeof(MSG485_HEADER) +
								(uint32_t)sizeof(MSG485_DATA_COMMON) +
								(uint32_t)sizeof(MSG485_ADDRESS_INF_CMD_RES);
			}
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	返信電文作成共通関数
//=============================================================================
void	g_rs485ResponseMakeCommon(uint32_t flag,uint32_t wLen,MSG485_FORMAT *mf,MSG485_FORMAT *wd)
{
	wd->head.dummyData			=	COM_CODE_DUM;
	wd->head.startOfText		=	COM_CODE_STX;
	wd->head.srcID				=	(flag == 0)?(commonData.slotNo + 1):0x01;
	wd->head.dstID				=	ID485_ICF;
    wd->head.length				=	wLen - ((uint32_t)sizeof(MSG485_HEADER) + (uint32_t)sizeof(MSG485_DATA_COMMON) + (uint32_t)sizeof(MSG485_FOOTER));
	wd->head.sequenceNo			=	mf->head.sequenceNo;
	wd->head.total				=	mf->head.total;
	wd->head.no					=	mf->head.no;
	wd->dataCommon.itemCode		=	mf->dataCommon.itemCode;
	wd->dataCommon.controlCode	=	mf->dataCommon.controlCode | 0x80;
	*((uint8_t *)wd + wLen - 2)	=	g_calcCS((uint8_t *)wd,1,(wLen - 3));
	*((uint8_t *)wd + wLen - 1)	=	COM_CODE_ETX;
}
//=============================================================================
//==	アラームステータス情報をユニット情報に変換
//=============================================================================
void	g_alarmStatus2unitInf(uint8_t *unitInf,uint8_t tx_or_rx)
{
	memset(unitInf,0,20);
	//	B-BIT
	if (alarmStatus.cpuWakeUpFail_P			==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x80;		//	ＣＰＵ異常起動
	if (alarmStatus.fpga1WakeUpFail_P		==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x40;		//	ＦＰＧＡ１　ＤＯＮＥ不正
	if (alarmStatus.fpga1MagicNoFail_P		==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x20;		//	ＦＰＧＡ１　マジックナンバーエラー
	if (alarmStatus.fpga2WakeUpFail_P		==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x10;		//	ＦＰＧＡ２　ＤＯＮＥ不正
	if (alarmStatus.fpga2MagicNoFail_P		==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x08;		//	ＦＰＧＡ２　マジックナンバーエラー
	if (alarmStatus.fpga3WakeUpFail_P		==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x04;		//	ＦＰＧＡ３　ＤＯＮＥ不正
	if (alarmStatus.txFault_P[0]			==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x02;		//	ＴＸ＿ＦＡＵＬＴ　Ａ
	if (alarmStatus.txFault_P[1]			==	ALM_STATUS_DETECT)	*(unitInf)	|=	0x01;		//	ＴＸ＿ＦＡＵＬＴ　Ｂ
	if (alarmStatus.rxLos_P[0]				==	ALM_STATUS_DETECT)	*(unitInf + 1)	|=	0x80;	//	ＲＸ＿ＬＯＳ　Ａ
	if (alarmStatus.rxLos_P[1]				==	ALM_STATUS_DETECT)	*(unitInf + 1)	|=	0x40;	//	ＲＸ＿ＬＯＳ　Ｂ
	if (alarmStatus.rearBoardMissMatch_P	==	ALM_STATUS_DETECT)	*(unitInf + 1)	|=	0x20;	//	リアボード不適合
	if (alarmStatus.dipSwMissMatch_P		==	ALM_STATUS_DETECT)	*(unitInf + 1)	|=	0x10;	//	ディップスイッチミスマッチ
	//	C-BIT
	if (alarmStatus.txFault_C[0]			==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x80;	//	ＴＸ＿ＦＡＵＬＴ　Ａ
	if (alarmStatus.txFault_C[1]			==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x40;	//	ＴＸ＿ＦＡＵＬＴ　Ｂ
	if (alarmStatus.rxLos_C[0]				==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x20;	//	ＲＸ＿ＬＯＳ　Ａ
	if (alarmStatus.rxLos_C[1]				==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x10;	//	ＲＸ＿ＬＯＳ　Ｂ
	if (alarmStatus.sdiVideoStatusTx[0]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x08;	//	ＴＸ　Ｖｉｄｅｏ１　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoStatusRx[0]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x08;	//	ＲＸ　Ｖｉｄｅｏ１　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoCrcErr[0]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x04;	//	ＴＸ　Ｖｉｄｅｏ１　ＣＲＣ　Ｅｒｒｏｒ
	if (alarmStatus.sdiVideoStatusTx[1]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x02;	//	ＴＸ　Ｖｉｄｅｏ２　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoStatusRx[1]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x02;	//	ＲＸ　Ｖｉｄｅｏ２　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoCrcErr[1]		==	ALM_STATUS_DETECT)	*(unitInf + 4)	|=	0x01;	//	ＴＸ　Ｖｉｄｅｏ２　ＣＲＣ　Ｅｒｒｏｒ
	if (alarmStatus.sdiVideoStatusTx[2]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x80;	//	ＴＸ　Ｖｉｄｅｏ３　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoStatusRx[2]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x80;	//	ＲＸ　Ｖｉｄｅｏ３　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoCrcErr[2]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x40;	//	ＴＸ　Ｖｉｄｅｏ３　ＣＲＣ　Ｅｒｒｏｒ
	if (alarmStatus.sdiVideoStatusTx[3]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x20;	//	ＴＸ　Ｖｉｄｅｏ４　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoStatusRx[3]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x20;	//	ＲＸ　Ｖｉｄｅｏ４　Ｓｔａｔｕｓ
	if (alarmStatus.sdiVideoCrcErr[3]		==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x10;	//	ＴＸ　Ｖｉｄｅｏ４　ＣＲＣ　Ｅｒｒｏｒ
		//	IEEE1588 受信あり　未実装
	if (alarmStatus.rtpFifoEmpty[0]			==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x04;	//	ＲＴＰ１　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.rtpFifoEmpty[1]			==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x02;	//	ＲＴＰ２　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.rtpFifoEmpty[2]			==	ALM_STATUS_DETECT)	*(unitInf + 5)	|=	0x01;	//	ＲＴＰ３　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.rtpFifoEmpty[3]			==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x80;	//	ＲＴＰ４　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.rtpFifoFull[0]			==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x40;	//	ＲＴＰ１　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.rtpFifoFull[1]			==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x20;	//	ＲＴＰ２　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.rtpFifoFull[2]			==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x10;	//	ＲＴＰ３　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.rtpFifoFull[3]			==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x08;	//	ＲＴＰ４　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoEmpty[0][0]		==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x04;	//	ＵＤＰＡ１　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[0][1]		==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x02;	//	ＵＤＰＡ２　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[0][2]		==	ALM_STATUS_DETECT)	*(unitInf + 6)	|=	0x01;	//	ＵＤＰＡ３　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[0][3]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x80;	//	ＵＤＰＡ４　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[1][0]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x40;	//	ＵＤＰＢ１　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[1][1]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x20;	//	ＵＤＰＢ２　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[1][2]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x10;	//	ＵＤＰＢ３　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoEmpty[1][3]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x08;	//	ＵＤＰＢ４　ＦＩＦＯ　ＥＭＰＴＹ
	if (alarmStatus.udpFifoFull[0][0]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x04;	//	ＵＤＰＡ１　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[0][1]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x02;	//	ＵＤＰＡ２　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[0][2]		==	ALM_STATUS_DETECT)	*(unitInf + 7)	|=	0x01;	//	ＵＤＰＡ３　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[0][3]		==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x80;	//	ＵＤＰＡ４　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[1][0]		==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x40;	//	ＵＤＰＢ１　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[1][1]		==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x20;	//	ＵＤＰＢ２　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[1][2]		==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x10;	//	ＵＤＰＢ３　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.udpFifoFull[1][3]		==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x08;	//	ＵＤＰＢ４　ＦＩＦＯ　ＦＵＬＬ
	if (alarmStatus.packetCountError[0][0]	==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x04;	//	受信パケット監視Ａ１
	if (alarmStatus.packetCountError[0][1]	==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x02;	//	受信パケット監視Ａ２
	if (alarmStatus.packetCountError[0][2]	==	ALM_STATUS_DETECT)	*(unitInf + 8)	|=	0x01;	//	受信パケット監視Ａ３
	if (alarmStatus.packetCountError[0][3]	==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x80;	//	受信パケット監視Ａ４
	if (alarmStatus.packetCountError[1][0]	==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x40;	//	受信パケット監視Ｂ１
	if (alarmStatus.packetCountError[1][1]	==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x20;	//	受信パケット監視Ｂ２
	if (alarmStatus.packetCountError[1][2]	==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x10;	//	受信パケット監視Ｂ３
	if (alarmStatus.packetCountError[1][3]	==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x08;	//	受信パケット監視Ｂ４
	if (alarmStatus.refInFail				==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x04;	//	ＲＥＦ＿ＩＮ
	if (alarmStatus.psStatusFail			==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x02;	//	ＰＳ　Ｓｔａｔｕｓ
	if (alarmStatus.tempFail				==	ALM_STATUS_DETECT)	*(unitInf + 9)	|=	0x01;	//	温度異常
	if (alarmStatus.l1GLinkDown				==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x80;	//	１Ｇ　ＬＩＮＫ　ＤＯＷＮ
	//	追加
	if (alarmStatus.sdiVideoCrcErrRx[0][0]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x40;	//	ＲＸ　Ｖｉｄｅｏ１　ＣＲＣ　Ｅｒｒｏｒ　Ａ
	if (alarmStatus.sdiVideoCrcErrRx[0][1]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x20;	//	ＲＸ　Ｖｉｄｅｏ２　ＣＲＣ　Ｅｒｒｏｒ　Ａ
	if (alarmStatus.sdiVideoCrcErrRx[0][2]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x10;	//	ＲＸ　Ｖｉｄｅｏ３　ＣＲＣ　Ｅｒｒｏｒ　Ａ
	if (alarmStatus.sdiVideoCrcErrRx[0][3]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x08;	//	ＲＸ　Ｖｉｄｅｏ４　ＣＲＣ　Ｅｒｒｏｒ　Ａ
	if (alarmStatus.sdiVideoCrcErrRx[1][0]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x04;	//	ＲＸ　Ｖｉｄｅｏ１　ＣＲＣ　Ｅｒｒｏｒ　Ｂ
	if (alarmStatus.sdiVideoCrcErrRx[1][1]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x02;	//	ＲＸ　Ｖｉｄｅｏ２　ＣＲＣ　Ｅｒｒｏｒ　Ｂ
	if (alarmStatus.sdiVideoCrcErrRx[1][2]	==	ALM_STATUS_DETECT)	*(unitInf + 10)	|=	0x01;	//	ＲＸ　Ｖｉｄｅｏ３　ＣＲＣ　Ｅｒｒｏｒ　Ｂ
	if (alarmStatus.sdiVideoCrcErrRx[1][3]	==	ALM_STATUS_DETECT)	*(unitInf + 11)	|=	0x80;	//	ＲＸ　Ｖｉｄｅｏ４　ＣＲＣ　Ｅｒｒｏｒ　Ｂ

	if (alarmStatus.ipMcMismatchRx[0]		==	ALM_STATUS_DETECT)	*(unitInf + 11)	|=	0x40;	//	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ　１
	if (alarmStatus.ipMcMismatchRx[1]		==	ALM_STATUS_DETECT)	*(unitInf + 11)	|=	0x20;	//	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ　２
	if (alarmStatus.ipMcMismatchRx[2]		==	ALM_STATUS_DETECT)	*(unitInf + 11)	|=	0x10;	//	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ　３
	if (alarmStatus.ipMcMismatchRx[3]		==	ALM_STATUS_DETECT)	*(unitInf + 11)	|=	0x08;	//	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ　４
}
