#include "IPGW11X_cfg.h"

/* タスクID-スレッド管理テーブル */ 
thread_table_t g_cfg_tskid_thread_manage_table[] = {
/*   tskid,               thread_func,                 priority, stacksize, thread_id */
	{ID_TASK_G_UDP_LOG,   (void *)&g_udp_log_task,     8,        0,         (pthread_t)0},
	{ID_TASK_G_UDP_UPD,   (void *)&g_udp_upd_task,     8,        0,         (pthread_t)0},
	{ID_TASK_G_UDP_CNT,   (void *)&g_udp_cnt_task,     85,       0,         (pthread_t)0},
	{ID_TASK_G_SNMP_SG,   (void *)&g_snmp_sg_task,     50,       0,         (pthread_t)0}, 
	{ID_TASK_G_SNMP,      (void *)&g_snmp_task,        50,       0,         (pthread_t)0},
	{ID_TASK_G_1588,      (void *)&g_1588_task,        99,       0,         (pthread_t)0},
	{ID_TASK_G_SERIAL,    (void *)&g_serial_task,      36,       0,         (pthread_t)0},
	{ID_TASK_G_RS485,     (void *)&g_rs485_task,       64,       0,         (pthread_t)0},
	{ID_TASK_G_LOG,       (void *)&g_log_task,         12,       0,         (pthread_t)0},
	{ID_TASK_G_DEVICE,    (void *)&g_device_task,      99,       0,         (pthread_t)0},
	{ID_TASK_G_CYCLE,     (void *)&g_cycle_task,       50,       0,         (pthread_t)0},
	{ID_TASK_G_CYCLE_ALM, (void *)&g_cycle_alarm_task, 43,       0,         (pthread_t)0},
	{ID_TASK_G_UDP_MONI,  (void *)&g_udp_monitor_task, 71,       0,         (pthread_t)0},
	{TASK_TBL_END,        NULL,                        0,        0,         (pthread_t)0}
};

/* セマフォID-ミューテックス管理テーブル */
mutex_table_t g_cfg_semid_mutex_manage_table[] = {
/*   semid,             mutex */
	{ID_SEM_SAVE_DATA,  PTHREAD_MUTEX_INITIALIZER},
	{SEMAPHORE_TBL_END, PTHREAD_MUTEX_INITIALIZER},
};

/* UARTチャンネル管理テーブル */
uart_table_t g_cfg_uart_device_manage_table[] = {
/*   channel,          deviceName,               baudRate, fd */
	{UART_485_CH,      CFG_UART_485_DEVICE_PATH, B230400,  DEVICE_FD_INIT_VAL},
	{UART_USB_CH,      CFG_UART_USB_DEVICE_PATH, B115200,  DEVICE_FD_INIT_VAL},
	{CFG_UART_TBL_END, NULL,                     0,        DEVICE_FD_INIT_VAL},
};

/* settingアドレス管理テーブル */
setting_table_t g_cfg_setting_table[] = {
/*   address,                         fileName, */
	{SETTING_DATA_CPU_VIDEO_ADR,      CFG_SETTING_CPU_VIDEO_FILE_PATH,      },
	{SETTING_DATA_ALARM_MASK_ADR,     CFG_SETTING_ALARM_MASK_FILE_PATH,     },
	{SETTING_DATA_INDIVIDUAL_SET_ADR, CFG_SETTING_INDIVIDUAL_SET_FILE_PATH, },
	{SETTING_DATA_DS100BR_REG,        CFG_SETTING_DS100BR_FILE_PATH,        },
	{SETTING_DATA_CSZ3_ADC_VOL,       CFG_SETTING_CSZ3_ADC_FILE_PATH,       },
	{SETTING_DATA_SYSTEM_AB_ADR,      CFG_SETTING_SYSTEM_AB_FILE_PATH,      },
	{SETTING_DATA_SDIIPMIS_ADR,       CFG_SETTING_SDIIPMIS_FILE_PATH,       },
	{SETTING_DATA_ADR_END,            NULL,                                 },
};
