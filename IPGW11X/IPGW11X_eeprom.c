#include <fcntl.h> // open
#include <sys/ioctl.h> // ioctl
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <pthread.h> // pthread_mutex_t, pthread_mutex_lock, pthread_mutex_unlock

#include <errno.h>
#include <string.h> // strerror

#include "eeprom.h"
#include "IPGW11X_cfg.h" // CFG_EEP_REAR_DEVICE_PATH
#include "log.h"

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

ER_RET eep_init(uint8_t ch, uint8_t iic_adr);
ER_RET eep_write(uint32_t eep_addr, uint8_t *data, uint32_t len);
ER_RET eep_read(uint32_t eep_addr, uint8_t *data, uint32_t len);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static int s_eep_ioctl(struct i2c_rdwr_ioctl_data *p_ioctl_data);

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

static int s_eeprom_fd = DEVICE_FD_INIT_VAL;
static pthread_mutex_t s_eeprom_mutex = PTHREAD_MUTEX_INITIALIZER;

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

/**
 ******************************************************************************
  @brief  Initialize EEPROM control
  @param  [in] ch      -- (未使用)IIC channel
  @param  [in] iic_adr -- IIC address
  @return Error condition
  @retval ER_OK    -- Successful
  @retval ER_PARAM -- Parameter error
  @retval ER_NG    -- Error
 ******************************************************************************
 */
ER_RET eep_init(uint8_t ch, uint8_t iic_adr) {
	int fd;
	char *p_device_path = NULL;

	/* パラメータチェックは不要(呼び出し元で保証) */
#if 0
	if (I2C_EEPROM_CH != ch) {
		LOG_E("unsupported ch: %u\n", ch);
		return ER_PARAM;
	}	
#endif

	/** @todo デバイスパス確認  */
	switch (iic_adr) {
		case EEP_ADDRESS_REAR:
			p_device_path = (char *)&(CFG_EEP_REAR_DEVICE_PATH);
			break;

		default:
			break;
	}

	/* ここには来ない */
	if (NULL == p_device_path) {
		LOG_E("unsupported iic_adr: %u\n", iic_adr);
		return ER_PARAM;
	}

	/* デバイスオープン */
	fd = open(p_device_path, O_RDWR);
	if (fd < 0) {
		LOG_E("open(%s) failed: %d(%s), (ch: %u, iic_adr: %u)\n", p_device_path, errno, strerror(errno), ch, iic_adr);
		return ER_NG;
	}
	s_eeprom_fd = fd;

	LOG_TRACE("[EEPROM] fd = %d, dev = %s, ch: %u, iic_adr: %u\n", fd, p_device_path, ch, iic_adr);
	return ER_OK;
}

/**
 ******************************************************************************
  @brief  Write data
  @param  [in] eep_addr -- EEPROM address
  @param  [in] *data    -- Write data
  @param  [in] len      -- Transfer data length (Byte)
  @return Error condition
  @retval ER_OK    -- Successful
  @retval ER_PARAM -- Parameter error
  @retval ER_NG    -- Error
 ******************************************************************************
 */
ER_RET eep_write(uint32_t eep_addr, uint8_t *data, uint32_t len) {
	int ret;
	struct i2c_msg msg[1];
	struct i2c_rdwr_ioctl_data ioctl_data;
	uint8_t buf[2 + sizeof(SAVE_PHYSICAL_LAYER_EEPROM_STR)] = {0};

	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == s_eeprom_fd) {
		return ER_NG;
	}

	/* パラメータチェックは不要(呼び出し元で保証) */
#if 0
	if (NULL == data) {
		return ER_PARAM;
	}

	if (sizeof(buf) < (size_t)(len + 2)) {
		return ER_PARAM;
	}
#endif

	/* 書き込みアドレス */
	buf[0] = ((uint16_t)eep_addr) >> 8;
	buf[1] = ((uint16_t)eep_addr) & 0x00FF;
	/* 書き込み値 */
	memcpy(&buf[2], data, len);

	/* メッセージ作成 */
	msg[0].addr = CFG_EEP_REAR_ADDRESS;
	msg[0].flags = 0; // 0 = 書き込み
	msg[0].len = (uint16_t)(2 + len);
	msg[0].buf = buf;

 	/* 制御データ作成 */
	ioctl_data.msgs = msg;
	ioctl_data.nmsgs = sizeof(msg) / sizeof(msg[0]); // メッセージ数

	/* I2C制御 */
	ret = s_eep_ioctl(&ioctl_data);
	if (ret < 0) {
		LOG_E("%s(%d, len: %u) failed: %d(%s)\n", __func__, s_eeprom_fd, len, ret, strerror(ret));
		return ER_NG;
	}
	return ER_OK;
}

/**
 ******************************************************************************
  @brief  Read data
  @param  [in]  eep_addr -- EEPROM address
  @param  [out] *data    -- Read data
  @param  [in]  len      -- Transfer data length (Byte)
  @return Error condition
  @retval ER_OK    -- Successful
  @retval ER_PARAM -- Parameter error
  @retval ER_NG    -- Error
 ******************************************************************************
 */
ER_RET eep_read(uint32_t eep_addr, uint8_t *data, uint32_t len) {
	int ret;
	struct i2c_msg msg[2]; /* アドレス書き込み -> 値読み込み */
	struct i2c_rdwr_ioctl_data ioctl_data;
	uint8_t buf[2];

	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == s_eeprom_fd) {
		return ER_NG;
	}

	/* パラメータチェックは不要(呼び出し元で保証) */
#if 0
	if (NULL == data) {
		return ER_PARAM;
	}
#endif

	/* 書き込みアドレス */
	buf[0] = ((uint16_t)eep_addr) >> 8;
	buf[1] = ((uint16_t)eep_addr) & 0x00FF;

	/* メッセージ作成 */
	msg[0].addr = CFG_EEP_REAR_ADDRESS;
	msg[0].flags = 0; // 0 = 書き込み
	msg[0].len = sizeof(buf);
	msg[0].buf = buf;

	msg[1].addr = CFG_EEP_REAR_ADDRESS;
	msg[1].flags = I2C_M_RD;
	msg[1].len = (uint16_t)len;
	msg[1].buf = data;

 	/* 制御データ作成 */
	ioctl_data.msgs = msg;
	ioctl_data.nmsgs = 2; // メッセージ数

	/* I2C制御 */
	ret = s_eep_ioctl(&ioctl_data);
	if (0 != ret) {
		LOG_E("%s(%d, eep_addr: %u, len: %u) failed: %d(%s)\n", __func__, s_eeprom_fd, eep_addr, len, ret, strerror(ret));
		return ER_NG;
	}

	LOG_D("%s(%d, eep_addr: %u, len: %u): %d\n", __func__, s_eeprom_fd, eep_addr, len, *data);
	return ER_OK;
}

/**
 * @brief EEPROM I2C制御
 * 
 * @param p_ioctl_data 
 * @return int 
 * @retval 0　　 成功
 * @retval 0以外 失敗時のerrno
 */
int s_eep_ioctl(struct i2c_rdwr_ioctl_data *p_ioctl_data) {
	int ret = 0;
	int tmp_ret = 0;
	pthread_mutex_lock(&s_eeprom_mutex);
	tmp_ret = ioctl(s_eeprom_fd, I2C_RDWR, p_ioctl_data);
	if (tmp_ret < 0) {
		ret = errno;
		/** @note ここではエラーログは出力しない */
	}
	pthread_mutex_unlock(&s_eeprom_mutex);
	return ret;
}