#ifndef __IPGW11X_GPIO_H__
#define __IPGW11X_GPIO_H__

#include	<stdint.h>

#include	"gpio.h"
#include	"IPGW11X_RIN.h"

/* GPIO */
#define GPIO_CHIP_DEVICE_PATH "/dev/gpiochip10"

/** @note gpioinfoコマンドから取得して反映する */
typedef enum gpio_offset_e {
	GPIO_OFFSET_MIN = 0U,
	/* ここから実機に存在 */
	GPIO_OFFSET_CPU_IO0 = GPIO_OFFSET_MIN,
	GPIO_OFFSET_CPU_IO1,
	GPIO_OFFSET_CPU_IO2,
	GPIO_OFFSET_CPU_IO3,
	GPIO_OFFSET_CPU_IO4,
	GPIO_OFFSET_CPU_IO5,
	GPIO_OFFSET_CPU_IO6,
	GPIO_OFFSET_CPU_IO7,
	GPIO_OFFSET_CPU_IO8,
	GPIO_OFFSET_CPU_IO9,
	GPIO_OFFSET_CPU_IO10,
	GPIO_OFFSET_CPU_IO11,
	GPIO_OFFSET_ALM_OUT,
	GPIO_OFFSET_FAN_ALM1,
	GPIO_OFFSET_CPU_PWRUP,
	GPIO_OFFSET_CPU_LED0,
	GPIO_OFFSET_CPU_LED1,
	GPIO_OFFSET_CPU_LED2,
	GPIO_OFFSET_CPU_LED3,
	GPIO_OFFSET_CPU_LED4,
	GPIO_OFFSET_CPU_LED5,
	GPIO_OFFSET_CPU_LED6,
	GPIO_OFFSET_CPU_LED7,
	GPIO_OFFSET_CPU_LED8,
	GPIO_OFFSET_CPU_LED9,
	GPIO_OFFSET_CPU_LED10,
	GPIO_OFFSET_CPU_LED11,
	GPIO_OFFSET_CPU_LED12,
	GPIO_OFFSET_CPU_LED13,
	GPIO_OFFSET_CPU_LED14,
	GPIO_OFFSET_CPU_DSW0,
	GPIO_OFFSET_CPU_DSW1,
	GPIO_OFFSET_CPU_DSW2,
	GPIO_OFFSET_CPU_DSW3,
	GPIO_OFFSET_CPU_DSW4,
	GPIO_OFFSET_CPU_DSW5,
	GPIO_OFFSET_CPU_DSW6,
	GPIO_OFFSET_CPU_DSW7,
	GPIO_OFFSET_CPU_RSW0,
	GPIO_OFFSET_CPU_RSW1,
	GPIO_OFFSET_CPU_RSW2,
	GPIO_OFFSET_CPU_RSW3,
	GPIO_OFFSET_CPU_RSW4,
	GPIO_OFFSET_CPU_RSW5,
	GPIO_OFFSET_CPU_RSW6,
	GPIO_OFFSET_CPU_RSW7,
	GPIO_OFFSET_R_ID0,
	GPIO_OFFSET_R_ID1,
	GPIO_OFFSET_R_ID2,
	GPIO_OFFSET_R_ID3,
	GPIO_OFFSET_ID0,
	GPIO_OFFSET_ID1,
	GPIO_OFFSET_ID2,
	GPIO_OFFSET_ID3,
	GPIO_OFFSET_SYS_ALM_A,
	GPIO_OFFSET_SYS_ALM_B,
	GPIO_OFFSET_FPGA1_DONE,
	GPIO_OFFSET_FPGA2_DONE,
	GPIO_OFFSET_FPGA3_DONE,
	GPIO_OFFSET_PWRGD3,
	GPIO_OFFSET_RA_OUT,
	GPIO_OFFSET_V_INT,
	/* ここまで実機に存在 */
	GPIO_OFFSET_MAX,

	/* RTOS仕様互換 */
	/* 常に1 */
	GPIO_OFFSET_PRESERVED_1,
	/* 常に0 */
	GPIO_OFFSET_PRESERVED_0,
} gpio_offset_e;

//=============================================================================
//==	関数
//=============================================================================

/* マクロ関数用テーブル */
extern struct gpiod_line *g_gpiod_lines[GPIO_OFFSET_MAX];
extern const gpio_rin_table_t *g_gpio_rin_tbl[RIN_TYPE_MAX];

/** @brief GPIO初期化マクロ */
#define GPIO_INIT()                                                               \
	do {                                                                          \
		com_gpio_init(GPIO_CHIP_DEVICE_PATH);                                     \
		com_gpio_get_all_lines(GPIO_OFFSET_MAX, &g_gpiod_lines[GPIO_OFFSET_MIN]); \
	} while (0)

/**
 * @brief GPIO読み込みマクロ
 * @param[in] rin_type_e
 * @param[in] rin_port_e	rin_gpio_port_e / rin_gpio_port_in_e / rin_rtport_port_e / rin_rtport_port_in_eを指定する
 * @param[in] bitmap		0x00-0xFFを指定できる(0x00を指定する意味はない)
 * @return    uint8_t		bitmapに従って、値を取得してONであればそのビットを1にして返す
 */
#define GPIO_READ(rin_type_e, rin_port_e, bitmap) com_gpio_rin_read(g_gpio_rin_tbl[rin_type_e] + rin_port_e, bitmap)

/**
 * @brief GPIO書き込みマクロ
 * @param[in] rin_type_e
 * @param[in] rin_port_e	rin_gpio_port_e / rin_gpio_port_in_e / rin_rtport_port_e / rin_rtport_port_in_eを指定する
 * @param[in] bitmap		0x00-0xFFを指定できる(0x00を指定する意味はない)
 * @param[in] onoff			0 or 1
 * @return    void			
 * @note bitmapに従って、onoff値を設定する
 */
#define GPIO_WRITE(rin_type_e, rin_port_e, bitmap, onoff) com_gpio_rin_write(g_gpio_rin_tbl[rin_type_e] + rin_port_e, bitmap, onoff)

/** @brief GPIO終了マクロ */
#define GPIO_CLOSE() com_gpio_close(GPIO_OFFSET_MAX, &g_gpiod_lines[GPIO_OFFSET_MIN])

/** @note IPGW11X仕様 */
int gpio_read(const unsigned int offset);
bool gpio_write(const unsigned int offset, const int onoff);

#endif /* __IPGW11X_GPIO_H__ */