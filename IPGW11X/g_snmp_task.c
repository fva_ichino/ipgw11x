//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	ＳＮＭＰタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
#define		SNMP_SG_MAC_LEN		1472
#define		PACKET_COUNT_ERROR	2000
#define		TEMP_GET_COUNT		6		//	１００ｍｓ
#define		TRAP_QUE_OID_MAX	16
#define		TRAP_QUE_MAX		256
//#define		TXRX_TEMP_DET		850		//	異常検出温度（８５℃）
//#define	TXRX_TEMP_DET		720		//	異常検出温度（７２℃）
#define		TXRX_TEMP_HIS		30		//	正常復帰ヒステリシス（３℃）
#define		TXRX_TEMP_LOW		100		//	温度最小値（１０℃）
#define		TXRX_TEMP_HIGH		1000	//	温度最大値（１００℃）
//--	１秒ルール用
typedef	struct
{
	uint32_t	seqNo;
	uint8_t 	ent_oidHex[TRAP_QUE_OID_MAX];
	int			len;
	int			par;
	int			txrx;
}	TRAP_QUE_STR;
//--	アラームステータスoid
typedef	struct
{
	uint8_t		oidLen;			//	OID長（ユニーク部のみ）
	uint8_t		oid[8];			//	IOD
	uint8_t		txrx;			//	TX,RXを意識する必要ありフラグ
	uint8_t		*dataPtr;
}	ALARM_STATUS_OID_STR;
//=============================================================================
//==	ｏｉｄ情報
//=============================================================================
static	uint8_t	preHex[8]		=	{	0x2b,	0x06,	0x01,	0x04,	0x01,	0x82,	0xd9,	0x38	};	//	1.3.6.1.4.1.44216
static	uint8_t	preAscii[18]	=	{	'1','.','3','.','6','.','1','.','4','.','1','.','4','4','2','1','6','.'	};
//static	VB	*community			=	"public";
#define		BOARD_MIB_MAX	10
static	VB	*boardMib[BOARD_MIB_MAX]		=	{	"2.2.1.1.",		//	101A
													"2.2.1.1.",		//	101B
													"2.2.1.1.",		//	101C
													"2.2.2.1.",		//	102A
													"2.2.3.1.",		//	103A
													"2.2.4.1.",		//	104A
													"2.2.5.1.",		//	205A
													"2.2.6.1.",		//	206A
													"2.2.7.1.",		//	207A
//													"2.2.8.1."		//	208A
													"2.2.6.1."		//	208A	206,207は作らないので
												};
static	VB	boardMibHex[BOARD_MIB_MAX][4]	=	{	{0x02,0x02,0x01,0x01},	//	101A
													{0x02,0x02,0x01,0x01},	//	101B
													{0x02,0x02,0x01,0x01},	//	101C
													{0x02,0x02,0x02,0x01},	//	102A
													{0x02,0x02,0x03,0x01},	//	103A
													{0x02,0x02,0x04,0x01},	//	104A
													{0x02,0x02,0x05,0x01},	//	205A
													{0x02,0x02,0x06,0x01},	//	206A
													{0x02,0x02,0x07,0x01},	//	207A
//													{0x02,0x02,0x08,0x01}	//	208A
													{0x02,0x02,0x06,0x01}	//	208A	206,207は作らないので
												};
static	VB	*txrxMib[3]			=	{	"1.",			//	TX
										"2.",			//	RX
										"3."			//	TXRX
									};
static	VB	txrxMibHex[3]		=	{	0x01,			//	TX
										0x02,			//	RX
										0x03			//	TXRX
									};
//static		VB		*txFaultMib				=	"41.1.1";
static		VB		txFaultMibHex[3]			=	{	0x29,	0x01,	0x01	};
//static		VB		*rxLosMib				=	"41.1.3";
static		VB		rxLosMibHex[3]				=	{	0x29,	0x01,	0x03	};
//static		VB		*sdiVideoStatusMib		=	"41.2.1.1";
static		VB		sdiVideoStatusMibHex[4]		=	{	0x29,	0x02,	0x01,	0x01	};
//static		VB		*sdiVideoCrcErrMib		=	"41.2.3.1";
static		VB		sdiVideoCrcErrMibHex[4]		=	{	0x29,	0x02,	0x03,	0x01	};
//static		VB		*rtpFifoEmptyMib		=	"41.4.1.1";
static		VB		rtpFifoEmptyMibHex[4]		=	{	0x29,	0x04,	0x01,	0x01	};
//static		VB		*rtpFifoFullMib			=	"41.4.2.1";
static		VB		rtpFifoFullMibHex[4]		=	{	0x29,	0x04,	0x02,	0x01	};
//static		VB		*udpFifoEmptyMib		=	"41.4.3.1.1";
static		VB		udpFifoEmptyMibHex[5]		=	{	0x29,	0x04,	0x03,	0x01,	0x01	};
//static		VB		*udpFifoFullMib			=	"41.4.4.1.1";
static		VB		udpFifoFullMibHex[5]		=	{	0x29,	0x04,	0x04,	0x01,	0x01	};
//static		VB		*packetCountErrorMib	=	"41.5.1.1.1";
static		VB		packetCountErrorMibHex[5]	=	{	0x29,	0x05,	0x01,	0x01,	0x01	};
//static		VB		*refInFailMib			=	"41.7";
static		VB		refInFailMibHex[2]			=	{	0x29,	0x07	};
//static		VB		*psStatusFailMib		=	"41.8";
static		VB		psStatusFailMibHex[2]		=	{	0x29,	0x08	};
//static		VB		*tempFailMib			=	"41.9";
static		VB		tempFailMibHex[2]			=	{	0x29,	0x09	};
//static		VB		*l1GLinkDownMib			=	"41.10.1";
static		VB		l1GLinkDownMibHex[3]		=	{	0x29,	0x0A,	0x01	};

//static		VB		*ipMcMismatch			=	"41.2.5.1";
static		VB		ipMcMismatchHex[4]			=	{	0x29,	0x02,	0x05,	0x01	};

ALARM_STATUS_OID_STR	alarmStatusOids[]	=	{
//		len		oid
						//	P-BIT
	{	3,	{	0x05,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.cpuWakeUpFail_P		},	//	5.1.1
	{	4,	{	0x05,	0x02,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.fpga1WakeUpFail_P	},	//	5.2.1.1
	{	4,	{	0x05,	0x02,	0x02,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.fpga1MagicNoFail_P	},	//	5.2.2.1
	{	4,	{	0x05,	0x02,	0x01,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.fpga2WakeUpFail_P	},	//	5.2.1.2
	{	4,	{	0x05,	0x02,	0x02,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.fpga2MagicNoFail_P	},	//	5.2.2.2
	{	4,	{	0x05,	0x02,	0x01,	0x03,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.fpga3WakeUpFail_P	},	//	5.2.1.3
	{	3,	{	0x05,	0x03,	0x01,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.txFault_P[0]			},	//	5.3.1
	{	3,	{	0x05,	0x03,	0x02,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.txFault_P[1]			},	//	5.3.2
	{	3,	{	0x05,	0x03,	0x03,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rxLos_P[0]			},	//	5.3.3
	{	3,	{	0x05,	0x03,	0x04,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rxLos_P[1]			},	//	5.3.4
	{	2,	{	0x05,	0x07,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rearBoardMissMatch_P	},	//	5.7
	{	2,	{	0x05,	0x06,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.dipSwMissMatch_P		},	//	5.6
						//	C-BIT
	{	3,	{	0x06,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.txFault_C[0]			},	//	6.1.1
	{	3,	{	0x06,	0x01,	0x02,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.txFault_C[1]			},	//	6.1.2
	{	3,	{	0x06,	0x01,	0x03,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rxLos_C[0]			},	//	6.1.3
	{	3,	{	0x06,	0x01,	0x04,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rxLos_C[1]			},	//	6.1.4
	{	4,	{	0x06,	0x02,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusTx[0]	},	//	6.2.1.1(TX)
	{	4,	{	0x06,	0x02,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusRx[0]	},	//	6.2.1.1(RX)
	{	4,	{	0x06,	0x02,	0x03,	0x01,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErr[0]	},	//	6.2.3.1
	{	4,	{	0x06,	0x02,	0x03,	0x01,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErrRx[0][0]},	//	6.2.3.1(RX)		追加
	{	4,	{	0x06,	0x02,	0x01,	0x02,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusTx[1]	},	//	6.2.1.2(TX)
	{	4,	{	0x06,	0x02,	0x01,	0x02,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusRx[1]	},	//	6.2.1.2(RX)
	{	4,	{	0x06,	0x02,	0x03,	0x02,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErr[1]	},	//	6.2.3.2
	{	4,	{	0x06,	0x02,	0x03,	0x02,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErrRx[0][1]},	//	6.2.3.2(RX)		追加
	{	4,	{	0x06,	0x02,	0x01,	0x03,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusTx[2]	},	//	6.2.1.3(TX)
	{	4,	{	0x06,	0x02,	0x01,	0x03,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusRx[2]	},	//	6.2.1.3(RX)
	{	4,	{	0x06,	0x02,	0x03,	0x03,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErr[2]	},	//	6.2.3.3
	{	4,	{	0x06,	0x02,	0x03,	0x03,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErrRx[0][2]},	//	6.2.3.3(RX)		追加
	{	4,	{	0x06,	0x02,	0x01,	0x04,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusTx[3]	},	//	6.2.1.4(TX)
	{	4,	{	0x06,	0x02,	0x01,	0x04,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoStatusRx[3]	},	//	6.2.1.4(RX)
	{	4,	{	0x06,	0x02,	0x03,	0x04,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErr[3]	},	//	6.2.3.4
	{	4,	{	0x06,	0x02,	0x03,	0x04,	0x00,	0x00,	0x00,	0x00	},	1,&alarmStatus.sdiVideoCrcErrRx[0][3]},	//	6.2.3.4(RX)		追加
	{	4,	{	0x06,	0x04,	0x01,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoEmpty[0]		},	//	6.4.1.1
	{	4,	{	0x06,	0x04,	0x01,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoEmpty[1]		},	//	6.4.1.2
	{	4,	{	0x06,	0x04,	0x01,	0x03,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoEmpty[2]		},	//	6.4.1.3
	{	4,	{	0x06,	0x04,	0x01,	0x04,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoEmpty[3]		},	//	6.4.1.4
	{	4,	{	0x06,	0x04,	0x02,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoFull[0]		},	//	6.4.2.1
	{	4,	{	0x06,	0x04,	0x02,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoFull[1]		},	//	6.4.2.2
	{	4,	{	0x06,	0x04,	0x02,	0x03,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoFull[2]		},	//	6.4.2.3
	{	4,	{	0x06,	0x04,	0x02,	0x04,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.rtpFifoFull[3]		},	//	6.4.2.4
	{	5,	{	0x06,	0x04,	0x03,	0x01,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[0][0]	},	//	6.4.3.1.1
	{	5,	{	0x06,	0x04,	0x03,	0x01,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[0][1]	},	//	6.4.3.1.2
	{	5,	{	0x06,	0x04,	0x03,	0x01,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[0][2]	},	//	6.4.3.1.3
	{	5,	{	0x06,	0x04,	0x03,	0x01,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[0][3]	},	//	6.4.3.1.4
	{	5,	{	0x06,	0x04,	0x03,	0x02,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[1][0]	},	//	6.4.3.2.1
	{	5,	{	0x06,	0x04,	0x03,	0x02,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[1][1]	},	//	6.4.3.2.2
	{	5,	{	0x06,	0x04,	0x03,	0x02,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[1][2]	},	//	6.4.3.2.3
	{	5,	{	0x06,	0x04,	0x03,	0x02,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoEmpty[1][3]	},	//	6.4.3.2.4
	{	5,	{	0x06,	0x04,	0x04,	0x01,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[0][0]	},	//	6.4.4.1.1
	{	5,	{	0x06,	0x04,	0x04,	0x01,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[0][1]	},	//	6.4.4.1.2
	{	5,	{	0x06,	0x04,	0x04,	0x01,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[0][2]	},	//	6.4.4.1.3
	{	5,	{	0x06,	0x04,	0x04,	0x01,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[0][3]	},	//	6.4.4.1.4
	{	5,	{	0x06,	0x04,	0x04,	0x02,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[1][0]	},	//	6.4.4.2.1
	{	5,	{	0x06,	0x04,	0x04,	0x02,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[1][1]	},	//	6.4.4.2.2
	{	5,	{	0x06,	0x04,	0x04,	0x02,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[1][2]	},	//	6.4.4.2.3
	{	5,	{	0x06,	0x04,	0x04,	0x02,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.udpFifoFull[1][3]	},	//	6.4.4.2.4
	{	5,	{	0x06,	0x05,	0x01,	0x01,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[0][0]},	//	6.5.1.1.1
	{	5,	{	0x06,	0x05,	0x01,	0x01,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[0][1]},	//	6.5.1.1.2
	{	5,	{	0x06,	0x05,	0x01,	0x01,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[0][2]},	//	6.5.1.1.3
	{	5,	{	0x06,	0x05,	0x01,	0x01,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[0][3]},	//	6.5.1.1.4
	{	5,	{	0x06,	0x05,	0x01,	0x02,	0x01,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[1][0]},	//	6.5.1.2.1
	{	5,	{	0x06,	0x05,	0x01,	0x02,	0x02,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[1][1]},	//	6.5.1.2.2
	{	5,	{	0x06,	0x05,	0x01,	0x02,	0x03,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[1][2]},	//	6.5.1.2.3
	{	5,	{	0x06,	0x05,	0x01,	0x02,	0x04,	0x00,	0x00,	0x00	},	0,&alarmStatus.packetCountError[1][3]},	//	6.5.1.2.4
	{	2,	{	0x06,	0x07,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.refInFail			},	//	6.7
	{	2,	{	0x06,	0x08,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.psStatusFail			},	//	6.8
	{	2,	{	0x06,	0x09,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.tempFail				},	//	6.9
	{	3,	{	0x06,	0x0A,	0x01,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.l1GLinkDown			},	//	6.10.1
	//	追加
	{	4,	{	0x06,	0x02,	0x04,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.sdiVideoCrcErrRx[1][0]},	//	6.2.4.1(RX)
	{	4,	{	0x06,	0x02,	0x04,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.sdiVideoCrcErrRx[1][1]},	//	6.2.4.2(RX)
	{	4,	{	0x06,	0x02,	0x04,	0x03,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.sdiVideoCrcErrRx[1][2]},	//	6.2.4.3(RX)
	{	4,	{	0x06,	0x02,	0x04,	0x04,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.sdiVideoCrcErrRx[1][3]},	//	6.2.4.4(RX)
	{	4,	{	0x06,	0x02,	0x05,	0x01,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.ipMcMismatchRx[0]	},	//	6.2.5.1(RX)
	{	4,	{	0x06,	0x02,	0x05,	0x02,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.ipMcMismatchRx[1]	},	//	6.2.5.2(RX)
	{	4,	{	0x06,	0x02,	0x05,	0x03,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.ipMcMismatchRx[2]	},	//	6.2.5.3(RX)
	{	4,	{	0x06,	0x02,	0x05,	0x04,	0x00,	0x00,	0x00,	0x00	},	0,&alarmStatus.ipMcMismatchRx[3]	},	//	6.2.5.4(RX)
	
	{	0,	{	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00	},	0,0	},	//	End mark
};

//=============================================================================
//==	ローカル変数定義
//=============================================================================
static	uint32_t	tempGetTiming	=	0;
static	uint32_t	oneSec			=	0;
//--	トラップキュー//	今は、１秒ルールは適用しない
/*
TRAP_QUE_STR	trapQue[TRAP_QUE_MAX];
static	uint32_t	trapQue_rpt		=	0;
static	uint32_t	trapQue_wpt		=	0;
*/
static	uint8_t		snmpData[2048];
static	uint32_t	snmpDataLen;
static	uint8_t		snmpGetResBuf[2048];
//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	void		tempMovAvr(uint32_t idx,uint16_t val);	//	温度移動平均処理
static	uint32_t	makeSnmpGetResponseFix(void);
static	void		makeSnmpGetResponseOid(int32_t hitIndex,uint32_t *wrtPtr,int txrx);
static	void		makeSnmpGetResponseLen(uint32_t wrtPtr);
static	void		alarmTrap_TXRX(void);		//	ＴＸＲＸ　アラームトラップ検出
static	void		alarmTrap_TX(void);			//	ＴＸ　アラームトラップ検出
static	void		alarmTrap_RX(void);			//	ＲＸ　アラームトラップ検出
static	BOOL		isFamilyOk(uint32_t hac);	//	ＲＸ　Ｆａｍｉｌｙ　ＯＫ判定
static	BOOL		isTxFormatFail(uint8_t ch);	//	ＴＸ　ＶＩＤＥＯフォーマット不正判定
static	BOOL		isRxFormatFail(uint8_t ch);	//	ＲＸ　ＶＩＤＥＯフォーマット不正判定
static	void		alarmTrapClean(void);		//	アラームトラップ状態クリーンアップ
//=============================================================================
//==	ＳＮＭＰ　ＧＥＴ　ＲＥＱＵＥＳＴタスク関数
//=============================================================================
void	g_snmp_sg_task(void)
{
	LOG_THREAD();

	uint32_t			i,dataIndex,objLength,oidLength;
	uint32_t			totLength,doneLength,wrtPtr,nextStart;
	int32_t				hitIndex;
	uint8_t				txrxMibData;
	UB					portId;
	struct ip_addr		addr;
	UH					port;
	//-------------------------------------------------------------------------
	//--	初期化
	//-------------------------------------------------------------------------
    snmp_raw_init_port0(&xipNet[0].xIpAddr, SNMP_PORT_R);
    snmp_raw_init_port1(&xipNet[1].xIpAddr, SNMP_PORT_R);
	//-------------------------------------------------------------------------
	//--	初期化完了待ち
	//-------------------------------------------------------------------------
	while (1)
	{
		if (commonData.taskInit == 1)	break;
		tslp_tsk(10);
	}
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
        snmpDataLen = snmp_raw_recv_mnt(&snmpData[0], &portId, &addr, &port);
        if (snmpDataLen == 0)
        {
            continue;
        }
		//---------------------------------------------------------------------
		//--	受信データ解析
		//---------------------------------------------------------------------
		if (snmpDataLen > 35)
		{
			//-----------------------------------------------------------------
			//--	フレーム解析
			//-----------------------------------------------------------------
			dataIndex	=	13;											//	Length + Version + Community
			if (snmpData[1] == 0x81)		dataIndex	+=	1;
			else if (snmpData[1] == 0x82)	dataIndex	+=	2;
			if (snmpData[dataIndex] != 0xA0)	continue;				//	Only GetRequest
			if (snmpData[dataIndex + 1] == 0x81)		dataIndex	+=	1;
			else if (snmpData[dataIndex + 1] == 0x82)	dataIndex	+=	2;
			dataIndex	+=	12;											//	Command + Request ID + Error Status + Error Index + Length
			if (snmpData[dataIndex] == 0x81)
			{
				dataIndex	+=	1;
				totLength	=	snmpData[dataIndex];									//	oid total length
				dataIndex	+=	1;
			}
			else if (snmpData[dataIndex] == 0x82)
			{
				dataIndex	+=	1;
				totLength	=	snmpData[dataIndex] * 0x100 + snmpData[dataIndex + 1];	//	oid total length
				dataIndex	+=	2;
			}
			else
			{
				totLength	=	snmpData[dataIndex];									//	oid total length
				dataIndex	+=	1;
			}
			wrtPtr		=	makeSnmpGetResponseFix();
			doneLength	=	0;
			nextStart	=	dataIndex;
			while (1)
			{
				if ((totLength - doneLength) < 18)	break;
				dataIndex	=	nextStart;
				dataIndex	+=	1;
				objLength	=	snmpData[dataIndex];					//	object Length
				nextStart	=	dataIndex + objLength + 1;
				dataIndex	+=	2;
				oidLength	=	snmpData[dataIndex];					//	oid Length
				dataIndex++;
				//-------------------------------------------------------------
				//--	Prefixチェック
				//-------------------------------------------------------------
				if (memcmp(preHex,&snmpData[dataIndex],8))
				{
					doneLength	+=	(objLength + 2);
					continue;
				}
				dataIndex	+=	8;
				//-------------------------------------------------------------
				//--	Boardチェック
				//-------------------------------------------------------------
				if (memcmp(boardMibHex[commonData.boardKind],&snmpData[dataIndex],4))
				{
					doneLength	+=	(objLength + 2);
					continue;
				}
				dataIndex	+=	4;
				//-------------------------------------------------------------
				//--	Tx,Rxチェック
				//-------------------------------------------------------------
				if (snmpData[dataIndex] == txrxMibHex[HDIPTX] && commonData.tx_or_rx == HDIPRX)
				{
					doneLength	+=	(objLength + 2);
					continue;
				}
				if (snmpData[dataIndex] == txrxMibHex[HDIPRX] && commonData.tx_or_rx == HDIPTX)
				{
					doneLength	+=	(objLength + 2);
					continue;
				}
				txrxMibData	=	snmpData[dataIndex];
				dataIndex	+=	1;
				//-------------------------------------------------------------
				//--	oid検索
				//-------------------------------------------------------------
				for ( i = 0,hitIndex = -1 ; ; i++ )
				{
					if (!alarmStatusOids[i].oidLen)	break;
					if (oidLength != (alarmStatusOids[i].oidLen + 13))	continue;
					if (!memcmp(&snmpData[dataIndex],alarmStatusOids[i].oid,alarmStatusOids[i].oidLen))
					{
						hitIndex	=	i;
						if (alarmStatusOids[i].txrx == 1 && txrxMibData == txrxMibHex[HDIPRX])	hitIndex++;
						break;
					}
				}
				if (hitIndex == -1)		//	HIT無し
				{
					doneLength	+=	(objLength + 2);
					continue;
				}
				//-------------------------------------------------------------
				//--	oid有り
				//-------------------------------------------------------------
				makeSnmpGetResponseOid(hitIndex,&wrtPtr,((txrxMibData == txrxMibHex[HDIPTX])?HDIPTX:HDIPRX));
				doneLength	+=	(objLength + 2);
			}
			//-----------------------------------------------------------------
			//--	GetResponse送信
			//-----------------------------------------------------------------
			makeSnmpGetResponseLen(wrtPtr);
			udp_raw_send((UB*)&snmpGetResBuf[0],wrtPtr,&addr,SNMP_PORT_S,portId,SNMP_PORT_R,RAW_SEND_PRI);
		}
	}
}
//=============================================================================
//==	GetResponse固定部作成
//=============================================================================
static	uint32_t	makeSnmpGetResponseFix(void)
{
	//	UDPパケット作成

	snmpGetResBuf[0]		=	0x30;
	snmpGetResBuf[1]		=	0x82;
	snmpGetResBuf[2]		=	0;		//	length H
	snmpGetResBuf[3]		=	0;		//	length L

	snmpGetResBuf[4]		=	0x02;	//	V2c
	snmpGetResBuf[5]		=	0x01;
	snmpGetResBuf[6]		=	0x01;

	snmpGetResBuf[7]		=	0x04;	//	"public"
	snmpGetResBuf[8]		=	0x06;
	snmpGetResBuf[9]		=	0x70;
	snmpGetResBuf[10]		=	0x75;
	snmpGetResBuf[11]		=	0x62;
	snmpGetResBuf[12]		=	0x6c;
	snmpGetResBuf[13]		=	0x69;
	snmpGetResBuf[14]		=	0x63;

	snmpGetResBuf[15]		=	0xa2;	//	PDU Type(response)
	snmpGetResBuf[16]		=	0x82;

	snmpGetResBuf[17]		=	0;		//	length H
	snmpGetResBuf[18]		=	0;		//	length L

	snmpGetResBuf[19]		=	0x02;	//	リクエストID
	snmpGetResBuf[20]		=	0x04;
	snmpGetResBuf[21]		=	0x12;
	snmpGetResBuf[22]		=	0x34;
	snmpGetResBuf[23]		=	0x56;
	snmpGetResBuf[24]		=	0x78;

	snmpGetResBuf[25]		=	0x02;	//	エラーステータス
	snmpGetResBuf[26]		=	0x01;
	snmpGetResBuf[27]		=	0x00;

	snmpGetResBuf[28]		=	0x02;	//	エラーインデックス
	snmpGetResBuf[29]		=	0x01;
	snmpGetResBuf[30]		=	0x00;

	snmpGetResBuf[31]		=	0x30;	//	LENGTH
	snmpGetResBuf[32]		=	0x82;
	snmpGetResBuf[33]		=	0;		//	length H
	snmpGetResBuf[34]		=	0;		//	length L

	return(35);
}
//=============================================================================
//==	GetResponseoid部作成
//=============================================================================
static	void		makeSnmpGetResponseOid(int32_t hitIndex,uint32_t *wrtPtr,int txrx)
{
	uint32_t	len,pos;

	len	=	alarmStatusOids[hitIndex].oidLen;
	pos	=	*wrtPtr;

	//	HEADER
	snmpGetResBuf[pos + 0]		=	0x30;
	snmpGetResBuf[pos + 1]		=	18 + len;	//	length

	//	OID
	snmpGetResBuf[pos + 2]		=	0x06;
	snmpGetResBuf[pos + 3]		=	13 + len;	//	length

	//	prefix
	memcpy(&snmpGetResBuf[pos + 4],preHex,8);

	//	board
	memcpy(&snmpGetResBuf[pos + 12],boardMibHex[commonData.boardKind],4);

	//	board TX/RX
	snmpGetResBuf[pos + 16]		=	txrxMibHex[txrx];

	//	unique
	memcpy(&snmpGetResBuf[pos + 17],alarmStatusOids[hitIndex].oid,len);

	snmpGetResBuf[pos + 17 + len]	=	0x02;	//	INT

	snmpGetResBuf[pos + 18 + len]	=	0x01;
	snmpGetResBuf[pos + 19 + len]	=	*alarmStatusOids[hitIndex].dataPtr;
//	snmpGetResBuf[pos + 19 + len]	=	0x01;	//	for test always ERROR

	*wrtPtr	=	pos + 20 + len;		//	Write length
}
//=============================================================================
//==	GetResponse長さ部作成
//=============================================================================
static	void		makeSnmpGetResponseLen(uint32_t wrtPtr)
{
	snmpGetResBuf[2]		=	(wrtPtr - 4) / 0x100;		//	length H
	snmpGetResBuf[3]		=	(wrtPtr - 4) % 0x100;		//	length L

	snmpGetResBuf[17]		=	(wrtPtr - 19) / 0x100;		//	length H
	snmpGetResBuf[18]		=	(wrtPtr - 19) % 0x100;		//	length L

	snmpGetResBuf[33]		=	(wrtPtr - 35) / 0x100;		//	length H
	snmpGetResBuf[34]		=	(wrtPtr - 35) % 0x100;		//	length L
}
//=============================================================================
//==	ＳＮＭＰタスク関数
//=============================================================================
static	uint32_t	tempB;
void	g_snmp_task(void)
{
	LOG_THREAD();
		
	uint16_t	tempCal;
	tslp_tsk(2000);
	//-------------------------------------------------------------------------
	//--	初期化完了待ち
	//-------------------------------------------------------------------------
	while (1)
	{
		if (commonData.taskInit == 1)	break;
		tslp_tsk(10);
  	}
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		tslp_tsk(5);
		//---------------------------------------------------------------------
		//--	テストＰＩＮ（周期・時間計測用　確認時はコメント解除
		//---------------------------------------------------------------------
//		GPIO_WRITE(RIN_GPIO, P2B, 0x40, 1);
		//---------------------------------------------------------------------
		//--	Ｔｒａｐ送信周期１秒を作成（検出はこの限りではない！！）
		//---------------------------------------------------------------------
		oneSec++;
		if (oneSec >= 20)
		{
			oneSec	=	0;
			//	今は、１秒ルールは適用しない
		}
		//---------------------------------------------------------------------
		//--	アラームマスク情報が更新されたら、中途半端な状態を
		//--	残さないために、クリーアップを行う
		//---------------------------------------------------------------------
		if (commonData.writeSaveAlarmMaskClean)
		{
			alarmTrapClean();
			commonData.writeSaveAlarmMaskClean	=	0;
		}
		//---------------------------------------------------------------------
		//--	Ｖ−ＩＮＴ１回に１回処理を回す
		//--	ｓｎｍｐ関連関数を割り込み内で呼べないため
		//---------------------------------------------------------------------
		if (commonData.vIntSnmpProc == 0)	continue;
		//---------------------------------------------------------------------
		//--	温度取得（毎回の必要はない）６Ｖ＝１００ｍｓ周期
		//---------------------------------------------------------------------
		tempGetTiming++;
		if (tempGetTiming >= TEMP_GET_COUNT)
		{
			tempGetTiming	=	0;
			//	FPGA1
			tempB	=	xiGetTemperature();
			tempB	&=	0x00ff;
			tempCal	=	(uint16_t)((double)tempB * 10.0 * 503.975 / 256.0 - 2731.5);
			tempMovAvr(0,tempCal);
			//	FPGA2
			tempB	=	csz3GetTemp();
			tempB	&=	0xff00;
			tempB	=	tempB >> 8;
			tempCal	=	(uint16_t)((double)tempB * 10.0 * 503.975 / 256.0 - 2731.5);
			tempMovAvr(1,tempCal);
			//	FPGA3
			commonData.fpgaTemp[2]	=	0;
		}
		//---------------------------------------------------------------------
		//--	ＴＲＡＰ出力アラーム監視
		//---------------------------------------------------------------------
		if (alarmTrapDetectData.alarmDetectStartWait == 1)
		{
			alarmTrap_TXRX();
			if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	alarmTrap_TX();
			if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	alarmTrap_RX();
		}

		commonData.vIntSnmpProc	=	0;

		//---------------------------------------------------------------------
		//--	テストＰＩＮ（周期・時間計測用　確認時はコメント解除
		//---------------------------------------------------------------------
//		GPIO_WRITE(RIN_GPIO, P2B, 0x40, 0);
	}
}
//=============================================================================
//==	温度移動平均処理
//=============================================================================
static	void		tempMovAvr(uint32_t idx,uint16_t val)
{
	uint32_t	cVal,i;
	uint32_t	ptr;
	if (val >= TXRX_TEMP_LOW && val <= TXRX_TEMP_HIGH)	//	範囲内
	{
		cVal	=	val;
	}
	else												//	範囲外
	{
		if (commonData.fpgaTempAvrCnt[idx] != 0)		//	前回値採用
		{
			ptr		=	(commonData.fpgaTempAvrWpt[idx] == 0)?(TEMP_MOV_AVR - 1):(commonData.fpgaTempAvrWpt[idx] - 1);
			cVal	=	commonData.fpgaTempAvrBuf[idx][ptr];
		}
		else
		{
			return;
		}
	}
	//	バッファに格納
	commonData.fpgaTempAvrBuf[idx][commonData.fpgaTempAvrWpt[idx]]	=	cVal;
	commonData.fpgaTempAvrWpt[idx]++;
	if (commonData.fpgaTempAvrWpt[idx] == TEMP_MOV_AVR)	commonData.fpgaTempAvrWpt[idx]	=	0;
	if (commonData.fpgaTempAvrCnt[idx] < TEMP_MOV_AVR)	commonData.fpgaTempAvrCnt[idx]++;
	//	移動平均計算
	if (commonData.fpgaTempAvrCnt[idx] == TEMP_MOV_AVR)
	{
		for ( cVal = i = 0 ; i < TEMP_MOV_AVR ; i++ )
		{
			cVal	+=	commonData.fpgaTempAvrBuf[idx][i];
		}
		commonData.fpgaTemp[idx]	=	(uint16_t)(cVal / TEMP_MOV_AVR);
	}
}
//=============================================================================
//==	ＳＮＭＰトラップ送信関数
//=============================================================================
void	g_snmpSendTrap(uint8_t *ent_oidHex,int len,int par,int txrx)
{
	UB			buf[256];
	UH			trp_len;
	uint32_t	i,pos;
	//	UDPパケット作成

	buf[0]		=	0x30;
	buf[1]		=	47 + len;	//	length

	buf[2]		=	0x02;	//	V2c
	buf[3]		=	0x01;
	buf[4]		=	0x01;

	buf[5]		=	0x04;	//	"public"
	buf[6]		=	0x06;
	buf[7]		=	0x70;
	buf[8]		=	0x75;
	buf[9]		=	0x62;
	buf[10]		=	0x6c;
	buf[11]		=	0x69;
	buf[12]		=	0x63;

	buf[13]		=	0xa7;	//	PDU Type(trap)

	buf[14]		=	34 + len;	//	length

	buf[15]		=	0x02;	//	リクエストID
	buf[16]		=	0x04;
	buf[17]		=	0x12;
	buf[18]		=	0x34;
	buf[19]		=	0x56;
	buf[20]		=	0x78;

	buf[21]		=	0x02;	//	エラーステータス
	buf[22]		=	0x01;
	buf[23]		=	0x00;

	buf[24]		=	0x02;	//	エラーインデックス
	buf[25]		=	0x01;
	buf[26]		=	0x00;

	//	トラップデータ
	buf[27]		=	0x30;
	buf[28]		=	20 + len;	//	length

	//	SEQUENCE
	buf[29]		=	0x30;
	buf[30]		=	18 + len;	//	length

	//	OID
	buf[31]		=	0x06;
	buf[32]		=	13 + len;	//	length

	//	prefix
	memcpy(&buf[33],preHex,8);

	//	board
	memcpy(&buf[41],boardMibHex[commonData.boardKind],4);

	//	board TX/RX
	buf[45]		=	txrxMibHex[txrx];

	//	unique
	memcpy(&buf[46],ent_oidHex,len);

	buf[46 + len]	=	0x02;	//	INT

	buf[47 + len]	=	0x01;
	buf[48 + len]	=	par;

	trp_len	=	49 + len;		//	Total length

	//	Send trap	TO A
	udp_raw_send((UB*)&buf[0],trp_len,&snmp_ipA,SNMP_PORT_S,(u8_t)0,SNMP_PORT_R,RAW_SEND_PRI);
	//	Send trap	TO B
	udp_raw_send((UB*)&buf[0],trp_len,&snmp_ipB,SNMP_PORT_S,(u8_t)1,SNMP_PORT_R,RAW_SEND_PRI);
	//	(debug) copy to SNMP Trap Monitor Buffer
	memset(snmpTrapMoniBuf[snmpTrapMoniWptr],0,128);
	memcpy(&snmpTrapMoniBuf[snmpTrapMoniWptr][0],preAscii,18);
	memcpy(&snmpTrapMoniBuf[snmpTrapMoniWptr][18],boardMib[commonData.boardKind],8);
	memcpy(&snmpTrapMoniBuf[snmpTrapMoniWptr][26],txrxMib[txrx],2);
	for ( i = 0 ; i < len ; i++ )
	{
		pos	=	strlen((char *)snmpTrapMoniBuf[snmpTrapMoniWptr]);
		sprintf((char *)&snmpTrapMoniBuf[snmpTrapMoniWptr][pos],"%d",ent_oidHex[i]);
		if (i != (len - 1))
		{
			snmpTrapMoniBuf[snmpTrapMoniWptr][strlen((char *)snmpTrapMoniBuf[snmpTrapMoniWptr])]	=	'.';
		}
		else
		{
			snmpTrapMoniBuf[snmpTrapMoniWptr][strlen((char *)snmpTrapMoniBuf[snmpTrapMoniWptr])]	=	' ';
			snmpTrapMoniBuf[snmpTrapMoniWptr][strlen((char *)snmpTrapMoniBuf[snmpTrapMoniWptr])]	=	(par)?'D':'R';
		}
	}
	snmpTrapMoniWptr++;
	if (snmpTrapMoniWptr >= SNMP_TRAP_MONI_MAX)	snmpTrapMoniWptr	=	0;
}
//=============================================================================
//==	ＴＸＲＸ　アラームトラップ検出
//=============================================================================
#define	TXRX_LINKDOWN_COUNT				30		//	V-INT 30回
#define	TXRX_LINKDOWN_COMEBACK_COUNT	1000
#define	TXRX_REF_IN_COUNT				30
#define	TXRX_PS_STATUS_COUNT			30
#define	TXRX_TEMP_COUNT					1		//	移動平均をしているので、常時監視
#define	TXRX_1G_LINK_DOWN_COUNT			30
static	void	alarmTrap_TXRX(void)
{
	uint8_t		i,pVal;
	uint32_t	dmx,l3st;
	uint8_t		mibBuf[16];
	uint16_t	sta;

	//-------------------------------------------------------------------------
	//--	ＴＸ＿ＦＡＵＬＴ、ＲＸ＿ＬＯＳ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )	//	A,Bループ
	{
		dmx	=	xiGetDemuxControl1(i,0);
		//---------------------------------------------------------------------
		//--	ＴＸ＿ＦＡＵＬＴ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_TX_FAULT_A_FAIL) ||
			(i == 1 && !ALM_MASK_C_TX_FAULT_B_FAIL))
		{
			//	正常なう
			if (!(dmx & 0x40000000))
			{
				if (alarmStatus.txFault_C[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.txFault_C_Count[i]++;
					if (alarmTrapDetectData.txFault_C_Count[i] >= TXRX_LINKDOWN_COMEBACK_COUNT)
					{	//	正常復帰
						alarmStatus.txFault_C[i]				=	ALM_STATUS_NON;
						alarmTrapDetectData.txFault_C_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,txFaultMibHex,3);
						mibBuf[2]	+=	i;
						if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPTX);
						if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.txFault_C_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.txFault_C[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.txFault_C_Count[i]++;
					if (alarmTrapDetectData.txFault_C_Count[i] >= TXRX_LINKDOWN_COUNT)
					{	//	発生
						alarmStatus.txFault_C[i]				=	ALM_STATUS_DETECT;
						alarmTrapDetectData.txFault_C_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,txFaultMibHex,3);
						mibBuf[2]	+=	i;
						if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPTX);
						if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.txFault_C_Count[i]	=	0;
				}
			}
		}
		//---------------------------------------------------------------------
		//--	ＲＸ＿ＬＯＳ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_RX_LOSS_A_FAIL) ||
			(i == 1 && !ALM_MASK_C_RX_LOSS_B_FAIL))
		{
			//	正常なう
//			if (!(dmx & 0x80000000))
			if (!(dmx & 0xC0000000))
			{
				if (alarmStatus.rxLos_C[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.rxLos_C_Count[i]++;
					if (alarmTrapDetectData.rxLos_C_Count[i] >= TXRX_LINKDOWN_COMEBACK_COUNT)
					{	//	正常復帰
						alarmStatus.rxLos_C[i]					=	ALM_STATUS_NON;
						alarmTrapDetectData.rxLos_C_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rxLosMibHex,3);
						mibBuf[2]	+=	i;
						if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPTX);
						if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rxLos_C_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.rxLos_C[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.rxLos_C_Count[i]++;
					if (alarmTrapDetectData.rxLos_C_Count[i] >= TXRX_LINKDOWN_COUNT)
					{	//	発生
						alarmStatus.rxLos_C[i]					=	ALM_STATUS_DETECT;
						alarmTrapDetectData.rxLos_C_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rxLosMibHex,3);
						mibBuf[2]	+=	i;
						if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPTX);
						if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rxLos_C_Count[i]	=	0;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＲＥＦ＿ＩＮ
	//-------------------------------------------------------------------------
	if (!ALM_MASK_C_REF_IN_FAIL)
	{
		sta	=	csz3GetStatus();
		//	正常なう
		if ((sta & 0x0080))
		{
			if (alarmStatus.refInFail != ALM_STATUS_NON)
			{
				alarmTrapDetectData.refInFail_Count++;
				if (alarmTrapDetectData.refInFail_Count >= TXRX_REF_IN_COUNT)
				{	//	正常復帰
					alarmStatus.refInFail				=	ALM_STATUS_NON;
					alarmTrapDetectData.refInFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,refInFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.refInFail_Count	=	0;
			}
		}
		//	異常なう
		else
		{
			if (alarmStatus.refInFail != ALM_STATUS_DETECT)
			{
				alarmTrapDetectData.refInFail_Count++;
				if (alarmTrapDetectData.refInFail_Count >= TXRX_REF_IN_COUNT)
				{	//	発生
					alarmStatus.refInFail				=	ALM_STATUS_DETECT;
					alarmTrapDetectData.refInFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,refInFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.refInFail_Count	=	0;
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＰＳ＿Ｓｔａｔｕｓ
	//-------------------------------------------------------------------------
	if (!ALM_MASK_C_PS_STATUS_FAIL)
	{
		pVal	=	GPIO_READ(RIN_GPIO, PIN1B, 0x02);
		//	正常なう
		if ((pVal & 0x02))
		{
			if (alarmStatus.psStatusFail != ALM_STATUS_NON)
			{
				alarmTrapDetectData.psStatusFail_Count++;
				if (alarmTrapDetectData.psStatusFail_Count >= TXRX_PS_STATUS_COUNT)
				{	//	正常復帰
					alarmStatus.psStatusFail				=	ALM_STATUS_NON;
					alarmTrapDetectData.psStatusFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,psStatusFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.psStatusFail_Count	=	0;
			}
		}
		//	異常なう
		else
		{
			if (alarmStatus.psStatusFail != ALM_STATUS_DETECT)
			{
				alarmTrapDetectData.psStatusFail_Count++;
				if (alarmTrapDetectData.psStatusFail_Count >= TXRX_PS_STATUS_COUNT)
				{	//	発生
					alarmStatus.psStatusFail				=	ALM_STATUS_DETECT;
					alarmTrapDetectData.psStatusFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,psStatusFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.psStatusFail_Count	=	0;
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	温度異常
	//-------------------------------------------------------------------------
	if (!ALM_MASK_C_TEMP_FAIL)
	{
		//	初期検出
		if (alarmStatus.tempFail == ALM_STATUS_INIT)
		{
			if (commonData.fpgaTemp[0] > TXRX_TEMP_DET ||
				commonData.fpgaTemp[1] > TXRX_TEMP_DET ||
				commonData.fpgaTemp[2] > TXRX_TEMP_DET)
			{
				alarmStatus.tempFail				=	ALM_STATUS_DETECT;
				alarmTrapDetectData.tempFail_Count	=	0;
				//	トラップ
				memcpy(mibBuf,tempFailMibHex,2);
				if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPTX);
				if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPRX);
			}
			else
			{
				alarmStatus.tempFail				=	ALM_STATUS_NON;
				alarmTrapDetectData.tempFail_Count	=	0;
				//	トラップ
				memcpy(mibBuf,tempFailMibHex,2);
				if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPTX);
				if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPRX);
			}
		}
		//	異常チェック
		else if (alarmStatus.tempFail == ALM_STATUS_NON)
		{
			if (commonData.fpgaTemp[0] > TXRX_TEMP_DET ||
				commonData.fpgaTemp[1] > TXRX_TEMP_DET ||
				commonData.fpgaTemp[2] > TXRX_TEMP_DET)
			{
				alarmTrapDetectData.tempFail_Count++;
				if (alarmTrapDetectData.tempFail_Count >= TXRX_TEMP_COUNT)
				{	//	発生
					alarmStatus.tempFail				=	ALM_STATUS_DETECT;
					alarmTrapDetectData.tempFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,tempFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,1,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.tempFail_Count	=	0;
			}
		}
		//	正常復帰チェック
		else
		{
			if (commonData.fpgaTemp[0] <= (TXRX_TEMP_DET - TXRX_TEMP_HIS) &&
				commonData.fpgaTemp[1] <= (TXRX_TEMP_DET - TXRX_TEMP_HIS) &&
				commonData.fpgaTemp[2] <= (TXRX_TEMP_DET - TXRX_TEMP_HIS))
			{
				alarmTrapDetectData.tempFail_Count++;
				if (alarmTrapDetectData.tempFail_Count >= TXRX_TEMP_COUNT)
				{	//	発生
					alarmStatus.tempFail				=	ALM_STATUS_NON;
					alarmTrapDetectData.tempFail_Count	=	0;
					//	トラップ
					memcpy(mibBuf,tempFailMibHex,2);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,2,0,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.tempFail_Count	=	0;
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	１Ｇ　ＬＩＮＫ　ＤＯＷＮ
	//-------------------------------------------------------------------------
	if (!ALM_MASK_C_1G_LINK_DOWN_FAIL &&
		(commonData.boardKind == BK_104A || commonData.boardKind == BK_208A))
	{
		l3st	=	xiGetL3SwitchControl12();
		//	正常なう
		if ((l3st & 0x00000400))
		{
			if (alarmStatus.l1GLinkDown != ALM_STATUS_NON)
			{
				alarmTrapDetectData.l1GLinkDown_Count++;
				if (alarmTrapDetectData.l1GLinkDown_Count >= TXRX_1G_LINK_DOWN_COUNT)
				{	//	正常復帰
					alarmStatus.l1GLinkDown					=	ALM_STATUS_NON;
					alarmTrapDetectData.l1GLinkDown_Count	=	0;
					//	トラップ
					memcpy(mibBuf,l1GLinkDownMibHex,3);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,0,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.l1GLinkDown_Count	=	0;
			}
		}
		//	異常なう
		else
		{
			if (alarmStatus.l1GLinkDown != ALM_STATUS_DETECT)
			{
				alarmTrapDetectData.l1GLinkDown_Count++;
				if (alarmTrapDetectData.l1GLinkDown_Count >= TXRX_1G_LINK_DOWN_COUNT)
				{	//	発生
					alarmStatus.l1GLinkDown					=	ALM_STATUS_DETECT;
					alarmTrapDetectData.l1GLinkDown_Count	=	0;
					//	トラップ
					memcpy(mibBuf,l1GLinkDownMibHex,3);
					if (commonData.tx_or_rx == HDIPTX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPTX);
					if (commonData.tx_or_rx == HDIPRX || commonData.tx_or_rx == HDIPTXRX)	g_snmpSendTrap(mibBuf,3,1,HDIPRX);
				}
			}
			else
			{
				alarmTrapDetectData.l1GLinkDown_Count	=	0;
			}
		}
	}
}
//=============================================================================
//==	ＴＸ　アラームトラップ検出
//=============================================================================
#define	TX_SDI_VIDEO_COUNT		30		//	V-INT 30回
static	void	alarmTrap_TX(void)
{
	uint8_t		i;
	uint32_t	rxMoni;
	uint8_t		mibBuf[16];
	BOOL		outStop;
	uint8_t		srgPSel;
	//-------------------------------------------------------------------------
	//--	Ｖｉｄｅｏ　Ｓｔａｔｕｓ　＆　ＣＲＣ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		rxMoni	=	xiGetRxMonitor1(i);
		//---------------------------------------------------------------------
		//--	Ｖｉｄｅｏ　Ｓｔａｔｕｓ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_VIDEO1_STATUS_FAIL) ||
			(i == 1 && !ALM_MASK_C_VIDEO2_STATUS_FAIL) ||
			(i == 2 && !ALM_MASK_C_VIDEO3_STATUS_FAIL) ||
			(i == 3 && !ALM_MASK_C_VIDEO4_STATUS_FAIL))
		{
			//	正常なう
			if ((rxMoni & 0x00004000) && isTxFormatFail(i) == FALSE)
			{
				if (alarmStatus.sdiVideoStatusTx[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.sdiVideoStatusTx_Count[i]++;
					if (alarmTrapDetectData.sdiVideoStatusTx_Count[i] >= TX_SDI_VIDEO_COUNT)
					{	//	正常復帰
						alarmStatus.sdiVideoStatusTx[i]					=	ALM_STATUS_NON;
						alarmTrapDetectData.sdiVideoStatusTx_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoStatusMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,0,HDIPTX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoStatusTx_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.sdiVideoStatusTx[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.sdiVideoStatusTx_Count[i]++;
					if (alarmTrapDetectData.sdiVideoStatusTx_Count[i] >= TX_SDI_VIDEO_COUNT)
					{	//	発生
						alarmStatus.sdiVideoStatusTx[i]					=	ALM_STATUS_DETECT;
						alarmTrapDetectData.sdiVideoStatusTx_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoStatusMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,1,HDIPTX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoStatusTx_Count[i]	=	0;
				}
			}
		}
		//	フォーマット不正　ＵＮＩＴ　ＣＯＮＴＲＯＬ１　リセットビット制御
		if (rxMoni & 0x00004000)
		{
			srgPSel	=	csz3GetSdiXpPsel(i);
			// 2019.06.07 [Add] 入力FS 時の不具合
//			if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09 || srgPSel == 0x0C)
			if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09)
			{
				//	何もしない -> 2019.06.07 [Add] 入力FS 時の不具合
				xiSetResetWhenTxFormatFail(i,FALSE);
				xiMoniSetResetWhenTxFormatFail(i,FALSE);
			}
			else
			{
				outStop	=	isTxFormatFail(i);
				xiSetResetWhenTxFormatFail(i,outStop);
				//	ＭＯＮＩ追加仕様 2018/06/20
				if (g_isBoard20X())
				{
					if (!(commonData.dipSw4 & 0x04))	//	他走モードのみチェック
					{
						if (g_getSDIMode(saveIndividualSet.data.txVideoFormat[i]) != 0x00)
						{	//	設定 HD以外
							outStop	=	TRUE;
						}
						xiMoniSetResetWhenTxFormatFail(i,outStop);
					}
				}
			}
		}
		//---------------------------------------------------------------------
		//--	Ｖｉｄｅｏ　ＣＲＣ　ＥＲＲＯＲ
		//---------------------------------------------------------------------
#if 0	//	ＳＤＩ　ＥＲＲに変更
		if ((i == 0 && !ALM_MASK_C_VIDEO1_CRCERR_FAIL) ||
			(i == 1 && !ALM_MASK_C_VIDEO2_CRCERR_FAIL) ||
			(i == 2 && !ALM_MASK_C_VIDEO3_CRCERR_FAIL) ||
			(i == 3 && !ALM_MASK_C_VIDEO4_CRCERR_FAIL))
		{
			//	正常なう
			if (!(rxMoni & 0x00000400))
			{
				if (alarmStatus.sdiVideoCrcErr[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.sdiVideoCrcErr_Count[i]++;
					if (alarmTrapDetectData.sdiVideoCrcErr_Count[i] >= TX_SDI_VIDEO_COUNT)
					{	//	正常復帰
						alarmStatus.sdiVideoCrcErr[i]				=	ALM_STATUS_NON;
						alarmTrapDetectData.sdiVideoCrcErr_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,0,HDIPTX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoCrcErr_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.sdiVideoCrcErr[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.sdiVideoCrcErr_Count[i]++;
					if (alarmTrapDetectData.sdiVideoCrcErr_Count[i] >= TX_SDI_VIDEO_COUNT)
					{	//	発生
						alarmStatus.sdiVideoCrcErr[i]				=	ALM_STATUS_DETECT;
						alarmTrapDetectData.sdiVideoCrcErr_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,1,HDIPTX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoCrcErr_Count[i]	=	0;
				}
			}
		}
#endif
	}
	//-------------------------------------------------------------------------
	//--	ＳＤＩ　ＥＲＲ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		if ((i == 0 && !ALM_MASK_C_VIDEO1_CRCERR_FAIL) ||
			(i == 1 && !ALM_MASK_C_VIDEO2_CRCERR_FAIL) ||
			(i == 2 && !ALM_MASK_C_VIDEO3_CRCERR_FAIL) ||
			(i == 3 && !ALM_MASK_C_VIDEO4_CRCERR_FAIL))
		{
			if (alarmStatus.sdiVideoCrcErr[i] != alarmBackupStatus.sdiVideoCrcErr[i])
			{
				if (alarmStatus.sdiVideoCrcErr[i] == ALM_STATUS_NON)
				{	//	回復
					//	トラップ
					memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
					mibBuf[3]	+=	i;
					g_snmpSendTrap(mibBuf,4,0,HDIPTX);
				}
				else
				{	//	発生
					//	トラップ
					memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
					mibBuf[3]	+=	i;
					g_snmpSendTrap(mibBuf,4,1,HDIPTX);
				}
				alarmBackupStatus.sdiVideoCrcErr[i]	=	alarmStatus.sdiVideoCrcErr[i];
			}
		}
	}
}

/*
#define	TX_LOCKED_FAIL_COUNT		30
#define	TX_EMPTY_FAIL_COUNT			30
static	void	alarmTrapTX(void)
{
	uint8_t		i,ii;
	uint16_t	errcnt,moni;
	uint32_t	sta;
	uint8_t		mibBuf[16];

	//-------------------------------------------------------------------------
	//--	ＲＸ　ＭＯＮＩＴＯＲ　ＲＸ＿ＭＯＤＥ＿ＬＯＣＫＥＤエラー
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < BUSNO_MAX ; i++ )	//	CHループ
	{
		xiGetRxMonitor1B(i,&errcnt,&moni);
		//---------------------------------------------------------------------
		//--	現在正常
		//---------------------------------------------------------------------
		if ((moni & 0x8000))
		{
			if (alarmTrapDetectData.txLockedFailStatus[i] == 1)
			{	//	正常復帰
				alarmTrapDetectData.txLockedFailStatus[i]	=	0;
				//	トラップ
				memcpy(mibBuf,txLockedFailMibHex,4);
				mibBuf[4]	=	i + 1;
				g_snmpSendTrap(mibBuf,5,0,HDIPTX);
			}
			alarmTrapDetectData.txLockedFailStatusCount[i]	=	0;
		}
		//---------------------------------------------------------------------
		//--	現在異常
		//---------------------------------------------------------------------
		else
		{
			if (alarmTrapDetectData.txLockedFailStatus[i] == 0)
			{
				alarmTrapDetectData.txLockedFailStatusCount[i]++;
				if (alarmTrapDetectData.txLockedFailStatusCount[i] >= TX_LOCKED_FAIL_COUNT)
				{	//	エラー発生
					alarmTrapDetectData.txLockedFailStatus[i]	=	1;
					//	トラップ
					memcpy(mibBuf,txLockedFailMibHex,4);
					mibBuf[4]	=	i + 1;
					g_snmpSendTrap(mibBuf,5,1,HDIPTX);
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＥＭＰＴＹエラー
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )	//	A,Bループ
	{
		for ( ii = 0 ; ii < BUSNO_MAX ; ii++ )	//	CHループ
		{
			sta	=	xiGetHeaderAddControl2(i,0,0,ii);
			//-----------------------------------------------------------------
			//--	現在正常
			//-----------------------------------------------------------------
			if (!(sta & 0x00000002))
			{
				if (alarmTrapDetectData.txEmptyFailStatus[i][ii] == 1)
				{	//	正常復帰
					alarmTrapDetectData.txEmptyFailStatus[i][ii]	=	0;
					//	トラップ
					memcpy(mibBuf,txEmptyFailMibHex,3);
					mibBuf[3]	=	i + 1;
					mibBuf[4]	=	ii + 1;
					g_snmpSendTrap(mibBuf,5,0,HDIPTX);
				}
				alarmTrapDetectData.txEmptyFailStatusCount[i][ii]	=	0;
			}
			//-----------------------------------------------------------------
			//--	現在異常
			//-----------------------------------------------------------------
			else
			{
				if (alarmTrapDetectData.txEmptyFailStatus[i][ii] == 0)
				{
					alarmTrapDetectData.txEmptyFailStatusCount[i][ii]++;
					if (alarmTrapDetectData.txEmptyFailStatusCount[i][ii] >= TX_EMPTY_FAIL_COUNT)
					{	//	エラー発生
						alarmTrapDetectData.txEmptyFailStatus[i][ii]	=	1;
						//	トラップ
						memcpy(mibBuf,txEmptyFailMibHex,3);
						mibBuf[3]	=	i + 1;
						mibBuf[4]	=	ii + 1;
						g_snmpSendTrap(mibBuf,5,1,HDIPTX);
					}
				}
			}
		}
	}
}
*/
//=============================================================================
//==	ＲＸ　アラームトラップ検出
//=============================================================================
#define	RX_SDI_VIDEO_COUNT		30		//	V-INT 30回
#define	RX_PCK_EMPTY_FULL_FAIL_COUNT	100
static	void	alarmTrap_RX(void)
{
	uint8_t		i,ii,nowErr;
	uint32_t	vsMoni;
	uint32_t	hDelCtl;
	uint32_t	hDelCnt;
	uint32_t	hAddCnl;
	uint8_t		mibBuf[16];

	uint8_t		alm;
	//-------------------------------------------------------------------------
	//--	Ｖｉｄｅｏ　Ｓｔａｔｕｓ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		hAddCnl	=	xiGetHeaderAddControl1(0,i);
		//---------------------------------------------------------------------
		//--	Ｖｉｄｅｏ　Ｓｔａｔｕｓ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_VIDEO1_STATUS_FAIL) ||
			(i == 1 && !ALM_MASK_C_VIDEO2_STATUS_FAIL) ||
			(i == 2 && !ALM_MASK_C_VIDEO3_STATUS_FAIL) ||
			(i == 3 && !ALM_MASK_C_VIDEO4_STATUS_FAIL))
		{
			//	正常なう
			if ((!(hAddCnl & 0x00800000) || (isFamilyOk(hAddCnl))) && isRxFormatFail(i) == FALSE)
			{
				if (alarmStatus.sdiVideoStatusRx[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.sdiVideoStatusRx_Count[i]++;
					if (alarmTrapDetectData.sdiVideoStatusRx_Count[i] >= RX_SDI_VIDEO_COUNT)
					{	//	正常復帰
						alarmStatus.sdiVideoStatusRx[i]					=	ALM_STATUS_NON;
						alarmTrapDetectData.sdiVideoStatusRx_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoStatusMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,0,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoStatusRx_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.sdiVideoStatusRx[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.sdiVideoStatusRx_Count[i]++;
					if (alarmTrapDetectData.sdiVideoStatusRx_Count[i] >= RX_SDI_VIDEO_COUNT)
					{	//	発生
						alarmStatus.sdiVideoStatusRx[i]					=	ALM_STATUS_DETECT;
						alarmTrapDetectData.sdiVideoStatusRx_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,sdiVideoStatusMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,1,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.sdiVideoStatusRx_Count[i]	=	0;
				}
			}
		}
		//	フォーマット不正　ＵＮＩＴ　ＣＯＮＴＲＯＬ１　リセットビット制御
		if ((hAddCnl & 0x00800000))
		{
			if (commonData.unitStartFlag == 1)
			{
				xiSetResetWhenRxFormatFail(i,isRxFormatFail(i));
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＲＴＰ　ＦＯＦＯ　ＥＭＰＴＹ　＆　ＦＵＬＬ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 4 ; i++ )
	{
		vsMoni	=	xiGetVideoStreamMonitor(i);
		//---------------------------------------------------------------------
		//--	ＥＭＰＴＹ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_RTP1_FIFO_EMPTY_FAIL) ||
			(i == 1 && !ALM_MASK_C_RTP2_FIFO_EMPTY_FAIL) ||
			(i == 2 && !ALM_MASK_C_RTP3_FIFO_EMPTY_FAIL) ||
			(i == 3 && !ALM_MASK_C_RTP4_FIFO_EMPTY_FAIL))
		{
			//	正常なう
			if (!(vsMoni & 0x00000002))
			{
				if (alarmStatus.rtpFifoEmpty[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.rtpFifoEmpty_Count[i]++;
					if (alarmTrapDetectData.rtpFifoEmpty_Count[i] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
					{	//	正常復帰
						alarmStatus.rtpFifoEmpty[i]				=	ALM_STATUS_NON;
						alarmTrapDetectData.rtpFifoEmpty_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rtpFifoEmptyMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,0,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rtpFifoEmpty_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.rtpFifoEmpty[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.rtpFifoEmpty_Count[i]++;
					if (alarmTrapDetectData.rtpFifoEmpty_Count[i] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
					{	//	発生
						alarmStatus.rtpFifoEmpty[i]				=	ALM_STATUS_DETECT;
						alarmTrapDetectData.rtpFifoEmpty_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rtpFifoEmptyMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,1,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rtpFifoEmpty_Count[i]	=	0;
				}
			}
		}
		//---------------------------------------------------------------------
		//--	ＦＵＬＬ
		//---------------------------------------------------------------------
		if ((i == 0 && !ALM_MASK_C_RTP1_FIFO_FULL_FAIL) ||
			(i == 1 && !ALM_MASK_C_RTP2_FIFO_FULL_FAIL) ||
			(i == 2 && !ALM_MASK_C_RTP3_FIFO_FULL_FAIL) ||
			(i == 3 && !ALM_MASK_C_RTP4_FIFO_FULL_FAIL))
		{
			//	正常なう
			if (!(vsMoni & 0x00000001))
			{
				if (alarmStatus.rtpFifoFull[i] != ALM_STATUS_NON)
				{
					alarmTrapDetectData.rtpFifoFull_Count[i]++;
					if (alarmTrapDetectData.rtpFifoFull_Count[i] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
					{	//	正常復帰
						alarmStatus.rtpFifoFull[i]					=	ALM_STATUS_NON;
						alarmTrapDetectData.rtpFifoFull_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rtpFifoFullMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,0,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rtpFifoFull_Count[i]	=	0;
				}
			}
			//	異常なう
			else
			{
				if (alarmStatus.rtpFifoFull[i] != ALM_STATUS_DETECT)
				{
					alarmTrapDetectData.rtpFifoFull_Count[i]++;
					if (alarmTrapDetectData.rtpFifoFull_Count[i] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
					{	//	発生
						alarmStatus.rtpFifoFull[i]					=	ALM_STATUS_DETECT;
						alarmTrapDetectData.rtpFifoFull_Count[i]	=	0;
						//	トラップ
						memcpy(mibBuf,rtpFifoFullMibHex,4);
						mibBuf[3]	+=	i;
						g_snmpSendTrap(mibBuf,4,1,HDIPRX);
					}
				}
				else
				{
					alarmTrapDetectData.rtpFifoFull_Count[i]	=	0;
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＵＤＰ　ＦＩＦＯ　ＥＭＰＴＹ　＆　ＦＵＬＬ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < 4 ; ii++ )
		{
			hDelCtl	=	xiGetHeaderDelControl1(i,0,ii);
			//-----------------------------------------------------------------
			//--	ＥＭＰＴＹ
			//-----------------------------------------------------------------
			if ((i == 0 && ii == 0 && !ALM_MASK_C_UDP_FIFO_EMPTY1_A_FAIL) ||
				(i == 0 && ii == 1 && !ALM_MASK_C_UDP_FIFO_EMPTY2_A_FAIL) ||
				(i == 0 && ii == 2 && !ALM_MASK_C_UDP_FIFO_EMPTY3_A_FAIL) ||
				(i == 0 && ii == 3 && !ALM_MASK_C_UDP_FIFO_EMPTY4_A_FAIL) ||
				(i == 1 && ii == 0 && !ALM_MASK_C_UDP_FIFO_EMPTY1_B_FAIL) ||
				(i == 1 && ii == 1 && !ALM_MASK_C_UDP_FIFO_EMPTY2_B_FAIL) ||
				(i == 1 && ii == 2 && !ALM_MASK_C_UDP_FIFO_EMPTY3_B_FAIL) ||
				(i == 1 && ii == 3 && !ALM_MASK_C_UDP_FIFO_EMPTY4_B_FAIL))
			{
				if (xiGetTxControlTxLoopMode(ii) == 0)
					alm	=	(hDelCtl & 0x00000002)?1:0; 
				else
					alm	=	0;
				//	正常なう
//				if (!(hDelCtl & 0x00000002))
				if (!(alm))
				{
					if (alarmStatus.udpFifoEmpty[i][ii] != ALM_STATUS_NON)
					{
						alarmTrapDetectData.udpFifoEmpty_Count[i][ii]++;
						if (alarmTrapDetectData.udpFifoEmpty_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	正常復帰
							alarmStatus.udpFifoEmpty[i][ii]					=	ALM_STATUS_NON;
							alarmTrapDetectData.udpFifoEmpty_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,udpFifoEmptyMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,0,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.udpFifoEmpty_Count[i][ii]	=	0;
					}
				}
				//	異常なう
				else
				{
					if (alarmStatus.udpFifoEmpty[i][ii] != ALM_STATUS_DETECT)
					{
						alarmTrapDetectData.udpFifoEmpty_Count[i][ii]++;
						if (alarmTrapDetectData.udpFifoEmpty_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	発生
							alarmStatus.udpFifoEmpty[i][ii]					=	ALM_STATUS_DETECT;
							alarmTrapDetectData.udpFifoEmpty_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,udpFifoEmptyMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,1,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.udpFifoEmpty_Count[i][ii]	=	0;
					}
				}
			}
			//-----------------------------------------------------------------
			//--	ＦＵＬＬ
			//-----------------------------------------------------------------
			if ((i == 0 && ii == 0 && !ALM_MASK_C_UDP_FIFO_FULL1_A_FAIL) ||
				(i == 0 && ii == 1 && !ALM_MASK_C_UDP_FIFO_FULL2_A_FAIL) ||
				(i == 0 && ii == 2 && !ALM_MASK_C_UDP_FIFO_FULL3_A_FAIL) ||
				(i == 0 && ii == 3 && !ALM_MASK_C_UDP_FIFO_FULL4_A_FAIL) ||
				(i == 1 && ii == 0 && !ALM_MASK_C_UDP_FIFO_FULL1_B_FAIL) ||
				(i == 1 && ii == 1 && !ALM_MASK_C_UDP_FIFO_FULL2_B_FAIL) ||
				(i == 1 && ii == 2 && !ALM_MASK_C_UDP_FIFO_FULL3_B_FAIL) ||
				(i == 1 && ii == 3 && !ALM_MASK_C_UDP_FIFO_FULL4_B_FAIL))
			{
				if (xiGetTxControlTxLoopMode(ii) == 0)
					alm	=	(hDelCtl & 0x00000001)?1:0; 
				else
					alm	=	0;
				//	正常なう
//				if (!(hDelCtl & 0x00000001))
				if (!(alm))
				{
					if (alarmStatus.udpFifoFull[i][ii] != ALM_STATUS_NON)
					{
						alarmTrapDetectData.udpFifoFull_Count[i][ii]++;
						if (alarmTrapDetectData.udpFifoFull_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	正常復帰
							alarmStatus.udpFifoFull[i][ii]					=	ALM_STATUS_NON;
							alarmTrapDetectData.udpFifoFull_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,udpFifoFullMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,0,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.udpFifoFull_Count[i][ii]	=	0;
					}
				}
				//	異常なう
				else
				{
					if (alarmStatus.udpFifoFull[i][ii] != ALM_STATUS_DETECT)
					{
						alarmTrapDetectData.udpFifoFull_Count[i][ii]++;
						if (alarmTrapDetectData.udpFifoFull_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	発生
							alarmStatus.udpFifoFull[i][ii]					=	ALM_STATUS_DETECT;
							alarmTrapDetectData.udpFifoFull_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,udpFifoFullMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,1,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.udpFifoFull_Count[i][ii]	=	0;
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	受信パケットエラー
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < 4 ; ii++ )
		{
			nowErr	=	0;
			if (xiGetTxControlTxLoopMode(ii) == 0)
			{
				//	折り返し無
				hDelCtl	=	xiGetHeaderDelControl1(i,0,ii);
				hDelCnt	=	xiGetHeaderDelControl2(i,ii);
				if (!(hDelCtl & 0x00008000) || hDelCnt < PACKET_COUNT_ERROR)	nowErr	=	1;
			}
			//-----------------------------------------------------------------
			//--	ＰＡＣＫＥＴ
			//-----------------------------------------------------------------
			if ((i == 0 && ii == 0 && !ALM_MASK_C_PACKET_COUNT1_A_FAIL) ||
				(i == 0 && ii == 1 && !ALM_MASK_C_PACKET_COUNT2_A_FAIL) ||
				(i == 0 && ii == 2 && !ALM_MASK_C_PACKET_COUNT3_A_FAIL) ||
				(i == 0 && ii == 3 && !ALM_MASK_C_PACKET_COUNT4_A_FAIL) ||
				(i == 1 && ii == 0 && !ALM_MASK_C_PACKET_COUNT1_B_FAIL) ||
				(i == 1 && ii == 1 && !ALM_MASK_C_PACKET_COUNT2_B_FAIL) ||
				(i == 1 && ii == 2 && !ALM_MASK_C_PACKET_COUNT3_B_FAIL) ||
				(i == 1 && ii == 3 && !ALM_MASK_C_PACKET_COUNT4_B_FAIL))
			{
				//	正常なう
				if (!nowErr)
				{
					if (alarmStatus.packetCountError[i][ii] != ALM_STATUS_NON)
					{
						alarmTrapDetectData.packetCountError_Count[i][ii]++;
						if (alarmTrapDetectData.packetCountError_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	正常復帰
							alarmStatus.packetCountError[i][ii]					=	ALM_STATUS_NON;
							alarmTrapDetectData.packetCountError_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,packetCountErrorMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,0,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.packetCountError_Count[i][ii]	=	0;
					}
				}
				//	異常なう
				else
				{
					if (alarmStatus.packetCountError[i][ii] != ALM_STATUS_DETECT)
					{
						alarmTrapDetectData.packetCountError_Count[i][ii]++;
						if (alarmTrapDetectData.packetCountError_Count[i][ii] >= RX_PCK_EMPTY_FULL_FAIL_COUNT)
						{	//	発生
							alarmStatus.packetCountError[i][ii]					=	ALM_STATUS_DETECT;
							alarmTrapDetectData.packetCountError_Count[i][ii]	=	0;
							//	トラップ
							memcpy(mibBuf,packetCountErrorMibHex,5);
							mibBuf[3]	+=	i;
							mibBuf[4]	+=	ii;
							g_snmpSendTrap(mibBuf,5,1,HDIPRX);
						}
					}
					else
					{
						alarmTrapDetectData.packetCountError_Count[i][ii]	=	0;
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＩＰ　ＣＲＣＥＲＲ
	//-------------------------------------------------------------------------
	for ( i = 0 ; i < 2 ; i++ )
	{
		for ( ii = 0 ; ii < 4 ; ii++ )
		{
			if ((i == 0 && ii == 0 && !ALM_MASK_C_VIDEO1_CRCERR_A_FAIL) ||
				(i == 0 && ii == 1 && !ALM_MASK_C_VIDEO2_CRCERR_A_FAIL) ||
				(i == 0 && ii == 2 && !ALM_MASK_C_VIDEO3_CRCERR_A_FAIL) ||
				(i == 0 && ii == 3 && !ALM_MASK_C_VIDEO4_CRCERR_A_FAIL) ||
				(i == 1 && ii == 0 && !ALM_MASK_C_VIDEO1_CRCERR_B_FAIL) ||
				(i == 1 && ii == 1 && !ALM_MASK_C_VIDEO2_CRCERR_B_FAIL) ||
				(i == 1 && ii == 2 && !ALM_MASK_C_VIDEO3_CRCERR_B_FAIL) ||
				(i == 1 && ii == 3 && !ALM_MASK_C_VIDEO4_CRCERR_B_FAIL))
			{
				if (alarmStatus.sdiVideoCrcErrRx[i][ii] != alarmBackupStatus.sdiVideoCrcErrRx[i][ii])
				{
					if (alarmStatus.sdiVideoCrcErrRx[i][ii] == ALM_STATUS_NON)
					{	//	回復
						//	トラップ
						memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
						mibBuf[2]	+=	i;
						mibBuf[3]	+=	ii;
						g_snmpSendTrap(mibBuf,4,0,HDIPRX);
					}
					else
					{	//	発生
						//	トラップ
						memcpy(mibBuf,sdiVideoCrcErrMibHex,4);
						mibBuf[2]	+=	i;
						mibBuf[3]	+=	ii;
						g_snmpSendTrap(mibBuf,4,1,HDIPRX);
					}
					alarmBackupStatus.sdiVideoCrcErrRx[i][ii]	=	alarmStatus.sdiVideoCrcErrRx[i][ii];
				}
			}
		}
	}
	//-------------------------------------------------------------------------
	//--	ＩＰ　ＭＣ　ＭＩＳＭＡＴＣＨ
	//-------------------------------------------------------------------------
	//	IP CRCERR監視 かつ [TX_FORMAT_OK][TX_LOCKED]が有効？
	for ( i = 0 ; i < 4 ; i++ )
	{
		if ((i == 0 && !ALM_MASK_C_IP_MC_MISMATCH1_FAIL) ||
			(i == 1 && !ALM_MASK_C_IP_MC_MISMATCH2_FAIL) ||
			(i == 2 && !ALM_MASK_C_IP_MC_MISMATCH3_FAIL) ||
			(i == 3 && !ALM_MASK_C_IP_MC_MISMATCH4_FAIL))
		{
			if (alarmStatus.ipMcMismatchRx[i] != alarmBackupStatus.ipMcMismatchRx[i])
			{
				if (alarmStatus.ipMcMismatchRx[i] == ALM_STATUS_NON)
				{
					//	トラップ
					memcpy(mibBuf,ipMcMismatchHex,4);
					mibBuf[3]	+=	i;
					g_snmpSendTrap(mibBuf,4,0,HDIPRX);
				}
				else
				{
					//	トラップ
					memcpy(mibBuf,ipMcMismatchHex,4);
					mibBuf[3]	+=	i;
					g_snmpSendTrap(mibBuf,4,1,HDIPRX);
				}
				alarmBackupStatus.ipMcMismatchRx[i]	=	alarmStatus.ipMcMismatchRx[i];
			}
		}
	}
}
//=============================================================================
//==	ＲＸ　Ｆａｍｉｌｙ　ＯＫ判定
//=============================================================================
static	BOOL	isFamilyOk(uint32_t hac)
{
	BOOL		ret	=	FALSE;
	uint32_t	family	=	(hac & 0x0f000000) >> 24;
	if (family == 0x00000000)	ret	=	TRUE;	//	0000 SMPTE274	1920×1080
//	if (family == 0x00000001)	ret	=	TRUE;	//	0001 SMPTE296	1280×720
//	if (family == 0x00000002)	ret	=	TRUE;	//	0010 SMPTE2048	2048×1080
	if (family == 0x00000003)	ret	=	TRUE;	//	0011 SMPTE295	1920×1080
	if (family == 0x00000008)	ret	=	TRUE;	//	1000 NTSC		720×486
//	if (family == 0x00000009)	ret	=	TRUE;	//	1001 PAL		720×576

	return(ret);
}
//=============================================================================
//==	ＴＸ　ＶＩＤＥＯフォーマット不正判定
//=============================================================================
static	BOOL		isTxFormatFail(uint8_t ch)
{
	BOOL		ret	=	FALSE;
	uint8_t		srgPSel;
	uint16_t	srgST;
	uint32_t	rxMoni,addc1,rxMoni2;
//	if (!(commonData.dipSw4 & 0x04))	//	他走モードのみチェック
//	{
		srgPSel	=	csz3GetSdiXpPsel(ch);
		// 2019.06.07 [Add] 入力FS 時の不具合
//		if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09 || srgPSel == 0x0C)
		if (srgPSel == 0x01 || srgPSel == 0x02 || srgPSel == 0x09)
		{
			srgST	=	csz3GetSdiStatus(ch);
			if ((srgST & 0x00ff) == 0x003F)	ret	=	TRUE;
		}
		else
		{
			rxMoni	=	xiGetRxMonitor1(ch);
			if ((rxMoni & 0x00004000))		//	RX_T_LOCKED
			{
				addc1	=	xiGetHeaderAddControl1(0,ch);
				rxMoni2	=	xiGetRxMonitor2(ch);
				//	RATE
				if (((rxMoni & 0x000000F0) >> 4) != ((addc1 & 0x0000F000) >> 12))
				{
					ret	=	TRUE;
				}
				//	FAMILY
				if ((rxMoni & 0x0000000F) != ((addc1 & 0x00000F00) >> 8))
				{
					ret	=	TRUE;
				}
				//	SCAN
				if (((rxMoni & 0x00001000) >> 12) != ((addc1 & 0x00000040) >> 6))
				{
					ret	=	TRUE;
				}
				//	FORMAT NG
				if (!(rxMoni2 & 0x08000000) && (rxMoni2 & 0x10000000))
				{
					ret	=	TRUE;
				}
			}
		}
//	}
	return(ret);
}
//=============================================================================
//==	ＲＸ　ＶＩＤＥＯフォーマット不正判定
//=============================================================================
static	BOOL		isRxFormatFail(uint8_t ch)
{
	BOOL		ret	=	FALSE;
	uint32_t	addc1,txcnt,fmt,rate,scan;
	uint32_t	TX_MODE,TX_M;
	if (!(commonData.dipSw4 & 0x04))	//	他走モードのみチェック
	{
		addc1	=	xiGetHeaderAddControl1(0,ch);
		if ((addc1 & 0x00800000))		//	DEL_VIDEO_LOCKED
		{
			rate	=	(addc1 & 0xF0000000) >> 28;
			fmt		=	(addc1 & 0x0F000000) >> 24;
			scan	=	(addc1 & 0x00400000);
			txcnt	=	xiGetTxControl(0,ch);
			TX_MODE	=	txcnt & 0x00000003;
			TX_M	=	(txcnt & 0x0000000C) >> 2;
			if (TX_MODE == 0x00000000 && TX_M == 0x00000001)			//	HD TX_M:1
			{
				if (scan)	ret	=	TRUE;
				if (rate != 0x00000006 || fmt != 0x00000000)	ret	=	TRUE;
			}
			else if (TX_MODE == 0x00000000 && TX_M == 0x00000000)		//	HD TX_M:0
			{
				if (scan)	ret	=	TRUE;
				if (rate != 0x00000005 || fmt != 0x00000000)	ret	=	TRUE;
			}
			else if (TX_MODE == 0x00000002 && TX_M == 0x00000001)		//	3G TX_M:1
			{
				if (!scan)	ret	=	TRUE;
				if (rate != 0x0000000A || fmt != 0x00000000)	ret	=	TRUE;
			}
			else if (TX_MODE == 0x00000002 && TX_M == 0x00000000)		//	3G TX_M:0
			{
				if (!scan)	ret	=	TRUE;
				if (rate != 0x00000009 || fmt != 0x00000000)	ret	=	TRUE;
			}
			else if (TX_MODE == 0x00000001 && TX_M == 0x00000001)		//	SD TX_M:1
			{
				if (scan)	ret	=	TRUE;
				if (rate != 0x00000006 || fmt != 0x00000008)	ret	=	TRUE;
			}
			else if (TX_MODE == 0x00000001 && TX_M == 0x00000000)		//	SD TX_M:0
			{
				if (scan)	ret	=	TRUE;
				if (rate != 0x00000005 || fmt != 0x00000009)	ret	=	TRUE;
			}
		}
	}
	return(ret);
}
//=============================================================================
//==	アラームトラップ状態クリーンアップ
//=============================================================================
static	void	alarmTrapClean(void)
{
	//	C-BIT
	if (ALM_MASK_C_TX_FAULT_A_FAIL)
	{
		alarmStatus.txFault_C[0]				=	ALM_STATUS_NON;
		alarmTrapDetectData.txFault_C_Count[0]	=	0;
	}
	if (ALM_MASK_C_TX_FAULT_B_FAIL)
	{
		alarmStatus.txFault_C[1]				=	ALM_STATUS_NON;
		alarmTrapDetectData.txFault_C_Count[1]	=	0;
	}
	if (ALM_MASK_C_RX_LOSS_A_FAIL)
	{
		alarmStatus.rxLos_C[0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rxLos_C_Count[0]	=	0;
	}
	if (ALM_MASK_C_RX_LOSS_B_FAIL)
	{
		alarmStatus.rxLos_C[1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rxLos_C_Count[1]	=	0;
	}
	if (ALM_MASK_C_VIDEO1_STATUS_FAIL)
	{
		alarmStatus.sdiVideoStatusTx[0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusTx_Count[0]	=	0;
		alarmStatus.sdiVideoStatusRx[0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusRx_Count[0]	=	0;
	}
	if (ALM_MASK_C_VIDEO1_CRCERR_FAIL)
	{
		alarmStatus.sdiVideoCrcErr[0]				=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErr[0]			=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErr_Count[0]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[0]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_Count[0]		=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_mode[0]			=	0;
	}
	if (ALM_MASK_C_VIDEO2_STATUS_FAIL)
	{
		alarmStatus.sdiVideoStatusTx[1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusTx_Count[1]	=	0;
		alarmStatus.sdiVideoStatusRx[1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusRx_Count[1]	=	0;
	}
	if (ALM_MASK_C_VIDEO2_CRCERR_FAIL)
	{
		alarmStatus.sdiVideoCrcErr[1]				=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErr[1]			=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErr_Count[1]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[1]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_Count[1]		=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_mode[1]			=	0;
	}
	if (ALM_MASK_C_VIDEO3_STATUS_FAIL)
	{
		alarmStatus.sdiVideoStatusTx[2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusTx_Count[2]	=	0;
		alarmStatus.sdiVideoStatusRx[2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusRx_Count[2]	=	0;
	}
	if (ALM_MASK_C_VIDEO3_CRCERR_FAIL)
	{
		alarmStatus.sdiVideoCrcErr[2]				=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErr[2]			=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErr_Count[2]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[2]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_Count[2]		=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_mode[2]			=	0;
	}
	if (ALM_MASK_C_VIDEO4_STATUS_FAIL)
	{
		alarmStatus.sdiVideoStatusTx[3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusTx_Count[3]	=	0;
		alarmStatus.sdiVideoStatusRx[3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoStatusRx_Count[3]	=	0;
	}
	if (ALM_MASK_C_VIDEO4_CRCERR_FAIL)
	{
		alarmStatus.sdiVideoCrcErr[3]				=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErr[3]			=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErr_Count[3]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTxTime_Count[3]	=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_Count[3]		=	0;
		alarmTrapDetectData.sdiVideoFormatErrTx_mode[3]			=	0;
	}
	if (ALM_MASK_C_RTP1_FIFO_EMPTY_FAIL)
	{
		alarmStatus.rtpFifoEmpty[0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoEmpty_Count[0]	=	0;
	}
	if (ALM_MASK_C_RTP2_FIFO_EMPTY_FAIL)
	{
		alarmStatus.rtpFifoEmpty[1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoEmpty_Count[1]	=	0;
	}
	if (ALM_MASK_C_RTP3_FIFO_EMPTY_FAIL)
	{
		alarmStatus.rtpFifoEmpty[2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoEmpty_Count[2]	=	0;
	}
	if (ALM_MASK_C_RTP4_FIFO_EMPTY_FAIL)
	{
		alarmStatus.rtpFifoEmpty[3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoEmpty_Count[3]	=	0;
	}
	if (ALM_MASK_C_RTP1_FIFO_FULL_FAIL)
	{
		alarmStatus.rtpFifoFull[0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoFull_Count[0]	=	0;
	}
	if (ALM_MASK_C_RTP2_FIFO_FULL_FAIL)
	{
		alarmStatus.rtpFifoFull[1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoFull_Count[1]	=	0;
	}
	if (ALM_MASK_C_RTP3_FIFO_FULL_FAIL)
	{
		alarmStatus.rtpFifoFull[2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoFull_Count[2]	=	0;
	}
	if (ALM_MASK_C_RTP4_FIFO_FULL_FAIL)
	{
		alarmStatus.rtpFifoFull[3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.rtpFifoFull_Count[3]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY1_A_FAIL)
	{
		alarmStatus.udpFifoEmpty[0][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[0][0]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY2_A_FAIL)
	{
		alarmStatus.udpFifoEmpty[0][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[0][1]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY3_A_FAIL)
	{
		alarmStatus.udpFifoEmpty[0][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[0][2]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY4_A_FAIL)
	{
		alarmStatus.udpFifoEmpty[0][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[0][3]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY1_B_FAIL)
	{
		alarmStatus.udpFifoEmpty[1][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[1][0]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY2_B_FAIL)
	{
		alarmStatus.udpFifoEmpty[1][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[1][1]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY3_B_FAIL)
	{
		alarmStatus.udpFifoEmpty[1][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[1][2]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_EMPTY4_B_FAIL)
	{
		alarmStatus.udpFifoEmpty[1][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoEmpty_Count[1][3]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL1_A_FAIL)
	{
		alarmStatus.udpFifoFull[0][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[0][0]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL2_A_FAIL)
	{
		alarmStatus.udpFifoFull[0][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[0][1]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL3_A_FAIL)
	{
		alarmStatus.udpFifoFull[0][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[0][2]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL4_A_FAIL)
	{
		alarmStatus.udpFifoFull[0][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[0][3]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL1_B_FAIL)
	{
		alarmStatus.udpFifoFull[1][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[1][0]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL2_B_FAIL)
	{
		alarmStatus.udpFifoFull[1][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[1][1]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL3_B_FAIL)
	{
		alarmStatus.udpFifoFull[1][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[1][2]	=	0;
	}
	if (ALM_MASK_C_UDP_FIFO_FULL4_B_FAIL)
	{
		alarmStatus.udpFifoFull[1][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.udpFifoFull_Count[1][3]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT1_A_FAIL)
	{
		alarmStatus.packetCountError[0][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[0][0]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT2_A_FAIL)
	{
		alarmStatus.packetCountError[0][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[0][1]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT3_A_FAIL)
	{
		alarmStatus.packetCountError[0][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[0][2]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT4_A_FAIL)
	{
		alarmStatus.packetCountError[0][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[0][3]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT1_B_FAIL)
	{
		alarmStatus.packetCountError[1][0]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[1][0]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT2_B_FAIL)
	{
		alarmStatus.packetCountError[1][1]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[1][1]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT3_B_FAIL)
	{
		alarmStatus.packetCountError[1][2]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[1][2]	=	0;
	}
	if (ALM_MASK_C_PACKET_COUNT4_B_FAIL)
	{
		alarmStatus.packetCountError[1][3]					=	ALM_STATUS_NON;
		alarmTrapDetectData.packetCountError_Count[1][3]	=	0;
	}
	if (ALM_MASK_C_REF_IN_FAIL)
	{
		alarmStatus.refInFail				=	ALM_STATUS_NON;
		alarmTrapDetectData.refInFail_Count	=	0;
	}
	if (ALM_MASK_C_PS_STATUS_FAIL)
	{
		alarmStatus.psStatusFail				=	ALM_STATUS_NON;
		alarmTrapDetectData.psStatusFail_Count	=	0;
	}
	if (ALM_MASK_C_TEMP_FAIL)
	{
		alarmStatus.tempFail				=	ALM_STATUS_NON;
		alarmTrapDetectData.tempFail_Count	=	0;
	}
	if (ALM_MASK_C_1G_LINK_DOWN_FAIL)
	{
		alarmStatus.l1GLinkDown					=	ALM_STATUS_NON;
		alarmTrapDetectData.l1GLinkDown_Count	=	0;
	}
	//	追加	
	if (ALM_MASK_C_VIDEO1_CRCERR_A_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[0][0]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[0][0]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[0][0]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[0][0]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[0][0]			=	0;
	}
	if (ALM_MASK_C_VIDEO2_CRCERR_A_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[0][1]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[0][1]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[0][1]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[0][1]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[0][1]			=	0;
	}
	if (ALM_MASK_C_VIDEO3_CRCERR_A_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[0][2]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[0][2]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[0][2]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[0][2]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[0][2]			=	0;
	}
	if (ALM_MASK_C_VIDEO4_CRCERR_A_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[0][3]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[0][3]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[0][3]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[0][3]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[0][3]			=	0;
	}
	if (ALM_MASK_C_VIDEO1_CRCERR_B_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[1][0]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[1][0]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[1][0]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[1][0]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[1][0]			=	0;
	}
	if (ALM_MASK_C_VIDEO2_CRCERR_B_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[1][1]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[1][1]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[1][1]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[1][1]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[1][1]			=	0;
	}
	if (ALM_MASK_C_VIDEO3_CRCERR_B_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[1][2]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[1][2]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[1][2]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[1][2]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[1][2]			=	0;
	}
	if (ALM_MASK_C_VIDEO4_CRCERR_B_FAIL)
	{
		alarmStatus.sdiVideoCrcErrRx[1][3]						=	ALM_STATUS_NON;
		alarmBackupStatus.sdiVideoCrcErrRx[1][3]				=	ALM_STATUS_NON;
		alarmTrapDetectData.sdiVideoCrcErrRxTime_Count[1][3]	=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_Count[1][3]		=	0;
		alarmTrapDetectData.sdiVideoCrcErrRx_mode[1][3]			=	0;
	}
	if (ALM_MASK_C_IP_MC_MISMATCH1_FAIL)
	{
		alarmStatus.ipMcMismatchRx[0]					=	ALM_STATUS_NON;
		alarmBackupStatus.ipMcMismatchRx[0]				=	ALM_STATUS_NON;
		alarmTrapDetectData.ipMcMismatchRxTime_Count[0]	=	0;
		alarmTrapDetectData.ipMcMismatchRx_Count[0]		=	0;
	}
	if (ALM_MASK_C_IP_MC_MISMATCH2_FAIL)
	{
		alarmStatus.ipMcMismatchRx[1]					=	ALM_STATUS_NON;
		alarmBackupStatus.ipMcMismatchRx[1]				=	ALM_STATUS_NON;
		alarmTrapDetectData.ipMcMismatchRxTime_Count[1]	=	0;
		alarmTrapDetectData.ipMcMismatchRx_Count[1]		=	0;
	}
	if (ALM_MASK_C_IP_MC_MISMATCH3_FAIL)
	{
		alarmStatus.ipMcMismatchRx[2]					=	ALM_STATUS_NON;
		alarmBackupStatus.ipMcMismatchRx[2]				=	ALM_STATUS_NON;
		alarmTrapDetectData.ipMcMismatchRxTime_Count[2]	=	0;
		alarmTrapDetectData.ipMcMismatchRx_Count[2]		=	0;
	}
	if (ALM_MASK_C_IP_MC_MISMATCH4_FAIL)
	{
		alarmStatus.ipMcMismatchRx[3]					=	ALM_STATUS_NON;
		alarmBackupStatus.ipMcMismatchRx[3]				=	ALM_STATUS_NON;
		alarmTrapDetectData.ipMcMismatchRxTime_Count[3]	=	0;
		alarmTrapDetectData.ipMcMismatchRx_Count[3]		=	0;
	}
}
