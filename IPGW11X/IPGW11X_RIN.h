#ifndef __IPGW11X_RIN_H__
#define __IPGW11X_RIN_H__

/** @note ・comに置いても問題ないかも
 *        ・rin_type_eはいらなかったかも
 *        ⇒rin_gpio_port_eとrin_rtport_port_eを連番にして管理すべきだったか
 *        ⇒RIN_GPIO_TBLとRIN_RTPORT_TBLで区別するために残す方針とする(区別する意味があるかは不明)
 */

/** @note 各種列挙型変数名は移植を行いやすくするため、そのまま定義 */

/* RIN_XXXX 種類 */
typedef enum rin_type_e {
	RIN_TYPE_MIN = 0U,
	RIN_GPIO = RIN_TYPE_MIN,
	RIN_RTPORT,
	RIN_TYPE_MAX,
} rin_type_e;

/* RIN_GPIO ポート番号 */
typedef enum rin_gpio_port_e {
	RIN_GPIO_PORT_MIN = 0U,
	P0B = RIN_GPIO_PORT_MIN,
	P1B,
	P2B,
	P3B,
	P4B,
	P5B,
	P6B,
	P7B,
	RIN_GPIO_PORT_MAX,
} rin_gpio_port_e;

/* RIN_GPIO(input互換) ポート番号 */
typedef enum rin_gpio_port_in_e {
	PIN0B = RIN_GPIO_PORT_MIN,
	PIN1B,
	PIN2B,
	PIN3B,
	PIN4B,
	PIN5B,
	PIN6B,
	PIN7B,
} rin_gpio_port_in_e;

/* RIN_RTPORT ポート番号 */
typedef enum rin_rtport_port_e {
	RIN_RTPORT_PORT_MIN = 0U,
	RP0B = RIN_RTPORT_PORT_MIN,
	RP1B,
	RP2B,
	RP3B,
	RIN_RTPORT_PORT_MAX,
} rin_rtport_port_e;

/* RIN_RTPORT(input互換) ポート番号 */
typedef enum rin_rtport_port_in_e {
	RPIN0B = RIN_RTPORT_PORT_MIN,
	RPIN1B,
	RPIN2B,
	RPIN3B,
} rin_rtport_port_in_e;

#endif /* __IPGW11X_RIN_H__ */