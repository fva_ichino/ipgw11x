#ifndef	__IPGW11X_DS100BR_H__
#define	__IPGW11X_DS100BR_H__

#include <stdint.h>

void ds100br_init(void);
void ds100br_write(uint16_t addr, uint8_t *data, uint16_t len);
void ds100br_read(uint16_t addr, uint8_t *data, uint16_t len);

#endif /* __IPGW11X_DS100BR_H__ */