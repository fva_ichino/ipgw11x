//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	アップデートＵＤＰタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル関数
//=============================================================================
static	void	promErase(void);
//static	void	promRead(uint16_t* buf, uint32_t addr, uint32_t size);//（現在未使用）
static	void	promWrite(uint16_t* buf, uint32_t addr, uint32_t size);
//=============================================================================
//==	グローバル定義
//=============================================================================
extern	uint8_t				udp_readData[UDP_MAX_LEN];
extern	uint32_t			udp_readDataLen;
extern	MSG_BSC_FORMAT		udp_writeData;
extern	uint32_t			udp_writeDataLen;
extern	MSG_BSC_FORMAT		usb_writeData;		//	## USB UPDATE ##
extern	uint32_t			usb_writeDataLen;	//	## USB UPDATE ##
//=============================================================================
//==	ローカル定義
//=============================================================================
#define	PROM_TOP			0x10000000
#define	ERASE_SECTOR_NUM	11
static	uint8_t	fwUpdateRWBuf[FW_UPDATE_BLOCK_LEN];

// RTOS由来
#define	__IO	volatile		/*!< defines 'read / write' permissions              */
//=============================================================================
//==	アップデートＵＤＰタスク関数
//=============================================================================
void	g_udp_upd_task(void)
{
	LOG_THREAD();

	uint32_t	i;
	//-------------------------------------------------------------------------
	//--	初期化完了待ち
	//-------------------------------------------------------------------------
	while (1)
	{
		if (commonData.taskInit == 1)	break;
		tslp_tsk(10);
  	}
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		tslp_tsk(1000);
		if (fwUpdateBuf.updateStatus == FW_UPDATE_ST_PRG)
		{
			//-----------------------------------------------------------------
			//--	フラッシュ初期化	TOP 256K erase
			//-----------------------------------------------------------------
			promErase();
			tslp_tsk(300);
			//-----------------------------------------------------------------
			//--	バックアップエリアからコピー
			//-----------------------------------------------------------------
			for ( i = 0 ; i < fwUpdateBuf.blockNum ; i++ )
			{
/** @todo 処理削除予定 */
#if 0
				setting_read((uint8_t *)&fwUpdateRWBuf[0],SETTING_PRG_BACKUP + i * FW_UPDATE_BLOCK_LEN,FW_UPDATE_BLOCK_LEN);
#endif
				promWrite((uint16_t *)&fwUpdateRWBuf[0],i * FW_UPDATE_BLOCK_LEN,FW_UPDATE_BLOCK_LEN);
				fwUpdateBuf.writeDoneBlockNo++;
				tslp_tsk(4);
			}
			fwUpdateBuf.updateStatus	=	FW_UPDATE_ST_DON;
		}
	}
}
//=============================================================================
//==	アップデート電文関数
//=============================================================================
uint32_t	g_updateControl(MSG_BSC_FORMAT *mf,int32_t AorB)
{
	uint32_t	writeLen	=	0;
	uint32_t	i;
	uint16_t	idx;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ファイル転送開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_FILE_START:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_FST_SET_TO_IPG)))
		{
			//	CPU
			if (mf->data.updFstSetToIpg.kind == UPD_KIND_CPU)
			{
				if (fwUpdateBuf.updateStatus == FW_UPDATE_ST_PRG)	udp_writeData.data.updFstIpgToSet.result	=	1;	//	書き込み中ＮＧ
				else
				{
					//---------------------------------------------------------
					//--	フラッシュ初期化	BACKUP Buffer 256K erase
					//---------------------------------------------------------
					for ( i = 0 ; i < 4 ; i++ )
					{
/** @todo 処理削除予定 */
#if 0
						// setting_erase(SETTING_PRG_BACKUP + i * SETTING_PRG_SECSIZE);
#endif
					}
					udp_writeData.data.updFstIpgToSet.result	=	0;
					fwUpdateBuf.kind			=	UPD_KIND_CPU;
					memset(&fwUpdateBuf.flag[0],0,FW_UPDATE_BUFFER_MAX);
					fwUpdateBuf.updateStatus	=	FW_UPDATE_ST_DLD;
					fwUpdateBuf.blockNum		=	(mf->data.updFstSetToIpg.totalSize - 1) / FW_UPDATE_BLOCK_LEN + 1;
				}
				udp_writeData.data.updFstIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_FST_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_UPD_FST_IPG_TO_SET);
			}
			//	FPGA1(Kintex-7,UltraScaleKintex7)
			else if (mf->data.updFstSetToIpg.kind == UPD_KIND_FPGA1)
			{
				//-------------------------------------------------------------
				//--	正常応答を返すだけ
				//-------------------------------------------------------------
				udp_writeData.data.updFstIpgToSet.result	=	0;
				fwUpdateBuf.kind			=	UPD_KIND_FPGA1;
				fwUpdateBuf.updateStatus	=	FW_UPDATE_ST_DLD;
				fwUpdateBuf.blockNum		=	(mf->data.updFstSetToIpg.totalSize - 1) / FW_UPDATE_BLOCK_LEN + 1;
				udp_writeData.data.updFstIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_FST_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_UPD_FST_IPG_TO_SET);
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	ブロック転送
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_BLOCK_SEND:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_BLS_SET_TO_IPG)))
		{
			//	CPU
			if (fwUpdateBuf.kind == UPD_KIND_CPU)
			{
				if (fwUpdateBuf.updateStatus != FW_UPDATE_ST_DLD)	udp_writeData.data.updBlsIpgToSet.result	=	1;	//	転送中以外ＮＧ
				else
				{
					udp_writeData.data.updBlsIpgToSet.result	=	0;
					idx	=	mf->data.updBlsSetToIpg.bNo;
					if (idx >= 1 && idx <= fwUpdateBuf.blockNum)
					{
						if (fwUpdateBuf.flag[idx - 1] == 0)
						{
/** @todo 処理削除予定 */
#if 0
							setting_write((uint8_t *)mf->data.updBlsSetToIpg.buf,SETTING_PRG_BACKUP + (idx - 1) * FW_UPDATE_BLOCK_LEN,FW_UPDATE_BLOCK_LEN);
#endif
							fwUpdateBuf.flag[idx - 1]	=	1;
						}
					}
				}
				udp_writeData.data.updBlsIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET);
			}
			//	FPGA1(Kintex-7,UltraScaleKintex7)
			else if (fwUpdateBuf.kind == UPD_KIND_FPGA1)
			{
				if (fwUpdateBuf.updateStatus != FW_UPDATE_ST_DLD)	udp_writeData.data.updBlsIpgToSet.result	=	1;	//	転送中以外ＮＧ
				else
				{
					udp_writeData.data.updBlsIpgToSet.result	=	0;
					idx	=	mf->data.updBlsSetToIpg.bNo;
					if (idx >= 1 && idx <= fwUpdateBuf.blockNum)
					{
						//	ここでＦＰＧＡへの転送を行う
					}
				}
				udp_writeData.data.updBlsIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET);
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	書き込み開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_PRG_START:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_PGO_SET_TO_IPG)))
		{
			//	CPU
			if (mf->data.updPgoSetToIpg.kind == UPD_KIND_CPU)
			{
				if (fwUpdateBuf.updateStatus != FW_UPDATE_ST_DLD)	udp_writeData.data.updPgoIpgToSet.result	=	0xffff;	//	転送中以外ＮＧ
				else
				{
					udp_writeData.data.updPgoIpgToSet.result	=	0;
					//	全データ受信チェック
					for ( i = 0 ; i < fwUpdateBuf.blockNum ; i++ )
					{
						if (fwUpdateBuf.flag[i] == 0)
						{
							udp_writeData.data.updPgoIpgToSet.result	=	i + 1;
							break;
						}
					}
					//	書き込み開始
					if (udp_writeData.data.updPgoIpgToSet.result == 0)
					{
						fwUpdateBuf.writeDoneBlockNo	=	0;
						fwUpdateBuf.updateStatus		=	FW_UPDATE_ST_PRG;
					}
				}
				udp_writeData.data.updPgoIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_PGO_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_UPD_PGO_IPG_TO_SET);
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	書き込み状況問い合わせ
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_PRG_STATUS:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_PST_SET_TO_IPG)))
		{
			if (fwUpdateBuf.updateStatus < FW_UPDATE_ST_PRG)	udp_writeData.data.updPstIpgToSet.bNo	=	0;	//	書き込み中未満ＮＧ
			else
			{
				udp_writeData.data.updPstIpgToSet.bNo	=	fwUpdateBuf.writeDoneBlockNo;
			}
			udp_writeData.data.updPstIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_PST_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_PST_IPG_TO_SET);
		}
		break;
	//-------------------------------------------------------------------------
	//--	リセット要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_RESET:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_RST_SET_TO_IPG)))
		{
			g_reset();
		}
		break;
	//-------------------------------------------------------------------------
	//--	その他の情報交換要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_ETC:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_ETC_COMMON)))
		{
			memset(udp_writeData.data.updEtcCommon.data,0,FW_UPDATE_ETC_LEN);
			udp_writeData.data.updEtcCommon.subCode1	=	mf->data.updEtcCommon.subCode1;
			udp_writeData.data.updEtcCommon.subCode2	=	mf->data.updEtcCommon.subCode2;
			//	バージョン情報
			if (mf->data.updEtcCommon.subCode1 == 1)
			{
				memcpy(&udp_writeData.data.updEtcCommon.data[0],modelInf[0].firmwareVer,4);
				memcpy(&udp_writeData.data.updEtcCommon.data[4],modelInf[0].FPGA1Ver,4);
				memcpy(&udp_writeData.data.updEtcCommon.data[8],modelInf[0].FPGA2Ver,4);
				memcpy(&udp_writeData.data.updEtcCommon.data[12],modelInf[0].FPGA3Ver,4);
				memcpy(&udp_writeData.data.updEtcCommon.data[16],modelInf[0].setGVer,4);
				memcpy(&udp_writeData.data.updEtcCommon.data[20],modelInf[0].setPVer,4);
			}
			udp_writeData.data.updEtcCommon.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_ETC_COMMON) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_ETC_COMMON);
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	アップデート電文関数（ＵＳＢ）	## USB UPDATE ##
//=============================================================================
uint32_t	g_updateControlUsb(MSG_BSC_FORMAT *mf)
{
	uint32_t	writeLen	=	0;
	uint32_t	i;
	uint16_t	idx;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ファイル転送開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_FILE_START:
		//	CPU
		if (mf->data.updFstSetToIpg.kind == UPD_KIND_CPU)
		{
			if (fwUpdateBuf.updateStatus == FW_UPDATE_ST_PRG)	usb_writeData.data.updFstIpgToSet.result	=	1;	//	書き込み中ＮＧ
			else
			{
				//-------------------------------------------------------------
				//--	フラッシュ初期化	BACKUP Buffer 256K erase
				//-------------------------------------------------------------
				for ( i = 0 ; i < 4 ; i++ )
				{
/** @todo 処理削除予定 */
#if 0
					// setting_erase(SETTING_PRG_BACKUP + i * SETTING_PRG_SECSIZE);
#endif
				}
				usb_writeData.data.updFstIpgToSet.result	=	0;
				memset(&fwUpdateBuf.flag[0],0,FW_UPDATE_BUFFER_MAX);
				fwUpdateBuf.updateStatus	=	FW_UPDATE_ST_DLD;
				fwUpdateBuf.blockNum		=	(mf->data.updFstSetToIpg.totalSize - 1) / FW_UPDATE_BLOCK_LEN + 1;
			}
			usb_writeData.data.updFstIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_FST_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_FST_IPG_TO_SET);
		}
		break;
	//-------------------------------------------------------------------------
	//--	ブロック転送
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_BLOCK_SEND:
		if (fwUpdateBuf.updateStatus != FW_UPDATE_ST_DLD)	usb_writeData.data.updBlsIpgToSet.result	=	1;	//	転送中以外ＮＧ
		else
		{
			usb_writeData.data.updBlsIpgToSet.result	=	0;
			idx	=	mf->data.updBlsSetToIpg.bNo;
			if (idx >= 1 && idx <= fwUpdateBuf.blockNum)
			{
				if (fwUpdateBuf.flag[idx - 1] == 0)
				{
/** @todo 処理削除予定 */
#if 0
					setting_write((uint8_t *)mf->data.updBlsSetToIpg.buf,SETTING_PRG_BACKUP + (idx - 1) * FW_UPDATE_BLOCK_LEN,FW_UPDATE_BLOCK_LEN);
#endif
					fwUpdateBuf.flag[idx - 1]	=	1;
				}
			}
		}
		usb_writeData.data.updBlsIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
		writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
						(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
						(uint32_t)sizeof(MSG_BSC_UPD_BLS_IPG_TO_SET);
		break;
	//-------------------------------------------------------------------------
	//--	書き込み開始
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_PRG_START:
		//	CPU
		if (mf->data.updPgoSetToIpg.kind == UPD_KIND_CPU)
		{
			if (fwUpdateBuf.updateStatus != FW_UPDATE_ST_DLD)	usb_writeData.data.updPgoIpgToSet.result	=	0xffff;	//	転送中以外ＮＧ
			else
			{
				usb_writeData.data.updPgoIpgToSet.result	=	0;
				//	全データ受信チェック
				for ( i = 0 ; i < fwUpdateBuf.blockNum ; i++ )
				{
					if (fwUpdateBuf.flag[i] == 0)
					{
						usb_writeData.data.updPgoIpgToSet.result	=	i + 1;
						break;
					}
				}
				//	書き込み開始
				if (usb_writeData.data.updPgoIpgToSet.result == 0)
				{
					fwUpdateBuf.writeDoneBlockNo	=	0;
					fwUpdateBuf.updateStatus		=	FW_UPDATE_ST_PRG;
				}
			}
			usb_writeData.data.updPgoIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_PGO_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_UPD_PGO_IPG_TO_SET);
		}
		break;
	//-------------------------------------------------------------------------
	//--	書き込み状況問い合わせ
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_PRG_STATUS:
		if (fwUpdateBuf.updateStatus < FW_UPDATE_ST_PRG)	usb_writeData.data.updPstIpgToSet.bNo	=	0;	//	書き込み中未満ＮＧ
		else
		{
			usb_writeData.data.updPstIpgToSet.bNo	=	fwUpdateBuf.writeDoneBlockNo;
		}
		usb_writeData.data.updPstIpgToSet.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_UPD_PST_IPG_TO_SET) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
		writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
						(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
						(uint32_t)sizeof(MSG_BSC_UPD_PST_IPG_TO_SET);
		break;
	//-------------------------------------------------------------------------
	//--	リセット要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_UPDATE_RESET:
		g_reset();
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	ＰＲＯＭ消去関数
//=============================================================================
static	uint32_t	sectorAdr[ERASE_SECTOR_NUM]	=	{
	0x00000000,		0x00002000,		0x00004000,		0x00006000,		0x00008000,		0x0000A000,		0x0000C000,		0x0000E000,
	0x00010000,		0x00020000,		0x00030000
};
static	void	promErase(void)
{
	uint32_t	i;
	for ( i = 0 ; i < ERASE_SECTOR_NUM ; i++ )
	{
		/* Write Unlock Cycles */
		*(__IO uint16_t*)(PROM_TOP + (0x555 << 1)) = 0xaa;
		*(__IO uint16_t*)(PROM_TOP + (0x2AA << 1)) = 0x55;
		/* Setup Command */
		*(__IO uint16_t*)(PROM_TOP + (0x555 << 1)) = 0x80;
		/* Write Unlock Cycles */
		*(__IO uint16_t*)(PROM_TOP + (0x555 << 1)) = 0xaa;
		*(__IO uint16_t*)(PROM_TOP + (0x2AA << 1)) = 0x55;
		/* Sector Erase Command */
		*(__IO uint16_t*)(PROM_TOP + sectorAdr[i]) = 0x30;
		/* Data# Polling */
		while (*(__IO uint16_t*)(PROM_TOP + sectorAdr[i]) != 0xffff)
		{
			if (commonData.wdtEnable)
			{
			}
			__NOP();
		}
	}
}
//=============================================================================
//==	ＰＲＯＭ読み込み関数（現在未使用）
//=============================================================================
/*
static	void	promRead(uint16_t* buf, uint32_t addr, uint32_t size)
{
	uint32_t	i;
	uint32_t	pad	=	addr + PROM_TOP;
	for ( i = 0 ; i < (size / 2) ; i++ )
	{
		*buf++	=	*(__IO uint16_t*)(pad + i);
	}
}
*/
//=============================================================================
//==	ＰＲＯＭ書き込み関数
//=============================================================================
static	void	promWrite(uint16_t* buf, uint32_t addr, uint32_t size)
{
	uint32_t	i;
	uint32_t	pad	=	addr;
	for ( i = 0 ; i < (size / 2) ; i++,pad += 2 )
	{
		/* Write Unlock Cycles */
		*(__IO uint16_t*)(PROM_TOP + (0x0555 << 1)) = 0x00aa;
		*(__IO uint16_t*)(PROM_TOP + (0x02AA << 1)) = 0x0055;
		/* Write Program Command */
		*(__IO uint16_t*)(PROM_TOP + (0x0555 << 1)) = 0x00a0;
		/* Program Data to Address */
		*(__IO uint16_t*)(PROM_TOP + pad) = *(buf + i);
		/* Data# Polling */
		while (*(__IO uint16_t*)(PROM_TOP + pad) != *(buf + i))
		{
			if (commonData.wdtEnable)
			{
			}
			__NOP();
		}
	}
}
