//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	制御ＵＤＰタスク
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	ローカル定義
//=============================================================================
#define		UDP_RCV_TMOUT_MS	10000
static		clock_t				clk[3];
//=============================================================================
//==	グローバル定義
//=============================================================================
uint8_t				udp_readData[UDP_MAX_LEN];
uint32_t			udp_readDataLen;
MSG_BSC_FORMAT		udp_writeData;
uint32_t			udp_writeDataLen;
//=============================================================================
//==	ローカル関数定義
//=============================================================================
static	uint32_t	systemControl(MSG_BSC_FORMAT *mf,int32_t AorB);
static	uint32_t	mtxControl(MSG_BSC_FORMAT *mf,int32_t AorB);
//=============================================================================
//==	制御ＵＤＰタスク関数
//=============================================================================
void	g_udp_cnt_task(void)
{
	LOG_THREAD();

	struct ip_addr	addr;
	UB				portId;
	UH				port;
	MSG_BSC_FORMAT	*mf;
	int32_t			msIndex;
	//-------------------------------------------------------------------------
	//--	初期化完了待ち
	//-------------------------------------------------------------------------
	while (1)
	{
		if (commonData.taskInit == 1)	break;
		tslp_tsk(10);
  	}
	//-------------------------------------------------------------------------
	//--	初期化
	//-------------------------------------------------------------------------
    udp_raw_init_port0(&xipNet[0].xIpAddr,UDP_PORT_R_A);
    udp_raw_init_port1(&xipNet[1].xIpAddr,UDP_PORT_R_B);
	//-------------------------------------------------------------------------
	//--	メインループ
	//-------------------------------------------------------------------------
	while (1)
	{
		//---------------------------------------------------------------------
		//--	受信待ち
		//---------------------------------------------------------------------
		udp_readDataLen = udp_raw_recv_mnt(&udp_readData[0],&portId,&addr,&port);
		clk[0]	=	clock();
		if (udp_readDataLen == 0)
		{
			continue;
        }
		mf = (MSG_BSC_FORMAT*)&udp_readData[0];

		//	管理制御棚判定用Ｉ／Ｆ作成
//		sourceHost.ipa	=	g_c4To32((uint8_t*)&addr.addr);
//		msIndex			=	g_IsManagementServerIndex(&sourceHost);
		msIndex			=	(port < UDP_PORT_R_B)?0:1;	//	これが正解！
		if (msIndex == -1)	continue;
		//---------------------------------------------------------------------
		//--	受信データ解析
		//---------------------------------------------------------------------
		if (udp_readDataLen > (uint32_t)sizeof(MSG_BSC_HEADER))
		{
			udp_writeDataLen	=	0;
			//-----------------------------------------------------------------
			//--	システム制御電文
			//-----------------------------------------------------------------
			if (mf->dataHead.itemCode == ITEM_CODE_SYSTEM_CONTROL)
			{
				udp_writeDataLen	=	systemControl(mf,msIndex);
			}
			//-----------------------------------------------------------------
			//--	ＭＴＸ制御電文
			//-----------------------------------------------------------------
			else if (mf->dataHead.itemCode == ITEM_CODE_MTX_CONTROL)
			{
				udp_writeDataLen	=	mtxControl(mf,msIndex);
			}
			//-----------------------------------------------------------------
			//--	アップデート電文
			//-----------------------------------------------------------------
			else if (mf->dataHead.itemCode == ITEM_CODE_FWUPDATE)
			{
				udp_writeDataLen	=	g_updateControl(mf,msIndex);
			}
			//-----------------------------------------------------------------
			//--	設定電文
			//--	ディップスイッチで、インバンド設定有効時のみ（未定）
			//-----------------------------------------------------------------
//			else if (commonData.setting_InBand_or_485 == SETTING_IN_BAND)
//			{
//			}
			//-----------------------------------------------------------------
			//--	応答送信
			//-----------------------------------------------------------------
			if (udp_writeDataLen != 0)
			{
				//-------------------------------------------------------------
				//--	応答ヘッダー作成
				//-------------------------------------------------------------
				udp_writeData.head.len				=	g_endianConv(udp_writeDataLen - sizeof(MSG_BSC_HEADER) - 2);
				udp_writeData.head.id				=	0x00;
				udp_writeData.head.srcCodeNo		=	myCode[msIndex];
				udp_writeData.head.dstCodeNo		=	mf->head.srcCodeNo;
				udp_writeData.head.total			=	0x01;
				udp_writeData.head.no				=	0x01;
				udp_writeData.head.sequenceNo		=	mf->head.sequenceNo;
				udp_writeData.head.result			=	0x00;
				udp_writeData.head.AUX[0]			=	0x00;
				udp_writeData.head.AUX[1]			=	0x00;
				udp_writeData.head.AUX[2]			=	0x00;
				udp_writeData.head.AUX[3]			=	0x00;
				udp_writeData.dataHead.itemCode		=	mf->dataHead.itemCode;
				if (mf->dataHead.itemCode == ITEM_CODE_MTX_CONTROL &&
					mf->dataHead.controlCode == CONT_CODE_MTX_CHG_REQ_IPRX)
						udp_writeData.dataHead.controlCode	=	CONT_CODE_MTX_CHG_NOTI;
				else if (mf->dataHead.itemCode == ITEM_CODE_MTX_CONTROL &&
					mf->dataHead.controlCode == CONT_CODE_VRT_CHG_REQ_IPTX)
						udp_writeData.dataHead.controlCode	=	CONT_CODE_VRT_CHG_NOTI;
				else	udp_writeData.dataHead.controlCode	=	mf->dataHead.controlCode | 0x80;
				//-------------------------------------------------------------
				//--	送信
				//-------------------------------------------------------------
if (udp_writeData.dataHead.controlCode == 0xF8)
{
	debugData.data[2]++;	//	For debug
}
				udp_raw_send((uint8_t *)&udp_writeData,udp_writeDataLen,&addr,
							 ((msIndex == 0)?UDP_PORT_S_A:UDP_PORT_S_B),
							 portId,
							 ((msIndex == 0)?UDP_PORT_R_A:UDP_PORT_R_B),RAW_SEND_PRI);
				//--	受信から送信までの時間計測
				clk[1]	=	clock();
				if (clk[1] >= clk[0])	clk[2]	=	clk[1] - clk[0];
				else					clk[2]	=	(0xffffffff - clk[0]) + clk[1];
				if (clk[2] > debugData.data[0])	debugData.data[0]	=	clk[2];
			}
		}
	}
}
//=============================================================================
//==	システム制御電文関数
//=============================================================================
static	uint32_t	systemControl(MSG_BSC_FORMAT *mf,int32_t AorB)
{
	uint32_t	writeLen	=	0;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ヘルスチェック
	//-------------------------------------------------------------------------
	case	CONT_CODE_HEALTH_REQ:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_HEALTH_IFS_TO_TXRX)))
		{
			if (commonData.connectRequest[AorB] == 1)	//	ヘルスチェック応答は、接続要求受信後のみ
			{
				udp_writeData.data.healthTxRxToIfs.AUX	=	0;
				udp_writeData.data.healthTxRxToIfs.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_HELTH_IFS_TO_TXRX) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
				writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_HELTH_IFS_TO_TXRX);
				//-------------------------------------------------------------
				//--	接続要求受信済フラグの解除用カウンタクリア
				//-------------------------------------------------------------
				commonData.connectRequestLossCount[AorB]	=	0;
			}
			//	2020.07.14	Nii
			if (AorB == 0)
			{
				debugData.data[3]++;
			}
			else
			{
				debugData.data[4]++;
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	機器情報要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_INF_REQ:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_INF_IFS_TO_TXRX)))
		{
			g_getFPGAVersion();
			udp_writeData.data.infTxRxToIfs.mchData	=	modelInf[AorB];
			udp_writeData.data.infTxRxToIfs.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_INF_TXRX_TO_IFS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_INF_TXRX_TO_IFS);
		}
		break;
	//-------------------------------------------------------------------------
	//--	接続要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_CONNECT_REQ:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_CONNECT_IFS_TO_TXRX)))
		{
			debugData.data[1]++;	//	For debug
			g_getFPGAVersion();
			memcpy((char *)&udp_writeData.data.connectTxRxToIfs,(char *)&modelInf[AorB],sizeof(MSG_BSC_INF_TXRX_TO_IFS));
			udp_writeData.data.connectTxRxToIfs.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_INF_TXRX_TO_IFS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_INF_TXRX_TO_IFS);
			//-----------------------------------------------------------------
			//--	ＩＰアドレスから、管理制御棚を識別して、
			//--	接続要求受信済フラグをセット
			//-----------------------------------------------------------------
			commonData.connectRequest[AorB]				=	1;
			commonData.connectRequestLossCount[AorB]	=	0;
			commonData.bcVRTrecFlag[AorB]				=	0;
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	ＭＴＸ制御電文関数
//=============================================================================
static	uint32_t	VRT_Rec_Count	=	0;
static	uint32_t	VRT_Chg_Count	=	0;

static	uint32_t	mtxControl(MSG_BSC_FORMAT *mf,int32_t AorB)
{
	uint32_t	i;
//	uint32_t	j;
	uint16_t	si;
	uint32_t	writeLen	=	0;
//    uint8_t     rxCurrentInputNo;
	uint32_t	delc1;
	uint16_t	sno;
	switch (mf->dataHead.controlCode)
	{
	//-------------------------------------------------------------------------
	//--	ＭＴＸ切替要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_MTX_CHG_REQ_IPRX:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_MTX_CHG_IFS_TO_RX)) &&
			commonData.connectRequest[AorB] == 1)	//	接続要求受信済
		{
			/* 時間計測開始 */
			DBG_TIMER_START();
			//-----------------------------------------------------------------
			//--	既に入力されている素材はＶ−ＩＮＴで行う
			//-----------------------------------------------------------------
//			wai_sem(ID_SEM_SAVE_DATA);
            //-----------------------------------------------------------------
			//--	素材変更は有り?
			//-----------------------------------------------------------------
//            rxCurrentInputNo    = 0;
//			for ( i = 0 ; i < BUSNO_MAX ; i++ )
//			{
//				si	=	g_endianConv(mf->data.mtxChgIfsToRx.inputNo[i]) - 1;
//				if (si != commonData.rxCurrentInputNo[i] && commonData.inputChange[i] != 1)
//				{
//					rxCurrentInputNo    = 1;
//				}
//			}
			//--	すでに入力されている素材か?
//			for ( i = 0 ; i < BUSNO_MAX ; i++ )
//			{
//				si	=	g_endianConv(mf->data.mtxChgIfsToRx.inputNo[i]) - 1;
//				//変化?
////				if (si != commonData.rxCurrentInputNo[i])
//                   if (rxCurrentInputNo == 1)
//				{
//					for ( j = 0 ; j < BUSNO_MAX ; j++ )
//					{
//						//すでに入力されてる?
//						if (si == commonData.rxCurrentInputNo[j])
//						{
//							//V-INT同期
//							commonData.rxCurrentInputNo[i]	=	si;
//							commonData.inputChange[i]		=	1;
//						}
//					}
//				}
//			}
			//	ＦＰＧＡにて切替タイミング調整を行うので、ＣＰＵ側でのタイミング合わせは不要になった！！
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				si	=	g_endianConv(mf->data.mtxChgIfsToRx.inputNo[i]) - 1;
				//-------------------------------------------------------------
				//--	新素材は、ここで更新
				//--	入力素材番号変更（新素材）
				//-------------------------------------------------------------
				if (si != commonData.rxCurrentInputNo[i] && commonData.inputChange[i] != 1)
				{
					g_mtxChange(i,si);
					g_txLoopModeChange(i,si);
				}
			}
//			sig_sem(ID_SEM_SAVE_DATA);
			/* 時間計測終了 */
			DBG_TIMER_END();
			/* 時間差分ログ出力 */
			DBG_TIMER_DIFF();
			//-----------------------------------------------------------------
			//--	応答データ作成
			//-----------------------------------------------------------------
			for ( i = 0 ; i < 4 ; i++ )
			{
				udp_writeData.data.mtxChgRxToIfs.inputNo[i]	=	mf->data.mtxChgIfsToRx.inputNo[i];
			}
			udp_writeData.data.mtxChgRxToIfs.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_MTX_CHG_RX_TO_IFS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
								(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
								(uint32_t)sizeof(MSG_BSC_MTX_CHG_RX_TO_IFS);
			//-----------------------------------------------------------------
			//--	ユニット有効処理
			//-----------------------------------------------------------------
			if (commonData.firstMTXChangeRec == 0)
			{
				commonData.firstMTXChangeRec	=	1;
				if (commonData.startEnableFrom485 == 1)
				{
					g_videoStart();
				}
			}
			//	2020.07.14	Nii
			if (AorB == 0)
			{
				debugData.data[5]++;
			}
			else
			{
				debugData.data[6]++;
			}
		}
		break;
	//-------------------------------------------------------------------------
	//--	ＶＲＴ切替要求
	//-------------------------------------------------------------------------
	case	CONT_CODE_VRT_CHG_REQ_IPTX:
VRT_Rec_Count++;
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_VRT_CHG_IFS_TO_TX)) &&
			commonData.connectRequest[AorB] == 1)	//	接続要求受信済
		{
			//-----------------------------------------------------------------
			//--	ブロードキャストは、３回送られてくるので
			//--	初回受信以外は処理しない
			//-----------------------------------------------------------------
			if (g_isBoardBC())
			{
				if (commonData.bcVRTrecFlag[AorB] != 0 &&
					mf->head.sequenceNo == commonData.bcVRTsequenceNo[AorB])
				{
					return(0);
				}
				//	シーケンス番号を保存
				commonData.bcVRTrecFlag[AorB]		=	1;
				commonData.bcVRTsequenceNo[AorB]	=	mf->head.sequenceNo;
			}
            if (commonData.vrtSwTx[0] != mf->data.vrtChgIfsToTx.vrtNo)
            {
                wai_sem(ID_SEM_SAVE_DATA);
                for ( i = 0 ; i < BUSNO_MAX ; i++ )
                {
                    g_vrtChange(i,mf->data.vrtChgIfsToTx.vrtNo);
                }
                sig_sem(ID_SEM_SAVE_DATA);
                //-------------------------------------------------------------
                //--	選択完了待ち
                //-------------------------------------------------------------
                for ( i = 0 ; i < 100 ; i++ )
                {
                    if (   (commonData.vrtSwTxDone[0][0] == 1) && (commonData.vrtSwTxDone[1][0] == 1)
                       && (commonData.vrtSwTxDone[2][0] == 1) && (commonData.vrtSwTxDone[3][0] == 1))
                    {
                        break;
                    }
                    tslp_tsk(1);
                }
				//-------------------------------------------------------------
				//--	レスポンスの集中を回避するため
				//--	ボードインデックスにより待たせる時間を作成
				//--	102,20Xのみ
				//-------------------------------------------------------------
				if (g_isBoardBC())
				{
					tslp_tsk(((commonData.bdIndex % 20) * 2));
				}
VRT_Chg_Count++;
            }
			//-----------------------------------------------------------------
			//--	応答データ作成
			//-----------------------------------------------------------------
			udp_writeData.data.vrtChgTxToIfs.vrtNo			=	mf->data.vrtChgIfsToTx.vrtNo;
			udp_writeData.data.vrtChgTxToIfs.checkData		=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_VRT_CHG_TX_TO_IFS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_VRT_CHG_TX_TO_IFS);
		}
		break;
	//-------------------------------------------------------------------------
	//	ＳＤＩ出力映像系統情報
	//-------------------------------------------------------------------------
	case	CONT_CODE_RX_SDI_OUT:
		if (udp_readDataLen == ((uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_RX_SDI_OUT_ALS_TO_RX)))
		{
			//-----------------------------------------------------------------
			//--	応答データ作成
			//-----------------------------------------------------------------
			for ( i = 0 ; i < BUSNO_MAX ; i++ )
			{
				delc1	=	xiGetHeaderDelControl1(0,0,i);
				udp_writeData.data.rxSdiOutRxToAls.sdi[i].AB		=	(delc1 & 0x00004000)?0x01:0x00;
				sno	=	(commonData.rxCurrentInputNo[i] == -1)?0xffff:(commonData.rxCurrentInputNo[i] + 1);
				udp_writeData.data.rxSdiOutRxToAls.sdi[i].inputNo	=	((sno & 0x00ff) << 8) | ((sno & 0xff00) >> 8);
			}
			udp_writeData.data.rxSdiOutRxToAls.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_RX_SDI_OUT_RX_TO_ALS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
			writeLen	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
							(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
							(uint32_t)sizeof(MSG_BSC_RX_SDI_OUT_RX_TO_ALS);
		}
		break;
	default:
		break;
	}
	return(writeLen);
}
//=============================================================================
//==	ＭＴＸ切替関数
//=============================================================================
void	g_mtxChange(uint8_t busNo,uint16_t matNo)
{
	commonData.rxCurrentInputNo[busNo]	=	matNo;
	//-------------------------------------------------------------------------
	//--	ＦＰＧＡ出力
	//-------------------------------------------------------------------------
	//	SW
	commonData.vrtSwRx[busNo]	^=	1;
	//	A CH
//	xiSetDemuxControl5(0,commonData.vrtSwRx[busNo],busNo,g_c4To32WithRSW_upN(saveCpuVideoLayer.data.multicastStartIpAddress[0],matNo));
	xiSetDemuxControl5(0,commonData.vrtSwRx[busNo],busNo,g_c4To32WithIndex_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[0],matNo));	//	for NHK
	xiSetDemuxControl4(0,commonData.vrtSwRx[busNo],busNo,g_c2To16(saveCpuVideoLayer.data.materialPortNo[busNo]));
	//	B CH
//	xiSetDemuxControl5(1,commonData.vrtSwRx[busNo],busNo,g_c4To32WithRSW_upN(saveCpuVideoLayer.data.multicastStartIpAddress[1],matNo));
	xiSetDemuxControl5(1,commonData.vrtSwRx[busNo],busNo,g_c4To32WithIndex_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[1],matNo));	//	for NHK
	xiSetDemuxControl4(1,commonData.vrtSwRx[busNo],busNo,g_c2To16(saveCpuVideoLayer.data.materialPortNo[busNo]));
	//	SW Flag A CH
	xiSetSw12Flag(0,busNo,commonData.vrtSwRx[busNo]);
	//	SW Flag B CH
	xiSetSw12Flag(1,busNo,commonData.vrtSwRx[busNo]);
	//	Update
	UC2_DEMUX_B_UPDATE(busNo);
	UC2_DEMUX_A_UPDATE(busNo);
	xiSetUnitControl2(busNo,UC_INIT_VAL_RX_CHG);
	commonData.inputChange[busNo]	=	0;
}
//=============================================================================
//==	ＴＸ　ＬＯＯＰ　ＭＯＤＥ切替関数
//=============================================================================
void	g_txLoopModeChange(uint8_t busNo, uint16_t matNo)
{
	uint32_t	i;
	uint32_t	dsip	=	0;
	uint32_t	haip	=	0;
	uint32_t	flag = 0;
	//	TX LOOP MODE
	//	切替マルチキャストアドレス算出
	dsip = g_c4To32WithIndex_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[0],matNo);
	flag = 0;
	for (i = 0; i < BUSNO_MAX; i++)
	{
		//	自マルチキャストアドレス算出
		haip = g_c4To32WithRSW_MC_NHK(saveCpuVideoLayer.data.multicastStartIpAddress[0],0,i);
		//	一致
		if (dsip == haip)
		{
			//	ＳＤＩ設定
			xiSetTxControlTxLoopMode(busNo, i + 1);
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		//	ＩＰ設定
		xiSetTxControlTxLoopMode(busNo, 0);
	}
	//	Update
	UC2_TRIPLE_RATE_SDI_TX_UPDATE(busNo);
}
//=============================================================================
//==	ＶＲＴ切替関数
//=============================================================================
void	g_vrtChange(uint8_t busNo,uint8_t sw)
{
	uint8_t		vMacAddress[6];
	commonData.vrtSwTx[busNo]	=	sw;
	//	SW Flag A CH
	xiSetHeaderAddControl23(0,0,busNo,savePhysicalLayerEeprom.data.videoMacAddress[0], sw,
					saveCpuVideoLayer.data.Extend[0][busNo],
//					saveCpuVideoLayer.data.VLANFlag[0][busNo],
					saveIndividualSet.data.vlan[busNo],	//	個別設定を使用する
//					saveCpuVideoLayer.data.TOSFlag[0][busNo]);
					saveIndividualSet.data.tos[busNo]);	//	個別設定を使用する
	//	SW Flag B CH
	xiSetHeaderAddControl23(1,0,busNo,savePhysicalLayerEeprom.data.videoMacAddress[1], sw,
					saveCpuVideoLayer.data.Extend[1][busNo],
//					saveCpuVideoLayer.data.VLANFlag[1][busNo],
					saveIndividualSet.data.vlan[busNo],	//	個別設定を使用する
//					saveCpuVideoLayer.data.TOSFlag[1][busNo]);
					saveIndividualSet.data.tos[busNo]);	//	個別設定を使用する
	//	For MONI
	if (g_isBoard20X())
	{
		//	SW Flag A CH
		memcpy(vMacAddress,savePhysicalLayerEeprom.data.videoMacAddress[0],6);
//		vMacAddress[5]	+=	0x02;
		xiMoniSetHeaderAddControl23(0,0,busNo,vMacAddress, sw,
						saveCpuVideoLayer.data.Extend[0][busNo],
//						saveCpuVideoLayer.data.VLANFlag[0][busNo],
//						saveIndividualSet.data.moni_vlan[busNo],	//	個別設定を使用する
						saveIndividualSet.data.moniVlan[busNo],		//	個別設定を使用する
//						saveCpuVideoLayer.data.TOSFlag[0][busNo]);
						saveIndividualSet.data.moni_tos[busNo]);	//	個別設定を使用する
		//	SW Flag B CH
		memcpy(vMacAddress,savePhysicalLayerEeprom.data.videoMacAddress[1],6);
//		vMacAddress[5]	+=	0x02;
		xiMoniSetHeaderAddControl23(1,0,busNo,vMacAddress, sw,
						saveCpuVideoLayer.data.Extend[1][busNo],
//						saveCpuVideoLayer.data.VLANFlag[1][busNo],
//						saveIndividualSet.data.moni_vlan[busNo],	//	個別設定を使用する
						saveIndividualSet.data.moniVlan[busNo],		//	個別設定を使用する
//						saveCpuVideoLayer.data.TOSFlag[1][busNo]);
						saveIndividualSet.data.moni_tos[busNo]);	//	個別設定を使用する
	}
	//	Update
	UC2_UDPIP_HEADER_ADD_B_UPDATE(busNo);
	UC2_UDPIP_HEADER_ADD_A_UPDATE(busNo);
	UC2_RTP_HEADER_ADD_UPDATE(busNo);
	xiSetUnitControl2(busNo,UC_INIT_VAL_TX_CHG);
    // VRT選択完了クリア
    commonData.vrtSwTxDone[busNo][0]    = 0;
}

//=============================================================================
//==	テストパケット送信（シリアルモニター）
//=============================================================================
void g_testUdp0PaketSend(uint16_t port)
{
	static		MSG_BSC_FORMAT		udp_writeData1;
	static		uint32_t			writeLen1;
	struct		ip_addr				ipaddr;

	g_getFPGAVersion();
	udp_writeData1.data.infTxRxToIfs.mchData	=	modelInf[0];
	udp_writeData1.data.infTxRxToIfs.checkData	=	~(g_endianConv((uint16_t)(sizeof(MSG_BSC_INF_TXRX_TO_IFS) + (uint16_t)sizeof(MSG_BSC_DATA_HEADER) - 2)));
	writeLen1	=	(uint32_t)sizeof(MSG_BSC_HEADER) +
					(uint32_t)sizeof(MSG_BSC_DATA_HEADER) +
					(uint32_t)sizeof(MSG_BSC_INF_TXRX_TO_IFS);

	IP4_ADDR(&ipaddr,	saveCpuVideoLayer.data.settingServerIpAddress[port][0],
						saveCpuVideoLayer.data.settingServerIpAddress[port][1],
	  					saveCpuVideoLayer.data.settingServerIpAddress[port][2],
						saveCpuVideoLayer.data.settingServerIpAddress[port][3]);

	udp_raw_send((uint8_t *)&udp_writeData1,writeLen1,&ipaddr,
				 UDP_PORT_S_A,port,UDP_PORT_R_A,RAW_SEND_PRI);
}
