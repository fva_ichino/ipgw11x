#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <linux/serial.h>

#include <errno.h>
#include <string.h> // strerror

#include "errcodes.h"
#include "IPGW11X_cfg.h"

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

ER_RET uart_init(uint8_t ch);
ER_RET uart_write(uint8_t ch, uint8_t data);
ER_RET uart_read(uint8_t ch, uint8_t *data);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static uart_table_t* s_uart_table_get_from_channel(uint8_t ch);

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

/**
	@ref Renesas R-IN32M3 シリーズ プログラミングマニュアル （ドライバ編） Rev.6.01

	6.3.1 UART モジュールの初期化
	uart_init
	(1) 概要
	UART モジュールの初期化
	(2) C 言語形式
	ER_RET uart_init(uint8_t ch);
	(3) パラメータ
	I/O パラメータ 説明
	I uint8_t ch チャネル選択引数
	0：チャネル0
	1：チャネル1
	(4) 機能
	チャネル選択引数で指定したチャネルのボー・レートや、ビット・サイズなどの、各種設定を行います。
	チャネル選択引数が 0 または 1 以外の場合、ER_PARAM（パラメータ・エラー）を返します。
	チャネルの選択は system_RIN32M3.h にて定義します。
	(5) 戻り値
	戻り値 意味
	ER_OK 初期化成功
	ER_PARAM 指定チャネルが0または1以外の場合、パラメータ・エラー
*/
ER_RET uart_init(uint8_t ch) {
	uart_table_t *p_uart_table = NULL;
	int fd;
	const char *deviceName;
	int ret;
	struct termios tio;
	/* RS485専用 */
	struct serial_rs485 rs485conf;

	p_uart_table = s_uart_table_get_from_channel(ch);
	/* ここには来ない */
	if (NULL == p_uart_table) {
		/* s_uart_table_get_from_channel()でログ出力済み */
		return ER_PARAM;
	}

	deviceName = p_uart_table->deviceName;

	/* non-blockingモードにする必要あり */
	fd = open(deviceName, O_RDWR | O_NDELAY);
	if (fd < 0) {
		LOG_E("open(%s) failed: %d(%s), (ch: %u)\n", deviceName, errno, strerror(errno), ch);
		return ER_PARAM;
	}

	/** @ref http://armdevs.com/note/note224.html */
	/** @todo どちらが良いかは実機で確認 */
	// memset(&tio, 0x00, sizeof(tio));
	ret = tcgetattr(fd, &tio);
	if (0 != ret) {
		LOG_E("tcgetattr(%s) failed: %d\n", deviceName, ret);
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return ER_PARAM;
	}
	else {
		LOG_D("tcgetattr(%s) succeed\n", deviceName);
	}

	tio.c_cflag |= CREAD;
	tio.c_cflag |= CLOCAL;
	/* データビット:8bit */
	tio.c_cflag |= CS8;
	/* ボーレート */
	ret = cfsetspeed(&tio, p_uart_table->baudRate);
	if (0 != ret) {
		LOG_E("cfsetspeed(%s) failed: %d\n", deviceName, ret);
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return ER_PARAM;
	}
	else {
		LOG_D("cfsetspeed(%s) succeed\n", deviceName);
	}

	/* RAWモード */
	cfmakeraw(&tio); 

	/* デバイスに設定を行う */
	ret = tcsetattr(fd, TCSANOW, &tio); 
	if (0 != ret) {
		LOG_E("tcsetattr(%s) failed: %d\n", deviceName, ret);
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時に行うことを記載する */
		}
		return ER_PARAM;
	}
	else {
		LOG_D("tcsetattr(%s) succeed\n", deviceName);
	}

	/* RS485設定  */
	if (0xff /*UART_485_CH*/ == ch) {
		memset(&rs485conf, 0x00, sizeof(rs485conf));

		/* Enable RS485 mode: */
		rs485conf.flags |= SER_RS485_ENABLED;

		/* Set logical level for RTS pin equal to 1 when sending: */
		rs485conf.flags |= SER_RS485_RTS_ON_SEND;

		/* Set logical level for RTS pin equal to 1 after sending: */
		rs485conf.flags |= SER_RS485_RTS_AFTER_SEND;

		/* Set this flag if you want to receive data even while sending data */
		rs485conf.flags |= SER_RS485_RX_DURING_TX;

		ret = ioctl(fd, TIOCSRS485, &rs485conf);
		if (0 != ret) {
			LOG_E("ioctl(%s) failed: %d(%s)\n", deviceName, errno, strerror(errno));
			ret = close(fd);
			if (0 != ret) {
				/* クローズ失敗時に行うことを記載する */
			}
			return ER_PARAM;
		}
		else {
			LOG_D("ioctl(%s) succeed\n", deviceName);
		}
	}

	/* ファイルディスクリプタ保持 */
	p_uart_table->fd = fd;

	LOG_TRACE("[UART] fd = %d, dev = %s, ch = %u\n", fd, deviceName, ch);
	return ER_OK;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/**
	@ref Renesas R-IN32M3 シリーズ プログラミングマニュアル （ドライバ編） Rev.6.01

	6.3.2 UART による 1 バイト・キャラクタ・データの送信
	uart_write
	(1) 概要
	UART による 1 バイト・キャラクタ・データの送信
	(2) C 言語形式
	ER_RET uart_write(uint8_t ch,uint8_t data);
	(3) パラメータ
	I/O パラメータ 説明
	I uint8_t ch チャネル選択引数
	0：チャネル0
	1：チャネル1
	I uint8_t data 1バイト・キャラクタの送信データ
	(4) 機能
	選択したチャネルへ 1 バイト・キャラクタのデータを送信します。ただし、送信 FIFO が FULL の場合は、
	空きができるまで待機します。
	チャネル選択引数が 0 または 1 以外の場合、ER_PARAM（パラメータ・エラー）を返します。
	チャネルの選択は system_RIN32M3.h にて定義します。
	(5) 戻り値
	戻り値 意味
	ER_OK 送信成功
	ER_PARAM パラメータ・エラー
	・指定チャネルが0または1以外の場合
*/
ER_RET uart_write(uint8_t ch, uint8_t data) {
	uart_table_t *p_uart_table = NULL;
	ssize_t len;
	int fd;

	p_uart_table = s_uart_table_get_from_channel(ch);
	/* ここには来ない */
	if (NULL == p_uart_table) {
		/* s_uart_table_get_from_channel()でログ出力済み */
		return ER_PARAM;
	}

	fd = p_uart_table->fd;
	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == fd) {
		/* uart_init()でログ出力済み */
		return ER_PARAM;
	}

	len = write(fd, &data, sizeof(uint8_t));
	if (len < 0) {
		LOG_E("write(%d) failed: %d(%s)\n", fd, errno, strerror(errno));
		return ER_PARAM;
	}
#ifdef _LOG_UART_ENABLE
	else {
		LOG_D("write(%d) succeed: len = %d, 0x%02X\n", fd, len, data);
	}
#endif

	return ER_OK;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/**
	@ref Renesas R-IN32M3 シリーズ プログラミングマニュアル （ドライバ編） Rev.6.01

	6.3.3 UART による 1 バイト・キャラクタ・データの受信
	uart_read
	(1) 概要
	UART による 1 バイト・キャラクタ・データの受信
	(2) C 言語形式
	ER_RET uart_read(uint8_t ch,uint8_t *data);
	(3) パラメータ
	I/O パラメータ 説明
	I uint8_t ch チャネル選択引数
	0：チャネル0
	1：チャネル1
	O uint8_t * data 1バイト・キャラクタの受信データポインタ
	(4) 機能
	選択したチャネルから 1 バイト・キャラクタのデータを受信します。受信データがある場合、受信データ
	を引数 data のポインタとして渡し、戻り値に 1 を返します。受信データがない場合、戻り値に 0 を返しま
	す。また、チャネル選択引数が 0 または 1 以外の場合、ER_PARAM（パラメータ・エラー）を返します。
	チャネルの選択は system_RIN32M3.h にて定義します。
	(5) 戻り値
	戻り値 意味
	1 受信データあり
	0 受信データなし
	ER_PARAM パラメータ・エラー
	・指定チャネルが0または1以外の場合
*/
ER_RET uart_read(uint8_t ch, uint8_t *data) {
	uart_table_t *p_uart_table = NULL;
	ssize_t len;
	int fd;
	int ret;

	p_uart_table = s_uart_table_get_from_channel(ch);
	/* ここには来ない */
	if (NULL == p_uart_table) {
		/* s_uart_table_get_from_channel()でログ出力済み */
		return ER_PARAM;
	}

	fd = p_uart_table->fd;
	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == fd) {
		/* uart_init()でログ出力済み */
		return ER_PARAM;
	}

	len = read(fd, data, sizeof(uint8_t));
	if (len < 0) {
		/* 入力なしの場合、0を返す */
		if (EAGAIN == errno) {
			return 0;
		}
		/* エラー時 */
		LOG_E("read(%d) failed: %d(%s)\n", fd, errno, strerror(errno));
		return ER_PARAM;
	}
	else if (0 == len) {
		/* ケーブル抜去検出時 */
		ret = close(fd);
		if (0 != ret) {
			/* クローズ失敗時はログを出した方が良いか？ */
			LOG_E("close(%d) failed: %d(%s)\n", fd, errno, strerror(errno));
		}
		else {
			/* クローズ成功時は念のためにFDを初期値に戻しておく */
			p_uart_table->fd = DEVICE_FD_INIT_VAL;
			/* 再オープン(TRACEログを出したいのでもう一度初期化する) */
			uart_init(ch);
		}
		return 0;
	}
#ifdef _LOG_UART_ENABLE
	else {
		LOG_D("read(%d) succeed: len = %d, data = 0x%02X\n", fd, len, *data);
	}
#endif

	/** @note 読み込み成功時に1を返す(ただし、1バイト以外は0とする) */
	return (sizeof(uint8_t) == len) ? 1 : 0;
}

//-------------------------------------------------------------
// Linux用内部関数
//-------------------------------------------------------------
static uart_table_t* s_uart_table_get_from_channel(uint8_t ch) {
	uart_table_t *p_uart_table = NULL;

	/* テーブル検索 */
	for (
		p_uart_table = (uart_table_t *)&g_cfg_uart_device_manage_table[0];
		p_uart_table->channel != CFG_UART_TBL_END;
		p_uart_table++
	) {
		if (p_uart_table->channel == ch) {
			return p_uart_table;
		}
	}

	/* ここには来ない(未登録のチャンネル) */
	LOG_E("unsupported ch: %u\n", ch);
	return NULL;
}