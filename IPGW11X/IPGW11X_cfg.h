#ifndef	__IPGW11X_CFG_H__
#define __IPGW11X_CFG_H__

#include "device.h"
#include "kernel.h"
#include "setting.h"
#include "kernel_id.h"
#include "uart.h"

#include "g_common.h"

/** @note GPIO関連の内容はこのファイルには記載しない */

/* プロセス名 */
#define CFG_APL_NAME "ipgw11x"

/** インターフェース名(SFP) */
#define CFG_IF_A_NAME "eth0"
#define CFG_IF_B_NAME "eth1"
#define CFG_IF_A_DEFAULT_ADDRESS "192.168.10.100"
#define CFG_IF_A_DEFAULT_MASK 24
#define CFG_IF_B_DEFAULT_ADDRESS "192.168.11.100"
#define CFG_IF_B_DEFAULT_MASK 24

/* UART */
#define CFG_UART_485_DEVICE_PATH "/dev/ttymxc1"
#define CFG_UART_USB_DEVICE_PATH "/dev/ttyGS0"
#define CFG_UART_TBL_END ((uint8_t)0xFF)

/* setting */
#define __CFG_SETTING_BASE_FILE_PATH 			"/mnt/mmc/ipgw11x/setting/"
#define CFG_SETTING_CPU_VIDEO_FILE_PATH			__CFG_SETTING_BASE_FILE_PATH "cpu_video"
#define CFG_SETTING_ALARM_MASK_FILE_PATH	 	__CFG_SETTING_BASE_FILE_PATH "alarm_mask"
#define CFG_SETTING_INDIVIDUAL_SET_FILE_PATH	__CFG_SETTING_BASE_FILE_PATH "individual_set"
#define CFG_SETTING_DS100BR_FILE_PATH			__CFG_SETTING_BASE_FILE_PATH "ds100br"
#define CFG_SETTING_CSZ3_ADC_FILE_PATH	  		__CFG_SETTING_BASE_FILE_PATH "csz3_adc"
#define CFG_SETTING_SYSTEM_AB_FILE_PATH	 		__CFG_SETTING_BASE_FILE_PATH "system_ab"
#define CFG_SETTING_SDIIPMIS_FILE_PATH	  		__CFG_SETTING_BASE_FILE_PATH "sdiipmis"

/* EEPROM */
#define CFG_EEP_REAR_DEVICE_PATH	"/dev/i2c-0"
#define	CFG_EEP_REAR_ADDRESS		((uint16_t)0b1010001)

/* I2C(DS100BR) */
#define CFG_I2C_DS100BR_DEVICE_PATH "/dev/i2c-3"
#define	CFG_I2C_DS100BR_ADDRESS		((uint16_t)0b1011000)

/* タスクID-スレッド管理テーブル */ 
extern thread_table_t g_cfg_tskid_thread_manage_table[];

/* セマフォID-ミューテックス管理テーブル */
extern mutex_table_t g_cfg_semid_mutex_manage_table[];

/* UARTチャンネル管理テーブル */
extern uart_table_t g_cfg_uart_device_manage_table[];

/* fileアドレス管理テーブル */
extern setting_table_t g_cfg_setting_table[];

/* 全スレッド関数 */
extern void g_main_task(void);
extern void g_udp_log_task(void);
extern void g_udp_upd_task(void);
extern void g_udp_cnt_task(void);
extern void g_snmp_sg_task(void);
extern void g_snmp_task(void);
extern void g_1588_task(void);
extern void g_serial_task(void);
extern void g_rs485_task(void);
extern void g_log_task(void);
extern void g_device_task(void);
extern void g_cycle_task(void);
extern void g_v_int(void);
extern void g_udp_monitor_task(void);
extern void g_cycle_alarm_task(void);

#endif /* __IPGW11X_CFG_H__ */