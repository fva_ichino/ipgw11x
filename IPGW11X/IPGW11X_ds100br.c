#include <fcntl.h> // open
#include <sys/ioctl.h> // ioctl
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <pthread.h> // pthread_mutex_t, pthread_mutex_lock, pthread_mutex_unlock

#include <errno.h>
#include <string.h> // strerror

#include "IPGW11X_ds100br.h"
#include "IPGW11X_cfg.h" // CFG_I2C_DS100BR_DEVICE_PATH, DEVICE_FD_INIT_VAL
#include "log.h"

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

void ds100br_init(void);
void ds100br_write(uint16_t addr, uint8_t *data, uint16_t len);
void ds100br_read(uint16_t addr, uint8_t *data, uint16_t len);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static int s_ds100br_ioctl(struct i2c_rdwr_ioctl_data *p_ioctl_data);

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

static int s_ds100br_fd = DEVICE_FD_INIT_VAL;
static pthread_mutex_t s_ds100br_mutex = PTHREAD_MUTEX_INITIALIZER;

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

void ds100br_init(void) {
	int fd;
	const char *deviceName = CFG_I2C_DS100BR_DEVICE_PATH;
	/**
	 *  @note ２重オープンガード処理
	 *        g_main_task()とg_v_int()の両方で呼ばれるため
	 */
	pthread_mutex_lock(&s_ds100br_mutex);
	if (DEVICE_FD_INIT_VAL == s_ds100br_fd) {
		/* デバイスオープン */
		fd = open(deviceName, O_RDWR);
		if (fd < 0) {
			LOG_E("open(%s) failed: %d(%s)\n", deviceName, errno, strerror(errno));
			pthread_mutex_unlock(&s_ds100br_mutex);
			return;
		}
		s_ds100br_fd = fd;
		LOG_TRACE("[DS100BR] fd = %d, dev = %s\n", fd, deviceName);
	}
	pthread_mutex_unlock(&s_ds100br_mutex);
	return;
}

void ds100br_write(uint16_t addr, uint8_t *data, uint16_t len) {
	int ret;
	struct i2c_msg msg[1];
	struct i2c_rdwr_ioctl_data ioctl_data;
	uint8_t buf[2 + 1] = {0};

	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == s_ds100br_fd) {
		/* ds100br_init()でログ出力済み */
		return;
	}

	if (NULL == data) {
		LOG_E("Invalid parameter: %p\n", data);
		return;
	}

	if (sizeof(buf) < (size_t)(len + 1)) {
		LOG_E("Invalid parameter: %u\n", len);
		return;
	}

	/* 書き込みアドレス */
	buf[0] = ((uint16_t)addr) >> 8;
	buf[1] = ((uint16_t)addr) & 0x00FF;
	/* 書き込み値 */
	buf[2] = *data;

	//変更
	buf[0] = addr & 0xFF;
	buf[1] = *data;

	/* メッセージ作成 */
	msg[0].addr = CFG_I2C_DS100BR_ADDRESS;
	msg[0].flags = 0; // 0 = 書き込み
	msg[0].len = (uint16_t)(1 + len);
	//msg[0].buf = data;
	msg[0].buf = buf;

	/* 制御データ作成 */
	ioctl_data.msgs = msg;
	ioctl_data.nmsgs = 1; // メッセージ数

	/* I2C制御 */
	ret = s_ds100br_ioctl(&ioctl_data);
	if (ret < 0) {
		LOG_E("%s(%d, addr = %u, len = %u) failed: %d(%s)\n", __func__, s_ds100br_fd, addr, len, ret, strerror(ret));
	}
	return;
}

void ds100br_read(uint16_t addr, uint8_t *data, uint16_t len) {
	int ret;
	struct i2c_msg msg[2]; /* アドレス書き込み -> 値読み込み */
	struct i2c_rdwr_ioctl_data ioctl_data;
	uint8_t buf[2];

	/* デバイスオープン失敗時 */
	if (DEVICE_FD_INIT_VAL == s_ds100br_fd) {
		/* ds100br_init()でログ出力済み */
		return;
	}

	if (NULL == data) {
		LOG_E("Invalid parameter: %p\n", data);
		return;
	}

	/* 書き込みアドレス */
	//buf[0] = addr >> 8;
	//buf[1] = addr & 0x00FF;
	buf[0] = addr & 0xFF;

	/* メッセージ作成 */
	msg[0].addr = CFG_I2C_DS100BR_ADDRESS;
	msg[0].flags = 0; // 0 = 書き込み
	msg[0].len = 1;//sizeof(buf);
	msg[0].buf = buf;

	msg[1].addr = CFG_I2C_DS100BR_ADDRESS;
	msg[1].flags = I2C_M_RD;
	msg[1].len = len;
	msg[1].buf = data;

	/* 制御データ作成 */
	ioctl_data.msgs = msg;
	ioctl_data.nmsgs = 1; // メッセージ数

	/* I2C制御 */
	ret = s_ds100br_ioctl(&ioctl_data);
	if (0 != ret) {
		LOG_E("%s(%d, addr = %u, len = %u) failed: %d(%s)\n", __func__, s_ds100br_fd, addr, len, ret, strerror(ret));
	}
	return;
}

/**
 * @brief DS100BR I2C制御
 * 
 * @param p_ioctl_data 
 * @param err 
 * @return int 
 * @retval 0　　 成功
 * @retval 0以外 失敗時のerrno
 */
int s_ds100br_ioctl(struct i2c_rdwr_ioctl_data *p_ioctl_data) {
	int ret = 0;
	int tmp_ret = 0;
	pthread_mutex_lock(&s_ds100br_mutex);
	tmp_ret = ioctl(s_ds100br_fd, I2C_RDWR, p_ioctl_data);
	if (tmp_ret < 0) {
		ret = errno;
		/** @note ここではエラーログは出力しない */
	}
	pthread_mutex_unlock(&s_ds100br_mutex);
	return ret;
}