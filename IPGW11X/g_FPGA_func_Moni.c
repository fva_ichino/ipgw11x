//=============================================================================
//==	Nii(All)	10G-IPTX/IPRX	対ＦＰＧＡ関数群（ＭＯＮＩ−２０Ｘ用）
//=============================================================================
#include	"g_common.h"
//=============================================================================
//==	FPGAアドレス定義
//=============================================================================
//-----------------------------------------------------------------------------
//--	ＭＯＮＩ
//-----------------------------------------------------------------------------
volatile	uint32_t		XI_MONI_UNIT_CONTROL1			=	0x00005000;
volatile	uint32_t		XI_MONI_UNIT_CONTROL2			=	0x00005004;
volatile	uint32_t		XI_MONI_DEBUG					=	0x00005008;
volatile	uint32_t		XI_MONI_TICO_ENCODE1			=	0x0000500C;
volatile	uint32_t		XI_MONI_TICO_ENCODE2			=	0x00005010;
volatile	uint32_t		XI_MONI_TICO_ENCODE3			=	0x00005014;
volatile	uint32_t		XI_MONI_TICO_ENCODE4			=	0x00005018;
volatile	uint32_t		XI_MONI_VIDEO_CONT				=	0x0000501C;
volatile	uint32_t		XI_MONI_AUDIO_PES1				=	0x00005020;
volatile	uint32_t		XI_MONI_AUDIO_PES2				=	0x00005024;
volatile	uint32_t		XI_MONI_AUDIO_STATUS1			=	0x00005028;
volatile	uint32_t		XI_MONI_AUDIO_STATUS2			=	0x0000502C;
volatile	uint32_t		XI_MONI_ANC_CONT1				=	0x00005030;
volatile	uint32_t		XI_MONI_ANC_CONT2				=	0x00005034;
volatile	uint32_t		XI_MONI_ANC_PES1				=	0x00005038;
volatile	uint32_t		XI_MONI_ANC_PES2				=	0x0000503C;
volatile	uint32_t		XI_MONI_ANC_DATA				=	0x00005040;
volatile	uint32_t		XI_MONI_PID1					=	0x00005044;
volatile	uint32_t		XI_MONI_PID2					=	0x00005048;
volatile	uint32_t		XI_MONI_PID3					=	0x0000504C;
volatile	uint32_t		XI_MONI_PID4					=	0x00005050;
volatile	uint32_t		XI_MONI_PID5					=	0x00005054;
volatile	uint32_t		XI_MONI_SMPTE2022_1_2			=	0x00005058;
volatile	uint32_t		XI_MONI_VIDEO_STREAM_MONITOR	=	0x0000505C;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL1		=	0x00005060;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL2		=	0x00005064;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL3		=	0x00005068;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL4		=	0x0000506C;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL5		=	0x00005070;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL6		=	0x00005074;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL7		=	0x00005078;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL8		=	0x0000507C;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL9		=	0x00005080;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL10	=	0x00005084;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL11	=	0x00005088;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL12	=	0x0000508C;
volatile	uint32_t		XI_MONI_HEADER_ADD_CONTROL13	=	0x00005090;
//=============================================================================
//==	ローカル定義
//=============================================================================
#define		XI_MONI_BUS_OFFSET		0x200
#define		XI_MONI_SW_OFFSET		0x100
#define		XI_MONI_UPD_OFFSET		0x2000
#define		XI_MONI_CH_OFFSET		0x8000
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）３２ｂｉｔｓ取得関数
//=============================================================================
uint32_t	xiMoniGetRegister(uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.us[0]	=	*((uint16_t *)(adr));
	uu.us[1]	=	*((uint16_t *)(adr + 2));
	return(uu.ui);
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiMoniGetUnitControl1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ１設定関数
//=============================================================================
void	xiMoniSetUnitControl1(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
	//	TICO ENCODE2 TICO_E_VLDをTriple-Rate SDI RX ONに同期させる
	if ((data & 0x00010000))	//	ON
	{
		*((uint16_t *)(XI_MONI_TICO_ENCODE1 + XI_MONI_BUS_OFFSET * busNo))	|=	0x0080;
	}
	else						//	OFF
	{
		*((uint16_t *)(XI_MONI_TICO_ENCODE1 + XI_MONI_BUS_OFFSET * busNo))	&=	~0x0080;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ１
//==	Ｓｔｒｅａｍ　Ｖｉｄｅｏ　Ｏｕｔ　＆
//==	ＴｒｉｐｌｅーＲａｔｅ　ＳＤＩ　ＴＸ
//==	ＯＮ／ＯＦＦ関数
//=============================================================================
void	xiMoniSetUnitControl1OutOnOff(uint8_t busNo,uint8_t onoff)
{
	if (onoff == 1)
	{
		*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo + 2))	|=	UC_INIT_ENB;
	}
	else
	{
		*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo + 2))	&=	~UC_INIT_ENB;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ１
//==	フォーマット不正時のリセットビット操作（ＴＸ）
//=============================================================================
#define	UC_MONI_FMT_FAIL_TX_BITS		0x00C0
void	xiMoniSetResetWhenTxFormatFail(uint8_t busNo,BOOL fail)
{
	if (fail == TRUE)
	{
		*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo))	|=	UC_MONI_FMT_FAIL_TX_BITS;
	}
	else
	{
		*((uint16_t *)(XI_MONI_UNIT_CONTROL1 + XI_MONI_BUS_OFFSET * busNo))	&=	~UC_MONI_FMT_FAIL_TX_BITS;
	}
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiMoniGetUnitControl2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ２ＳＹＮＣ設定関数
//=============================================================================
void	xiMoniSetUnitControl2Sync(uint8_t data)
{
//	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + 2))	|=	((uint16_t)(data & 0xf0) << 4);
	uint16_t	us;
	us	=	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + 2));
	us	&=	0xF0FF;
	us	|=	((uint16_t)(data & 0xf0) << 4);
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + 2))	=	us;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ２設定関数
//=============================================================================
void	xiMoniSetUnitControl2(uint8_t busNo,uint16_t data)
{
//	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo))		=	(data & 0x00FF);
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//$$	UNIT CONTROL2 No14 の DEMUX A Update は必ずONにすること！！
	//$$	FPGAが1つのDEMUXを4つにするために利用している！！
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo))		=	(data & 0x00FF) | 0x4000;
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo + 2))	&=	~0x8000;	//	PK_CUT OFF
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo + 2))	|=	0x4000;		//	FILTER_EN ON
//	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	0x0000;		//	PK_CUT OFF & FILTER_EN OFF
	__NOP();
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo))		=	0x4000;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＵＮＩＴ　ＣＯＮＴＲＯＬ２ ビットセット関数
//=============================================================================
void	xiMoniBitSetUnitControl2(uint8_t busNo,uint8_t bit)
{
	*((uint16_t *)(XI_MONI_UNIT_CONTROL2 + XI_MONI_BUS_OFFSET * busNo))		|=	(0x0001 << bit);
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＤＥＢＵＧ取得関数
//=============================================================================
uint32_t	xiMoniGetDebug(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_DEBUG + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ１取得関数
//=============================================================================
uint32_t	xiMoniGetTicoEncode1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_TICO_ENCODE1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ１設定関数
//=============================================================================
void	xiMoniSetTicoEncode1(uint8_t busNo,uint16_t height,uint16_t width)
{
	*((uint16_t *)(XI_MONI_TICO_ENCODE1 + XI_MONI_BUS_OFFSET * busNo))		=	width;
	*((uint16_t *)(XI_MONI_TICO_ENCODE1 + XI_MONI_BUS_OFFSET * busNo + 2))	=	height;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ２取得関数
//=============================================================================
uint32_t	xiMoniGetTicoEncode2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_TICO_ENCODE2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ２設定関数
//=============================================================================
void	xiMoniSetTicoEncode2(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_TICO_ENCODE2 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_TICO_ENCODE2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ３取得関数
//=============================================================================
uint32_t	xiMoniGetTicoEncode3(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_TICO_ENCODE3 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ３設定関数
//=============================================================================
void	xiMoniSetTicoEncode3(uint8_t busNo,uint32_t bgt)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	bgt;
	*((uint16_t *)(XI_MONI_TICO_ENCODE3 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_TICO_ENCODE3 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ４取得関数
//=============================================================================
uint32_t	xiMoniGetTicoEncode4(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_TICO_ENCODE4 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＴＩＣＯ　ＥＮＣＯＤＥ４設定関数
//=============================================================================
void	xiMoniSetTicoEncode4(uint8_t busNo,uint32_t weight)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	weight;
	*((uint16_t *)(XI_MONI_TICO_ENCODE4 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_TICO_ENCODE4 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＶＩＤＥＯ　ＣＯＮＴ取得関数
//=============================================================================
uint32_t	xiMoniGetVideoCont(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_VIDEO_CONT + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＶＩＤＥＯ　ＣＯＮＴ設定関数
//=============================================================================
void	xiMoniSetVideoCont(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_VIDEO_CONT + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_VIDEO_CONT + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＵＤＩＯ　ＰＥＳ１取得関数
//=============================================================================
uint32_t	xiMoniGetAudioPes1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_AUDIO_PES1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＵＤＩＯ　ＰＥＳ２取得関数
//=============================================================================
uint32_t	xiMoniGetAudioPes2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_AUDIO_PES2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＵＤＩＯ　ＰＥＳ２設定関数
//=============================================================================
void	xiMoniSetAudioPes2(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_AUDIO_PES2 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_AUDIO_PES2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＵＤＩＯ　ＳＴＡＴＵＳ１取得関数
//=============================================================================
uint32_t	xiMoniGetAudioStatus1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_AUDIO_STATUS1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＵＤＩＯ　ＳＴＡＴＵＳ２取得関数
//=============================================================================
uint32_t	xiMoniGetAudioStatus2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_AUDIO_STATUS2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＣＯＮＴ１取得関数
//=============================================================================
uint32_t	xiMoniGetAncCont1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_ANC_CONT1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＣＯＮＴ１設定関数
//=============================================================================
void	xiMoniSetAncCont1(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_ANC_CONT1 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_ANC_CONT1 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＣＯＮＴ２取得関数
//=============================================================================
uint32_t	xiMoniGetAncCont2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_ANC_CONT2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＣＯＮＴ２設定関数
//=============================================================================
void	xiMoniSetAncCont2(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_ANC_CONT2 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_ANC_CONT2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＰＥＳ１取得関数
//=============================================================================
uint32_t	xiMoniGetAncPes1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_ANC_PES1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＰＥＳ２取得関数
//=============================================================================
uint32_t	xiMoniGetAncPes2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_ANC_PES2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＡＮＣ　ＤＡＴＡ取得関数
//=============================================================================
uint32_t	xiMoniGetAncData(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_ANC_DATA + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ１取得関数
//=============================================================================
uint32_t	xiMoniGetPid1(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_PID1 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ１設定関数
//=============================================================================
void	xiMoniSetPid1(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_PID1 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_PID1 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ２取得関数
//=============================================================================
uint32_t	xiMoniGetPid2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_PID2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ２設定関数
//=============================================================================
void	xiMoniSetPid2(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_PID2 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_PID2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ３取得関数
//=============================================================================
uint32_t	xiMoniGetPid3(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_PID3 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ３設定関数
//=============================================================================
void	xiMoniSetPid3(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_PID3 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_PID3 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ４取得関数
//=============================================================================
uint32_t	xiMoniGetPid4(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_PID4 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ４設定関数
//=============================================================================
void	xiMoniSetPid4(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_PID4 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_PID4 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ５取得関数
//=============================================================================
uint32_t	xiMoniGetPid5(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_PID5 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＰＩＤ５設定関数
//=============================================================================
void	xiMoniSetPid5(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_PID5 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_PID5 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＳＭＰＴＥ２０２２＿１＿２取得関数
//=============================================================================
uint32_t	xiMoniGetSmpte2022_1_2(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_SMPTE2022_1_2 + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＳＭＰＴＥ２０２２＿１＿２設定関数
//=============================================================================
void	xiMoniSetSmpte2022_1_2(uint8_t busNo,uint32_t data)
{
	union
	{
		uint16_t	us[2];
		uint32_t		ui;
	}	uu;
	uu.ui	=	data;
	*((uint16_t *)(XI_MONI_SMPTE2022_1_2 + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_SMPTE2022_1_2 + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＶＩＤＥＯ　ＳＴＲＥＡＭ　ＭＯＮＩＴＯＲ取得関数
//=============================================================================
uint32_t	xiMoniGetVideoStreamMonitor(uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_VIDEO_STREAM_MONITOR + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl1(uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL1 + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１設定関数
//==	（ＲＡＴＥ＆ＦＡＭＩＬＹ）
//=============================================================================
void	xiMoniSetHeaderAddControl1(uint8_t busNo,uint16_t fmt)
{
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL1 + XI_MONI_BUS_OFFSET * busNo))		=	fmt;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ２取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl2(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL2 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ２フラグ設定関数
//=============================================================================
void	xiMoniSetHeaderAddControl2Flags(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t flags)
{
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL2 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	flags;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ３取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl3(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL3 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ２，３設定関数
//==	（ＳＯＵＲＣＥ　ＭＡＣ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiMoniSetHeaderAddControl23(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr,uint8_t sel,uint8_t ext,uint8_t vlan,uint8_t tos)
{
	union
	{
		uint8_t		cc[4];
		uint16_t	us[2];
	}	uu;
	uu.cc[0]	=	0;
	uu.cc[1]	=	sel << 7;
	uu.cc[1]	|=	(ext << 6);
	uu.cc[1]	|=	(vlan << 5);
	uu.cc[1]	|=	(tos << 4);
//uu.cc[1]=0x30;
	uu.cc[2]	=	*(adr + 1);
	uu.cc[3]	=	*(adr + 0);
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL2 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL2 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
	uu.cc[0]	=	*(adr + 5);
	uu.cc[1]	=	*(adr + 4);
	uu.cc[2]	=	*(adr + 3);
	uu.cc[3]	=	*(adr + 2);
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL3 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL3 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl4(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ５取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl5(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL5 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４，５設定関数
//==	（ＤＥＳＴＩＮＡＴＩＯＮ　ＭＡＣ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiMoniSetHeaderAddControl45(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t *adr)
{
	union
	{
		uint8_t		cc[4];
		uint16_t	us[2];
	}	uu;
	uu.cc[0]	=	*(adr + 1);
	uu.cc[1]	=	*(adr + 0);
	uu.cc[2]	=	0;
	uu.cc[3]	=	0;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	uu.cc[0]	=	*(adr + 5);
	uu.cc[1]	=	*(adr + 4);
	uu.cc[2]	=	*(adr + 3);
	uu.cc[3]	=	*(adr + 2);
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL5 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL5 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＴＴＬ）
//=============================================================================
void	xiMoniSetHeaderAddControl4TTL(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TTL)
{
	union
	{
		uint8_t		cc[2];
		uint16_t	us[1];
	}	uu;
	uu.us[0]	=	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2));
	uu.cc[1]	=	TTL;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[0];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ４設定関数
//==	（ＴＯＳ）
//=============================================================================
void	xiMoniSetHeaderAddControl4TOS(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint8_t TOS)
{
	union
	{
		uint8_t		cc[2];
		uint16_t	us[1];
	}	uu;
	uu.us[0]	=	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2));
	uu.cc[0]	=	TOS;
	uu.cc[1]	=	saveCpuVideoLayer.data.moni.txTtl[busNo];	//	レジスタが読めないので無理やりセット
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL4 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[0];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ６取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL6 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ６設定関数
//==	（ＳＯＵＲＣＥ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiMoniSetHeaderAddControl6(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL6 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL6 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ７取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL7 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ７設定関数
//==	（ＤＥＳＴＩＮＡＴＩＯＮ　ＩＰ　ＡＤＤＲＥＳＳ）
//=============================================================================
void	xiMoniSetHeaderAddControl7(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t adr)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	adr;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL7 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL7 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ８取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL8 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ８設定関数
//==	（ＳＯＵＲＣＥ＆ＤＥＳＴＩＮＡＴＩＯＮ　ＰＯＲＴ）
//=============================================================================
void	xiMoniSetHeaderAddControl8(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint16_t src,uint16_t dst)
{
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL8 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	dst;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL8 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	src;
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ９取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL9 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ９設定関数
//==	（ＳＳＲＣ）
//=============================================================================
void	xiMoniSetHeaderAddControl9(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t ssrc)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	ssrc;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL9 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL9 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１０取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t upd,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL10 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_UPD_OFFSET * upd + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１０設定関数
//==	（ＳＤＩＯＡ＿ＶＬＡＮ＿０）
//=============================================================================
void	xiMoniSetHeaderAddControl10(uint8_t chNo,uint8_t swNo,uint8_t busNo,uint32_t sdioa_vlan_0)
{
	union
	{
		uint16_t	us[2];
		uint32_t	ui;
	}	uu;
	uu.ui	=	sdioa_vlan_0;
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL10 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo))		=	uu.us[0];
	*((uint16_t *)(XI_MONI_HEADER_ADD_CONTROL10 + XI_MONI_CH_OFFSET * chNo + XI_MONI_SW_OFFSET * swNo + XI_MONI_BUS_OFFSET * busNo + 2))	=	uu.us[1];
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１１取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl11(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL11 + XI_MONI_CH_OFFSET * chNo + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１２取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl12(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL12 + XI_MONI_CH_OFFSET * chNo + XI_MONI_BUS_OFFSET * busNo));
}
//=============================================================================
//==	ＸＩＬＩＮＸ（ＭＯＮＩ）　ＨＥＡＤＥＲ　ＡＤＤ　ＣＯＮＴＲＯＬ１３取得関数
//=============================================================================
uint32_t	xiMoniGetHeaderAddControl13(uint8_t chNo,uint8_t busNo)
{
	return(xiGetRegister(XI_MONI_HEADER_ADD_CONTROL13 + XI_MONI_CH_OFFSET * chNo + XI_MONI_BUS_OFFSET * busNo));
}
