#ifndef	__SETTING_H__
#define	__SETTING_H__

#include <stdint.h>

#include "device.h"
#include "errcodes.h"

#define SETTING_DATA_ADR_END ((uint32_t)0)

typedef struct {
	const uint32_t address;
	const char *fileName;
} setting_table_t;

extern void setting_reg_table(setting_table_t *p_setting_table);
extern ER_RET setting_read(uint8_t* buf, uint32_t addr, uint32_t size);
extern ER_RET setting_write(uint8_t* buf, uint32_t addr, uint32_t size);
extern ER_RET setting_erase(uint32_t addr);

#endif /* __SETTING_H__ */
