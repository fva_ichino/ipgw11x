#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <spawn.h>
#include <string.h>
#include <sys/wait.h>

#include "run_cmd.h"
#include "log.h"

static char **environ;

int32_t run_cmd(char *cmd) {
	pid_t pid;
	char *argv[] = {"sh", "-c", cmd, NULL};
	int32_t status;
	posix_spawn_file_actions_t action;

	posix_spawn_file_actions_init(&action);
	posix_spawn_file_actions_addopen(&action, STDOUT_FILENO, "/dev/null", O_RDONLY, 0);
	posix_spawn_file_actions_addopen(&action, STDERR_FILENO, "/dev/null", O_RDONLY, 0);

	LOG_TRACE("[Exec] %s", cmd);

	status = posix_spawn(&pid, "/bin/sh", &action, NULL, argv, environ);
	if (0 == status) {
		if (-1 != waitpid(pid, &status, 0)) {
			if (0 != status) {
				LOG_E("Exec \"%s\" exited with status %d\n", cmd, status);
			}
		} else {
			if (0 != status) {
				LOG_E("Exec \"%s\" exited with status %d\n", cmd, status);
			}
		}
	} else {
		LOG_E("posix_spawn: %s\n", strerror(status));
	}

	posix_spawn_file_actions_destroy(&action);

	return status;
}