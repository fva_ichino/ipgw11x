#ifndef __RTOS_NET_HDR_H__
#define __RTOS_NET_HDR_H__

#include "itron.h"

#define DEV_STR_LEN     8       /* Device Name Length */
#define ETH_ADR_LEN     6

#define NET_DEV_TYPE_ETH    1   /* Ethernet Device  */

typedef struct t_net_buf {
    UW  *next;
    ID  mpfid;
    struct t_net  *net;
    struct t_net_dev  *dev;
    struct t_net_soc  *soc;
    ER  ercd;       /* Socket Error                 */
    UH  flg;        /* Broadcast/Multicast          */
    UH  seq;        /* IP Fragment Sequence         */
    UH  hdr_len;    /* *hdr length                  */
    UH  dat_len;    /* *dat length                  */
    UB  *hdr;       /* 2byte Aligned                */
    UB  *dat;       /* 4byte Aligned                */
    UB  buf[2];     /* Packet Data                  */
}T_NET_BUF;

typedef struct t_net_dev {
    UB name[DEV_STR_LEN];       /* Device Name                  */
    UH num;                     /* Device Number                */
    UH type;                    /* Device Type                  */
    UH sts;                     /* Device Status                */
    UH flg;                     /* Dummy                        */
    ER (*ini)(UH);              /* Initialize the device        */
    ER (*cls)(UH);              /* Uninitialize the device      */
    ER (*ctl)(UH,UH,VP);        /* Configure the device         */
    ER (*ref)(UH,UH,VP);        /* Read status of the device    */
    ER (*out)(UH,T_NET_BUF*);   /* Write frame to the device    */
    void (*cbk)(UH,UH,VP);      /* Device event notifier        */
    UW *tag;                    /* Device specific              */
    union  {                    /* Address                      */
    struct {
    UB mac[ETH_ADR_LEN];
    }eth;
    }cfg;
    UH  hhdrsz;                 /* Device header length         */
    UH  hhdrofs;                /* Device header offset         */
}T_NET_DEV;

typedef struct t_net_adr {
    UB ver;         /* IP4/IPv6         */
    UB mode;        /* STATIC or DHCP   */
    UW ipaddr;      /* Default IP Addr  */
    UW gateway;     /* Default Gateway  */
    UW mask;        /* Subnet Mask      */
}T_NET_ADR;

typedef struct t_node {
    UH  port;   /* Port number 1 - 65535, 0 -> PORT any */
    UB  ver;    /* IP Address type */
    UB  num;    /* Device number   */
    UW  ipa;    /* IPv4 Address    */
#ifdef IPV6_SUP
    UW  ip6a[4];/* IPv6 Address    */
#endif
}T_NODE;

#endif /* __RTOS_NET_HDR_H__ */