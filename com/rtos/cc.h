#ifndef __RTOS_CC_H__
#define __RTOS_CC_H__

typedef unsigned   char    u8_t;
typedef signed     char    s8_t;
typedef unsigned   short   u16_t;
typedef signed     short   s16_t;
typedef unsigned   long    u32_t;
typedef signed     long    s32_t;

#endif /* __RTOS_CC_H__ */