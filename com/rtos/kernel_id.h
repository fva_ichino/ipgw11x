#ifndef	__RTOS_KERNEL_ID_H__
#define	__RTOS_KERNEL_ID_H__

/*============================================================================*/
/* D E F I N E                                                                */
/*============================================================================*/
// --- Task ID ---
//#define ID_TASK_INIT	 1
#define ID_TASK_MAIN	 2
//uNet3
#define ID_TASK_TCP_TIM  3
//#define ID_TASK_PHY_LINK 4
#define ID_TASK_ETH_SND  5
#define ID_TASK_ETH_RCV  6
#define ID_TASK_ETH_OVF 15
//HTTP server sample
#define ID_TASK_HTTPS    7
//PHY
#define ID_TASK_PHY0_LINK 8
#define ID_TASK_PHY1_LINK 9

//EtherSW
#define ID_TASK_MAC_LRN 13

//Ethernet RAW frame receive
#define ID_TASK_ETH_RAW_RCV 11

//  Nii
#define	ID_TASK_G_MAIN		19
#define	ID_TASK_G_UDP_LOG	20
#define	ID_TASK_G_UDP_UPD	21
#define	ID_TASK_G_UDP_CNT	22
#define	ID_TASK_G_SNMP		23
#define	ID_TASK_G_1588		24
#define	ID_TASK_G_SERIAL	25
#define	ID_TASK_G_RS485		26
#define	ID_TASK_G_LOG		27
#define	ID_TASK_G_DEVICE	28
#define	ID_TASK_G_CYCLE		29

#define	ID_TASK_G_UDP_MONI	31
#define	ID_TASK_G_SNMP_SG	32
#define	ID_TASK_G_CYCLE_ALM	33
#define	ID_TASK_G_HDRSDR	34

//SNMP
#define ID_TASK_SNMP_RCV 50
#define ID_TASK_SNMP_TIM 51
#define ID_TASK_SNMP_TRP 52

#define ID_TASK_IDLE	63

// --- Semaphore/Mutex ID ---
#define ID_APL_SEM1		 1
#define ID_APL_SEM2		 2
#define ID_APL_MTX1		 3
#define ID_APL_MTX2		 4
//uNet3
#define ID_SEM_TCP		 5
#define ID_SEM_INTDMA	 6
//SNMP
#define ID_SEM_SNMP_MIB  7
#define ID_SEM_SNMP_TIM  8
#define ID_SEM_SNMP_TRP  9
//	Nii
#define	ID_SEM_SAVE_DATA 10

// --- Eventflag ID ---
#define ID_APL_FLG1		 1
#define ID_APL_FLG2		 2
//uNet3
#define ID_FLG_PHY_STS	 3
#define ID_FLG_ETH_TX_MAC 4
#define ID_FLG_ETH_RX_MAC 5
#define ID_FLG_SYSTEM    6
//SNMP
#define ID_FLG_SNMP_STS  7
#define ID_FLG_SNMP_TRP  8

// --- Mailbox ID ---
#define ID_APL_MBX1		 1
#define ID_APL_MBX2		 2
//uNet3
#define ID_MBX_MEMPOL	 3
#define ID_MBX_ETH_SND	 4
//Ethernet RAW frame receive
#define ID_MBX_ETH_RAW_RCV	5
//SNMP
#define ID_MBX_SNMP_TRP  6

#define ID_MBX_UDP_RECV		15
#define ID_MBX_SNMP_RECV	16
#define ID_MBX_MONI_RECV	17

#endif /* __RTOS_KERNEL_ID_H__ */
