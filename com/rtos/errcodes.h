#ifndef __RTOS_ERRCODES_H__
#define __RTOS_ERRCODES_H__

/* type of error code */
typedef int ER_RET;							/*!< Function return type will return the error code */

/* define of error code */
#define ER_OK      ((ER_RET)0)				/*!< Error code for Normal end (no error) */
#define ER_NG      ((ER_RET)-1)				/*!< Error code for Abnormal end (error)  */
#define ER_SYS     ((ER_RET)(2 * ER_NG))	/*!< Error code for Undefined error       */
#define ER_PARAM   ((ER_RET)(3 * ER_NG))	/*!< Error code for Invalid parameter     */
#define ER_NOTYET  ((ER_RET)(4 * ER_NG))	/*!< Error code for Incomplete processing */
#define ER_NOMEM   ((ER_RET)(5 * ER_NG))	/*!< Error code for Out of memory         */
#define ER_BUSY    ((ER_RET)(6 * ER_NG))	/*!< Error code for Busy                  */
#define ER_INVAL   ((ER_RET)(7 * ER_NG))	/*!< Error code for Invalid state         */
#define ER_TIMEOUT ((ER_RET)(8 * ER_NG))	/*!< Error code for Timeout occurs        */

#endif	/* __RTOS_ERRCODES_H__ */