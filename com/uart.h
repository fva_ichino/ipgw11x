#ifndef	__UART_H__
#define	__UART_H__

#include <stdint.h>
#include <termios.h>
#include "errcodes.h"

typedef struct {
	const uint8_t channel;
	const char *deviceName;
	const speed_t baudRate;
	int fd;
} uart_table_t;

extern ER_RET uart_init(uint8_t ch);
extern ER_RET uart_write(uint8_t ch,uint8_t data);
extern ER_RET uart_read(uint8_t ch,uint8_t* data);

#endif /* __UART_H__ */