#include	<errno.h>
#include	<string.h>

#include	"gpio.h"
#include	"log.h"

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

/** IS_EVERY_TIME_GET_GPIOD_LINE
 * @brief 毎回struct gpiod_lineを取得するかのマクロ変数
 * @note 一度取得したものが使いまわせない場合にtrueにする
 */
#define IS_EVERY_TIME_GET_GPIOD_LINE false
#if ((false != IS_EVERY_TIME_GET_GPIOD_LINE) && (true != IS_EVERY_TIME_GET_GPIOD_LINE))
	#error "IS_EVERY_TIME_GET_GPIOD_LINE is true or false"
#endif

static struct gpiod_chip *s_chip = NULL;

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

/** @note Linux仕様 */
bool com_gpio_init(const char *descr);
bool com_gpio_get_all_lines(const unsigned int num_lines, struct gpiod_line **lines);
bool com_gpio_get_line(const unsigned int offset, struct gpiod_line **p_line);
bool com_gpio_read(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, int *onoff);
bool com_gpio_write(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, const int onoff);
void com_gpio_close(unsigned int num_lines, struct gpiod_line **lines);

/** @note RTOS互換仕様 */
uint8_t com_gpio_rin_read(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap);
void com_gpio_rin_write(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap, const int onoff);

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/
bool com_gpio_init(const char *descr) {
	// GPIOデバイスをオープンする
	if (NULL == descr) {
		// 空文字で良いのか？
		s_chip = gpiod_chip_open_lookup("");
	}
	else {
		s_chip = gpiod_chip_open_lookup(descr);
	}
	
	if (NULL == s_chip) {
		LOG_E("gpiod_chip_open_lookup failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	LOG_TRACE("[GPIO] chip = %p\n", s_chip);
	return true;
}

bool com_gpio_get_all_lines(const unsigned int num_lines, struct gpiod_line **lines) {
	int ret;
	struct gpiod_line_bulk bulk;
	unsigned int real_num_lines;
	unsigned int offset;

	if (NULL == s_chip) {
		/* com_gpio_init()でログ出力済み */
		return false;
	}

	if (NULL == lines) {
		LOG_E("invalid parameter: lines = %p\n", lines);
		return false;
	}

	ret = gpiod_chip_get_all_lines(s_chip, &bulk);
	if (0 != ret) {
		LOG_E("gpiod_chip_get_all_lines failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	real_num_lines = gpiod_line_bulk_num_lines(&bulk);
	if (num_lines != real_num_lines) {
		LOG_E("gpiod_line_bulk_num_lines mismatched: %u, wanted = %u\n", real_num_lines, num_lines);
		return false;
	}

	/* 取得したgpiod_line_bulkをgpiod_lineにして、格納 */
	for (offset = 0; offset < num_lines; offset++) {
		*(lines + offset) = gpiod_line_bulk_get_line(&bulk, offset);
		if (NULL == *(lines + offset)) {
			LOG_E("gpiod_line_bulk_get_line failed: %d(%s), offset = %u\n", errno, strerror(errno), offset);
			/* 処理継続 */
			continue;
		}
	}

	return true;
}

bool com_gpio_get_line(const unsigned int offset, struct gpiod_line **p_line) {
	if (NULL == p_line) {
		LOG_E("invalid parameter: p_line = %p\n", p_line);
		return false;
	}

#if (false == IS_EVERY_TIME_GET_GPIOD_LINE)
  	// 指定したGPIOポートが空の場合はハンドラ取得
	if (NULL == *p_line) {
#endif

		*p_line = gpiod_chip_get_line(s_chip, offset);
		if (NULL == *p_line) {
			LOG_E("gpiod_chip_get_line failed: %u\n", offset);
			return false;
		}

#if (false == IS_EVERY_TIME_GET_GPIOD_LINE)
	}
#endif

	return true;
}

bool com_gpio_read(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, int *onoff) {
	int ret;
	bool ok;

	if (NULL == consumer || NULL == p_line || NULL == onoff) {
		LOG_E("invalid parameter: consumer = %p, p_line = %p, onoff = %p\n", consumer, p_line, onoff);
		return false;
	}

	if (NULL == s_chip) {
		/* com_gpio_init()でログ出力済み */
		return false;
	}

	// 指定したGPIOポートのハンドラ取得
	ok = com_gpio_get_line(offset, p_line);
	if (false == ok) {
		/* com_gpio_get_line()でログ出力済み */
		return false;
	}	

	// ポート予約(入力設定)
	ret = gpiod_line_request_input(*p_line, consumer);
	if (0 != ret) {
		LOG_E("gpiod_line_request_input failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	// GPIOポートの入力値取得(0/1: success, -1: failure)
	ret = gpiod_line_get_value(*p_line);
	if (-1 == ret) {
		LOG_E("gpiod_line_get_value failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	// ポート予約解除
	gpiod_line_release(*p_line);

	// 値を設定(0/1)
	*onoff = ret;
	return true;
}

bool com_gpio_write(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, const int onoff) {
	int ret;
	bool ok;

	if (NULL == consumer || NULL == p_line) {
		LOG_E("invalid parameter: consumer = %p, p_line = %p\n", consumer, p_line);
		return false;
	}

	/** @note 呼び出し元で保証しておくこと */ 
#if 0
	if (0 != onoff && 1 != onoff) {
		LOG_E("Unsupported value: %d\n", onoff);
		return false;
	}
#endif

	if (NULL == s_chip) {
		/* com_gpio_init()でログ出力済み */
		return false;
	}

  	// 指定したGPIOポートのハンドラ取得
	ok = com_gpio_get_line(offset, p_line);
	if (false == ok) {
		/* com_gpio_get_line()でログ出力済み */
		return false;
	}

	// ポート予約(出力設定)
	ret = gpiod_line_request_output(*p_line, consumer, 0);	// 第三引数はデフォルト値: int default_val
	if (0 != ret) {
		LOG_E("gpiod_line_request_output failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	// GPIOポートの出力値設定
	ret = gpiod_line_set_value(*p_line, onoff);
	if (0 != ret) {
		LOG_E("gpiod_line_set_value failed: %d(%s)\n", errno, strerror(errno));
		return false;
	}

	// ポート予約解除
	gpiod_line_release(*p_line);

	return true;
}

void com_gpio_close(unsigned int num_lines, struct gpiod_line **lines) {
	if (NULL != *lines) {
		while (num_lines) {
			if (NULL == *lines) {
				continue;
			}

			gpiod_line_release(*lines);
			*lines = NULL;

			lines++;
			num_lines--;
		}
	}

	if (NULL == s_chip) {
		return;
	}

  	// GPIOデバイスをクローズする(戻り値はない)
  	gpiod_chip_close(s_chip);
	s_chip = NULL;
	return;
}

/* ------------------ */
/* 以降は、RTOS互換仕様 */
/* ------------------ */

uint8_t com_gpio_rin_read(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap) {
	uint8_t value = 0x00;	/** @note デフォルト値 */
	int bit;
	unsigned int offset;
	uint8_t bits;
	int onoff;

	/** @note 呼び出し元で保証しておくこと */ 
#if 0
	if (NULL == p_gpio_rin_tbl) {
		LOG_E("invalid parameter: p_gpio_rin_tbl = %p\n", p_gpio_rin_tbl);
		return value;
	}
#endif

	/* bitmapの各bitがonであれば読み込みを行い、値がonであれば戻り値のbitもonにする */
	for (bit = 0; bit < CHAR_BIT; bit++) {
		bits = (uint8_t)(0x1 << bit);
		if (bitmap & bits) {
			offset = p_gpio_rin_tbl->offsets[bit];
			onoff = gpio_read(offset);
			if (1 == onoff) {
				value |= bits;
			}
		}
	}
	
	return value;
}

void com_gpio_rin_write(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap, const int onoff) {
	int bit;
	uint8_t bits;
	unsigned int offset;

	/** @note 呼び出し元で保証しておくこと */ 
#if 0
	if (NULL == p_gpio_rin_tbl) {
		LOG_E("invalid parameter: p_gpio_rin_tbl = %p\n", p_gpio_rin_tbl);
		return false;
	}
#endif

	/* bitmapの各bitがonであればonoff値の書き込みを行う */
	for (bit = 0; bit < CHAR_BIT; bit++) {
		bits = (uint8_t)(0x1 << bit);
		if (bitmap & bits) {
			offset = p_gpio_rin_tbl->offsets[bit];
			gpio_write(offset, onoff);
		}
	}

	return;
}
