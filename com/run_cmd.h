#ifndef __RUN_CMD_H__
#define __RUN_CMD_H__

#include <stdint.h>

int32_t run_cmd(char *cmd);

#endif