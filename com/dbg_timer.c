#include "dbg_timer.h"

/*
struct timeval {
    time_t tv_sec;            	// Seconds
    susecond_t tv_usec;    // Microseconds
};
*/

__thread struct timeval dbg_timer_start = {0};
__thread struct timeval dbg_timer_end = {0};
__thread struct timeval dbg_timer_diff = {0};