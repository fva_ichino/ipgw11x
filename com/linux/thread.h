#ifndef __LINUX_THREAD_H__
#define __LINUX_THREAD_H__

#include "kernel.h" // ID
#include "pthread.h" // pthread_t

typedef struct {
	ID			tskid;					/* Task ID */
	void* 		(*thread_func)(void*);	/* スレッド関数 */
	int			priority;				/* スレッド優先度 */
	size_t 		stacksize;				/* スレッドスタックサイズ */
	pthread_t	thread_id;				/* スレッドID */
} thread_table_t;

#endif /* __LINUX_THREAD_H__ */