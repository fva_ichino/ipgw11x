#ifndef __LINUX_MUTEX_H__
#define __LINUX_MUTEX_H__

#include "kernel.h" // ID
#include "pthread.h" // pthread_mutex_t

typedef struct {
	ID				semid;				/* セマフォID */
	pthread_mutex_t	mutex;				/* ミューテックス */
} mutex_table_t;

#endif /* __LINUX_MUTEX_H__ */