#include <stdio.h>
#include <string.h>

#include <errno.h>

#include "setting.h"
#include "log.h"

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

/* ファイル管理テーブル */ 
static setting_table_t *s_setting_table = NULL;

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

void setting_reg_table(setting_table_t *p_setting_table);
ER_RET setting_read(uint8_t* buf, const uint32_t addr, uint32_t size);
ER_RET setting_write(uint8_t* buf, const uint32_t addr, uint32_t size);
ER_RET setting_erase(const uint32_t addr);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static setting_table_t* s_setting_table_t_get_from_address(const uint32_t addr);

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

//-------------------------------------------------------------
// Linux用外部関数
//-------------------------------------------------------------

/**
 * @brief ファイル管理テーブル登録
 * 
 * @param p_setting_table ファイル管理テーブル
 */
void setting_reg_table(setting_table_t *p_setting_table) {
	if (NULL == p_setting_table) {
		LOG_E("Invalid patamer: %p\n", p_setting_table);
		return;
	}
	s_setting_table = p_setting_table;
	return;
}

/**
 * @brief ファイル読み込み
 * 
 * @param buf 読み込み結果格納ポインタ
 * @param addr ファイルパス検索用アドレス
 * @param size 読み込みサイズ
 * @return ER_RET 処理結果
 * 
 * @retval ER_OK 正常
 * @retval ER_NG 失敗
 * @retval ER_PARAM addrが未登録
 */
ER_RET setting_read(uint8_t* buf, const uint32_t addr, uint32_t size) {
	setting_table_t *p_setting_table = NULL;
	ER_RET er_ret = ER_OK;
	size_t len;
	FILE *fp;
	int ret;

	/* all 0で初期化 */
	memset(buf, 0x00, size);

	/* addrでファイル管理テーブルを検索 */
	p_setting_table = s_setting_table_t_get_from_address(addr);
	/* 未登録の場合 */
	if (NULL == p_setting_table) {
		/* s_setting_table_t_get_from_address()でログ出力済み */
		return ER_PARAM;
	}

	/* ファイルが存在する場合は、読み込み専用モードで開く */
	fp = fopen(p_setting_table->fileName, "rb");
	if (NULL == fp) {
		/* ファイルが存在しない場合 */
		if (ENOENT == errno) {
			/** @note 戻り値はOKとしておく */
			return ER_OK;
		}

		/* ファイルが存在しない場合以外のエラー */
		LOG_E("fopen(%s) failed: %d(%s)\n", p_setting_table->fileName, errno, strerror(errno));
		return ER_NG;
	}

	/* 読み込み */
	len = fread(buf, sizeof(uint8_t), size, fp);
	if (len < size) {
		LOG_E("fread(%s) invalid: len = %zu, wanted = %u\n", p_setting_table->fileName, len, size);
		/** @note 失敗時も閉じる必要があるため、処理継続 */
		er_ret = ER_NG;
	}
	else {
		LOG_I("fread(%s) succeed: len = %zu, wanted = %u\n", p_setting_table->fileName, len, size);
	}

	/* 閉じる */
	ret = fclose(fp);
	if (0 != ret) {
		LOG_E("fclose(%s) failed: %d(%s)\n", p_setting_table->fileName, errno, strerror(errno));
		/** @note NGは返さない */
		// er_ret = ER_NG;
	}

	return er_ret;
}

/**
 * @brief ファイル書き込み
 * 
 * @param buf 読み込み内容格納ポインタ
 * @param addr ファイルパス検索用アドレス
 * @param size サイズ
 * @return ER_RET 処理結果
 * 
 * @retval ER_OK 正常
 * @retval ER_NG 失敗
 * @retval ER_PARAM addrが未登録
 */
ER_RET setting_write(uint8_t* buf, const uint32_t addr, uint32_t size) {
	setting_table_t *p_setting_table = NULL;
	ER_RET er_ret = ER_OK;
	size_t len;
	FILE *fp;
	int ret;

	/* addrでファイル管理テーブルを検索 */
	p_setting_table = s_setting_table_t_get_from_address(addr);
	/* 未登録の場合 */
	if (NULL == p_setting_table) {
		/* s_setting_table_t_get_from_address()でログ出力済み */
		return ER_PARAM;
	}

	/* 書き込み専用モードで開く */
	fp = fopen(p_setting_table->fileName, "wb");
	if (NULL == fp) {
		LOG_E("fopen(%s) failed: %d(%s)\n", p_setting_table->fileName, errno, strerror(errno));
		return ER_NG;
	}

	/* 書き込み */
	len = fwrite(buf, sizeof(uint8_t), size, fp);
	if (len < size) {
		LOG_E("fwrite(%s) invalid: len = %zu, wanted = %u\n", p_setting_table->fileName, len, size);
		/** @note 失敗時も閉じる必要があるため、処理継続 */
		er_ret = ER_NG;
	}
	else {
		LOG_I("fwrite(%s) succeed: len = %zu, wanted = %u\n", p_setting_table->fileName, len, size);
	}
	
	/* 閉じる */
	ret = fclose(fp);
	if (0 != ret) {
		LOG_E("fclose(%s) failed: %d(%s)\n", p_setting_table->fileName, errno, strerror(errno));
		/** @note NGは返さない */
		// er_ret = ER_NG;
	}
	
	return er_ret;
}

/**
 * @brief ファイル削除
 * @attention 現在の処理で呼び出す必要がある部分はない
 * 
 * @param addr ファイルパス検索用アドレス
 * @return ER_RET 処理結果
 * 
 * @retval ER_OK 削除成功
 * @retval ER_NG 削除失敗
 * @retval ER_PARAM addrが未登録
 */
ER_RET setting_erase(const uint32_t addr) {
	setting_table_t *p_setting_table = NULL;
	int ret;

	/* addrでファイル管理テーブルを検索 */
	p_setting_table = s_setting_table_t_get_from_address(addr);
	/* 未登録の場合 */
	if (NULL == p_setting_table) {
		/* s_setting_table_t_get_from_address()でログ出力済み */
		return ER_PARAM;
	}

	/* ファイル削除 */
	ret = remove(p_setting_table->fileName);
	if (0 != ret) {
		LOG_E("remove(%s) failed: %d(%s)\n", p_setting_table->fileName, errno, strerror(errno));
		return ER_NG;
	}

	return ER_OK;
}

//-------------------------------------------------------------
// Linux用内部関数
//-------------------------------------------------------------

setting_table_t* s_setting_table_t_get_from_address(const uint32_t addr) {
	setting_table_t *p_setting_table = NULL;
	/* テーブル未登録 */
	if (NULL == s_setting_table) {
		LOG_E("setting table not registered\n");
		return NULL;
	}

	/* テーブル検索 */
	for (
		p_setting_table = (setting_table_t *)&s_setting_table[0];
		p_setting_table->address != SETTING_DATA_ADR_END;
		p_setting_table++
	) {
		if (addr == p_setting_table->address) {
			return p_setting_table;
		}
	}

	/* ここには来ない(未登録のアドレスの場合) */
	LOG_E("unsupported addr: %u\n", addr);
	return NULL;
}