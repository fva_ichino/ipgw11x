#ifndef	__KERNEL_H__
#define	__KERNEL_H__

#include <stdint.h>
#include "itron.h"

#include "thread.h" // thread_table_t
#include "mutex.h" // mutex_table_t

/*==========================================================================*/
/* F U N C T I O N   P R O T O T Y P E										*/
/*==========================================================================*/

extern void kernel_reg_thread_table(thread_table_t *p_thread_table);
extern void kernel_reg_mutex_table(mutex_table_t *p_mutex_table);

extern ER sta_tsk(ID tskid, VP_INT stacd);
extern ER tslp_tsk(TMO tmout);
extern ER wai_sem(ID semid);
extern ER sig_sem(ID semid);

/*==========================================================================*/
/* D E F I N E																*/
/*==========================================================================*/
//=============================================================
// Constants
//=============================================================
//-------------------------------------
// Object Attribute
//-------------------------------------
// ------ For task ------
#define	TA_HLNG		0x00				/*!< Start a processing though a high-level language interface	*/
#define	TA_ASM		0x01				/*!< Start a processing though an assembly language interface	*/
#define	TA_ACT		0x02				/*!< Task is activated after the creation						*/

// ------ For semaphores, eventflags,,,  ------
#define	TA_TFIFO	0x00				/*!< Task wait queue is in FIFO order							*/
#define	TA_TPRI		0x01				/*!< Task wait queue is in task priority order					*/

// ------ For eventflags  ------
#define	TA_WMUL		0x02				/*!< Multiple tasks are allowed to be in the waiting state
										     for the evntflag											*/
#define	TA_CLR		0x04				/*!< Eventflag's bit pattern is cleared when a task is released
											 from the waiting state for that eventflag					*/

// ------ For mailbox ------
#define TA_MFIFO	0x00				/*!< Message queue is in FIFO order								*/
#define TA_MPRI		0x02                /*!< Message queue is in message priority order					*/

// ------ For mutex  ------
#define TA_INHERIT	0x02				/*!< Mutex uses the priority inheriance protocol (Not used)		*/
#define TA_CEILING  0x03                /*!< Mutex uses the priority ceiling protocol (Not used)		*/

//-------------------------------------
// Service Call Opetational Mode
//-------------------------------------
// ------ For eventflags  ------
#define	TWF_ANDW	0x00				/*!< AND waiting condition for an eventflag						*/
#define	TWF_ORW		0x01				/*!< OR  waiting condition for an eventflag						*/

//-------------------------------------
// Other constants
//-------------------------------------
// ------ For task ------
#define TSK_SELF	0x00				/*!< Specifying invoking task									*/
#define TSK_NONE	0x00				/*!< NO applicable task											*/

#define TPRI_SELF	0x00				/*!< Specifying the vase priority of the invoking task			*/
#define TPRI_INI	0x00				/*!< Specifying the initial priority of the task				*/

//=============================================================
// Kernel Configuration Constants (depend on HW-RTOS architecture)
//=============================================================
//-------------------------------------
// Priority Range
//-------------------------------------
#define TMIN_TPRI	 1					/*!< Minimum task priority(=1)									*/
#define TMAX_TPRI   15					/*!< Maximum task priority										*/

#define TMIN_MPRI    1                  /*!< Minimum message priority(=1)                               */
#define TMAX_MPRI    7                  /*!< Maximum message priority                                   */

//-------------------------------------
// Number of Bits in Bitpatterns
//-------------------------------------
#define TBIT_FLGPTN	16					/*!< Number of bits asn eventflag								*/

//-------------------------------------
// Others
//-------------------------------------
#define TMAX_MAXSEM	31					/*!< Maximum value of the maximum definable semaphore resource count */

//-------------------------------------
// ID Range
//-------------------------------------
#define TMIN_TSKID			  1					/*!< Max task ID		*/
#define TMAX_TSKID			 64					/*!< Max task ID		*/
#define TMIN_SEMID			  1					/*!< Max semaphore ID	*/
#define TMAX_SEMID			128					/*!< Max semaphore ID	*/
#define TMIN_FLGID			  1					/*!< Max eventflag ID	*/
#define TMAX_FLGID			 64					/*!< Max eventflag ID	*/
#define TMIN_MBXID			  1	                /*!< Max mailbox ID		*/
#define TMAX_MBXID			 64	                /*!< Max mailbox ID		*/
#define TMIN_MTXID			TMIN_SEMID			/*!< Max mutex ID		*/
#define TMAX_MTXID			TMAX_SEMID			/*!< Max mutex ID		*/

/*==========================================================================*/
/* D E F I N E																*/
/*==========================================================================*/
#define MAX_CONTEXT_NUM		TMAX_TSKID			/*!< Max context number		*/
#define MAX_SEMAPHORE_NUM	TMAX_SEMID			/*!< Max semaphore number	*/
#define MAX_EVENTFLAG_NUM	TMAX_FLGID			/*!< Max eventflag number	*/
#define MAX_MAILBOX_NUM     TMAX_MBXID			/*!< Max mailbox number     */
#define MAX_MUTEX_NUM		MAX_SEMAPHORE_NUM	/*!< Max mutex number		*/

#define MAX_PRIORITY_NUM	TMAX_TPRI			/*!< Max priority number	*/
#define MAX_SEMAPHORE_CNT	TMAX_MAXSEM			/*!< Max semaphore count	*/

#define HWOS_TBL_END		(-1)				/*!< Initialize table end mark	*/
#define TASK_TBL_END		HWOS_TBL_END		/*!< Task table end ID		*/
#define SEMAPHORE_TBL_END	HWOS_TBL_END		/*!< Semaphore table end ID	*/
#define EVENTFLAG_TBL_END	HWOS_TBL_END		/*!< Eventflag table end ID	*/
#define MAILBOX_TBL_END		HWOS_TBL_END		/*!< Mailbox table end ID	*/
#define MUTEX_TBL_END		HWOS_TBL_END		/*!< Mutex table end ID		*/
#define INT_TBL_END			0xffffffff			/*!< Interrupt table end ID	*/
#define HWISR_TBL_END		0xffffffff			/*!< HW-ISR table end ID	*/

//-------------------------------------
// HW-ISR infomation
//-------------------------------------
#define MAX_QINT_NUM       32                   /*!< Max QINT number        */

#define HWISR_SET_FLG		1					/*!< set_flg				*/
#define HWISR_SIG_SEM		2					/*!< sig_sem				*/
#define HWISR_REL_WAI		3					/*!< rel_wai				*/
#define HWISR_WUP_TSK		4					/*!< wup_tsk				*/

//-------------------------------------
// Operation setting when the mail box is TA_MPRI attribute.
//-------------------------------------
#define HWOS_DISABLE_MPRI			(0)					/*!< Disable the API of mail sending and receiving.	*/
#define HWOS_ENABLE_MPRI			(1)					/*!< Check whether mail can send.					*/

#endif // __KERNEL_H__
