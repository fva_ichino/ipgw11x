#ifndef __LOG_H__
#define __LOG_H__

#include <syslog.h>
#include <unistd.h> /* syscall */
#include <sys/syscall.h>   /* SYS_xxx の定義用 */
#include <pthread.h> // pthread_mutex_t, lock, unlock

#define LOG_FACILITY LOG_USER
/* 共通出力フォーマット | $ファイル名($行数),$TID,$メッセージ */
#define LOG_COMMON_FORMAT "%s(%d),%d,"

#define LOG_E(format, args...) log_output(LOG_ERR, LOG_COMMON_FORMAT format, log_split_path_to_name(__FILE__), __LINE__, syscall(SYS_gettid), ##args)
#define LOG_W(format, args...) log_output(LOG_WARNING, LOG_COMMON_FORMAT format, log_split_path_to_name(__FILE__), __LINE__, syscall(SYS_gettid), ##args)
#define LOG_I(format, args...) log_output(LOG_INFO, LOG_COMMON_FORMAT format, log_split_path_to_name(__FILE__), __LINE__, syscall(SYS_gettid), ##args)

/* 「デバッグ」レベルはマクロ定数を定義しない限り出力しない */
#ifdef _LOG_DBG_ENABLE
	#define LOG_D(format, args...) log_output(LOG_DEBUG, LOG_COMMON_FORMAT format, log_split_path_to_name(__FILE__), __LINE__, syscall(SYS_gettid), ##args)
#else
	#define LOG_D(format, args...)
#endif /* _LOG_DBG_ENABLE */

/* ログ解析用トレース情報(ループ内では使用しないこと) */
#define LOG_TRACE(format, args...) log_output_with_order(LOG_INFO, format, ##args)
/* スレッド情報ダンプ用 */
#define LOG_THREAD() LOG_TRACE("[Thread] %s PID=%d TID=%d\n", __func__, syscall(SYS_getpid), syscall(SYS_gettid))

extern void log_init(const char *ident);
extern void log_output(int priority, const char *format, ...);
extern void log_output_with_order(int priority, const char *format, ...);
extern void log_close();
extern char* log_split_path_to_name(const char *file);

#endif /* __LOG_H__ */