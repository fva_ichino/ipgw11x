#include <stdio.h> // sprintf
#include <arpa/inet.h> // inet_ntop, INET_ADDRSTRLEN
#include <net/if.h> // IFNAMSIZ

#include <errno.h>
#include <string.h> // strerror

#include "interface.h"
#include "run_cmd.h"
#include "log.h"

/* インターフェース名(SFP) */
#define CFG_IF_A_NAME "eth0"
#define CFG_IF_B_NAME "eth1"
/* IPアドレスの第4オクテットを+1するための数字(0x1000000) */
#define OCTET4_PLUS_ONE 16777216


/**
 * @brief インターフェースIP削除
 * 
 * @param ifName 元のインターフェース名
 * @param xipNet インターフェース情報(Big Endian)
 * @return void 
 */
void interface_ipdel(const char *ifName, const char *ipAddr, const uint8_t mask) {
	int32_t ret;
	char cmd[256 + IFNAMSIZ * 2];
	LOG_TRACE("[IP DEL] if = %s\n", ifName);
	
	/* IPアドレス削除 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip address del %s/%u dev %s",
			ipAddr, mask, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}
	return;
}

/**
 * @brief インターフェースIP設定
 * 
 * @param ifName 元のインターフェース名
 * @param xipNet インターフェース情報(Big Endian)
 * @return void 
 */
void interface_ipset(const char *ifName, const XIP_NET_T xipNet) {
	int32_t ret;
	char cmd[256 + IFNAMSIZ * 2];
	char ipAddr[INET_ADDRSTRLEN] = {0};
	char netMaskAddr[INET_ADDRSTRLEN] = {0};
	char gatewayAddr[INET_ADDRSTRLEN] = {0};

	LOG_TRACE("[IP SET] if = %s, addr = 0x%08X, mask = 0x%08X, gw = 0x%08X\n",
				ifName,
				ntohl(xipNet.xIpAddr.addr),
				ntohl(xipNet.xNetMast.addr),
				ntohl(xipNet.xGateway.addr));

	/* 送信元IPアドレスをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xIpAddr.addr), ipAddr, sizeof(ipAddr))) {
		LOG_E("inet_ntop(xIpAddr: 0x%08X) failed: %d(%s)\n", xipNet.xIpAddr.addr, errno, strerror(errno));
		return;
	}

	/* サブネットマスクをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xNetMast.addr), netMaskAddr, sizeof(netMaskAddr))) {
		LOG_E("inet_ntop(xNetMast: 0x%08X) failed: %d(%s)\n", xipNet.xNetMast.addr, errno, strerror(errno));
		return;
	}

	/* GWアドレスをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xGateway.addr), gatewayAddr, sizeof(gatewayAddr))) {
		LOG_E("inet_ntop(xGateway: 0x%08X) failed: %d(%s)\n", xipNet.xGateway.addr, errno, strerror(errno));
		return;
	}

	/* IPアドレス付与 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip address add %s/%s dev %s",
			ipAddr, netMaskAddr, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	/* インターフェースを有効化(リンクアップ) */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip link set dev %s up",
			ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	/* デフォルトゲートウェイ設定(先にリンクアップが必要) */
	// interface_routingset()で別途ルーティングを設定しているため不要
	//memset(cmd, 0x00, sizeof(cmd));
	//sprintf(cmd,
	//		"ip route replace default via %s dev %s",
	//		gatewayAddr, ifName);
	//ret = run_cmd(cmd);
	//if (0 != ret) {
	//	/* run_cmd()でログ出力済み */
	//	return;
	//}

	return;
}

/**
 * @brief ルーティングテーブル初期設定削除
 * 
 * @param ifName インターフェース名
 * @param xipNet インターフェース情報(Big Endian)
 * @return void 
 */

void interface_routingdel(const char *ifName, const XIP_NET_T xipNet) {
	int32_t ret;
	char cmd[256 + IFNAMSIZ * 2];
	u32_t tmp;
	char delIp[INET_ADDRSTRLEN] = {0};
	char netIp[INET_ADDRSTRLEN] = {0};

	LOG_TRACE("[ROUTING DEL] if = %s\n", ifName);

	// 削除対象のルーティングIPを生成し、アドレス文字列に変換
	tmp = xipNet.xIpAddr.addr & xipNet.xNetMast.addr;
	if (NULL == inet_ntop(AF_INET, (void *)&tmp, delIp, sizeof(delIp))) {
		LOG_E("inet_ntop(tmp: 0x%08X) failed: %d(%s)\n", tmp, errno, strerror(errno));
		return;
	}

	// サブネットマスクをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&xipNet.xNetMast.addr, netIp, sizeof(netIp))) {
		LOG_E("inet_ntop(xipNet.xNetMast.addr: 0x%08X) failed: %d(%s)\n", xipNet.xNetMast.addr, errno, strerror(errno));
		return;
	}
	
	/* 初期ルーティング削除 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route delete %s/%s dev %s",
			delIp, netIp, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	return;
}

/**
 * @brief ルーティングテーブル設定
 * 
 * @param ifName インターフェース名
 * @param dstNet インターフェース情報(Big Endian)
 * @return void 
 */
void interface_routingset(const char *ifName, const LWII_XIP_T dstNet) {
	int32_t ret;
	u32_t tmp;
	char cmd[256 + IFNAMSIZ * 2];
	char alarmIpAddr[INET_ADDRSTRLEN] = {0};			// アラームサーバIPアドレス
	char settingPCIpAddr[INET_ADDRSTRLEN] = {0};		// 設定PC IPアドレス
	char mngCtlIpAddr[INET_ADDRSTRLEN] = {0};			// 管理制御棚IPアドレス
	char mngCtlIpAddrPlusOne[INET_ADDRSTRLEN] = {0};	// 管理制御棚IPアドレス+1

	LOG_TRACE("[ROUTING SET] if = %s, addr = 0x%08X, 0x%08X, 0x%08X\n",
				ifName,
				ntohl(dstNet.dst[0].addr),
				ntohl(dstNet.dst[1].addr),
				ntohl(dstNet.dst[2].addr));

	// アラームサーバIPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[0].addr), alarmIpAddr, sizeof(alarmIpAddr))) {
		LOG_E("inet_ntop(dst[0].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[0].addr, errno, strerror(errno));
		return;
	}

	// 設定PC IPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[1].addr), settingPCIpAddr, sizeof(settingPCIpAddr))) {
		LOG_E("inet_ntop(dst[1].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[1].addr, errno, strerror(errno));
		return;
	}

	// 管理制御棚IPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[2].addr), mngCtlIpAddr, sizeof(mngCtlIpAddr))) {
		LOG_E("inet_ntop(dst[2].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[2].addr, errno, strerror(errno));
		return;
	}

	// 冗長構成を考慮：管理制御棚IPアドレス第4オクテットを+1したものをアドレス文字列に変換
	tmp = dstNet.dst[2].addr + OCTET4_PLUS_ONE;
	if (NULL == inet_ntop(AF_INET, (void *)&(tmp), mngCtlIpAddrPlusOne, sizeof(mngCtlIpAddrPlusOne))) {
		LOG_E("inet_ntop(tmp: 0x%08X) failed: %d(%s)\n", tmp, errno, strerror(errno));
		return;
	}

	// アラームサーバIPアドレスをルーティングに設定
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s proto static metric 10",
			alarmIpAddr, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 設定PC IPアドレスをルーティングに設定
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s proto static metric 10",
			settingPCIpAddr, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 管理制御棚IPアドレスをルーティングに設定
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s proto static metric 10",
			mngCtlIpAddr, ifName);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 冗長構成を考慮：対となるIFにルーティングを設定
	if (strcmp(ifName, CFG_IF_A_NAME) == 0) {
		memset(cmd, 0x00, sizeof(cmd));
		sprintf(cmd,
			"ip route add %s dev %s proto static metric 10",
			mngCtlIpAddrPlusOne, CFG_IF_B_NAME);
		ret = run_cmd(cmd);
		if (0 != ret) {
			/* run_cmd()でログ出力済み */
			return;
		}
	} else if (strcmp(ifName, CFG_IF_B_NAME) == 0) {
		memset(cmd, 0x00, sizeof(cmd));
		sprintf(cmd,
			"ip route add %s dev %s proto static metric 10",
			mngCtlIpAddrPlusOne, CFG_IF_A_NAME);
		ret = run_cmd(cmd);
		if (0 != ret) {
			/* run_cmd()でログ出力済み */
			return;
		}
	} else {
		/* eth0, eth1以外は考慮しない */
	}

	return;
}


/**
 * @brief VLAN インターフェース作成＋設定
 * 
 * @param ifName 元のインターフェース名
 * @param xipNet インターフェース情報(Big Endian)
 * @param vlanTagId VLAN ID(Big Endian)
 * @return void 
 */
void interface_ipset_vlan(const char *ifName, const XIP_NET_T xipNet, const UH vlanTagId) {
	int32_t ret;
	char cmd[256 + IFNAMSIZ * 2];
	char ipAddr[INET_ADDRSTRLEN] = {0};
	char netMaskAddr[INET_ADDRSTRLEN] = {0};
	char gatewayAddr[INET_ADDRSTRLEN] = {0};
	const uint16_t vlanTagId_le = ntohs(vlanTagId);

	LOG_TRACE("[IP SET VLAN] if = %s, vlan = %u, addr = 0x%08X, mask = 0x%08X, gw = 0x%08X\n",
				ifName, vlanTagId_le,
				ntohl(xipNet.xIpAddr.addr),
				ntohl(xipNet.xNetMast.addr),
				ntohl(xipNet.xGateway.addr));

	/* 送信元IPアドレスをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xIpAddr.addr), ipAddr, sizeof(ipAddr))) {
		LOG_E("inet_ntop(xIpAddr: 0x%08X) failed: %d(%s)\n", xipNet.xIpAddr.addr, errno, strerror(errno));
		return;
	}

	/* サブネットマスクをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xNetMast.addr), netMaskAddr, sizeof(netMaskAddr))) {
		LOG_E("inet_ntop(xNetMast: 0x%08X) failed: %d(%s)\n", xipNet.xNetMast.addr, errno, strerror(errno));
		return;
	}

	/* GWアドレスをアドレス文字列に変換 */
	if (NULL == inet_ntop(AF_INET, (void *)&(xipNet.xGateway.addr), gatewayAddr, sizeof(gatewayAddr))) {
		LOG_E("inet_ntop(xGateway: 0x%08X) failed: %d(%s)\n", xipNet.xGateway.addr, errno, strerror(errno));
		return;
	}

	/* VLANインターフェース作成 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip link add link %s name %s.%u type vlan id %u",
			ifName, ifName, vlanTagId_le, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	/* IPアドレス付与 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip address add %s/%s dev %s.%u",
			ipAddr, netMaskAddr, ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	/* インターフェースを有効化(リンクアップ) */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip link set dev %s.%u up",
			ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	/* デフォルトゲートウェイ設定(先にリンクアップが必要) */
	// interface_routingset_vlanで別途ルーティングを設定しているため不要
	//memset(cmd, 0x00, sizeof(cmd));
	//sprintf(cmd,
	//		"ip route replace default via %s dev %s.%u",
	//		gatewayAddr, ifName, vlanTagId_le);
	//ret = run_cmd(cmd);
	//if (0 != ret) {
	//	/* run_cmd()でログ出力済み */
	//	return;
	//}

	return;
}

/**
 * @brief VLAN ルーティングテーブル設定
 * 
 * @param ifName インターフェース名
 * @param dstNet インターフェース情報(Big Endian)
 * @param vlanTagId VLAN ID(Big Endian)
 * 
 * @return void 
 */
void interface_routingset_vlan(const char *ifName, const LWII_XIP_T dstNet, const UH vlanTagId) {
	int32_t ret;
	u32_t tmp;
	char cmd[256 + IFNAMSIZ * 2];
	char alarmIpAddr[INET_ADDRSTRLEN] = {0};			// アラームサーバIPアドレス
	char settingPCIpAddr[INET_ADDRSTRLEN] = {0};		// 設定PC IPアドレス
	char mngCtlIpAddr[INET_ADDRSTRLEN] = {0};			// 管理制御棚IPアドレス
	char mngCtlIpAddrPlusOne[INET_ADDRSTRLEN] = {0};	// 管理制御棚IPアドレス+1
	const uint16_t vlanTagId_le = ntohs(vlanTagId);		// vlanTagIdをリトルエンディアンに変換したもの

	LOG_TRACE("[VLAN ROUTING SET] if = %s, vlan = %u, addr = 0x%08X, 0x%08X, 0x%08X\n",
				ifName,
				vlanTagId_le,
				ntohl(dstNet.dst[0].addr),
				ntohl(dstNet.dst[1].addr),
				ntohl(dstNet.dst[2].addr));

	// アラームサーバIPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[0].addr), alarmIpAddr, sizeof(alarmIpAddr))) {
		LOG_E("inet_ntop(dst[0].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[0].addr, errno, strerror(errno));
		return;
	}

	// 設定PC IPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[1].addr), settingPCIpAddr, sizeof(settingPCIpAddr))) {
		LOG_E("inet_ntop(dst[1].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[1].addr, errno, strerror(errno));
		return;
	}

	// 管理制御棚IPアドレスをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&(dstNet.dst[2].addr), mngCtlIpAddr, sizeof(mngCtlIpAddr))) {
		LOG_E("inet_ntop(dst[2].addr: 0x%08X) failed: %d(%s)\n", dstNet.dst[2].addr, errno, strerror(errno));
		return;
	}

	// 冗長構成を考慮：管理制御棚IPアドレス第4オクテットを+1したものをアドレス文字列に変換
	tmp = dstNet.dst[2].addr + OCTET4_PLUS_ONE;
	if (NULL == inet_ntop(AF_INET, (void *)&(tmp), mngCtlIpAddrPlusOne, sizeof(mngCtlIpAddrPlusOne))) {
		LOG_E("inet_ntop(tmp: 0x%08X) failed: %d(%s)\n", tmp, errno, strerror(errno));
		return;
	}

	// アラームサーバIPアドレスをルーティングに設定(vlan)
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s.%u proto static metric 10",
			alarmIpAddr, ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 設定PC IPアドレスをルーティングに設定(vlan)
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s.%u proto static metric 10",
			settingPCIpAddr, ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 管理制御棚IPアドレスをルーティングに設定(vlan)
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route add %s dev %s.%u proto static metric 10",
			mngCtlIpAddr, ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	// 冗長構成を考慮：対となるIFにルーティングを設定(vlan)
	if (strcmp(ifName, CFG_IF_A_NAME) == 0) {
		memset(cmd, 0x00, sizeof(cmd));
		sprintf(cmd,
			"ip route add %s dev %s.%u proto static metric 10",
			mngCtlIpAddrPlusOne, CFG_IF_B_NAME, vlanTagId_le);
		ret = run_cmd(cmd);
		if (0 != ret) {
			/* run_cmd()でログ出力済み */
			return;
		}
	} else if (strcmp(ifName, CFG_IF_B_NAME) == 0) {
		memset(cmd, 0x00, sizeof(cmd));
		sprintf(cmd,
			"ip route add %s dev %s.%u proto static metric 10",
			mngCtlIpAddrPlusOne, CFG_IF_A_NAME, vlanTagId_le);
		ret = run_cmd(cmd);
		if (0 != ret) {
			/* run_cmd()でログ出力済み */
			return;
		}
	} else {
		/* eth0, eth1以外は考慮しない */
	}

	return;
}

/**
 * @brief VLAN ルーティングテーブル初期設定削除
 * 
 * @param ifName インターフェース名
 * @param xipNet インターフェース情報(Big Endian)
 * @param vlanTagId VLAN ID(Big Endian)
 * @return void 
 */

void interface_routingdel_vlan(const char *ifName, const XIP_NET_T xipNet, const UH vlanTagId) {
	int32_t ret;
	char cmd[256 + IFNAMSIZ * 2];
	u32_t tmp;
	char delIp[INET_ADDRSTRLEN] = {0};
	char netIp[INET_ADDRSTRLEN] = {0};
	const uint16_t vlanTagId_le = ntohs(vlanTagId);		// vlanTagIdをリトルエンディアンに変換したもの

	LOG_TRACE("[VLAN ROUTING DEL] if = %s, vlan = %u\n", ifName, vlanTagId_le);

	// 削除対象のルーティングIPを生成し、アドレス文字列に変換
	tmp = xipNet.xIpAddr.addr & xipNet.xNetMast.addr;
	if (NULL == inet_ntop(AF_INET, (void *)&tmp, delIp, sizeof(delIp))) {
		LOG_E("inet_ntop(tmp: 0x%08X) failed: %d(%s)\n", tmp, errno, strerror(errno));
		return;
	}

	// サブネットマスクをアドレス文字列に変換
	if (NULL == inet_ntop(AF_INET, (void *)&xipNet.xNetMast.addr, netIp, sizeof(netIp))) {
		LOG_E("inet_ntop(xipNet.xNetMast.addr: 0x%08X) failed: %d(%s)\n", xipNet.xNetMast.addr, errno, strerror(errno));
		return;
	}
	
	/* 初期ルーティング削除 */
	memset(cmd, 0x00, sizeof(cmd));
	sprintf(cmd,
			"ip route delete %s/%s dev %s.%u",
			delIp, netIp, ifName, vlanTagId_le);
	ret = run_cmd(cmd);
	if (0 != ret) {
		/* run_cmd()でログ出力済み */
		return;
	}

	return;
}