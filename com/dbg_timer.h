#ifndef __DBG_TIMER_H__
#define __DBG_TIMER_H__

#include <sys/time.h>

#include <errno.h>
#include <string.h>

#include "log.h"

/** @note タイマーの有効化(0 or 1) */
#define DBG_TIMER_ENABLED 1

#if (DBG_TIMER_ENABLED == 1)
	extern __thread struct timeval dbg_timer_start;
	extern __thread struct timeval dbg_timer_end;
	extern __thread struct timeval dbg_timer_diff;

	#define DBG_TIMER_START() 	do {                                                                                        \
									if (0 == gettimeofday(&dbg_timer_start, NULL)) {                                        \
										LOG_D("[TIMER START] %ld.%ld\n", dbg_timer_start.tv_sec, dbg_timer_start.tv_usec);  \
									}                                                                                       \
									else {                                                                                  \
										LOG_E("[TIMER START FAILED] %d(%s)\n", errno, strerror(errno));                     \
									}                                                                                       \
								} while (0)

	#define DBG_TIMER_END() 	do {                                                                                        \
									if (0 == gettimeofday(&dbg_timer_end, NULL)) {                                          \
										LOG_D("[TIMER END] %ld.%ld\n", dbg_timer_end.tv_sec, dbg_timer_end.tv_usec);        \
									}                                                                                       \
									else {                                                                                  \
										LOG_E("[TIMER END FAILED] %d(%s)\n", errno, strerror(errno));                       \
									}                                                                                       \
								} while (0)

	#define DBG_TIMER_DIFF()	do {                                                                                        \
									if (dbg_timer_end.tv_usec >= dbg_timer_start.tv_usec) {                                 \
										dbg_timer_diff.tv_sec = dbg_timer_end.tv_sec - dbg_timer_start.tv_sec;              \
										dbg_timer_diff.tv_usec = dbg_timer_end.tv_usec - dbg_timer_start.tv_usec;           \
									}                                                                                       \
									else {                                                                                  \
										dbg_timer_diff.tv_sec = dbg_timer_end.tv_sec - 1 - dbg_timer_start.tv_sec;          \
										dbg_timer_diff.tv_usec = dbg_timer_end.tv_usec + 1000000 - dbg_timer_start.tv_usec; \
									}                                                                                       \
									LOG_I("[TIMER DIFF] %ld.%06ld\n", dbg_timer_diff.tv_sec, dbg_timer_diff.tv_usec);       \
								} while (0)

	#define DBG_TIMER_RESTART()	do {                                                                                        \
									dbg_timer_start.tv_sec = dbg_timer_end.tv_sec;                                          \
									dbg_timer_start.tv_usec = dbg_timer_end.tv_usec;                                        \
									LOG_D("[TIMER RESTART] %ld.%ld\n", dbg_timer_start.tv_sec, dbg_timer_start.tv_usec);    \
								} while (0)
#elif (DBG_TIMER_ENABLED == 0)
	/* 何もしない */
	#define DBG_TIMER_START()
	#define DBG_TIMER_END()
	#define DBG_TIMER_DIFF()
	#define DBG_TIMER_RESTART()
#else
	#error "DBG_TIMER_ENABLED is allowed only 0 or 1"
#endif /* DBG_TIMER_ENABLED */

#endif /* __DBG_TIMER_H__ */