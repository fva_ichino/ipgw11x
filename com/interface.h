#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdint.h>

#include "itron.h" // UH
#include "ethernetif.h" // XIP_NET_T

extern void interface_ipdel(const char *ifName, const char *ipAddr, const uint8_t mask);
extern void interface_ipset(const char *deviceName, const XIP_NET_T xipNet);
extern void interface_ipset_vlan(const char *deviceName, const XIP_NET_T xipNet, const UH vlanTagId);

extern void interface_routingdel(const char *ifName, const XIP_NET_T xipNet);
extern void interface_routingset(const char *ifName, const LWII_XIP_T dstNet);
extern void interface_routingset_vlan(const char *ifName, const LWII_XIP_T dstNet, const UH vlanTagId);
extern void interface_routingdel_vlan(const char *ifName, const XIP_NET_T xipNet, const UH vlanTagId);
#endif /* __INTERFACE_H__ */