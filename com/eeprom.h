#ifndef	__EEPROM_H__
#define	__EEPROM_H__

#include <stdint.h>
#include "errcodes.h"

extern ER_RET eep_init(uint8_t ch, uint8_t iic_adr);
extern ER_RET eep_write(uint32_t eep_addr, uint8_t *data, uint32_t len);
extern ER_RET eep_read(uint32_t eep_addr, uint8_t *data, uint32_t len);

#endif /* __EEPROM_H__ */