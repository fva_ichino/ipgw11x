#include "net_hdr.h"

/** @note define値はRTOSから流用 */
#define IP_VER4             0       /* IPv4 */
#define ETH_HDR_SZ      	14
#define CFG_NET_BUF_OFFSET     42

//=============================================================================
//==	グローバル変数定義
//=============================================================================

T_NODE	snmp_mgrA	=	{162, IP_VER4, 0, 0};	//	Nii	SNMP manager (A)
T_NODE	snmp_mgrB	=	{162, IP_VER4, 0, 0};	//	Nii	SNMP manager (B)

T_NET_ADR gNET_ADR[2] = {
    {
        0x0,            /* Reserved */
        0x0,            /* Reserved */
        0xC0A93701,     /* IP address  "192.169.55.1" */
        0xC0A9010A,     /* Gateway     "192.169.1.10"   */
        0xFFFF0000,     /* Subnet mask "255.255.0.0" */
    },
    {	//	Nii
        0x0,            /* Reserved */
        0x0,            /* Reserved */
        0xC0A93702,     /* IP address  "192.169.55.1" */
        0xC0A9010A,     /* Gateway     "192.169.1.01"   */
        0xFFFF0000,     /* Subnet mask "255.255.0.0" */
    }
};

/** @note 関数ポインタの実引数はすべてNULL指定 */
T_NET_DEV gNET_DEV[2] = {
    {
        "lan0",            /* Device Name      */
        1,                 /* Device Number    */
        NET_DEV_TYPE_ETH,  /* Device Type      */
        0,                 /* Status           */
        0,                 /* Flags            */
        NULL,           /* Device Init      */
        NULL,           /* Device Close     */
        NULL,           /* Device Configure */
        NULL,           /* Device Status    */
        NULL,           /* Device Transmit  */
        NULL,           /* Device Callback  */
        0,
        {{{ 0x12, 0x34, 0x56, 0x78, 0x9A, 0xA1 }}},  /* MAC Address */
        ETH_HDR_SZ,                                /* Link Header Size */
        CFG_NET_BUF_OFFSET                         /* Network buffer data Offset */
    }
	,
    {	//	Nii
        "lan1",            /* Device Name      */
        2,                 /* Device Number    */
        NET_DEV_TYPE_ETH,  /* Device Type      */
        0,                 /* Status           */
        0,                 /* Flags            */
        NULL,           /* Device Init      */
        NULL,           /* Device Close     */
        NULL,           /* Device Configure */
        NULL,           /* Device Status    */
        NULL,           /* Device Transmit  */
        NULL,           /* Device Callback  */
        0,
        {{{ 0x12, 0x34, 0x56, 0x78, 0x9A, 0xB1 }}},  /* MAC Address */
        ETH_HDR_SZ,                                /* Link Header Size */
        CFG_NET_BUF_OFFSET                         /* Network buffer data Offset */
    }
};