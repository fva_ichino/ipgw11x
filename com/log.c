/**
 * @file log.c
 * @brief SYSLOG関数
 */

#include <stdarg.h> // 可変長引数用
#include <string.h> // strrchr
#include "log.h"

static pthread_mutex_t s_log_mutex = PTHREAD_MUTEX_INITIALIZER; 

/**
 * @brief ログ初期化
 * 
 * @param ident プログラム名
 */
void log_init(const char *ident) {
#ifdef _LOG_CONSOLE_ENABLE
	// stdout: LOG_CONS, stderr: LOG_PERROR
	openlog(ident, (LOG_PERROR | LOG_PID), LOG_FACILITY);
#else
	openlog(ident, LOG_PID, LOG_FACILITY);
#endif
    return;
}

/**
 * @brief ログ終了
 * 
 */
void log_close(void) {
	closelog();
	return;
}

/**
 * @brief ログ出力
 * 
 * @param pri ログレベル
 * @param format フォーマット指定子
 * @param ... 
 */
void log_output(int pri, const char *format, ...) {
	va_list arg;
	va_start(arg, format);
	vsyslog(pri, format, arg);
	va_end(arg);
	return;
}

/**
 * @brief ログ出力(順序制御あり)
 * 
 * @param pri ログレベル
 * @param format フォーマット指定子
 * @param ... 
 */
void log_output_with_order(int pri, const char *format, ...) {
	va_list arg;
	va_start(arg, format);

    pthread_mutex_lock(&s_log_mutex);
    vsyslog(pri, format, arg);
    pthread_mutex_unlock(&s_log_mutex);

	va_end(arg);
    return;
}

/**
 * @brief ファイル名だけにする
 * 
 * @param file ファイルパス文字列
 * @return ファイル名の先頭アドレス
 */
char* log_split_path_to_name(const char *file) {
	/* 最後のデリミタ位置取得 */
	char *p_filename = strrchr((char *)&file[0], '/');
    if (NULL == p_filename) {
		/* デリミタなし */
        p_filename = (char *)&file[0];
    } else {
		/* デリミタの次の文字位置 */
        p_filename++;
    }
	return p_filename;
}
