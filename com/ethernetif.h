#ifndef __ETHERNETIF_H__
#define __ETHERNETIF_H__

#include	<stdint.h>

#include	"ip_addr.h" // struct ip_addr
#include    "err.h" // err_t
#include	"kernel.h" // UB, UH, T_MSG, and so on

typedef struct	{
	struct	ip_addr		xIpAddr;
	struct	ip_addr		xNetMast;
	struct	ip_addr		xGateway;
} XIP_NET_T;

typedef struct	{
	struct	ip_addr		dst[10];
} LWII_XIP_T;

extern XIP_NET_T		xipNet[2];
extern LWII_XIP_T		dstNet[2];

extern UH				vlanTagId;
extern UB				vlanTagFlag;

extern void udp_raw_init_port0(struct ip_addr *addr, u16_t port);
extern void udp_raw_init_port1(struct ip_addr *addr, u16_t port);
extern u32_t udp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
extern void udp_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos);
extern void snmp_raw_init_port0(struct ip_addr *addr, u16_t port);
extern void snmp_raw_init_port1(struct ip_addr *addr, u16_t port);
extern u32_t snmp_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
// UDP MONITOR
extern void udp_moni_raw_init_port0(struct ip_addr *addr, u16_t port);
extern void udp_moni_raw_init_port1(struct ip_addr *addr, u16_t port);
extern u32_t udp_moni_raw_recv_mnt(u8_t *dp, u8_t *id, struct ip_addr *addr, u16_t *port);
extern void udp_moni_raw_send(u8_t *dp, u32_t len, struct ip_addr *addr, u16_t port, const u8_t id, u16_t srcport, u8_t tos);

extern uint32_t eth_c4To32(uint8_t *c);

#endif /* __ETHERNETIF_H__ */