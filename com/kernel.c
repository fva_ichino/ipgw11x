/* #define _GNU_SOURCE // CPUアフィニティ利用時 */
#include <unistd.h> // usleep

#include <pthread.h>
#include <sched.h> 
#include <limits.h> // PTHREAD_STACK_MIN

#include <string.h> // strerror

#include "kernel_id.h" // ID_TASK_*
#include "errcodes.h"

#include "kernel.h"

#include "log.h"

/*============================================================================*/
/* V A R I A B L E                                                            */
/*============================================================================*/

/* スレッド管理テーブル */
static thread_table_t *s_thread_table = NULL;

/* ミューテックス管理テーブル */ 
static mutex_table_t *s_mutex_table = NULL;

/*============================================================================*/
/* F U N C T I O N   P R O T O T Y P E                                        */
/*============================================================================*/

void kernel_reg_thread_table(thread_table_t *p_thread_table);
void kernel_reg_mutex_table(mutex_table_t *p_mutex_table);
ER sta_tsk(ID tskid, VP_INT stacd);
ER tslp_tsk(TMO tmout);
ER wai_sem(ID semid);
ER sig_sem(ID semid);

/*============================================================================*/
/* S T A T I C   F U N C T I O N   P R O T O T Y P E                          */
/*============================================================================*/

static pthread_mutex_t* s_mutex_get_from_ID(ID semid);
static int s_pthread_create_from_thread_table(thread_table_t *p_tskid_thread_manage_table, VP_INT stacd);

/*============================================================================*/
/* P R O G R A M                                                              */
/*============================================================================*/

//-------------------------------------------------------------
// Linux用外部関数
//-------------------------------------------------------------

/**
 * @brief スレッド管理テーブル登録
 * 
 * @param p_thread_table 
 */
void kernel_reg_thread_table(thread_table_t *p_thread_table) {
	if (NULL == p_thread_table) {
		LOG_E("Invalid patamer: %p\n", p_thread_table);
		return;
	}
	s_thread_table = p_thread_table;
	return;
}

/*============================================================================*/

/**
 * @brief ミューテックス管理テーブル登録
 * 
 * @param p_mutex_table 
 */
void kernel_reg_mutex_table(mutex_table_t *p_mutex_table) {
	if (NULL == p_mutex_table) {
		LOG_E("Invalid patamer: %p\n", p_mutex_table);
		return;
	}
	s_mutex_table = p_mutex_table;
	return;
}

/*============================================================================*/

/** 
	@ref Renesas R-IN32M3シリーズ プログラミング・マニュアル （OS編） Rev.7.00

	sta_tsk
	概要
	タスクの起動
	C 言語形式
	ER sta_tsk(ID tskid, VP_INT stacd);
	パラメータ
	I/O パラメータ 説明
	I ID tskid タスクのID
	数値（1〜64） 対象タスクID
	I VP_INT stacd タスクの起動コード
	機能
	tskid で指定されたタスクを DORMANT 状態から READY 状態へと遷移させます。
	stacd には，対象タスクに引き渡す拡張情報を指定します。
	戻り値
	マクロ 数値 意味
	E_OK 0 正常終了
	E_ID -18 不正ID番号（tskidが不正あるいは使用できない）
	E_OBJ -41 オブジェクト状態エラー（対象タスクがDORMANT状態ではない）
	E_NOEXS -42 オブジェクト未生成（対象タスクが未登録）
*/

/**
 * @brief タスクの起動
 * 
 * @param tskid タスクのID
 * @param stacd タスクの起動コード
 * @return ER エラーコード
 * @retval E_OK 起動成功
 * @retval E_ID 起動失敗
 */
ER sta_tsk(ID tskid, VP_INT stacd) {
	thread_table_t *p_tskid_thread_manage_table;
	int ret;

	if (NULL == s_thread_table) {
		LOG_E("thread table not registered\n");
		return E_ID;
	}

	/* tskidでタスクID-スレッド管理テーブルを検索して、スレッド起動 */
	for (
		p_tskid_thread_manage_table = (thread_table_t *)&s_thread_table[0];
		p_tskid_thread_manage_table->tskid != TASK_TBL_END;
		p_tskid_thread_manage_table++
	) {
		if (p_tskid_thread_manage_table->tskid == tskid) {
			ret = s_pthread_create_from_thread_table(p_tskid_thread_manage_table, stacd);
			if (0 != ret) {
				LOG_E("thread run failed. tskid: %d, ret: %d\n", tskid, ret);
				return E_ID;
			}
			return E_OK;
		}
	}

	/* ここには来ない */
	LOG_W("thread not run. unsupported tskid: %d\n", tskid);
	return E_ID;
}

/*============================================================================*/

/** 
	@ref Renesas R-IN32M3シリーズ プログラミング・マニュアル （OS編） Rev.7.00

	tslp_tsk
	概要
	タスク起床待ち（タイムアウトあり）
	C 言語形式
	ER tslp_tsk(TMO tmout);
	パラメータ
	I/O パラメータ 説明
	I TMO tmout タイムアウト指定（単位はms）
	TMO_FEVR（-1） 永久待ち（slp_tsk()と同等の処理）
	TMO_POL(0) ポーリング
	数値 待ち時間
	機能
	自タスクを RUNNING 状態から起床待ち状態へと遷移させます。
	ただし，本サービスコールを発行した際，自タスクの起床要求がキューイングされていた（起床要求キュ
	ーイング数が 0x0 以外）場合には，状態操作処理は行わず，起床要求キューイング数から 0x1 を減算し実行
	を継続します。
	戻り値
	マクロ 数値 意味
	E_OK 0 正常終了
	E_PAR -17 パラメータエラー（tmoutが不正）
	E_CTX -25 CPUロック状態/ディスパッチ禁止状態/割り込みハンドラから実行した
	E_RLWAI -49 待ち状態の強制解除（待ち状態の間にrel_waiを受け付け）
	E_TMOUT -50 タイムアウト
*/

/**
 * @brief タスク起床待ち（タイムアウトあり）
 * 
 * @param  [in] millisec 待ち時間
 * @return ER エラーコード
 * @retval ER_OK     正常終了
 * @retval E_PAR     EINVAL
 * @retval E_RLWAI   EINTR 
 * @retval E_UNKNOWN millisecにTMO_POL, TMO_FEVRのいずれかを指定 
 * @attention millisecにTMO_POL, TMO_FEVRは指定しないこと
 * 
 * @note 呼び出し元の待ち時間で変えずに実装して問題ないかそれぞれ確認する必要がある  
 */
ER tslp_tsk(TMO millisec) {
	/** @note int usleep(useconds_t usec) */
	usleep(1000 * millisec);
	return ER_OK;
}

/*============================================================================*/

/**
	@ref Renesas R-IN32M3シリーズ プログラミング・マニュアル （OS編） Rev.7.00

	wai_sem
	概要
	セマフォ資源の獲得
	C 言語形式
	ER wai_sem(ID semid);
	パラメータ
	I/O パラメータ 説明
	I ID semid セマフォのID
	数値（1〜128） 対象セマフォID
	機能
	semid で指定されるセマフォから、資源を 1 つ獲得します。
	対象セマフォの資源数が 0 の場合は、自タスクをセマフォ待ち状態に遷移させます。
	wai_sem(smid)は twai_sem(semid, TMO_FEVR)と同一です。
	戻り値
	マクロ 数値 意味
	E_OK 0 正常終了
	E_ID -18 不正ID番号（semidが不正、あるいは使用できない）
	E_CTX -25 CPUロック状態/ディスパッチ禁止状態/割り込みハンドラから実行した
	E_NOEXS -42 オブジェクト未生成
	E_RLWAI -49 待ち状態の強制解除（待ち状態の間にrel_waiを受け付け）
	E_DLT -51 待ちオブジェクトの削除（待ち状態の間に対象セマフォが削除）
*/

/**
 * @brief セマフォ資源の獲得
 * 
 * @param semid セマフォのID
 * @return E_OK
 */
ER wai_sem(ID semid) {
	pthread_mutex_t *p_mutex = NULL;

	/* セマフォのIDからミューテックスを取得 */
	p_mutex = s_mutex_get_from_ID(semid);
	/* 対応するミューテックスが存在するとき、ロック */
	if (NULL != p_mutex) {
		/** @note int pthread_mutex_lock(pthread_mutex_t *mutex) */
		pthread_mutex_lock(p_mutex);
	}
	else {
		LOG_E("mutex not lock. unsupported semid: %d\n", semid);
	}
	return E_OK;
}

/*============================================================================*/

/**
	@ref Renesas R-IN32M3シリーズ プログラミング・マニュアル （OS編） Rev.7.00

	sig_sem / isig_sem
	概要
	セマフォ資源の返却
	C 言語形式
	ER sig_sem(ID semid);
	ER isig_sem(ID semid);
	パラメータ
	I/O パラメータ 説明
	I ID semid セマフォのID
	数値（1〜128） 対象セマフォID
	機能
	semid で指定されるセマフォから、資源を 1 つ返却します。
	対象セマフォに対して資源の獲得を待っているタスクがある場合には、待ち行列の先頭タスクを待ち解除
	します。
	戻り値
	マクロ 数値 意味
	E_OK 0 正常終了
	E_ID -18 不正ID番号（semidが不正、あるいは使用できない）
	E_CTX -25 CPUロック状態から実行した
	タスクから実行した（isig_semのみ）
	E_NOEXS -42 オブジェクト未生成（対象セマフォが未登録）
	E_QOVR -43 キューイングオーバフロー（最大資源数(31)を超える返却）
*/

/**
 * @brief セマフォ資源の返却
 * 
 * @param [in] semid 
 * @return E_OK
 */
ER sig_sem(ID semid) {
	pthread_mutex_t* p_mutex = NULL;

	/* セマフォのIDからミューテックスを取得 */
	p_mutex = s_mutex_get_from_ID(semid);
	/* 対応するミューテックスが存在するとき、アンロック */
	if (NULL != p_mutex) {
		/** @note int pthread_mutex_unlock(pthread_mutex_t *mutex) */
		pthread_mutex_unlock(p_mutex);
	}
	else {
		LOG_E("mutex not unlock. unsupported semid: %d\n", semid);
	}
	return E_OK;
}

//-------------------------------------------------------------
// Linux用内部関数
//-------------------------------------------------------------
/**
 * @brief ミューテックス取得
 * 
 * @param semid セマフォのID
 * @return ミューテックスポインタ
 * @retval NULL　　 対応するミューテックスがない
 * @retval NULL以外 対応するミューテックス
 */
pthread_mutex_t* s_mutex_get_from_ID(ID semid) {
	mutex_table_t *p_semid_mutex_manage_table;
	if (NULL == s_mutex_table) {
		LOG_E("mutex table not registered\n");
		return NULL;
	}

	/* semidでセマフォID-ミューテックス管理テーブルを検索して、返却 */
	for (
		p_semid_mutex_manage_table = (mutex_table_t *)&s_mutex_table[0];
		p_semid_mutex_manage_table->semid != SEMAPHORE_TBL_END;
		p_semid_mutex_manage_table++
	) {
		if (p_semid_mutex_manage_table->semid == semid) {
			return (pthread_mutex_t*)&(p_semid_mutex_manage_table->mutex);
		}
	}

	/* ここには来ない(未登録のセマフォIDの場合) */

	/** @note ここではログ表示せず、呼び出し元で出力する */
	// LOG_E("unsupported semid: %d\n", semid);
	return NULL;
}

/**
 * @brief スレッド起動
 * 
 * @param [in] p_tskid_thread_manage_table スレッドテーブル構造体ポインタ
 * @param [in] stacd 未使用
 * @return int 成否値
 * @retval 0　　 成功
 * @retval 0以外 失敗
 */
int s_pthread_create_from_thread_table(thread_table_t *p_tskid_thread_manage_table, VP_INT stacd) { 
	int thrd_priority;
	size_t stacksize;
	void* (*thread_func)(void*);
	void *arg;

	pthread_attr_t attr;
 	struct sched_param param;
	pthread_t th;
	int ret;

/** 
 * @note CPUアフィニティ用
 * @todo 性能評価後に必要があればCPUアフィニティを行う
 */
#if 0
	cpu_set_t cpuset;
	int cpu, cpu_max;
#endif

	if (NULL == p_tskid_thread_manage_table) {
		LOG_E("Invalid patamer: %p\n", p_tskid_thread_manage_table);
		return -1;
	}

	stacksize = p_tskid_thread_manage_table->stacksize;
	thrd_priority = p_tskid_thread_manage_table->priority;
	thread_func = p_tskid_thread_manage_table->thread_func;
	arg = (void *)stacd;

	/* pthread_attr_init()でpthread_attr_tを初期化 */
	ret = pthread_attr_init(&attr);
	if (0 != ret) {
		LOG_E("pthread_attr_init() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}

	/**
	 * @note スタックサイズ設定
	 * @ref https://linuxjm.osdn.jp/html/LDP_man-pages/man3/pthread_attr_setstacksize.3.html
	 *	pthread_attr_setstacksize() は以下のエラーで失敗する場合がある。
	 * 	EINVAL	スタックサイズが PTHREAD_STACK_MIN (16384) バイト未満である。
	 * 			いくつかのシステムでは、 pthread_attr_setstacksize() は stacksize がシステムのページサイズの倍数でない場合にも エラー EINVAL で失敗する。  
	 */
	if (stacksize >= PTHREAD_STACK_MIN) {
		/* pthread_attr_setstacksize()でスタックサイズを設定する */
		ret = pthread_attr_setstacksize(&attr, stacksize);
		if (0 != ret) {
			LOG_E("pthread_attr_setstacksize() failed: %d(%s)\n", ret, strerror(ret));
			return ret;
		}
	}

	/* pthread_attr_setschedpolicy()でスケジューリングポリシーを設定する */
	/** 
	 * @note スレッドのスケジューリングポリシー
	 *       SCHED_FIFO or SCHED_RR or SCHED_OTHER
	 */
	ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	if (0 != ret) {
		LOG_E("pthread_attr_setschedpolicy() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}

	/* pthread_attr_setschedparam()でプライオリティを設定する */
	param.sched_priority = thrd_priority;
	ret = pthread_attr_setschedparam(&attr, &param);
	if (0 != ret) {
		LOG_E("pthread_attr_setschedparam() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}

	/* pthread_attr_setinheritsched()でattrからスケジューリング属性を取得するように指定する */
	ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	if (0 != ret) {
		LOG_E("pthread_attr_setinheritsched() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}

	/* pthread_create()でattrを指定してスレッドを起動する */
	ret = pthread_create(&th, &attr, thread_func, arg);
  	if (0 != ret) {
		LOG_E("pthread_create() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}

	/* pthread_setaffinity_np()でCPUアフィニティを設定する */
#if 0
	/**
	 * @brief CPUアフィニティ設定
	 * @ref https://linuxjm.osdn.jp/html/LDP_man-pages/man3/pthread_setaffinity_np.3.html
	 * これらの関数は非標準の GNU による拡張である。 そのため、名前に "_np" (nonportable; 移植性がない) という接尾辞が 付いている。  
	 * @note この関数が使えない場合は、起動したスレッドから自身のtidを取得して、
	 *       sched_setaffinityを呼び出す方針とする
	 * 
	 * @ref http://linuxjm.osdn.jp/html/LDP_man-pages/man2/sched_setaffinity.2.html
	 * システムで利用可能な CPU 数を判定する方法はいくつかある。
	 * ・/proc/cpuinfo の内容を調べる、 
	 * ・sysconf(3) を使って _SC_NPROCESSORS_CONF と _SC_NPROCESSORS_ONLN の値を取得する、
	 * ・/sys/devices/system/cpu/ の CPU ディレクトリの一覧を調べる、
	 * などがある。
	 */

	cpu_max = (利用可能な CPU 数);
	CPU_ZERO(&cpuset);
	for (cpu = 0; cpu < cpu_max; cpu++) {
		CPU_SET(cpu, &cpuset);
	}
	ret = pthread_setaffinity_np(th, sizeof(cpuset), &cpuset);
  	if (0 != ret) {
		LOG_E("pthread_setaffinity_np() failed: %d(%s)\n", ret, strerror(ret));
		return ret;
	}
#endif

	p_tskid_thread_manage_table->thread_id = th;

	return ret;
}