#ifndef __COM_GPIO_H__
#define __COM_GPIO_H__

#include	<stdint.h>
#include	<stdbool.h>
#include	<limits.h> // CHAR_BIT

#include	"gpiod.h"

/** @note Linux仕様 */
bool com_gpio_init(const char *descr);
bool com_gpio_get_all_lines(const unsigned int num_lines, struct gpiod_line **lines);
bool com_gpio_get_line(const unsigned int offset, struct gpiod_line **p_line);
bool com_gpio_read(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, int *onoff);
bool com_gpio_write(const char *consumer, const unsigned int offset, struct gpiod_line **p_line, const int onoff);
void com_gpio_close(unsigned int num_lines, struct gpiod_line **lines);

typedef struct {
	const unsigned int offsets[CHAR_BIT];
} gpio_rin_table_t;

/** @note RTOS互換仕様 */
uint8_t com_gpio_rin_read(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap);
void com_gpio_rin_write(const gpio_rin_table_t *p_gpio_rin_tbl, const uint8_t bitmap, const int onoff);

/** @note 各プロジェクトで個別に実装が必要 */
extern int gpio_read(const unsigned int offset);
extern bool gpio_write(const unsigned int offset, const int onoff);

#endif /* __COM_GPIO_H__ */