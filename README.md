# IPGW11X README.md

## 開発環境構築手順

```
[Confluence] IPGW改版 > 内部資料一覧 > IPGW_開発環境構築.xlsx
```

## ソース説明(RTOSからの修正内容を含む)

```
[Confluence] IPGW改版 > 内部資料一覧 > IPGW11X_ソース説明.xlsx
```

## 本番機配置
```
(Buildroot)独自の起動スクリプト S99ipgw によって、mmc(/dev/mmcblk3pXXXX)が/mnt/mmcとしてマウントされる
バイナリファイルは、アプリ起動スクリプト ipgw.start を含めて、以下のツリーとして見えるように配置する
/mnt/mmc/ipgw11x/           ・ルートディレクトリ(※存在しなければ、S99ipgwによって作成される)
         ├── ipgw.start     ・アプリ起動スクリプト
         ├── ipgw11x        ・バイナリファイル
         └── setting/       ・(※配置しなくて良い)セッティングファイル格納用ディレクトリ
```
