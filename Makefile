# usage:
# 	※それぞれのMakefile毎にオプションを記載する必要があります。
#
# 	all：ビルド対象
#		対象をTARGET変数に記載して下さい
#
#	debug：デバッグビルド
#		デバッグレベルログ有効
#
#	rebuild：再ビルド
#		デフォルトはcom配下は再ビルドしない
#		必要であればTARGET変数のコメントアウトをトグル
#
# 	オブジェクトを更新せずビルド：make all
# 	オブジェクトを常に更新してビルド：make rebuild all
#
#	デバッグビルド：
#		オブジェクトを更新せずビルド：make debug all
#		オブジェクトを常に更新してビルド：make debug rebuild all

#
### 以降を任意で変更
#

### 常にオブジェクトを更新する場合はcleanを指定する
REBUILD :=
# REBUILD := clean

### 対象
# NOTE: com配下のrebuildに関しては下記をトグル
# 1. rebuildの対象にcom配下を含めない場合
TARGET := 
# 2. rebuildの対象にcom配下を含める場合
# TARGET := com

# IPGW11X
TARGET += ipgw11x
# NOTE: 基板プロジェクトの追加例
# TARGET += dummy

# compiler options
# gオプションはデフォルト値とする
CFLAGS := -g
# 最適化レベル(未指定だと最適化なし)
# CFLAGS += -O2
CFLAGS += -W
CFLAGS += -Wall
CFLAGS += -Wextra
# 警告をエラーにする
# CFLAGS += -Werror

#
### この行から「基板ビルドルール」までは変更不要
#

# ビルドタイプ(release or debug)
BUILD_TYPE ?= release

# バイナリ配置フォルダパス
OUTPUT_DIR := ./bin

# 1. ~/gcc-arm-none-linux-gnueabihf/bin配下に配置し、パスを通さない場合
CC := ~/gcc-arm-none-linux-gnueabihf/bin/arm-none-linux-gnueabihf-gcc
# 2. パスを通す場合
# CC := arm-linux-gnueabihf-gcc

# コマンド
MKDIR_P ?= mkdir --parent

# 共通API配置フォルダ
COM_DIR := ./com

# com配下(構成次第で基板毎に設定する必要が出てくるかも)
COM_SRCS := $(wildcard $(COM_DIR)/*.c)
COM_OBJS := $(COM_SRCS:%.c=%.o)

# linker options
# DEPS :=

# library options
LDLIBS = 
LDLIBS += -lpthread
LDLIBS += -lgpiod

# library path
LD_DIRS = 
#LD_DIRS += /usr/lib/arm-linux-gnueabihf/
LD_DIRS += ./target/usr/lib/
LD_FLAGS += $(addprefix -L,$(LD_DIRS))

#
# BRIEF: ターゲット基板ビルドマクロ
#
# PARAMS:
#	$1: プロジェクトフォルダ
#	$2: バイナリファイル名
#
# ATTENTION:
#	com配下のビルドとバイナリ配置フォルダの作成はしない
#
define MACRO_TARGET_BUILD
	$(eval TARGET_DIR = $1)
	$(eval TARGET_EXEC = $2)
	$(eval TARGET_SRCS = $(wildcard $(TARGET_DIR)/*.c))
	$(eval TARGET_OBJS = $(TARGET_SRCS:%.c=%.o))
	$(eval TARGET_INC_FLAGS = $(addprefix -I,$(TARGET_DIR)))
	$(MAKE) $(REBUILD) $(BUILD_TYPE) --directory=$(TARGET_DIR)
	$(CC) $(CFLAGS) $(COM_OBJS) $(TARGET_OBJS) -o $(OUTPUT_DIR)/$(TARGET_EXEC) $(LDLIBS) $(LD_FLAGS) $(TARGET_INC_FLAGS)
endef

# default: all
.PHONY: all
all: $(TARGET)

# 再ビルド
.PHONY: rebuild
rebuild:
	$(eval REBUILD := clean)

# デバッグビルド
.PHONY: debug
debug:
	$(eval BUILD_TYPE := debug)
	@echo BUILD_TYPE: $(BUILD_TYPE)

.PHONY: com
com: $(COM_SRCS)
	$(MAKE) $(REBUILD) $(BUILD_TYPE) --directory=$(COM_DIR)

.PHONY: clean
clean:
	$(RM) $(COM_OBJS)

# com配下未ビルド時
$(COM_OBJS): $(COM_SRCS)
	$(MAKE) $(BUILD_TYPE) --directory=$(COM_DIR)

# 出力フォルダ未作成時
$(OUTPUT_DIR):
	$(MKDIR_P) $(OUTPUT_DIR)

#
### 基板ビルドルール
#

.PHONY: ipgw11x
ipgw11x: $(COM_OBJS) $(OUTPUT_DIR)
	$(call MACRO_TARGET_BUILD, ./IPGW11X, ipgw11x)

### ダミー(基板追加例)
# NOTE: コメントアウトしなくても問題ない
# NOTE: 不要であれば削除
.PHONY: dummy
dummy: $(COM_OBJS) $(OUTPUT_DIR)
	$(call MACRO_TARGET_BUILD, ./DUMMY, dummy)
